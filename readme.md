# Projekt Coins4SMS.com
## Opis projektu
Coins4SMS to serwis umożliwiający zakup kryptowalut za pomocą telefonu komórkowego. Aplikacja składa się z dwóch części:
- Strony publicznej na której można kupić kryptowalutę
- Panel administracyjny

Z systemem zintegrowanych jest kilku dostawców SMS Premium w celu uniknięcia fraudów. Więcej informacji o projekcie znajduje się w dokumentacji technicznej.

## Budowanie aplikacji klienckiej

$> `gulp`

## Uruchamianie aplikacji klienckiej w środowisku programistycznym

$> `gulp && gulp watch`

## Changelog
### Wersja 0.1 wydana dnia 23.08.2016
- Inicjalizacja aplikacji Laravel
- Inicjalizacja aplikacji klienckiej napisanej w AngularJS
- Zamiana standardowego ORM dostępnego w Laravel na Doctrine
- Rozwiązanie problemów z integracją AngularJS z częścią serwerową
- Rozwiązanie problemów związanych z domyślnym systemem logowania w połączeniu z Doctrine
- Dodanie dokumentacji technicznej do projektu
- Utworzenie pliku .gitignore w celu zignorowania plików dynamicznie tworzonych przez GULP
.
.
.
-- removed --
