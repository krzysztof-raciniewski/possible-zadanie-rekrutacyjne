(function () {
    'use strict';

    angular.module('adminPanel.filters')
        .filter('paymentQueueStatus', PaymetQueueStatusFilter);

    PaymetQueueStatusFilter.$inject = [];
    function PaymetQueueStatusFilter() {
        return function(input) {
            if(input == true) {
                return 'wypłaty są uruchomione';
            } else {
                return 'wypłaty są nieaktywne'
            }
        };
    }
})();