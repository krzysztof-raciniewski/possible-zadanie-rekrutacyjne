(function () {
    'use strict';

    angular.module('adminPanel.filters')
        .filter('bbOrderType', BitBayOrderType);

    BitBayOrderType.$inject = [];
    function BitBayOrderType() {
        return function(input) {
            if(input == 'ask') {
                return 'sprzedaż';
            }
            if(input == 'bid') {
                return 'kupno';
            }
        };
    }
})();