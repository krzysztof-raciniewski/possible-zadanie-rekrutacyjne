(function() {
    'use strict';

    angular.module('adminPanel.filters')
        .filter('bbOperationType', BitBayOperationType);

    BitBayOperationType.$inject = [];

    function BitBayOperationType() {
        return function(input) {
            if (input == '-transfer') {
                return 'wypłata';
            }
            if (input == '+outside_income') {
                return 'wpłata z zewnątrz';
            }
            if (input == '+currency_transaction') {
                return 'wymiana walutowa';
            }
            if (input == 'fee') {
                return 'prowizja dla BitBay';
            }
            if (input == '+transfer_cancel') {
                return 'operacja anulowana';
            }
        };
    }
})();
