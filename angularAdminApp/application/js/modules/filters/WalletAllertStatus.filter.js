(function() {
    'use strict';

    angular.module('adminPanel.filters')
        .filter('walletAllertStatus', WalletAllertStatus);

    WalletAllertStatus.$inject = [];

    function WalletAllertStatus() {
        return function(input) {
            if (input == true) {
                return 'powiadomienia są uruchomione';
            } else {
                return 'powiadomienia są wyłączone'
            }
        };
    }
})();
