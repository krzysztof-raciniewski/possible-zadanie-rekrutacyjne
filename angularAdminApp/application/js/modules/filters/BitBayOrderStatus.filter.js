(function () {
    'use strict';

    angular.module('adminPanel.filters')
        .filter('bbOrderStatus', BitBayOrderStatus);

    BitBayOrderStatus.$inject = [];
    function BitBayOrderStatus() {
        return function(input) {
            if(input == 'active') {
                return 'aktywny';
            }
            if(input == 'inactive') {
                return 'nieaktywny';
            }
        };
    }
})();