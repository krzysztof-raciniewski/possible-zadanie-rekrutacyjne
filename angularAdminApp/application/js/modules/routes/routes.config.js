/**=========================================================
 * Module: config.js
 * App routes and resources configuration
 =========================================================*/


(function() {
    'use strict';

    angular
        .module('adminPanel.routes')
        .config(routesConfig);

    routesConfig.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider'];

    function routesConfig($stateProvider, $locationProvider, $urlRouterProvider, helper) {

        // Set the following to true to enable the HTML5 Mode
        // You may have to set <base> tag in index and a routing configuration in your server
        $locationProvider.html5Mode(true);

        // defaults to dashboard
        // $urlRouterProvider.otherwise('/dashboard');

        $urlRouterProvider.otherwise(function($injector) {
            var $state = $injector.get('$state');
            $state.go('app.dashboard');
        });

        // 
        // Application Routes
        // -----------------------------------   
        $stateProvider
            .state('app', {
                abstract: true,
                templateUrl: helper.basepath('app.html'),
                resolve: helper.resolveFor('modernizr', 'icons')
            })
            .state('app.welcome', {
                url: '/welcome',
                title: 'Welcome',
                templateUrl: helper.basepath('welcome.html'),
                authenticate: true
            })
            //
            // Material
            // -----------------------------------
            .state('app.cards', {
                url: '/cards',
                title: 'Material Cards',
                templateUrl: helper.basepath('material.cards.html'),
                authenticate: true
            })
            .state('app.forms', {
                url: '/forms',
                title: 'Material Forms',
                templateUrl: helper.basepath('material.forms.html'),
                authenticate: true
            })
            .state('app.whiteframe', {
                url: '/whiteframe',
                title: 'Material Whiteframe',
                templateUrl: helper.basepath('material.whiteframe.html'),
                authenticate: true
            })
            .state('app.matcolors', {
                url: '/matcolors',
                title: 'Material Colors',
                templateUrl: helper.basepath('material.colors.html'),
                authenticate: true
            })
            .state('app.lists', {
                url: '/lists',
                title: 'Material Lists',
                templateUrl: helper.basepath('material.lists.html'),
                authenticate: true
            })
            .state('app.inputs', {
                url: '/inputs',
                title: 'Material Inputs',
                templateUrl: helper.basepath('material.inputs.html'),
                authenticate: true
            })
            .state('app.matwidgets', {
                url: '/matwidgets',
                title: 'Material Widgets',
                templateUrl: helper.basepath('material.widgets.html'),
                resolve: helper.resolveFor('weather-icons', 'loadGoogleMapsJS', function() {
                    return loadGoogleMaps();
                }, 'ui.map'),
                authenticate: true
            })
            .state('app.ngmaterial', {
                url: '/ngmaterial',
                title: 'ngMaterial',
                templateUrl: helper.basepath('material.ngmaterial.html'),
                authenticate: true
            })
            //
            // Single Page Routes
            // -----------------------------------
            .state('page', {
                url: '/page',
                templateUrl: helper.basepath('pages/page.html'),
                resolve: helper.resolveFor('modernizr', 'icons'),
                controller: ['$rootScope', function($rootScope) {
                    $rootScope.app.layout.isBoxed = false;
                }],
                authenticate: true
            })
            .state('page.login', {
                url: '/login',
                title: 'Strona logowania',
                templateUrl: helper.basepath('pages/page.login.html'),
            })
            //
            //  --------- PAGES -----------
            //
            .state('app.dashboard', {
                url: '/dashboard',
                title: 'Dashboard',
                controller: 'DashboardPageController',
                controllerAs: 'dashboardPageController',
                templateUrl: helper.basepath('pages/dashboard/dashboard.html'),
                resolve: helper.resolveFor('flot-chart', 'flot-chart-plugins', 'vector-map', 'vector-map-maps', 'sparklines'),
                authenticate: true
            })
            .state('app.bitbay', {
                url: '/bitbay',
                title: 'Giełda BitBay',
                controller: 'BitBayController',
                controllerAs: 'bitbay',
                templateUrl: helper.basepath('pages/page.bitbay.html'),
                resolve: helper.resolveFor('flot-chart', 'flot-chart-plugins', 'vector-map', 'vector-map-maps', 'sparklines'),
                authenticate: true
            })
            .state('app.bitbay_configuration', {
                url: '/bitbay/configuration',
                title: 'Konfiguracja BitBay',
                controller: 'BitBayConfigurationController',
                controllerAs: 'ctrl',
                templateUrl: helper.basepath('pages/configuration/bitbay.html'),
                authenticate: true
            })
            .state('app.providers_list', {
                url: '/providers/list',
                title: 'Lista dostawców SMS',
                controller: 'ProvidersListController',
                controllerAs: 'providersList',
                templateUrl: helper.basepath('pages/configuration/providers_list.html'),
                authenticate: true
            })
            .state('app.provider_configuration', {
                url: '/providers/configuration/:providerId',
                title: 'Konfiguracja dostawcy usługi premium SMS',
                controller: 'ProviderConfigurationController',
                controllerAs: 'providerConfiguration',
                templateUrl: helper.basepath('pages/configuration/provider_configuration.html'),
                authenticate: true
            })
            .state('app.operators', {
                url: '/operators/list',
                title: 'Lista operatorów sieci komórkowych',
                controller: 'OperatorsPageController',
                controllerAs: 'operatorsPageController',
                templateUrl: helper.basepath('pages/configuration/operators.html'),
                authenticate: true
            })
            .state('app.currencies', {
                url: '/currencies/list',
                title: 'Lista walut',
                controller: 'CurrenciesPageController',
                controllerAs: 'currenciesPageController',
                templateUrl: helper.basepath('pages/configuration/currencies.html'),
                authenticate: true
            })
            .state('app.countries', {
                url: '/countries/list',
                title: 'Lista krajów',
                controller: 'CountriesPageController',
                controllerAs: 'countriesPageController',
                templateUrl: helper.basepath('pages/configuration/countries.html'),
                authenticate: true
            })
            /*
             * TRANSACTIONS
             */
            .state('app.transactions_list', {
                url: '/transactions/list',
                title: 'Lista wszystkich transakcji',
                controller: 'TransactionsListPageController',
                controllerAs: 'transactionsListPageController',
                templateUrl: helper.basepath('pages/transactions/list.html'),
                resolve: helper.resolveFor('datatables'),
                authenticate: true
            })
            .state('app.transaction_details', {
                url: '/transactions/detail/:transactionId',
                title: 'Strona szczegółów transakcji',
                controller: 'TransactionDetailsPageController',
                controllerAs: 'transactionDetailsPageController',
                templateUrl: helper.basepath('pages/transactions/details.html'),
                authenticate: true
            })
            /*
             * PAYMENTS
             */
            .state('app.payments_list', {
                url: '/payments/list',
                title: 'Kolejka wypłat',
                controller: 'PaymentsListPageController',
                controllerAs: 'paymentsListPageController',
                templateUrl: helper.basepath('pages/payments/list.html'),
                resolve: helper.resolveFor('datatables'),
                authenticate: true
            })
            .state('app.payment_details', {
                url: '/payments/detail/:paymentId',
                title: 'Strona szczegółów płatności',
                controller: 'PaymentDetailsPageController',
                controllerAs: 'paymentDetailsPageController',
                templateUrl: helper.basepath('pages/payments/details.html'),
                authenticate: true
            })
            /*
             * ANALYTICS
             */
            .state('app.analytics', {
                url: '/statistics/analytics',
                title: 'Statystyki odwiedzin strony głównej',
                controller: 'AnalyticsPageController',
                controllerAs: 'analyticsPageController',
                templateUrl: helper.basepath('pages/analytics/analytics.html'),
                authenticate: true
            })
            /*
             * SETTINGS
             */
            .state('app.settings_notifications', {
                url: '/settings/settings_notifications',
                title: 'Ustawienia powiadomień',
                controller: 'NotificationsPageController',
                controllerAs: 'notificationsPageController',
                templateUrl: helper.basepath('pages/settings/notifications.html'),
                authenticate: true
            })
            .state('app.settings_commission', {
                url: '/settings/commision',
                title: 'Ustawienia prowizji',
                controller: 'CommissionPageController',
                controllerAs: 'commissionPageController',
                templateUrl: helper.basepath('pages/settings/commission.html'),
                resolve: helper.resolveFor('angularjs-slider'),
                authenticate: true
            })
            .state('app.settings_users', {
                url: '/settings/users',
                title: 'Ustawienia użytkowników',
                controller: 'UsersPageController',
                controllerAs: 'usersPageController',
                templateUrl: helper.basepath('pages/settings/users.html'),
                authenticate: true
            })
            .state('app.settings_profile', {
                url: '/settings/user/profile',
                title: 'Profil użytkownika',
                controller: 'CurrentUserProfilePageController',
                controllerAs: 'currentUserProfilePageController',
                templateUrl: helper.basepath('pages/settings/profile.html'),
                authenticate: true
            })
            .state('app.settings_paymentqueue', {
                url: '/settings/payment/queue',
                title: 'Ustawienia kolejki wypłat',
                controller: 'PaymentQueuePageController',
                controllerAs: 'paymentQueuePageController',
                templateUrl: helper.basepath('pages/settings/paymentqueue.html'),
                authenticate: true
            });

    } // routesConfig

})();
