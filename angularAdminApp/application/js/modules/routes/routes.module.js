(function() {
    'use strict';

    angular
        .module('adminPanel.routes', [
            'app.lazyload'
        ]);
})();