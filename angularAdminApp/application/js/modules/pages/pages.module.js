(function() {
    'use strict';

    angular
        .module('adminPanel.pages', [
            'adminPanel.pages.login',
            'adminPanel.pages.bitbay',
            'adminPanel.pages.configuration',
            'adminPanel.pages.dashboard',
            'adminPanel.pages.settings',
            'adminPanel.pages.transactions',
            'adminPanel.pages.payments',
            'adminPanel.pages.analytics'
        ]);
})();
