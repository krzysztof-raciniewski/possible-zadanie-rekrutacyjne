(function() {
    'use strict';

    angular
        .module('adminPanel.pages.bitbay', [
            'adminPanel.charts'
        ]);
})();