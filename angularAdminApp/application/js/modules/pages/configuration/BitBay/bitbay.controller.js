(function() {
    'use strict';

    angular
        .module('adminPanel.pages.configuration')
        .controller('BitBayConfigurationController', BitBayConfigurationController);

    BitBayConfigurationController.$inject = ['$log', 'ConfigurationModel', 'Notify'];

    function BitBayConfigurationController($log, ConfigurationModel, Notify) {
        var vm = this;

        vm.data = {
            key: '',
            secret: ''
        };

        vm.limits = {
            'btc_min_payment': '',
            'ltc_min_payment': '',
            'lsk_min_payment': '',
            'eth_min_payment': ''
        }

        vm.fees = {
            'btc_payment_fee': '',
            'ltc_payment_fee': '',
            'lsk_payment_fee': '',
            'eth_payment_fee': ''
        }

        ConfigurationModel.getBitBayConfiguration().then(
            function(response) {
                $log.debug('Pobralem konfiguracje BitBay: ', response);
                vm.data.key = response.key;
                vm.data.secret = response.secret;

            }.bind(this)
        );

        ConfigurationModel.getBitBayLimits().then(
            function(response) {
                $log.debug('Pobralem limity BitBay: ', response);
                vm.limits.btc_min_payment = response.btcMinPayment;
                vm.limits.lsk_min_payment = response.lskMinPayment;
                vm.limits.ltc_min_payment = response.ltcMinPayment;
                vm.limits.eth_min_payment = response.ethMinPayment;
            }.bind(this)
        );

        ConfigurationModel.getBitBayFees().then(
            function(response) {
                $log.debug('Pobralem limity BitBay: ', response);
                vm.fees.btc_payment_fee = response.btcPaymentFee;
                vm.fees.lsk_payment_fee = response.lskPaymentFee;
                vm.fees.ltc_payment_fee = response.ltcPaymentFee;
                vm.fees.eth_payment_fee = response.ethPaymentFee;
            }.bind(this)
        );

        vm.saveAction = function() {
            ConfigurationModel.setBitBayConfiguration(vm.data).then(
                function(response) {
                    Notify.alert(
                        'Konfiguracja BitBay.pl została zapisana', { status: 'success' }
                    );
                },
                function(response) {
                    Notify.alert(
                        'Nie można zapisać konfiguracji', { status: 'danger' }
                    );
                }
            );
        };

        vm.saveLimitsAction = function() {
            ConfigurationModel.setBitBayLimits(vm.limits).then(
                function(response) {
                    Notify.alert(
                        'Limity zostały zapisane', { status: 'success' }
                    );
                },
                function(response) {
                    Notify.alert(
                        'Nie można zapisać limitów', { status: 'danger' }
                    );
                }
            );
        };

        vm.saveFeesAction = function() {
            ConfigurationModel.setBitBayFees(vm.fees).then(
                function(response) {
                    Notify.alert(
                        'Stałe doliczane do wypłat zostały zachowane', { status: 'success' }
                    );
                },
                function(response) {
                    Notify.alert(
                        'Nie można zapisać stałych doliczanych do wypłat', { status: 'danger' }
                    );
                }
            );
        };

    }
})();
