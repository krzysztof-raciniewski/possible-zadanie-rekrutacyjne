(function () {
    'use strict';

    angular
        .module('adminPanel.pages.configuration')
        .controller('ProviderConfigurationController', ProviderConfigurationController);

    ProviderConfigurationController.$inject = ['$log', '$scope', '$stateParams', '$state', 'ProviderModel', 'Notify'];
    function ProviderConfigurationController($log, $scope, $stateParams, $state, ProviderModel, Notify) {
        var vm = this;
        vm.providerId = $stateParams.providerId;
        vm.provider = [];
        vm.configuration = [];
        
        // get provider info
        ProviderModel.getById(vm.providerId).then(
            function (result) {
                if (result.length > 0) {
                    vm.provider = result[0];
                    $log.debug('Informacje o dostawcy: ', result[0]);
                }
            },
            function () {
                Notify.alert(
                    'Dostawca z ID:' + vm.providerId + ' nie istnieje',
                    { status: 'danger' }
                );
                $state.go('app.providers_list');
            }
        );

        // get info about configuration
        ProviderModel.getConfiguration(vm.providerId).then(
            function (result) {
                if (result.length > 0) {
                    vm.configuration = result[0];
                    $log.debug('Informacje o konfiguracji: ', result[0]);
                }
            },
            function () {
                Notify.alert(
                    'Nie udało się pobrać informacji o konfiguracji',
                    { status: 'danger' }
                );
            }
        );

        // Save main informations
        vm.saveMainInfoAction = function () {
            if ($scope.mainForm.$valid) {
                ProviderModel.updateMainInformation(vm.providerId, vm.provider).then(
                    function (result) {
                        Notify.alert(
                            'Informacje o dostawcy zostały zachowane',
                            { status: 'success' }
                        );
                        vm.provider = result;
                    },
                    function () {
                        Notify.alert(
                            'Nie można zapisać zmian',
                            { status: 'danger' }
                        );
                    }
                );
            } else {
                Notify.alert(
                    'Formularz jest błędnie wypełniony',
                    { status: 'danger' }
                );
            }
        };

        // Save main informations
        vm.saveConfigurationAction = function () {
            if ($scope.configurationForm.$valid) {
                ProviderModel.updateConfiguration(vm.providerId, vm.configuration).then(
                    function (result) {
                        Notify.alert(
                            'Informacje o konfiguracji zostały zapisane',
                            { status: 'success' }
                        );
                        vm.configuration = result;
                    },
                    function () {
                        Notify.alert(
                            'Nie można zapisać zmian',
                            { status: 'danger' }
                        );
                    }
                );
            } else {
                Notify.alert(
                    'Formularz jest błędnie wypełniony',
                    { status: 'danger' }
                );
            }
        };

    }
})();