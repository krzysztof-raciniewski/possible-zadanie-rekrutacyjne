(function () {
    'use strict';

    angular
        .module('adminPanel.pages.configuration')
        .controller('ProvidersListController', ProvidersListController);

    ProvidersListController.$inject = ['$log','ProviderModel', 'Notify'];
    function ProvidersListController($log,ProviderModel, Notify) {
        var vm = this;

        vm.providers = {};

        ProviderModel.getAll().then(
            function(results) {
                $log.debug('Pobrano dane o dostawcach', results);
                vm.providers = results;
            }
        );

        vm.changeProviderEnabledStatus = function(providerId) {
            ProviderModel.changeProviderStatus(providerId).then(
                function() {
                    Notify.alert(
                        'Status dostawcy został zmieniony',
                        { status: 'success' }
                    ); 
                },
                function() {
                    Notify.alert(
                        'Nie można zmienić statusu, bląd serwera',
                        { status: 'danger' }
                    ); 
                }
            )
        };

    }
})();