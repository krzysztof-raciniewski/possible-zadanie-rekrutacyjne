(function() {
    'use strict';

    angular
        .module('adminPanel.pages.payments')
        .controller('PaymentsListPageController', PaymentsListPageController);

    PaymentsListPageController.$inject = ['$scope', '$log', '$auth', '$filter', '$compile', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', 'TransactionModel', 'Notify'];

    function PaymentsListPageController($scope, $log, $auth, $filter, $compile, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, TransactionModel, Notify) {
        var vm = this;

        /**
         * Initialize BitBayPage
         */
        var initialize = (function() {

            vm.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('ajax', {
                    url: '/api/payments/',
                    contentType: 'application/json',
                    dataType: 'json',
                    accepts: "application/json",
                    headers: {
                        Accept: "application/json",
                        Authorization: 'Bearer ' + $auth.getToken()
                    },
                    "dataSrc": function(json) {
                        $log.debug('Datatable data: ', json);

                        for (var i = 0, len = json.data.length; i < len; i++) {
                            json.data[i]['smsNetPrice'] = $filter('currency')(json.data[i]['smsNetPrice'], '', 2);
                            json.data[i]['smsGrossPrice'] = $filter('currency')(json.data[i]['smsGrossPrice'], '', 2);
                            var href = $compile('<a ui-sref="app.payment_details({paymentId: \'' + json.data[i]['id'] + '\'})"></a>')($scope)[0].href;
                            // var href='test';
                            json.data[i]['actions'] = '<a href="' + href + '" class="mb-sm btn btn-default">Szczegóły</a>';

                            if (json.data[i]['paymentDate'] === '') {
                                json.data[i]['paymentDate'] = '-';
                            }
                            if (json.data[i]['payed'] == true) {
                                json.data[i]['status'] = '<button type="button" class="mb-sm btn btn-success">&nbsp;</button>';
                            } else {
                                if (json.data[i]['trialsNumber'] === 0) {
                                    json.data[i]['status'] = '<button type="button" class="mb-sm btn btn-warning">&nbsp;</button>';
                                } else {
                                    json.data[i]['status'] = '<button type="button" class="mb-sm btn btn-danger">&nbsp;</button>';
                                }
                            }
                        }

                        return json.data;
                    }
                })
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('processing', true)
                .withOption('order', [
                    [1, 'desc']
                ])
                .withPaginationType('full_numbers')
                .withLanguage({
                    'url': '/adminPanelApp/i18n/datatables.json'
                });

            vm.dtColumns = [
                // DTColumnBuilder.newColumn('status').withTitle('Status').notSortable().withOption('searchable', false),
                DTColumnBuilder.newColumn('status').withTitle('Status').notSortable().withOption('searchable', false),
                DTColumnBuilder.newColumn('created').withTitle('Data utowrzenia'),
                DTColumnBuilder.newColumn('created').withTitle('Data utowrzenia'),
                DTColumnBuilder.newColumn('updated').withTitle('Ostatnia aktualizacja'),
                DTColumnBuilder.newColumn('currency').withTitle('Kryptowaluta'),
                DTColumnBuilder.newColumn('paymentDate').withTitle('Data dokonania płatności'),
                DTColumnBuilder.newColumn('value').withTitle('Wartość'),
                DTColumnBuilder.newColumn('wallet').withTitle('Adres portfela'),
                DTColumnBuilder.newColumn('actions').withTitle('Akcje').notSortable().withOption('searchable', false)
            ];

        }.bind(this))();
    }
})();
