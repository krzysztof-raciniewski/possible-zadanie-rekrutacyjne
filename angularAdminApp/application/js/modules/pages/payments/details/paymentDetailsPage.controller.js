(function() {
    'use strict';

    angular
        .module('adminPanel.pages.payments')
        .controller('PaymentDetailsPageController', PaymentDetailsPageController);

    PaymentDetailsPageController.$inject = ['$log', '$stateParams', 'PaymentModel', 'Notify'];

    function PaymentDetailsPageController($log, $stateParams, PaymentModel, Notify) {
        var vm = this;
        vm.paymentId = $stateParams.paymentId;

        vm.payment = {};
        /**
         * Initialize BitBayPage
         */
        var initialize = (function() {

            PaymentModel.getById(vm.paymentId).then(
                function(response) {
                    $log.debug('Informacje o płatności: ', response);
                    vm.payment = response;
                },
                function(response) {
                    Notify.alert(
                        'Nie można pobrać informacji o płatności', { status: 'danger' }
                    );
                }
            );

        }.bind(this))();
    }
})();
