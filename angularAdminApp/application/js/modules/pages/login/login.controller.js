(function() {
    'use strict';

    angular
        .module('adminPanel.pages.login')
        .controller('LoginController', LoginFormController);

    LoginFormController.$inject = ['$log', '$scope', '$auth', '$state'];

    function LoginFormController($log, $scope, $auth, $state) {
        var vm = this;

        // bind here all data from the form
        vm.account = {};
        // place the message if something goes wrong
        vm.authMsg = '';

        vm.login = function() {
            vm.authMsg = '';

            if (vm.loginForm.$valid) {
                let user = {
                    email: vm.account.email,
                    password: vm.account.password
                };

                $auth.login(user)
                    .then(
                        function(response) {
                            $auth.setToken(response.data);
                            $state.go('app.dashboard');
                        },
                        function(response) {
                            $log.debug(response);
                            if (response.status == 422 || response.status == 401) {
                                vm.authMsg = 'Złe dane do logowania';
                            } else if (response.status == 403) {
                                vm.authMsg = 'Zbyt wiele prób logowania, spróbuj ponownie za 5 minut';
                            } else {
                                vm.authMsg = 'Błąd serwera';
                            }

                            vm.account = {};
                            $scope.login.loginForm.$setUntouched();
                        }
                    )
                    .catch(
                        function() {
                            vm.authMsg = 'Wewnętrzny błąd serwera';
                        }
                    );
            } else {
                // set as dirty if the user click directly to login so we show the validation messages
                /*jshint -W106*/
                vm.loginForm.account_email.$dirty = true;
                vm.loginForm.account_password.$dirty = true;
            }
        };

    }
})();
