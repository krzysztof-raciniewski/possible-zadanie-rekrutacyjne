(function() {
    'use strict';

    angular
        .module('adminPanel.pages.dashboard', [
            'adminPanel.charts'
        ]);
})();