(function() {
    'use strict';

    angular
        .module('adminPanel.pages.dashboard')
        .controller('DashboardPageController', DashboardPageController);

    DashboardPageController.$inject = ['$rootScope', 'Restangular', 'UserModel'];

    function DashboardPageController($rootScope, Restangular, UserModel) {
        var vm = this;

        vm.testApiMethod = function() {
            UserModel.getCurrentUser() // GET: /users
                .then(
                    (response) => {
                        console.log('Zapytanie powiodło się.');
                        console.log(response);
                    },
                    (response) => {
                        console.log(response);
                        console.log(response);
                    }
                );
        };

        vm.splineOptions = {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true,
                    radius: 4
                },
                splines: {
                    show: true,
                    tension: 0.4,
                    lineWidth: 1,
                    fill: 0.5
                }
            },
            grid: {
                borderColor: '#eee',
                borderWidth: 1,
                hoverable: true,
                backgroundColor: '#fcfcfc'
            },
            tooltip: true,
            tooltipOpts: {
                content: function(label, x, y) {
                    return x + ' : ' + y;
                }
            },
            xaxis: {
                tickColor: '#fcfcfc',
                mode: 'categories'
            },
            yaxis: {
                min: 0,
                max: 150, // optional: use it for a clear represetation
                tickColor: '#eee',
                position: ($rootScope.app.layout.isRTL ? 'right' : 'left'),
                tickFormatter: function(v) {
                    return v /* + ' visitors'*/ ;
                }
            },
            shadowSize: 0
        };




    }
})();
