(function() {
    'use strict';

    angular
        .module('adminPanel.pages.analytics')
        .controller('AnalyticsPageController', AnalyticsPageController);

    AnalyticsPageController.$inject = ['$window'];

    function AnalyticsPageController($window) {
        var vm = this;

        vm.goToGoogle = function() {
            $window.open('https://analytics.google.com/analytics/web/', '_blank');
        };

        vm.goToYandex = function() {
            $window.open('https://metrica.yandex.com', '_blank');
        };
    }
})();
