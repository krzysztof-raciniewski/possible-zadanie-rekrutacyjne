(function() {
    'use strict';

    angular
        .module('adminPanel.pages.settings')
        .controller('CurrentUserProfilePageController', CurrentUserProfilePageController);

    CurrentUserProfilePageController.$inject = ['$rootScope', '$log', 'UserModel', 'Notify'];

    function CurrentUserProfilePageController($rootScope, $log, UserModel, Notify) {
        var vm = this;

        vm.emptyPasswordData = {
            old_password: '',
            password: '',
            repeated_password: ''
        };

        vm.userData = {
            first_name: '',
            last_name: ''
        }

        vm.passwordData = angular.copy(vm.emptyPasswordData);

        UserModel.getCurrentUser().then(
            function(response) {
                vm.userData = response;
            }
        );

        vm.changePassword = function() {
            UserModel.changeCurrentUserPassword(vm.passwordData).then(
                function(response) {
                    Notify.alert(
                        'Hasło zostało zmienione', { status: 'success' }
                    );
                    // vm.passwordData = vm.emptyPasswordData;
                },
                function(response) {
                    if (response.data === 'bad_password') {
                        return Notify.alert(
                            'Dotychczasowe hasło jest nieprawidłowe', { status: 'danger' }
                        );
                    }
                    if (response.data === 'password_are_not_the_same') {
                        return Notify.alert(
                            'Hasła do siebie nie pasują', { status: 'danger' }
                        );
                    }
                    return Notify.alert(
                        'Nie udało się zmienić hasła - błąd serwera', { status: 'danger' }
                    );
                }
            )
        }

        vm.changeUserData = function() {
            UserModel.changeCurrentUserData(vm.userData).then(
                function(response) {
                    Notify.alert(
                        'Dane osobowe użytkownika zostały zmienione', { status: 'success' }
                    );

                    $rootScope.$broadcast('userDataChangedEvent', {
                      data: response // send whatever you want
                    });

                    vm.userData = response;
                },
                function(response) {
                    return Notify.alert(
                        'Nie udało się zmienić danych użytkownika', { status: 'danger' }
                    );
                }
            );
        }

    }
})();
