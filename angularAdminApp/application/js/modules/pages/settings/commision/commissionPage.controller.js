(function() {
    'use strict';

    angular
        .module('adminPanel.pages.settings')
        .controller('CommissionPageController', CommissionPageController);

    CommissionPageController.$inject = ['$log', 'SettingsModel', 'Notify'];

    function CommissionPageController($log, SettingsModel, Notify) {
        var vm = this;

        vm.data = {
            sale_commission: 10
        };


        SettingsModel.getCommission().then(
            function(response) {
                $log.debug('Pobrano znizke: ', response);
                vm.data = response;
            }.bind(this)
        );

        vm.saveCommission = function() {
            SettingsModel.setCommission(vm.data).then(
                function(response) {
                    Notify.alert(
                        'Nowa prowizja została zapisana', { status: 'success' }
                    );
                    vm.data = response;
                },
                function(response) {
                    Notify.alert(
                        'Nie udało się zapisać prowizji', { status: 'danger' }
                    );
                }
            );
        };
    }
})();
