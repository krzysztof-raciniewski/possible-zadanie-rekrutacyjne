(function() {
    'use strict';

    angular
        .module('adminPanel.pages.settings')
        .controller('PaymentQueuePageController', CurrentUserProfilePageController);

    CurrentUserProfilePageController.$inject = ['$log', 'SettingsModel', 'Notify'];

    function CurrentUserProfilePageController($log, SettingsModel, Notify) {
        var vm = this;

        vm.queueStatus = false;

        SettingsModel.getPaymentQueueStatus().then(
            function(response) {
                vm.queueStatus = response.status;
            },
            function(response) {
                Notify.alert(
                    'Nie można pobrać statusu kolejki', { status: 'danger' }
                );
            }
        );

        vm.saveQueueStatus = function() {
            SettingsModel.setPaymentQueueStatus(vm.queueStatus).then(
                function(response) {
                    Notify.alert(
                        'Status kolejki wypłat został zmieniony', { status: 'success' }
                    );
                    vm.queueStatus = response.status;
                },
                function(response) {
                    Notify.alert(
                        'Nie można zapisać statusu kolejki', { status: 'danger' }
                    );
                }
            );
        }

    }
})();
