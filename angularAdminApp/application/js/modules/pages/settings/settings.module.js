(function() {
    'use strict';

    angular
        .module('adminPanel.pages.settings', [
            'validation.match'
        ]);
})();