(function() {
    'use strict';

    angular
        .module('adminPanel.pages.settings')
        .controller('UsersPageController', UsersPageController);

    UsersPageController.$inject = ['$log', 'UserModel', 'Notify'];

    function UsersPageController($log, UserModel, Notify) {
        var vm = this;

        vm.users = [];

        UserModel.getAll().then(
            function(response) {
                $log.debug('Pobralem adresy e-mail: ', response);
                vm.users = response;
            }.bind(this)
        );

    }
})();
