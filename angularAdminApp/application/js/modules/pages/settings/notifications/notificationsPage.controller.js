(function() {
    'use strict';

    angular
        .module('adminPanel.pages.settings')
        .controller('NotificationsPageController', NotificationsPageController);

    NotificationsPageController.$inject = ['$log', 'SettingsModel', 'Notify'];

    function NotificationsPageController($log, SettingsModel, Notify) {
        var vm = this;


        vm.emails = {
            contact_email: '',
            notifications_email: ''
        };

        vm.wallet = {
            btcWalletFullValue: '',
            ltcWalletFullValue: '',
            lskWalletFullValue: '',
            ethWalletFullValue: '',
            btcWalletAlertValue: '',
            ltcWalletAlertValue: '',
            lskWalletAlertValue: '',
            ethWalletAlertValue: ''
        };

        vm.notification_status = {
            walletAllertNotifications: false
        }

        SettingsModel.getEmails().then(
            function(response) {
                $log.debug('Pobralem adresy e-mail: ', response);
                vm.emails = response;
            }.bind(this)
        );

        SettingsModel.getWallet().then(
            function(response) {
                $log.debug('Pobrano ustawienia portfela: ', response);
                vm.wallet = response;
            }.bind(this)
        );

        SettingsModel.getWalletAllertsStatus().then(
            function(response) {
                vm.notification_status.walletAllertNotifications = response.status;
            }.bind(this)
        )

        vm.saveEmailsSettingsAction = function() {
            SettingsModel.setEmails(vm.emails).then(
                function(response) {
                    Notify.alert(
                        'Zapisano adresy e-mail', { status: 'success' }
                    );
                    vm.emails = response;
                },
                function(response) {
                    Notify.alert(
                        'Nie można zapisać konfiguracji', { status: 'danger' }
                    );
                }
            );
        };

        vm.saveWalletSettingsAction = function() {
            SettingsModel.setWallet(vm.wallet).then(
                function(response) {
                    Notify.alert(
                        'Zapisano ustawienia', { status: 'success' }
                    );
                    vm.wallet = response;
                },
                function(response) {
                    Notify.alert(
                        'Nie można zapisać ustawień', { status: 'danger' }
                    );
                }
            );
        };

        vm.saveNotificationsSettingsAction = function() {
            SettingsModel.setWalletAllertsStatus(vm.notification_status.walletAllertNotifications).then(
                function(response) {
                    Notify.alert(
                        'Zapisano ustawienia', { status: 'success' }
                    );
                },
                function(response) {
                    Notify.alert(
                        'Nie można zapisać ustawień', { status: 'danger' }
                    );
                }
            );
        };
    }
})();
