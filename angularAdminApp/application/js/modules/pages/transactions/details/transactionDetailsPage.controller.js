(function() {
    'use strict';

    angular
        .module('adminPanel.pages.transactions')
        .controller('TransactionDetailsPageController', TransactionDetailsPageController);

    TransactionDetailsPageController.$inject = ['$log', '$stateParams', 'TransactionModel', 'Notify'];

    function TransactionDetailsPageController($log, $stateParams, TransactionModel, Notify) {
        var vm = this;
        vm.transactionId = $stateParams.transactionId;

        vm.transaction = {};
        /**
         * Initialize BitBayPage
         */
        var initialize = (function() {

            TransactionModel.getById(vm.transactionId).then(
                function(response) {
                    $log.debug('Informacje o transakcji: ', response);
                    vm.transaction = response;
                },
                function(response) {
                    Notify.alert(
                        'Nie można pobrać informacji o transakcji', { status: 'danger' }
                    );
                }
            );

        }.bind(this))();
    }
})();
