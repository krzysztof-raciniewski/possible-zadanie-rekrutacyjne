(function() {
    'use strict';

    angular
        .module('adminPanel.pages.transactions')
        .controller('TransactionsListPageController', TransactionsListPageController);

    TransactionsListPageController.$inject = ['$scope', '$log', '$auth', '$filter', '$compile', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', 'TransactionModel', 'Notify'];

    function TransactionsListPageController($scope, $log, $auth, $filter, $compile, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, TransactionModel, Notify) {
        var vm = this;

        /**
         * Initialize BitBayPage
         */
        var initialize = (function() {

            vm.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('ajax', {
                    url: '/api/transactions/',
                    contentType: 'application/json',
                    dataType: 'json',
                    accepts: "application/json",
                    headers: {
                        Accept: "application/json",
                        Authorization: 'Bearer ' + $auth.getToken()
                    },
                    "dataSrc": function(json) {
                        $log.debug('Datatable data: ', json);

                        for (var i = 0, len = json.data.length; i < len; i++) {
                            json.data[i]['smsNetPrice'] = $filter('currency')(json.data[i]['smsNetPrice'], '', 2);
                            json.data[i]['smsGrossPrice'] = $filter('currency')(json.data[i]['smsGrossPrice'], '', 2);
                            var href = $compile('<a ui-sref="app.transaction_details({transactionId: \'' + json.data[i]['id'] + '\'})"></a>')($scope)[0].href;
                            // var href='test';
                            json.data[i]['actions'] = '<a href="' + href + '" class="mb-sm btn btn-default">Szczegóły</a>';

                            if (json.data[i]['paymentOrder'] === null) {
                                json.data[i]['status'] = '<button type="button" class="mb-sm btn btn-inverse">&nbsp;</button>';
                            } else {
                                if (json.data[i]['paymentOrder'].payed == true) {
                                    json.data[i]['status'] = '<button type="button" class="mb-sm btn btn-success">&nbsp;</button>';
                                } else {
                                    json.data[i]['status'] = '<button type="button" class="mb-sm btn btn-warning">&nbsp;</button>';
                                }
                            }
                        }

                        return json.data;
                    }
                })
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('processing', true)
                .withOption('order', [
                    [2, 'desc']
                ])
                .withPaginationType('full_numbers')
                .withLanguage({
                    'url': '/adminPanelApp/i18n/datatables.json'
                });

            vm.dtColumns = [
                DTColumnBuilder.newColumn('status').withTitle('Status').notSortable().withOption('searchable', false),
                DTColumnBuilder.newColumn('id').withTitle('ID'),
                DTColumnBuilder.newColumn('created').withTitle('Data utowrzenia'),
                DTColumnBuilder.newColumn('updated').withTitle('Ostatnia aktualizacja'),
                DTColumnBuilder.newColumn('cryptocurrency').withTitle('Kryptowaluta'),
                DTColumnBuilder.newColumn('smsNetPrice').withTitle('Cena netto'),
                DTColumnBuilder.newColumn('smsGrossPrice').withTitle('Cena brutto'),
                DTColumnBuilder.newColumn('smsCurrency').withTitle('Waluta'),
                DTColumnBuilder.newColumn('actions').withTitle('Akcje').notSortable().withOption('searchable', false)
            ];

        }.bind(this))();
    }
})();
