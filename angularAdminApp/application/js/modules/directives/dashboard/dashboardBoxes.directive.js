(function() {
    'use strict';

    angular.module('adminPanel.directives').directive('dashboardBoxes', DashboardBoxesDirective);

    DashboardBoxesDirective.$inject = [];

    function DashboardBoxesDirective() {
        return {
            restrict: 'E',
            templateUrl: 'adminPanelApp/views/directives/dashboard/dashboardBoxes.html',
            controller: 'DashboardBoxesController',
            controllerAs: 'dashboardBoxesController'
        };
    }
})();
