(function() {
    'use strict';

    angular
        .module('adminPanel.directives')
        .controller('DashboardBoxesController', DashboardBoxesController);

    DashboardBoxesController.$inject = ['$log', 'TransactionModel', 'PaymentModel'];

    function DashboardBoxesController($log, TransactionModel, PaymentModel) {
        var vm = this;

        vm.counts = {
            allTransactions: 0,
            newTransactions: 0,
            allPayments: 0,
            unsuccessfulPayments: 0
        }

        /**
         * Initialize method
         */
        var initialize = (function() {
            TransactionModel.getAllCount().then(
                function(response) {
                    vm.counts.allTransactions = response.count;
                }
            );
            TransactionModel.getNewCount().then(
                function(response) {
                    vm.counts.newTransactions = response.count;
                }
            );
            PaymentModel.getAllCount().then(
                function(response) {
                    vm.counts.allPayments = response.count;
                }
            );
            PaymentModel.getUnsuccesfulCount().then(
                function(response) {
                    vm.counts.unsuccessfulPayments = response.count;
                }
            );
        }.bind(this))();
    }
})();
