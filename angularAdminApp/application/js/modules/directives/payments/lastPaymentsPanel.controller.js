(function() {
    'use strict';

    angular
        .module('adminPanel.directives')
        .controller('LastPaymentsPanelController', LastPaymentsPanelController);

    LastPaymentsPanelController.$inject = ['$log', 'PaymentModel'];

    function LastPaymentsPanelController($log, PaymentModel) {
        var vm = this;

        vm.payments = [];
        vm.paymentsCompleted = [];


        PaymentModel.getLastPayments().then(
            function(response) {
                vm.payments = response;
            }
        )
        PaymentModel.getLastCompletedPayments().then(
            function(response) {
                $log.debug('Pobrane: ', response);
                vm.paymentsCompleted = response;
            }
        )
    }
})();
