(function() {
    'use strict';

    angular.module('adminPanel.directives').directive('lastPaymentsPanel', LastPaymentsPanelDirective);

    LastPaymentsPanelDirective.$inject = [];

    function LastPaymentsPanelDirective() {
        return {
            restrict: 'E',
            templateUrl: 'adminPanelApp/views/directives/payments/lastPaymentsPanel.html',
            controller: 'LastPaymentsPanelController',
            controllerAs: 'lastPaymentsPanelController'
        };
    }
})();
