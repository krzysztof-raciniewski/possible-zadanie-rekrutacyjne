(function() {
    'use strict';

    angular.module('adminPanel.directives').directive('lastTransactionsPanel', LastTransactionsPanelDirective);

    LastTransactionsPanelDirective.$inject = [];

    function LastTransactionsPanelDirective() {
        return {
            restrict: 'E',
            templateUrl: 'adminPanelApp/views/directives/transactions/lastTransactionsPanel.html',
            controller: 'LastTransactionsPanelController',
            controllerAs: 'lastTransactionsPanelController'
        };
    }
})();
