(function() {
    'use strict';

    angular
        .module('adminPanel.directives')
        .controller('LastTransactionsPanelController', LastTransactionsPanelController);

    LastTransactionsPanelController.$inject = ['$log', 'TransactionModel'];

    function LastTransactionsPanelController($log, TransactionModel) {
        var vm = this;

        vm.transactions = [];
        vm.transactionsCompleted = [];


        TransactionModel.getLastTransactions().then(
            function(response) {
                vm.transactions = response;
            }
        );

        TransactionModel.getLastCompletedTransactions().then(
            function(response) {
                $log.debug('Pobrane: ', response);
                vm.transactionsCompleted = response;
            }
        );
    }
})();
