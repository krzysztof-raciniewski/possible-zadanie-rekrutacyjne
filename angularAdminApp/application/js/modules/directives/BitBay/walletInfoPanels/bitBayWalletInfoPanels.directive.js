(function () {
    'use strict';

    angular.module('adminPanel.directives').directive('bitBayWalletInfoPanels', BitBayWalletInfoPanelsDirective);

    BitBayWalletInfoPanelsDirective.$inject = [];
    function BitBayWalletInfoPanelsDirective() {
        return {
            restrict: 'E',
            templateUrl: 'adminPanelApp/views/directives/bitBayWalletInfoPanels.html',
            controller: 'BitBayWalletInfoPanelsController',
            controllerAs: 'walletInfoDirective'
        };
    }
})();