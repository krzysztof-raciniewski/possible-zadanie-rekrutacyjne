(function() {
    'use strict';

    angular
        .module('adminPanel.directives')
        .controller('BitBayWalletInfoPanelsController', BitBayWalletInfoPanelsController);

    BitBayWalletInfoPanelsController.$inject = ['$log', '$mdDialog','BitbayModel', 'SettingsModel'];

    function BitBayWalletInfoPanelsController($log, $mdDialog, BitbayModel, SettingsModel) {
        var vm = this;

        vm.balances = {
            processing: true,
            BTC: { available: 0, locked: 0, processing: true },
            LSK: { available: 0, locked: 0, processing: true },
            ETH: { available: 0, locked: 0, processing: true },
            LTC: { available: 0, locked: 0, processing: true }
        };

        vm.walletAddresses = {
            BTC: '',
            LTC: '',
            LSK: '',
            ETH: '',
        };

        vm.fullValues = {
            btcWalletFullValue: '',
            lskWalletFullValue: '',
            ltcWalletFullValue: '',
            ethWalletFullValue: '',
        };

        vm.calculateBtcCharge = function() {
            return (vm.balances.BTC.available / vm.fullValues.btcWalletFullValue) * 100;
        }
        vm.calculateLtcCharge = function() {
            return (vm.balances.LTC.available / vm.fullValues.ltcWalletFullValue) * 100;
        }
        vm.calculateLskCharge = function() {
            return (vm.balances.LSK.available / vm.fullValues.lskWalletFullValue) * 100;
        }
        vm.calculateEthCharge = function() {
            return (vm.balances.ETH.available / vm.fullValues.ethWalletFullValue) * 100;
        }

        vm.showWalletInfo = function(currency, ev) {
            $mdDialog.show(
              $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Adres portfela ' + currency)
                .content(vm.walletAddresses[currency])
                .ariaLabel('Wallet number')
                .ok('Zamknij okno!')
                .targetEvent(ev)
            );
        }

        /**
         * Initialize method
         */
        var initialize = (function() {
            // Get currencies from BitBay market
            BitbayModel.getAmount().then(
                function(response) {
                    console.log('Pobrano informacje o stanach portfela.', response);

                    vm.balances.BTC.available = response.balances.BTC.available;
                    vm.balances.BTC.locked = response.balances.BTC.locked;
                    vm.balances.BTC.processing = false;

                    vm.balances.LSK.available = response.balances.LSK.available;
                    vm.balances.LSK.locked = response.balances.LSK.locked;
                    vm.balances.LSK.processing = false;

                    vm.balances.ETH.available = response.balances.ETH.available;
                    vm.balances.ETH.locked = response.balances.ETH.locked;
                    vm.balances.ETH.processing = false;

                    vm.balances.LTC.available = response.balances.LTC.available;
                    vm.balances.LTC.locked = response.balances.LTC.locked;
                    vm.balances.LTC.processing = false;

                    vm.balances.processing = false;

                    vm.walletAddresses = {
                        BTC: response.addresses.BTC,
                        LTC: response.addresses.LTC,
                        LSK: response.addresses.LSK,
                        ETH: response.addresses.ETH,
                    }
                },
                function(response) {
                    $log.debug(response);
                }
            );

            SettingsModel.getWallet().then(
                function(response) {
                    vm.fullValues = {
                        btcWalletFullValue: response.btcWalletFullValue,
                        lskWalletFullValue: response.lskWalletFullValue,
                        ltcWalletFullValue: response.ltcWalletFullValue,
                        ethWalletFullValue: response.ethWalletFullValue,
                    };
                }

            )


        }.bind(this))();
    }
})();
