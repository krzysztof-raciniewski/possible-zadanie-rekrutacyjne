(function () {
    'use strict';

    angular.module('adminPanel.directives').directive('bitBayOrdersListPanel', BitBayOrdersListPanel);

    BitBayOrdersListPanel.$inject = [];
    function BitBayOrdersListPanel() {
        return {
            restrict: 'E',
            templateUrl: 'adminPanelApp/views/directives/bitBayOrdersListPanel.html',
            controller: 'BitBayOrdersListPanelController',
            controllerAs: 'ordersListCtrl'
        };
    }
})();