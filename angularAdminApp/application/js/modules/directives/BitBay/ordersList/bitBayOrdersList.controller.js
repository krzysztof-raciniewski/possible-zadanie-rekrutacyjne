(function () {
    'use strict';

    angular
        .module('adminPanel.directives')
        .controller('BitBayOrdersListPanelController', BitBayOrdersListPanelController);

    BitBayOrdersListPanelController.$inject = ['$log', 'BitbayModel'];
    function BitBayOrdersListPanelController($log, BitbayModel) {
        var vm = this;

        vm.orders = {
            processing: true,
            all: {
                processing: true,
                entities: {}
            },
            active: {
                entities: {}
            },
            inactive: {
                entities: {}
            }
        };

        var getActiveOrders = function () {
            var result = {};
            angular.forEach(vm.orders.all.entities, function (value, key) {
                if (value.status == 'active') {
                    result[key] = value;
                }
            });

            vm.orders.active.entities = result;
        };

        var getInactiveOrders = function () {
            var result = {};
            angular.forEach(vm.orders.all.entities, function (value, key) {
                if (value.status == 'inactive') {
                    result[key] = value;
                }
            });

            vm.orders.inactive.entities = result;
        };

        var getOrdersList = function () {

            vm.orders.processing = true;
            BitbayModel.getOrders().then(
                function (response) {
                    console.log('Pobrano listę zamówień z giełdy BitBay.');
                    console.log(response);
                    console.log("Ilosć wyników: " + response.length);
                    vm.orders.all.entities = response;

                    getActiveOrders();
                    getInactiveOrders();

                    vm.orders.processing = false;
                },
                function (response) {
                    console.log(response);
                    console.log(response);
                }
            );
        };

        /**
         * Initialize method
         */
        var initialize = (function () {
            // Get orders list
            getOrdersList();
        }.bind(this))();
    }
})();