(function () {
    'use strict';

    angular.module('adminPanel.directives').directive('bitBayHistoryPanel', BitBayHistoryPanel);

    BitBayHistoryPanel.$inject = [];
    function BitBayHistoryPanel() {
        return {
            restrict: 'E',
            templateUrl: 'adminPanelApp/views/directives/bitBayHistoryPanel.html',
            controller: 'BitBayHistoryPanelController',
            controllerAs: 'historyPanelDirective'
        };
    }
})();