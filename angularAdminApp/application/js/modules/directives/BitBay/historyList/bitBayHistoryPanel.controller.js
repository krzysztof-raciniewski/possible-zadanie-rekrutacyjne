(function () {
    'use strict';

    angular
        .module('adminPanel.directives')
        .controller('BitBayHistoryPanelController', BitBayHistoryPanelController);

    BitBayHistoryPanelController.$inject = ['$log', 'BitbayModel'];
    function BitBayHistoryPanelController($log, BitbayModel) {
        var vm = this;

        vm.history = {
            page_iterator: 5,
            BTC: {
                results: 5,
                processing: true,
                transactions: {}
            },
            LSK: {
                results: 5,
                processing: true,
                transactions: {}
            },
            ETH: {
                results: 5,
                processing: true,
                transactions: {}
            },
            LTC: {
                results: 5,
                processing: true,
                transactions: {}
            }
        };

        vm.openHistoryTabInitializator = function (currency) {
            if (currency == 'BTC') getBitBayHistoryForBTC();
            if (currency == 'LSK') getBitBayHistoryForLSK();
            if (currency == 'ETH') getBitBayHistoryForETH();
            if (currency == 'LTC') getBitBayHistoryForLTC();
        };

        vm.showMoreHistoryResults = function (currency) {
            if (currency == 'BTC') {
                vm.history.BTC.results += vm.history.page_iterator;
                getBitBayHistoryForBTC();
            }
            if (currency == 'LSK') {

                vm.history.LSK.results += vm.history.page_iterator;
                getBitBayHistoryForLSK();
            }
            if (currency == 'ETH') {
                vm.history.ETH.results += vm.history.page_iterator;
                getBitBayHistoryForETH();
            }
            if (currency == 'LTC') {
                vm.history.LTC.results += vm.history.page_iterator;
                getBitBayHistoryForLTC();
            }
        };

        var getBitBayHistoryForBTC = function () {
            vm.history.BTC.processing = true;
            $log.debug('Pobieram historie transakcji dla BTC');
            // Get BTC history
            BitbayModel.getHistory('BTC', '' + vm.history.BTC.results).then(
                function (response) {
                    console.log('Zapytanie powiodło się.', response);

                    vm.history.BTC.transactions = response;
                    vm.history.BTC.processing = false;
                },
                function (response) {
                    console.log(response);
                    console.log(response);
                }
            );
        };

        var getBitBayHistoryForLSK = function () {
            vm.history.LSK.processing = true;
            $log.debug('Pobieram historie transakcji dla LSK');
            BitbayModel.getHistory('LSK', '' + vm.history.LSK.results).then(
                function (response) {
                    console.log('Zapytanie powiodło się.');
                    console.log(response);
                    console.log("Ilosć wyników: " + response.length);

                    vm.history.LSK.transactions = response;
                    vm.history.LSK.processing = false;
                },
                function (response) {
                    console.log(response);
                    console.log(response);
                }
            );
        };

        var getBitBayHistoryForETH = function () {
            vm.history.ETH.processing = true;
            $log.debug('Pobieram historie transakcji dla ETH');
            BitbayModel.getHistory('ETH', '' + vm.history.ETH.results).then(
                function (response) {
                    console.log('Zapytanie powiodło się.');
                    console.log(response);
                    console.log("Ilosć wyników: " + response.length);

                    vm.history.ETH.transactions = response;
                    vm.history.ETH.processing = false;
                },
                function (response) {
                    console.log(response);
                    console.log(response);
                }
            );
        };

        var getBitBayHistoryForLTC = function () {
            vm.history.LTC.processing = true;
            $log.debug('Pobieram historie transakcji dla LTC');
            BitbayModel.getHistory('LTC', '' + vm.history.LTC.results).then(
                function (response) {
                    console.log('Zapytanie powiodło się.');
                    console.log(response);
                    console.log("Ilosć wyników: " + response.length);

                    vm.history.LTC.transactions = response;
                    vm.history.LTC.processing = false;
                },
                function (response) {
                    console.log(response);
                    console.log(response);
                }
            );
        };

        /**
         * Initialize method
         */
        var initialize = (function () {
            // Get BTC history
            getBitBayHistoryForBTC();
        }.bind(this))();
    }
})();