(function() {
    'use strict';
    angular
        .module('adminPanel.directives')
        .controller('OperatorsListController', OperatorsListController);
    OperatorsListController.$inject = ['$log', '$mdDialog', 'OperatorModel', 'Notify'];

    function OperatorsListController($log, $mdDialog, OperatorModel, Notify) {
        var vm = this;
        vm.operators = {};


        var getOperatorsListRequest = function() {
            // get sms list
            OperatorModel.getAll().then(
                function(result) {
                    if (result.length > 0) {
                        vm.operators = result;
                        $log.debug('Lista operatorów została pobrana: ', result);
                    }
                },
                function() {
                    Notify.alert(
                        'Nie udało się pobrać listy operatorów', { status: 'danger' }
                    );
                }
            );
        };

        var addNewOperatorRequest = function(name) {
            OperatorModel.addNew(name).then(
                function(result) {
                    vm.operators.push(result);
                },
                function(result) {
                    $log.debug(result);
                    var message = 'Nie udało się dodać nowego operatora - błąd serwera';
                    if (result.data == 'Operator exists') {
                        message = 'Operator o takiej nazwie istnieje!'
                    }
                    Notify.alert(
                        message, { status: 'danger' }
                    );
                }
            );
        }

        var editOperatorRequest = function(operator_id, newName) {
            OperatorModel.edit(operator_id, newName).then(
                function(result) {
                    Notify.alert(
                        'Nazwa operatora została zmieniona', { status: 'success' }
                    );
                    findAndUpdateOperator(operator_id, result);
                },
                function(response) {
                    var message = 'Nie udało się zmienić informacji o operatorze - błąd serwera';
                    if (response.data == 'Operator exists') {
                        message = 'Operator o takiej nazwie istnieje!'
                    }
                    Notify.alert(
                        message, { status: 'danger' }
                    );
                }
            );
        }

        var deleteOperatorRequest = function(operator_id, newName) {
            OperatorModel.delete(operator_id).then(
                function(result) {
                    Notify.alert(
                        'Operator został usunięty', { status: 'success' }
                    );
                    findAndRemoveOperator(operator_id);
                },
                function() {
                    Notify.alert(
                        'Nie udało się usunąć operatora - błąd serwera', { status: 'danger' }
                    );
                }
            );
        }

        var findAndUpdateOperator = function(id, newObject) {
            for (var i = 0; i < vm.operators.length; i++) {
                if (vm.operators[i].id === id) {
                    vm.operators[i] = newObject;
                    return;
                }
            }
        }

        var findAndRemoveOperator = function(id) {
            for (var i = 0; i < vm.operators.length; i++) {
                if (vm.operators[i].id === id) {
                    vm.operators.splice(i, 1);
                    return;
                }
            }
        }

        vm.showDeleteConfirmDialog = function(operator, ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Chcesz usunąć operatora?')
                .content('Usuwany operator zostanie wykluczony ze wszystkich opcji konfiguracyjnych wiadomości SMS!')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Tak, usuń operatora')
                .cancel('Anuluj operację');
            $mdDialog.show(confirm).then(function() {
                deleteOperatorRequest(operator.id)
            });
        };

        vm.showAddFormDialog = function(ev) {
            $mdDialog.show({
                    controller: 'NewOperatorDialogController',
                    controllerAs: 'newOperatorDialogController',
                    templateUrl: 'adminPanelApp/views/dialogs/operators/addOperatorDialog.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true
                })
                .then(function(name) {
                    addNewOperatorRequest(name);
                });
        };

        vm.showEditFormDialog = function(operator, ev) {
            $mdDialog.show({
                    controller: 'EditOperatorDialogController',
                    controllerAs: 'editOperatorDialogController',
                    bindToController: true,
                    templateUrl: 'adminPanelApp/views/dialogs/operators/editOperatorDialog.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        operator: operator
                    }
                })
                .then(function(name) {
                    editOperatorRequest(operator.id, name);
                });
        };


        /**
         * Initialize method
         */
        var initialize = function() {
            getOperatorsListRequest();
        }.bind(this);

        initialize();
    }
})();
