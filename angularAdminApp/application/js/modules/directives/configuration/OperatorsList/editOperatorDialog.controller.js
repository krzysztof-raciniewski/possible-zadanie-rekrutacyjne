(function () {
    'use strict';

    angular
        .module('adminPanel.directives')
        .controller('EditOperatorDialogController', EditOperatorDialogController);

    EditOperatorDialogController.$inject = ['$log', '$scope', '$mdDialog'];
    function EditOperatorDialogController($log, $scope, $mdDialog) {
        var vm = this;

        $log.debug(vm.operator);

        vm.name = vm.operator.name;

        vm.hide = function() {
            $mdDialog.hide();
        };
        vm.cancel = function() {
            $mdDialog.cancel();
        };
        vm.edit = function() {
            if($scope.newOperatorForm.$invalid) {
                $log.debug('Formularz jest nieprawidłowy');
                return;
            }
            $mdDialog.hide(vm.name);
        };

    }
})();