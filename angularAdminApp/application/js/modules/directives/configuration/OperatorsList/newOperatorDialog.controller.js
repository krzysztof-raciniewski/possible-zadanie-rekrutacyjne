(function () {
    'use strict';

    angular
        .module('adminPanel.directives')
        .controller('NewOperatorDialogController', NewOperatorDialogController);

    NewOperatorDialogController.$inject = ['$log', '$scope', '$mdDialog'];
    function NewOperatorDialogController($log, $scope, $mdDialog) {
        var vm = this;

        vm.name = '';

        vm.hide = function() {
            $mdDialog.hide();
        };
        vm.cancel = function() {
            $mdDialog.cancel();
        };
        vm.add = function() {
            if($scope.newOperatorForm.$invalid) {
                $log.debug('Formularz jest nieprawidłowy');
                return;
            }
            $mdDialog.hide(vm.name);
        };

    }
})();