(function () {
    'use strict';

    angular.module('adminPanel.directives').directive('operatorsList', OperatorsList);

    OperatorsList.$inject = [];
    function OperatorsList() {
        return {
            restrict: 'E',
            templateUrl: 'adminPanelApp/views/directives/operators_list.html',
            controller: 'OperatorsListController',
            controllerAs: 'operatorsListController'
        };
    }
})();