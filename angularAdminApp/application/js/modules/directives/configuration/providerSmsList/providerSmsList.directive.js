(function () {
    'use strict';

    angular.module('adminPanel.directives').directive('providerSmsList', ProviderSmsListDirective);

    ProviderSmsListDirective.$inject = [];
    function ProviderSmsListDirective() {
        return {
            restrict: 'E',
            templateUrl: 'adminPanelApp/views/directives/configuration/providerSmsListDirective.html',
            controller: 'ProviderSmsListController',
            controllerAs: 'providerSmsListController',
            scope: {
                providerId: '='
            },
            bindToController: true
        };
    }
})();