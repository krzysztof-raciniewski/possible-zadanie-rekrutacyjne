
(function () {
    'use strict';
    angular
        .module('adminPanel.directives')
        .controller('ProviderSmsListController', ProviderSmsListController);
    ProviderSmsListController.$inject = ['$log', '$scope', '$mdDialog', 'ProviderModel', 'Notify'];
    function ProviderSmsListController($log, $scope, $mdDialog, ProviderModel, Notify) {
        var vm = this;
        vm.sms = [];

        vm.changeSmsStatusRequest = function (smsId) {
            ProviderModel.changeSmsStatus(vm.providerId, smsId).then(
                function() {
                    Notify.alert(
                        'Status wiadomości SMS został zmieniony',
                        { status: 'success' }
                    );
                },
                function() {
                    Notify.alert(
                        'Nie udało się zmienić statusu wiadomości SMS',
                        { status: 'danger' }
                    );
                }
            )
        };

        var getSmsListRequest = function () {
            // get sms list
            ProviderModel.getSmsList(vm.providerId).then(
                function (result) {
                    if (result.length > 0) {
                        vm.sms = result;
                        $log.debug('Lista sms została pobrana: ', result);
                    }
                },
                function () {
                    Notify.alert(
                        'Nie udało się pobrać listy SMS',
                        { status: 'danger' }
                    );
                }
            );
        };

        vm.addSmsRequest = function(data) {
            ProviderModel.addSms(vm.providerId, data).then(
                function(newSmsModel) {
                    Notify.alert(
                        'Nowa definicja SMS została dodana do listy',
                        { status: 'success' }
                    );

                    vm.sms.push(newSmsModel);
                },
                function() {
                    Notify.alert(
                        'Nowa definicja SMS nie mogła zostać dodana, bląd serwera',
                        { status: 'danger' }
                    );  
                }
            );
        }

        vm.editSmsRequest = function(smsId, data) {
            ProviderModel.editSms(vm.providerId, smsId, data).then(
                function(newSmsModel) {
                    Notify.alert(
                        'Wiadomość sms została zmodyfikowana',
                        { status: 'success' }
                    );
                    vm.findAndModifySms(smsId, newSmsModel);
                    // vm.sms.push(newSmsModel);
                },
                function() {
                    Notify.alert(
                        'Nie można zmodyfikować infomacji, bląd serwera',
                        { status: 'danger' }
                    );  
                }
            );
        }
        
        vm.deleteSmsRequest = function(smsId) {
            ProviderModel.removeSms(vm.providerId, smsId).then(
                function() {
                    Notify.alert(
                        'Definicja SMS została usunięta',
                        { status: 'success' }
                    );
                    vm.removeSmsFromCollection(smsId);
                },
                function() {
                    Notify.alert(
                        'Definicja SMS nie mogła zostać usunięta',
                        { status: 'danger' }
                    );    
                }
            );      
        };

        //
        // --------------------------------------------------------------------------------
        //

        vm.findAndModifySms = function(smsId, data) {
            for(var i = 0; i < vm.sms.length; i++ ) {
                if(vm.sms[i].id === smsId) {
                    vm.sms[i] = data;
                    break;
                }
            }
        };

        vm.removeSmsFromCollection = function(smsId) {
            for(var i = 0; i < vm.sms.length; i++ ) {
                if( vm.sms[i].id === smsId ) {
                    vm.sms.splice(i, 1);
                    break;
                }
            }
        };


        // Dialog - delete confirmation
        vm.openDeleteConfirmationDialog = function (smsId, ev) {
            var confirm = $mdDialog.confirm()
                .title('Chcesz usunąć definicję SMS?')
                .content('Usunięcie nie zmieni nic we wpisach historycznych. W każdej chwili możesz dodać SMS ponownie')
                .ariaLabel('Lucky day')
                .ok('Usuń definicję SMS')
                .cancel('Anuluj operację')
                .clickOutsideToClose(true)
                .targetEvent(ev);
            $mdDialog.show(confirm).then(function () {
                vm.deleteSmsRequest(smsId);
            });
        };

        vm.openAddSmsDialog = function(ev) {
                $mdDialog.show({
                  controller: 'AddSmsDialogController',
                  controllerAs: 'addSmsDialogController',
                  templateUrl: 'adminPanelApp/views/directives/configuration/dialogs/addSmsDialog.html',
                  parent: angular.element(document.body),
                  targetEvent: ev,
                  clickOutsideToClose:true
                })
                .then(function(data) {
                  $log.debug('Odpowiedź z dialoga: ', data);
                  vm.addSmsRequest(data);
                });
        };

        vm.openEditSmsDialog = function(smsId, ev) {
                $mdDialog.show({
                  controller: 'EditSmsDialogController',
                  controllerAs: 'editSmsDialogController',
                  bindToController: true,
                  templateUrl: 'adminPanelApp/views/directives/configuration/dialogs/editSmsDialog.html',
                  parent: angular.element(document.body),
                  targetEvent: ev,
                  clickOutsideToClose:true,
                  locals: {
                    smsId: smsId,
                    providerId: vm.providerId
                  }
                })
                .then(function(data) {
                  $log.debug('Odpowiedź z dialoga: ', data);
                  vm.editSmsRequest(smsId, data);
                });
        };

        /**
         * Initialize method
         */
        var initialize = function () {
            getSmsListRequest();
        }.bind(this);

        initialize();      
    }
})();