(function() {
    'use strict';

    angular
        .module('adminPanel.directives')
        .constant('NET_PRICES_MAX_VALUE', 26)
        .controller('EditSmsDialogController', EditSmsDialogController);

    EditSmsDialogController.$inject = ['$log', '$scope', '$mdDialog', 'ProviderModel', 'CountryModel', 'CurrencyModel', 'OperatorModel', 'Notify', 'NET_PRICES_MAX_VALUE'];

    function EditSmsDialogController($log, $scope, $mdDialog, ProviderModel, CountryModel, CurrencyModel, OperatorModel, Notify, NET_PRICES_MAX_VALUE) {
        var vm = this;

        vm.filterSelected = true;

        vm.data = {
            net_price: '',
            gross_price: '',
            la_number: '',
            command: '',
            country: '',
            currency: '',
            service_id: '',
            brokerage: '',
            token: '',
            operators: [],
            enabled: false,
        }

        vm.operators = [];

        vm.net_prices = [];

        for (var i = 1; i < NET_PRICES_MAX_VALUE; i++) {
            vm.net_prices.push(i);
        }

        vm.countries = [];

        vm.currencies = [];

        vm.getSmsFromServer = function() {
            ProviderModel.getSms(vm.providerId, vm.smsId).then(
                function(result) {
                    $log.debug('Informacje o wiadomości SMS: ', result);
                    vm.data = {
                        net_price: parseFloat(result.net_price),
                        gross_price: parseFloat(result.gross_price),
                        la_number: parseInt(result.la_number),
                        command: result.command,
                        country: result.country,
                        currency: result.currency,
                        brokerage: result.brokerage,
                        operators: copyOperatorsObjects(result.operators),
                        service_id: result.service_id,
                        token: result.token,
                        enabled: result.enabled || false,
                    };
                },
                function() {
                    Notify.alert(
                        'Nie można pobrać informacji o wiadomości SMS', { status: 'danger' }
                    );
                }
            );
        }

        $scope.getCountriesFromServer = function() {
            return CountryModel.getList().then(
                function(response) {
                    vm.countries = response;
                }
            );
        }

        $scope.getCurrenciesFromServer = function() {
            return CurrencyModel.getList().then(
                function(response) {
                    vm.currencies = response;
                }
            );
        }

        $scope.getOperatorsFromServer = (function() {
            return OperatorModel.getList().then(
                function(response) {
                    vm.operators = response;
                    vm.getSmsFromServer();
                }
            );
        })();

        /**
         * Search for contacts.
         */
        $scope.querySearch = function(query) {
            var results = query ? vm.operators.filter(createFilterFor(query)) : [];
            return results;
        }

        /**
         * Create filter function for a query string
         */
        var createFilterFor = function(query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(operator) {
                if (angular.lowercase(operator.name).indexOf(lowercaseQuery) != -1) {
                    return operator.name
                }
            };
        }

        /**
         * Copy operators for chips
         */
        var copyOperatorsObjects = function(operators) {
            var newOperators = [];
            angular.forEach(operators, function(operator) {
                for (var i = 0; i < vm.operators.length; i++) {
                    if (vm.operators[i].id === operator.id) {
                        newOperators.push(vm.operators[i]);
                        break;
                    }

                }
            });

            return newOperators;
        }

        var transformData = function(data) {
            var ids = [];
            angular.forEach(data.operators, function(operator) {
                ids.push(operator.id);
            });

            var newData = angular.copy(vm.data);
            newData.operators = ids.join(',');
            return newData;
        }

        vm.hide = function() {
            $mdDialog.hide();
        };
        vm.cancel = function() {
            $mdDialog.cancel();
        };
        vm.editRequest = function() {
            if ($scope.editNewSmsForm.$invalid) {
                $log.debug('Formularz jest nieprawidłowy');
                return;
            }
            $log.debug('Dane przed formatowaniem: ', vm.data);
            $log.debug('Dane po formatowaniu: ', transformData(vm.data));
            $mdDialog.hide(transformData(vm.data));
        };

        $scope.getCountriesFromServer();
        $scope.getCurrenciesFromServer();

        $scope.$watch(angular.bind(vm, function() {
            return vm.data.net_price;
        }), function(newVal) {
            vm.data.gross_price = Math.round((newVal * 1.23) * 100) / 100;
        }, true);

    }
})();
