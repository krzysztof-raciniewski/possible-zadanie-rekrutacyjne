(function () {
    'use strict';

    angular
        .module('adminPanel.directives')
        .constant('NET_PRICES_MAX_VALUE', 26)
        .controller('AddSmsDialogController', AddSmsDialogController);

    AddSmsDialogController.$inject = ['$log', '$scope', '$mdDialog', 'ProviderModel', 'CountryModel', 'CurrencyModel', 'OperatorModel', 'Notify', 'NET_PRICES_MAX_VALUE'];

    function AddSmsDialogController($log, $scope, $mdDialog, ProviderModel, CountryModel, CurrencyModel, OperatorModel, Notify, NET_PRICES_MAX_VALUE) {
        var vm = this;

        vm.filterSelected = true;

        vm.data = {
            net_price: '',
            gross_price: '',
            la_number: '',
            command: '',
            country: '',
            currency: '',
            service_id: '',
            token: '',
            brokerage: '',
            operators: [],
            enabled: false,
        };

        vm.net_prices = [];

        for (var i = 1; i < NET_PRICES_MAX_VALUE; i++) {
            vm.net_prices.push(i);
        }

        vm.countries = [];

        vm.currencies = [];

        vm.operators = [];

        $scope.getCountriesFromServer = function () {
            return CountryModel.getList().then(
                function (response) {
                    vm.countries = response;
                }
            );
        };

        $scope.getCurrenciesFromServer = function () {
            return CurrencyModel.getList().then(
                function (response) {
                    vm.currencies = response;
                }
            );
        };

        $scope.getOperatorsFromServer = (function () {
            return OperatorModel.getList().then(
                function (response) {
                    vm.operators = response;
                }
            );
        })();

        /**
         * Search for contacts.
         */
        $scope.querySearch = function (query) {
            var results = query ? vm.operators.filter(createFilterFor(query)) : [];
            return results;
        };

        /**
         * Create filter function for a query string
         */
        var createFilterFor = function (query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(operator) {
                if (angular.lowercase(operator.name).indexOf(lowercaseQuery) != -1) {
                    return operator.name
                }
            };
        };

        var transformData = function (data) {
            var ids = [];
            angular.forEach(data.operators, function (operator) {
                ids.push(operator.id);
            });

            var newData = angular.copy(vm.data);
            newData.operators = ids.join(',');
            return newData;
        };

        vm.hide = function () {
            $mdDialog.hide();
        };
        vm.cancel = function () {
            $mdDialog.cancel();
        };
        vm.add = function () {
            if ($scope.addNewSmsForm.$invalid) {
                $log.debug('Formularz jest nieprawidłowy');
                return;
            }
            $log.debug('Dane po formatowaniu: ', transformData(vm.data));
            $mdDialog.hide(transformData(vm.data));
        };

        $scope.$watch(angular.bind(vm, function () {
            return vm.data.net_price;
        }), function (newVal) {
            vm.data.gross_price = newVal * 1.23;
        }, true);
    }
})();
