(function() {
    'use strict';
    angular
        .module('adminPanel.directives')
        .controller('CurrenciesListController', CurrenciesListController);
    CurrenciesListController.$inject = ['$log', '$mdDialog', 'CurrencyModel', 'Notify'];

    function CurrenciesListController($log, $mdDialog, CurrencyModel, Notify) {
        var vm = this;
        vm.currencies = {};


        var getCurrenciesListRequest = function() {
            // get sms list
            CurrencyModel.getAll().then(
                function(result) {
                    if (result.length > 0) {
                        vm.currencies = result;
                        $log.debug('Lista walut została pobrana: ', result);
                    }
                },
                function() {
                    Notify.alert(
                        'Nie udało się pobrać listy walut', { status: 'danger' }
                    );
                }
            );
        };

        var addNewCurrencyRequest = function(data) {
            CurrencyModel.addNew(data).then(
                function(result) {
                    vm.currencies.push(result);
                    Notify.alert(
                        'Waluta została dodana do systemu', { status: 'success' }
                    );
                },
                function(result) {
                    $log.debug(result);
                    var message = 'Nie udało się dodać nowej waluty - błąd serwera';
                    if (result.data == 'Code exists') {
                        message = 'Taka waluta już istnieje w systemie! Skrót waluty musi być unikalny'
                    }
                    Notify.alert(
                        message, { status: 'danger' }
                    );
                }
            );
        }

        var editCurrencyRequest = function(currency_id, data) {
            CurrencyModel.edit(currency_id, data).then(
                function(result) {
                    Notify.alert(
                        'Waluta została zmieniona', { status: 'success' }
                    );
                    findAndUpdateCurrency(currency_id, result);
                },
                function(response) {
                    var message = 'Nie udało się zmienić informacji o walucie - błąd serwera';
                    if (response.data == 'Code exists') {
                        message = 'Taka waluta już istnieje w systemie! Skrót waluty musi być unikalny'
                    }
                    Notify.alert(
                        message, { status: 'danger' }
                    );
                }
            );
        }

        var deleteCurrencyRequest = function(currency_id, newName) {
            CurrencyModel.delete(currency_id).then(
                function(result) {
                    Notify.alert(
                        'Waluta została usunięta', { status: 'success' }
                    );
                    findAndRemoveCurrency(currency_id);
                },
                function(response) {
                    var message = 'Nie udało się usunąć waluty - błąd serwera';
                    if (response.data === 'Currency has assignments') {
                        message = 'Nie można usunąć waluty, która już zostałą przypisana do definicji SMS';
                    }
                    Notify.alert(
                        message, { status: 'danger' }
                    );
                }
            );
        }

        var findAndUpdateCurrency = function(id, newObject) {
            for (var i = 0; i < vm.currencies.length; i++) {
                if (vm.currencies[i].id === id) {
                    vm.currencies[i] = newObject;
                    return;
                }
            }
        }

        var findAndRemoveCurrency = function(id) {
            for (var i = 0; i < vm.currencies.length; i++) {
                if (vm.currencies[i].id === id) {
                    vm.currencies.splice(i, 1);
                    return;
                }
            }
        }

        vm.showDeleteConfirmDialog = function(currency, ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Chcesz usunąć walutę?')
                .content('Usuwana waluta nie może być powiązana z żadną definicją SMS!')
                .ariaLabel('Usuwanie waluty')
                .targetEvent(ev)
                .ok('Tak, usuń walutę')
                .cancel('Anuluj operację');
            $mdDialog.show(confirm).then(function() {
                deleteCurrencyRequest(currency.id)
            });
        };

        vm.showAddFormDialog = function(ev) {
            $mdDialog.show({
                    controller: 'NewCurrencyDialogController',
                    controllerAs: 'newCurrencyDialogController',
                    templateUrl: 'adminPanelApp/views/dialogs/currencies/addCurrencyDialog.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true
                })
                .then(function(data) {
                    addNewCurrencyRequest(data);
                });
        };

        vm.showEditFormDialog = function(currency, ev) {
            $mdDialog.show({
                    controller: 'EditCurrencyDialogController',
                    controllerAs: 'editCurrencyDialogController',
                    bindToController: true,
                    templateUrl: 'adminPanelApp/views/dialogs/currencies/editCurrencyDialog.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        currency: currency
                    }
                })
                .then(function(data) {
                    editCurrencyRequest(currency.id, data);
                });
        };


        /**
         * Initialize method
         */
        var initialize = function() {
            getCurrenciesListRequest();
        }.bind(this);

        initialize();
    }
})();
