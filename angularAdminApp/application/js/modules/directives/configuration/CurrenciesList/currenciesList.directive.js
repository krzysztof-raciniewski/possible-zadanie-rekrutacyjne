(function () {
    'use strict';

    angular.module('adminPanel.directives').directive('currenciesList', CurrenciesList);

    CurrenciesList.$inject = [];
    function CurrenciesList() {
        return {
            restrict: 'E',
            templateUrl: 'adminPanelApp/views/directives/currencies_list.html',
            controller: 'CurrenciesListController',
            controllerAs: 'currenciesListController'
        };
    }
})();