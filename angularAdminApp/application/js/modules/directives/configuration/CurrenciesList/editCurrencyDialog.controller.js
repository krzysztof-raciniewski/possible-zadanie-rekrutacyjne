(function() {
    'use strict';

    angular
        .module('adminPanel.directives')
        .controller('EditCurrencyDialogController', EditCurrencyDialogController);

    EditCurrencyDialogController.$inject = ['$log', '$scope', '$mdDialog'];

    function EditCurrencyDialogController($log, $scope, $mdDialog) {
        var vm = this;

        vm.data = {
            name: vm.currency.name,
            code: vm.currency.code
        };

        vm.hide = function() {
            $mdDialog.hide();
        };
        vm.cancel = function() {
            $mdDialog.cancel();
        };
        vm.edit = function() {
            if ($scope.editCurrencyForm.$invalid) {
                $log.debug('Formularz jest nieprawidłowy');
                return;
            }
            $mdDialog.hide(vm.data);
        };

    }
})();
