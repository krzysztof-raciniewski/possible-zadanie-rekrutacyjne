(function() {
    'use strict';

    angular
        .module('adminPanel.directives')
        .controller('NewCurrencyDialogController', NewCurrencyDialogController);

    NewCurrencyDialogController.$inject = ['$log', '$scope', '$mdDialog'];

    function NewCurrencyDialogController($log, $scope, $mdDialog) {
        var vm = this;

        vm.data = {
            name: '',
            code: ''
        };

        vm.hide = function() {
            $mdDialog.hide();
        };
        vm.cancel = function() {
            $mdDialog.cancel();
        };
        vm.add = function() {
            if ($scope.newCurrencyForm.$invalid) {
                $log.debug('Formularz jest nieprawidłowy');
                return;
            }
            $mdDialog.hide(vm.data);
        };

    }
})();
