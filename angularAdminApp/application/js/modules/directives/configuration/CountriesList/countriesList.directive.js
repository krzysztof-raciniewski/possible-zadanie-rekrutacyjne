(function () {
    'use strict';

    angular.module('adminPanel.directives').directive('countriesList', CountriesList);

    CountriesList.$inject = [];
    function CountriesList() {
        return {
            restrict: 'E',
            templateUrl: 'adminPanelApp/views/directives/countries_list.html',
            controller: 'CountriesListController',
            controllerAs: 'countriesListController'
        };
    }
})();