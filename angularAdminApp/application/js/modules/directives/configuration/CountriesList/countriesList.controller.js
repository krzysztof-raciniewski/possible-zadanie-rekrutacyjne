(function() {
    'use strict';
    angular
        .module('adminPanel.directives')
        .controller('CountriesListController', CountriesListController);
    CountriesListController.$inject = ['$log', '$mdDialog', 'CountryModel', 'Notify'];

    function CountriesListController($log, $mdDialog, CountryModel, Notify) {
        var vm = this;
        vm.countries = {};


        var getCountriesListRequest = function() {
            // get sms list
            CountryModel.getAll().then(
                function(result) {
                    if (result.length > 0) {
                        vm.countries = result;
                        $log.debug('Lista krajów została pobrana: ', result);
                    }
                },
                function() {
                    Notify.alert(
                        'Nie udało się pobrać listy krajów', { status: 'danger' }
                    );
                }
            );
        };

        var addNewCountryRequest = function(data) {
            CountryModel.addNew(data).then(
                function(result) {
                    vm.countries.push(result);
                },
                function(result) {
                    $log.debug(result);
                    var message = 'Nie udało się dodać nowego kraju - błąd serwera';
                    if (result.data == 'Code exists') {
                        message = 'Taki kraj już istnieje w systemie! Skrót kraju musi być unikalny'
                    }
                    Notify.alert(
                        message, { status: 'danger' }
                    );
                }
            );
        }

        var editCountryRequest = function(Country_id, data) {
            CountryModel.edit(Country_id, data).then(
                function(result) {
                    Notify.alert(
                        'Informacje o kraju zostały zmienione', { status: 'success' }
                    );
                    findAndUpdateCountry(Country_id, result);
                },
                function(response) {
                    var message = 'Nie udało się zmienić informacji o kraju - błąd serwera';
                    if (response.data == 'Code exists') {
                        message = 'Taki kraj już istnieje w systemie! Skrót kraju musi być unikalny'
                    }
                    Notify.alert(
                        message, { status: 'danger' }
                    );
                }
            );
        }

        var deleteCountryRequest = function(Country_id, newName) {
            CountryModel.delete(Country_id).then(
                function(result) {
                    Notify.alert(
                        'Kraj został usunięty z systemu', { status: 'success' }
                    );
                    findAndRemoveCountry(Country_id);
                },
                function(response) {
                    var message = 'Nie udało się usunąć kraju - błąd serwera';
                    if (response.data === 'Country has assignments') {
                        message = 'Nie można usunąć kraju, który już został przypisany do definicji SMS';
                    }
                    Notify.alert(
                        message, { status: 'danger' }
                    );
                }
            );
        }

        var findAndUpdateCountry = function(id, newObject) {
            for (var i = 0; i < vm.countries.length; i++) {
                if (vm.countries[i].id === id) {
                    vm.countries[i] = newObject;
                    return;
                }
            }
        }

        var findAndRemoveCountry = function(id) {
            for (var i = 0; i < vm.countries.length; i++) {
                if (vm.countries[i].id === id) {
                    vm.countries.splice(i, 1);
                    return;
                }
            }
        }

        vm.showDeleteConfirmDialog = function(Country, ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Chcesz usunąć kraj?')
                .content('Usuwany kraj nie może być powiązany z żadną definicją SMS!')
                .ariaLabel('Usuwanie kraju')
                .targetEvent(ev)
                .ok('Tak, usuń kraj')
                .cancel('Anuluj operację');
            $mdDialog.show(confirm).then(function() {
                deleteCountryRequest(Country.id)
            });
        };

        vm.showAddFormDialog = function(ev) {
            $mdDialog.show({
                    controller: 'NewCountryDialogController',
                    controllerAs: 'newCountryDialogController',
                    templateUrl: 'adminPanelApp/views/dialogs/countries/addCountryDialog.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true
                })
                .then(function(data) {
                    addNewCountryRequest(data);
                });
        };

        vm.showEditFormDialog = function(Country, ev) {
            $mdDialog.show({
                    controller: 'EditCountryDialogController',
                    controllerAs: 'editCountryDialogController',
                    bindToController: true,
                    templateUrl: 'adminPanelApp/views/dialogs/countries/editCountryDialog.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        Country: Country
                    }
                })
                .then(function(data) {
                    editCountryRequest(Country.id, data);
                });
        };


        /**
         * Initialize method
         */
        var initialize = function() {
            getCountriesListRequest();
        }.bind(this);

        initialize();
    }
})();
