(function() {
    'use strict';

    angular
        .module('adminPanel.directives')
        .controller('EditCountryDialogController', EditCountryDialogController);

    EditCountryDialogController.$inject = ['$log', '$scope', '$mdDialog'];

    function EditCountryDialogController($log, $scope, $mdDialog) {
        var vm = this;

        vm.data = {
            name: vm.Country.name,
            code: vm.Country.code
        };

        vm.hide = function() {
            $mdDialog.hide();
        };
        vm.cancel = function() {
            $mdDialog.cancel();
        };
        vm.edit = function() {
            if ($scope.editCountryForm.$invalid) {
                $log.debug('Formularz jest nieprawidłowy');
                return;
            }
            $mdDialog.hide(vm.data);
        };

    }
})();
