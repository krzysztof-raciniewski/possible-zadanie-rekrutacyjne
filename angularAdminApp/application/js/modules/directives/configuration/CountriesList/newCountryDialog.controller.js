(function() {
    'use strict';

    angular
        .module('adminPanel.directives')
        .controller('NewCountryDialogController', NewCountryDialogController);

    NewCountryDialogController.$inject = ['$log', '$scope', '$mdDialog'];

    function NewCountryDialogController($log, $scope, $mdDialog) {
        var vm = this;

        vm.data = {
            name: '',
            code: ''
        };

        vm.hide = function() {
            $mdDialog.hide();
        };
        vm.cancel = function() {
            $mdDialog.cancel();
        };
        vm.add = function() {
            if ($scope.newCountryForm.$invalid) {
                $log.debug('Formularz jest nieprawidłowy');
                return;
            }
            $mdDialog.hide(vm.data);
        };

    }
})();
