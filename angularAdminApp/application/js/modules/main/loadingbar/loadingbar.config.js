(function() {
    'use strict';

    angular
        .module('adminPanel.loadingbar')
        .config(loadingbarConfig);
    loadingbarConfig.$inject = ['cfpLoadingBarProvider'];
    function loadingbarConfig(cfpLoadingBarProvider){
      cfpLoadingBarProvider.includeBar = true;
      cfpLoadingBarProvider.includeSpinner = false;
      cfpLoadingBarProvider.latencyThreshold = 1000;
      cfpLoadingBarProvider.parentSelector = '.wrapper > section';
    }
})();