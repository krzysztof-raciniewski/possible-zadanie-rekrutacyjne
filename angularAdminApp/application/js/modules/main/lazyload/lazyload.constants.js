(function() {
    'use strict';

    angular
        .module('app.lazyload')
        .constant('APP_REQUIRES', {
            // jQuery based and standalone scripts
            scripts: {
                'modernizr': ['adminPanelApp/vendor/modernizr/modernizr.custom.js'],
                'icons': [
                    'adminPanelApp/vendor/fontawesome/css/font-awesome.min.css',
                    'adminPanelApp/vendor/simple-line-icons/css/simple-line-icons.css'
                ],
                'weather-icons': [
                    'adminPanelApp/vendor/weather-icons/css/weather-icons.min.css',
                    'adminPanelApp/vendor/weather-icons/css/weather-icons-wind.min.css'
                ],
                'loadGoogleMapsJS': [
                    'adminPanelApp/vendor/load-google-maps/load-google-maps.js'
                ],
                'flot-chart': [
                    'adminPanelApp/vendor/flot/jquery.flot.js'
                ],
                'flot-chart-plugins': [
                    'adminPanelApp/vendor/flot.tooltip/js/jquery.flot.tooltip.min.js',
                    'adminPanelApp/vendor/flot/jquery.flot.resize.js',
                    'adminPanelApp/vendor/flot/jquery.flot.pie.js',
                    'adminPanelApp/vendor/flot/jquery.flot.time.js',
                    'adminPanelApp/vendor/flot/jquery.flot.categories.js',
                    'adminPanelApp/vendor/flot-spline/js/jquery.flot.spline.min.js'
                ],
                'vector-map': [
                    'adminPanelApp/vendor/ika.jvectormap/jquery-jvectormap-1.2.2.min.js',
                    'adminPanelApp/vendor/ika.jvectormap/jquery-jvectormap-1.2.2.css'
                ],
                'vector-map-maps': [
                    'adminPanelApp/vendor/ika.jvectormap/jquery-jvectormap-world-mill-en.js',
                    'adminPanelApp/vendor/ika.jvectormap/jquery-jvectormap-us-mill-en.js'
                ],
                'sparklines': ['adminPanelApp/vendor/sparkline/index.js'],
                'angularjs-slider': [
                    'adminPanelApp/vendor/angularjs-slider/dist/rzslider.min.css',
                    'adminPanelApp/vendor/angularjs-slider/dist/rzslider.min.js'
                ],


            },
            // Angular based script (use the right module name)
            modules: [
                // {name: 'toaster', files: ['vendor/angularjs-toaster/toaster.js', 'vendor/angularjs-toaster/toaster.css']}
                { name: 'ui.map', files: ['adminPanelApp/vendor/angular-ui-map/ui-map.js'] },

                {
                    name: 'datatables',
                    files: [
                        'adminPanelApp/vendor/datatables.net-dt/css/jquery.dataTables.min.css',
                        'adminPanelApp/vendor/datatables.net/js/jquery.dataTables.min.js',
                        'adminPanelApp/vendor/angular-datatables/dist/angular-datatables.js'
                    ],
                    serie: true
                },
            ]
        });

})();
