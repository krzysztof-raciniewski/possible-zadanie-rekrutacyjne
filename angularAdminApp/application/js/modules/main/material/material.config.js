
(function() {
    'use strict';
    // Used only for the BottomSheetExample
    angular
        .module('adminPanel.material')
        .config(materialConfig)
        ;
    materialConfig.$inject = ['$mdIconProvider'];
    function materialConfig($mdIconProvider){
      $mdIconProvider
        .icon('share-arrow', 'adminPanelApp/img/icons/share-arrow.svg', 24)
        .icon('upload', 'adminPanelApp/img/icons/upload.svg', 24)
        .icon('copy', 'adminPanelApp/img/icons/copy.svg', 24)
        .icon('print', 'adminPanelApp/img/icons/print.svg', 24)
        .icon('hangout', 'adminPanelApp/img/icons/hangout.svg', 24)
        .icon('mail', 'adminPanelApp/img/icons/mail.svg', 24)
        .icon('message', 'adminPanelApp/img/icons/message.svg', 24)
        .icon('copy2', 'adminPanelApp/img/icons/copy2.svg', 24)
        .icon('facebook', 'adminPanelApp/img/icons/facebook.svg', 24)
        .icon('twitter', 'adminPanelApp/img/icons/twitter.svg', 24);
    }
})();
