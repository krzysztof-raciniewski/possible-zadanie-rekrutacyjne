(function() {
    'use strict';
    // Used only for the BottomSheetExample
    angular
        .module('adminPanel.material')
        .run(materialRun)
        ;
    materialRun.$inject = ['$http', '$templateCache'];
    function materialRun($http, $templateCache){
      var urls = [
        'adminPanelApp/img/icons/share-arrow.svg',
        'adminPanelApp/img/icons/upload.svg',
        'adminPanelApp/img/icons/copy.svg',
        'adminPanelApp/img/icons/print.svg',
        'adminPanelApp/img/icons/hangout.svg',
        'adminPanelApp/img/icons/mail.svg',
        'adminPanelApp/img/icons/message.svg',
        'adminPanelApp/img/icons/copy2.svg',
        'adminPanelApp/img/icons/facebook.svg',
        'adminPanelApp/img/icons/twitter.svg'
      ];

      angular.forEach(urls, function(url) {
        $http.get(url, {cache: $templateCache});
      });

    }

})();
