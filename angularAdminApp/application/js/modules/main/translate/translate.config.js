(function () {
    'use strict';

    angular.module('adminPanel.translate').config(translateConfig);
    translateConfig.$inject = ['$translateProvider'];
    function translateConfig($translateProvider) {

        $translateProvider.useStaticFilesLoader({
            prefix: 'adminPanelApp/i18n/',
            suffix: '.json'
        });

        $translateProvider.preferredLanguage('pl');
        $translateProvider.useLocalStorage();
        $translateProvider.usePostCompiling(true);
        $translateProvider.useSanitizeValueStrategy('sanitizeParameters');

    }
})();