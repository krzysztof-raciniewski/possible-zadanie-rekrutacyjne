(function() {
    'use strict';

    angular
        .module('adminPanel.sidebar')
        .controller('UserBlockController', UserBlockController);

    UserBlockController.$inject = ['$rootScope', '$scope', '$log', 'UserModel'];

    function UserBlockController($rootScope, $scope, $log, UserModel) {

        activate();

        ////////////////

        function activate() {
            var self = this;
            var defaultImage = 'adminPanelApp/img/user/default.jpg';

            UserModel.getCurrentUser().then(
                function(result) {
                    $log.info('Informacje o użytkowniku zostały pobrane', result);

                    if (result.image != '') {
                        image = result.image;
                    }

                    $rootScope.user = {
                        name: result.first_name + ' ' + result.last_name,
                        email: result.email,
                        picture: defaultImage
                    };
                },
                function(result) {
                    $log.debug('Nie mogę pobrać informacji o użytkowniku', result);
                }
            );

            $rootScope.$on('userDataChangedEvent', function(event, result) {
                $log.debug(result);
                $rootScope.user = {
                    name: result.data.first_name + ' ' + result.data.last_name,
                    email: result.data.email,
                    picture: defaultImage
                };
            });

            // Hides/show user avatar on sidebar
            $rootScope.toggleUserBlock = function() {
                $rootScope.$broadcast('toggleUserBlock');
            };

            // Block visibility
            $rootScope.userBlockVisible = false;

            // Detach action
            var detach = $rootScope.$on('toggleUserBlock', function( /*event, args*/ ) {
                $rootScope.userBlockVisible = !$rootScope.userBlockVisible;
            });

            $scope.$on('$destroy', detach);
        }
    }
})();
