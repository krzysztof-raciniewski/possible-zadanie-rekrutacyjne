(function() {
    'use strict';

    angular.module('adminPanel.auth').controller('AuthController', authController);

    authController.$inject = ['$auth', '$state'];
    function authController($auth, $state) {
        var vm = this;
        vm.logout = function() {
            $auth.logout();
            $state.go('page.login');
        };
    }
})();