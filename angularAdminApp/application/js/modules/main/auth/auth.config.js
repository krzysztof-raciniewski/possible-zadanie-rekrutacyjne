/**=========================================================
 * Module: config.js
 * App routes and resources configuration
 =========================================================*/


(function () {
    'use strict';

    angular
        .module('adminPanel.auth')
        .config(satellizerConfiguration)
        .run(function($rootScope, $log, $state, $auth) {
            $rootScope.$on("$stateChangeStart", function(event, toState){
                if (!$auth.isAuthenticated() && toState.authenticate ) {
                    event.preventDefault();

                    // User isn’t authenticated
                    $state.transitionTo("page.login");
                    $log.debug(toState.name);
                    $log.debug('Przekierowanie użytkownika do strony logowania');

                    return;
                }

                if( $auth.isAuthenticated() && toState.name == 'page.login' ) {
                    event.preventDefault();
                    $state.transitionTo("page.dashboard");
                }
            });
        });

    satellizerConfiguration.$inject = ['$authProvider'];
    function satellizerConfiguration($authProvider) {

        $authProvider.httpInterceptor = function() {
            return true;
        };

        $authProvider.storageType = 'localStorage';
        $authProvider.tokenPrefix = 'coins4sms';
        $authProvider.tokenType = 'Bearer';
        $authProvider.tokenName = 'token';
        $authProvider.loginUrl = '/api/auth/login';
        // $authProvider.signupUrl = '/api/auth/register';
        $authProvider.tokenRoot = 'data';//compensates success response macro

    }
})();

