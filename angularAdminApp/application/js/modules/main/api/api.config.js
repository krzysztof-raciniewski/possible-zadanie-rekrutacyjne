/**=========================================================
 * Module: config.js
 * App routes and resources configuration
 =========================================================*/


(function () {
    'use strict';

    angular
        .module('adminPanel.api')
        .config(function ($windowProvider, RestangularProvider) {
            //content negotiation
            var headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/x.laravel.v1+json'
            };

            RestangularProvider
                .setBaseUrl('/api/')
                .setDefaultHeaders(headers)
                // .setDefaultHttpFields({cache: false})
                .setErrorInterceptor(function (response) {
                    if (response.status === 422 || response.status === 401) {
                        for (let error in response.data.errors) {
                            console.log('Błąd walidowania formualrza po stronie serwera');
                            console.log(response.data.errors);
                        }
                    }
                    if (response.status === 500) {
                        console.log(response.statusText);
                    }
                })
                .addFullRequestInterceptor(function (element, operation, what, url, headers) {
                    var window_obj = $windowProvider.$get();
                    let token = window_obj.localStorage.coins4sms_token;
                    if (token) {
                        headers.Authorization = 'Bearer ' + token;
                    }
                });
        });
})();

