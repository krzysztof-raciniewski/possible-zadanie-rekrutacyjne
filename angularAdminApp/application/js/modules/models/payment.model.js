(function() {
    'use strict';

    angular.module('adminPanel.models')
        .service('PaymentModel', PaymentModelImplementation);

    PaymentModelImplementation.$inject = ["$log", "Restangular"];

    function PaymentModelImplementation($log, Restangular) {
        var PaymentModel = Restangular.service('payments');

        PaymentModel.getById = function(id) {
            return PaymentModel.one(id).get();
        }

        PaymentModel.getAllCount = function() {
            return PaymentModel.one('all').one('count').get();
        }

        PaymentModel.getUnsuccesfulCount = function() {
            return PaymentModel.one('unsuccessful').one('count').get();
        }

        PaymentModel.getLastPayments = function() {
            return PaymentModel.one('last').one('10').get();
        }

        PaymentModel.getLastCompletedPayments = function() {
            return PaymentModel.one('last').one('completed').one('10').get();
        }


        Restangular.extendModel('payments', function(model) {
            return model;
        });

        return PaymentModel;
    }
})();
