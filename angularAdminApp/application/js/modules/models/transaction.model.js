(function() {
    'use strict';

    angular.module('adminPanel.models')
        .service('TransactionModel', TransactionModelImplementation);

    TransactionModelImplementation.$inject = ["$log", "Restangular"];

    function TransactionModelImplementation($log, Restangular) {
        var TransactionModel = Restangular.service('transactions');

        TransactionModel.getAll = function() {
            return TransactionModel.getList();
        };

        TransactionModel.getById = function(id) {
            return TransactionModel.one(id).get();
        }

        TransactionModel.getAllCount = function() {
            return TransactionModel.one('all').one('count').get();
        }

        TransactionModel.getNewCount = function() {
            return TransactionModel.one('new').one('count').get();
        }

        TransactionModel.getLastTransactions = function() {
            return TransactionModel.one('last').one('10').get();
        }

        TransactionModel.getLastCompletedTransactions = function() {
            return TransactionModel.one('last').one('completed').one('10').get();
        }

        Restangular.extendModel('transactions', function(model) {
            return model;
        });

        return TransactionModel;
    }
})();
