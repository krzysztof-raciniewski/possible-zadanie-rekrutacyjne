(function() {
    'use strict';

    angular.module('adminPanel.models')
        .service('CountryModel', CountryModel);

    CountryModel.$inject = ['$log', 'Restangular'];

    function CountryModel($log, Restangular) {
        var CountryModel = Restangular.service('countries');

        /**
         * Get all providers
         * @returns {*|{method, params, headers}}
         */
        CountryModel.getAll = function() {
            return CountryModel.getList();
        };

        CountryModel.addNew = function(data) {
            return CountryModel.post(data);
        };

        CountryModel.edit = function(operatorId, data) {
            return CountryModel.one(operatorId).put(data);
        };

        CountryModel.delete = function(operatorId) {
            return CountryModel.one(operatorId).remove();
        };

        /**
         * Model methods
         */
        Restangular.extendModel('countries', function(model) {
            return model;
        });

        return CountryModel;
    }
})();
