(function() {
    'use strict';

    angular.module('adminPanel.models')
        .service('ConfigurationModel', ConfigurationModelImplementation);

    ConfigurationModelImplementation.$inject = ["$log", "Restangular"];

    function ConfigurationModelImplementation($log, Restangular) {
        var ConfigurationModel = Restangular.service('configuration');

        /**
         * Get all amounts from BitBay
         * @returns {*|{method, params, headers}}
         */
        ConfigurationModel.getBitBayConfiguration = function() {
            return ConfigurationModel.one('bitbay').get();
        };

        ConfigurationModel.setBitBayConfiguration = function(data) {
            return ConfigurationModel.one('bitbay').put({
                'key': data.key,
                'secret': data.secret
            });
        };

        ConfigurationModel.getBitBayLimits = function() {
            return ConfigurationModel.one('bitbay').one('limits').get();
        };

        ConfigurationModel.getBitBayFees = function() {
            return ConfigurationModel.one('bitbay').one('fees').get();
        };

        ConfigurationModel.setBitBayLimits = function(data) {
            return ConfigurationModel.one('bitbay').one('limits').put({
                'btcMinPayment': data.btc_min_payment,
                'ltcMinPayment': data.ltc_min_payment,
                'lskMinPayment': data.lsk_min_payment,
                'ethMinPayment': data.eth_min_payment,
            });
        };

        ConfigurationModel.setBitBayFees = function(data) {
            return ConfigurationModel.one('bitbay').one('fees').put({
                'btcPaymentFee': data.btc_payment_fee,
                'ltcPaymentFee': data.ltc_payment_fee,
                'lskPaymentFee': data.lsk_payment_fee,
                'ethPaymentFee': data.eth_payment_fee,
            });
        };
        /**
         * Model methods
         */
        Restangular.extendModel('configuration', function(model) {
            return model;
        });

        return ConfigurationModel;
    }
})();
