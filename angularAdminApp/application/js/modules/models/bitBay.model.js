(function () {
    'use strict';

    angular.module('adminPanel.models')
        .service('BitbayModel', BitBayModelImplementation);

    BitBayModelImplementation.$inject = ["$log", "Restangular"];
    function BitBayModelImplementation($log, Restangular) {
        var BitbayModel = Restangular.service('bitbay');

        /**
         * Get all amounts from BitBay
         * @returns {*|{method, params, headers}}
         */
        BitbayModel.getAmount = function () {
            return BitbayModel.one('amount').get();
        };

        /**
         * Get amount for BTC
         */
        BitbayModel.getBtcAmount = function () {
            return BitbayModel.one('amount').one('BTC').get();
        };

        /**
         * Get amount for LSK
         */
        BitbayModel.getLskAmount = function () {
            return BitbayModel.one('amount').one('LSK').get();
        };

        /**
         * Get amount for ETH
         */
        BitbayModel.getEthAmount = function () {
            return BitbayModel.one('amount').one('ETH').get();
        };

        /**
         * Get amount for LTC
         */
        BitbayModel.getLtcAmount = function () {
            return BitbayModel.one('amount').one('LTC').get();
        };

        BitbayModel.getHistory = function (currency, limit) {
            return BitbayModel.one('history').one(currency).one(limit).getList();
        };

        BitbayModel.getOrders = function () {
            return BitbayModel.one('orders').getList();
        };

        /**
         * Get all transactions from market
         * @returns {*|{method, params, headers}}
         */
        BitbayModel.getMarketTransactions = function () {
            return BitbayModel.one('transactions').getList();
        };

        /**
         * Model methods
         */
        Restangular.extendModel('bitbay', function (model) {
            return model;
        });

        return BitbayModel;
    }
})();