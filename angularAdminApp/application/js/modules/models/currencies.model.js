(function() {
    'use strict';

    angular.module('adminPanel.models')
        .service('CurrencyModel', CurrencyModel);

    CurrencyModel.$inject = ['$log', 'Restangular'];

    function CurrencyModel($log, Restangular) {
        var CurrencyModel = Restangular.service('currencies');

        /**
         * Get all providers
         * @returns {*|{method, params, headers}}
         */
        CurrencyModel.getAll = function() {
            return CurrencyModel.getList();
        };

        CurrencyModel.addNew = function(data) {
            return CurrencyModel.post(data);
        };

        CurrencyModel.edit = function(operatorId, data) {
            return CurrencyModel.one(operatorId).put(data);
        };

        CurrencyModel.delete = function(operatorId) {
            return CurrencyModel.one(operatorId).remove();
        };

        /**
         * Model methods
         */
        Restangular.extendModel('currencies', function(model) {
            return model;
        });

        return CurrencyModel;
    }
})();
