(function () {
    'use strict';

    angular.module('adminPanel.models')
        .service('UserModel', UserModelImplementation);

    UserModelImplementation.$inject = ["Restangular"];
    function UserModelImplementation(Restangular) {
        var UserModel = Restangular.service('user');

        /**
         * Get current user information
         * @returns {*|{method, params, headers}}
         */
        UserModel.getCurrentUser = function() {
            return UserModel.one('current').get();
        };

        UserModel.getAll = function() {
            return UserModel.one('list').getList();
        }

        UserModel.changeCurrentUserPassword = function(data) {
            return UserModel.one('current').one('changepassword').put({
                old_password: data.old_password,
                password: data.password,
                repeated_password: data.repeated_password
            });
        }

        UserModel.changeCurrentUserData = function(data) {
            return UserModel.one('current').put({
                first_name: data.first_name,
                last_name: data.last_name
            });
        }

        /**
         * Model methods
         */
        Restangular.extendModel('user', function (model) {
            return model;
        });

        return UserModel;
    }
})();