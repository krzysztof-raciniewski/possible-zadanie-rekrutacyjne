(function() {
    'use strict';

    angular.module('adminPanel.models')
        .service('SettingsModel', SettingsModelImplementation);

    SettingsModelImplementation.$inject = ["$log", "Restangular"];

    function SettingsModelImplementation($log, Restangular) {
        var SettingsModel = Restangular.service('settings');

        SettingsModel.getEmails = function() {
            return SettingsModel.one('emails').get();
        };

        SettingsModel.setEmails = function(data) {
            return SettingsModel.one('emails').put({
                'notifications_email': data.notifications_email,
                'contact_email': data.contact_email
            });
        };

        SettingsModel.getWallet = function() {
            return SettingsModel.one('wallet').get();
        };

        SettingsModel.setWallet = function(data) {
            return SettingsModel.one('wallet').put({
                btcWalletFullValue: data.btcWalletFullValue,
                lskWalletFullValue: data.lskWalletFullValue,
                ltcWalletFullValue: data.ltcWalletFullValue,
                ethWalletFullValue: data.ethWalletFullValue,
                btcWalletAlertValue: data.btcWalletAlertValue,
                lskWalletAlertValue: data.lskWalletAlertValue,
                ltcWalletAlertValue: data.ltcWalletAlertValue,
                ethWalletAlertValue: data.ethWalletAlertValue,
            });
        };

        SettingsModel.getCommission = function() {
            return SettingsModel.one('commission').get();
        };

        SettingsModel.setCommission = function(data) {
            return SettingsModel.one('commission').put({
                'sale_commission': data.sale_commission
            });
        };

        SettingsModel.getPaymentQueueStatus = function() {
            return SettingsModel.one('paymentqueue').one('status').get();
        }

        SettingsModel.setPaymentQueueStatus = function(status) {
            return SettingsModel.one('paymentqueue').one('status').put({
                status: status
            });
        }

        SettingsModel.getWalletAllertsStatus = function() {
            return SettingsModel.one('walletallerts').one('status').get();
        }

        SettingsModel.setWalletAllertsStatus = function(status) {
            return SettingsModel.one('walletallerts').one('status').put({
                status: status
            });
        }

        Restangular.extendModel('configuration', function(model) {
            return model;
        });

        return SettingsModel;
    }
})();
