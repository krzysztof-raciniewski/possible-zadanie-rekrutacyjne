(function () {
    'use strict';

    angular.module('adminPanel.models')
        .service('ProviderModel', ProviderModelImplementation);

    ProviderModelImplementation.$inject = ['$log', 'Restangular'];
    function ProviderModelImplementation($log, Restangular) {
        var ProviderModel = Restangular.service('providers');

        /**
         * Get all providers
         * @returns {*|{method, params, headers}}
         */
        ProviderModel.getAll = function () {
            return ProviderModel.getList();
        };

        /**
         * Get provider by id method
         */
        ProviderModel.getById = function (id) {
            return ProviderModel.one(id).get();
        };

        /**
         * Update provider
         */
        ProviderModel.updateMainInformation = function(id, provider) {
            return ProviderModel.one(id).customPUT(provider);
        };

        /**
         * Get configuration for provider
         */
        ProviderModel.getConfiguration = function (id) {
            return ProviderModel.one(id).one('configuration').get();
        };

        /**
         * Update provider configuration
         */
        ProviderModel.updateConfiguration = function(id, configuration) {
            return ProviderModel.one(id).one('configuration').customPUT(
                {
                    'login': configuration.login,
                    'password': configuration.password,
                }
            );
        };


        /**
         * Get sms list for provider
         */
        ProviderModel.getSmsList = function (id) {
            return ProviderModel.one(id).one('sms').get();
        };

        /**
         * Remove sms from provider configuration
         */
        ProviderModel.removeSms = function(providerId, smsId) {
            return Restangular.one('providers',providerId).one('sms', smsId).remove();
        };

        /**
         * Add sms to provider model
         */
        ProviderModel.addSms = function(providerId, data) {
            return ProviderModel.one(providerId).all('sms').post(data);
        }

        /**
         * Edit sms in provider model
         */
        ProviderModel.editSms = function(providerId, smsId, data) {
            return ProviderModel.one(providerId).one('sms', smsId).put(data);
        }


        ProviderModel.getSms = function(providerId, smsId) {
            return ProviderModel.one(providerId).one('sms', smsId).get();
        }


        ProviderModel.changeSmsStatus = function(providerId, smsId) {
            return ProviderModel.one(providerId).one('sms', smsId).one('changestatus').put();
        }

        ProviderModel.changeProviderStatus = function(providerId) {
            return ProviderModel.one(providerId).one('changestatus').put();
        }

        /**
         * Model methods
         */
        Restangular.extendModel('provider', function (model) {
            return model;
        });

        return ProviderModel;
    }
})();