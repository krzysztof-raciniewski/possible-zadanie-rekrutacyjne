(function () {
    'use strict';

    angular.module('adminPanel.models')
        .service('OperatorModel', OperatorModel);

    OperatorModel.$inject = ['$log', 'Restangular'];
    function OperatorModel($log, Restangular) {
        var OperatorModel = Restangular.service('operators');

        /**
         * Get all providers
         * @returns {*|{method, params, headers}}
         */
        OperatorModel.getAll = function () {
            return OperatorModel.getList();
        };

        OperatorModel.addNew = function (name) {
            return OperatorModel.post({name: name});
        };

        OperatorModel.edit = function (operatorId, name) {
            return OperatorModel.one(operatorId).put({name: name});
        };

        OperatorModel.delete = function (operatorId) {
            return OperatorModel.one(operatorId).remove();
        };

        /**
         * Model methods
         */
        Restangular.extendModel('operators', function (model) {
            return model;
        });

        return OperatorModel;
    }
})();