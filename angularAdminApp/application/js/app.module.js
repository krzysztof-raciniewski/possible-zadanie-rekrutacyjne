/*!
 * 
 * Angle - Bootstrap Admin App + AngularJS Material
 * 
 * Version: 3.2.0
 * Author: @themicon_co
 * Website: http://themicon.co
 * License: https://wrapbootstrap.com/help/licenses
 * 
 */

// APP START
// ----------------------------------- 

(function () {
    'use strict';

    angular
        .module('adminPanel', [
            'adminPanel.core',
            'adminPanel.routes',
            'adminPanel.sidebar',
            'adminPanel.navsearch',
            'adminPanel.preloader',
            'adminPanel.loadingbar',
            'adminPanel.translate',
            'adminPanel.settings',
            'adminPanel.maps',
            'adminPanel.utils',
            'adminPanel.material',
            'adminPanel.auth',
            'adminPanel.api',
            'adminPanel.models',
            'adminPanel.forms',
            'adminPanel.filters',
            'adminPanel.directives',
            //
            // Notifications
            //
            'adminPanel.notify',
            //
            // Pages
            //
            'adminPanel.pages'
        ]);
})();

