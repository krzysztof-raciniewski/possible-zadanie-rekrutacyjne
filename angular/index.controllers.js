import { FirstStepController } from "./app/components/multi-steps-form/controllers/firstStep.controller";
import { MultiStepsFormController } from "./app/components/multi-steps-form/controllers/multiStepsForm.controller";

angular.module('app.controllers')
    .controller('MultiStepsFormController', MultiStepsFormController)
    .controller('FirstStepController', FirstStepController);
