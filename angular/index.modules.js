angular.module('app', [
    'app.run',
    'app.models',
    'app.controllers',
    'app.filters',
    'app.services',
    'app.components',
    'app.directives',
    'app.routes',
    'app.config',
    'app.partials'
]);

angular.module('app.run', []);
angular.module('app.routes', []);
angular.module('app.filters', []);
angular.module('app.services', []);
angular.module('app.config', []);
angular.module('app.directives', []);
angular.module('app.models', []);
angular.module('app.controllers', []);
angular.module('app.components', [
    'ui.router', 'ngMaterial', 'angular-loading-bar',
    'restangular', 'ngStorage', 'uiGmapgoogle-maps',
    'multiStepForm', 'vcRecaptcha', 'ngMessages', 'pascalprecht.translate',
    'environment'
]);
