import { CountryModel } from "./models/country.model";
import { CryptocurrencyModel } from "./models/cryptocurrency.model";
import { SmsModel } from "./models/sms.model";
import { TransactionModel } from "./models/transaction.model";
import { ContactModel } from "./models/contact.model";

angular.module('app.models')
    .service('CountryModel', CountryModel)
    .service('CryptocurrencyModel', CryptocurrencyModel)
    .service('SmsModel', SmsModel)
    .service('ContactModel', ContactModel)
    .service('TransactionModel', TransactionModel);
