class AboutUsGoogleMapController {
    constructor($document, uiGmapIsReady) {
        'ngInject';
        this.uiGmapIsReady = uiGmapIsReady;
        this.$document = $document;
        this.initializeMap();
    }

    initializeMap() {
        var styles = [
            { "featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{ "color": "#444444" }] },
            { "featureType": "landscape", "elementType": "all", "stylers": [{ "color": "#f2f2f2" }] },
            { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }] }, {
                "featureType": "road",
                "elementType": "all",
                "stylers": [{ "saturation": -100 },
                    { "lightness": 45 }
                ]
            }, { "featureType": "road.highway", "elementType": "all", "stylers": [{ "visibility": "simplified" }] },
            { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] },
            { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "off" }] }, {
                "featureType": "water",
                "elementType": "all",
                "stylers": [{ "color": "#46bcec" },
                    { "visibility": "on" }
                ]
            }
        ];
        var coords = {
            latitude: 52.238622,
            longitude: 21.046726
        };

        this.map = {
            center: coords,
            zoom: 14,
            bounds: {},
            markers: [{
                id: 1,
                coords: coords,
                options: {
                    labelClass: 'marker_labels',
                    labelContent: "Bitmobile sp. z.o.o.",
                    icon: '/img/pin.png',
                    animation: google.maps.Animation.DROP
                }
            }],
            dragging: false,
            events: {},
            options: {
                styles: styles
            }
        };
    }
}

export const
    AboutUsGoogleMapComponent = {
        templateUrl: './views/app/components/about-us-google-map/about-us-google-map.component.html',
        controller: AboutUsGoogleMapController,
        controllerAs: 'vm',
        bindings: {}
    };
