class MultiStepsFormController {
    constructor($log, $scope, $mdDialog, vcRecaptchaService, CountryModel, CryptocurrencyModel, TransactionModel, SmsModel, $translate) {
        'ngInject';
        this.$log = $log;
        this.vcRecaptchaService = vcRecaptchaService;
        this.reCaptchaKey = '6LdNJwcUAAAAAAoA0xWh6lBWgltFDcJi4DM__xhK';
        this.reCaptchaTheme = 'light'; // dark or light
        this.reCaptchaSize = 'normal'; // normal or compact
        this.reCaptchaType = 'image'; // audio or image
        this.response = null;
        this.widgetId = null;
        this.data = {
            country: null,
            operator: null,
            cryptocurrency: null,
            phone: null,
            sms_price: null,
            wallet: null,
            email: null,
            sms: null,
        };

        this.CountryModel = CountryModel;
        this.language = $translate.use();

        this.countries = [];
        this.cryptocurrencies = [];
        this.operators = [];
        this.sms = [];
        this.paymentInfo = null;

        this.transaction = null;
        this.smsCode = null;
        this.smsInfoProcessing = true;
        this.transactionSuccesed = null;
        this.smsInfoHasError = false;
        this.smsInfoError = {};


        this.steps = [{
            templateUrl: './views/app/components/multi-steps-form/steps/step1.html',
            title: 'MULTI_STEPS_FORM_START_NEW_TRANSACTION',
            hasForm: true
        }, {
            templateUrl: './views/app/components/multi-steps-form/steps/step2.html',
            title: 'MULTI_STEPS_FORM_MAKE_A_PAYMENT',
            hasForm: true
        }, {
            templateUrl: './views/app/components/multi-steps-form/steps/step3.html',
            title: 'MULTI_STEPS_FORM_COMPLETE_THE_TRANSACTION',
            hasForm: true
        }];

        // Get all available countries
        this.loadCountries = () => {
            CountryModel.getAll().then(
                (response) => {
                    this.$log.debug('Kraje: ', response);
                    this.countries = response;
                }
            );
        };

        // Get all available operators
        this.loadOperators = () => {
            CountryModel.getOperators(this.data.country).then(
                (response) => {
                    this.$log.debug('Operatorzy: ', response);
                    this.operators = response;
                }
            );
        };

        // Get all available cryptocurrencies
        this.loadCryptocurrencies = () => {
            CryptocurrencyModel.getAll().then(
                (response) => {
                    this.$log.debug('Operatorzy: ', response);
                    this.cryptocurrencies = response;
                }
            );
        };

        // Get all available cryptocurrencies
        this.loadTextMessages = () => {
            SmsModel.getAllForTransaction(this.transaction.id).then(
                (response) => {
                    this.$log.debug('Wiadomości sms: ', response);
                    this.sms = response;
                }
            );
        };

        this.setResponse = function(response) {
            this.response = response;
        };

        this.setWidgetId = function(widgetId) {
            this.widgetId = widgetId;
        };

        this.cbExpiration = function() {
            this.vcRecaptchaService.reload($scope.widgetId);
            this.response = null;
        };

        this.onStepChange = () => {
            if (this.data.country && this.data.operator && this.data.cryptocurrency && !this.transaction) {
                TransactionModel.createNew(this.data).then(
                    (response) => {
                        this.transaction = response;
                        $log.debug('Utworzylem nową transakcję: ', response);
                    },
                    (response) => {
                        $log.debug('Nie udało się utworzyć nowej transakcji: ', response);
                    }
                );
            } else {
                if (this.transaction) {
                    TransactionModel.update(this.transaction.id, this.data).then(
                        (response) => {
                            $log.debug('Transakcja została zaktualizowana: ', response);
                            this.transaction = response;
                        },
                        (response) => {
                            $log.debug('Transakcja nie została zaktualizowana: ', response);
                        }
                    );
                }
            }

            $log.debug("Dane: ", this.data);
        };

        this.checkCode = (ev) => {
            TransactionModel.checkCode(this.transaction.id, this.smsCode).then(
                (response) => {

                    if (response === 'BadCode') {


                        $translate(['MULTI_STEPS_FORM_CODE_IS_INCORRECT', 'MULTI_STEPS_FORM_TRY_ENTERING_THE_CODE_AGAIN', 'MULTI_STEPS_FORM_ENTER_THE_CODE_AGAIN_MESSAGE']).then(
                            (translations) => {
                                $log.debug('Kod jest zły');
                                this.codeIsValid = false;

                                $mdDialog.show(
                                    $mdDialog.alert()
                                    .parent(angular.element(document.querySelector('body')))
                                    .clickOutsideToClose(true)
                                    .title(translations.MULTI_STEPS_FORM_CODE_IS_INCORRECT)
                                    .textContent(translations.MULTI_STEPS_FORM_ENTER_THE_CODE_AGAIN_MESSAGE)
                                    .ariaLabel('Bad code')
                                    .ok(translations.MULTI_STEPS_FORM_TRY_ENTERING_THE_CODE_AGAIN)
                                    .targetEvent(ev)
                                );
                            });
                    }

                    if (response === 'CodeIsValid') {
                        $log.debug('Kod jest OK!');
                        this.transactionSuccesed = true;
                        this.codeIsValid = true;
                    }
                }
            );
        };

        this.getPaymentInfo = function() {
            this.smsInfoProcessing = true;
            if (this.transaction && this.data.sms) {
                TransactionModel.update(this.transaction.id, this.data).then(
                    (response) => {
                        $log.debug('Transakcja została zaktualizowana: ', response);
                        this.transaction = response;

                        TransactionModel.getInfo(this.transaction.id, this.data.sms).then(
                            (response) => {

                                if (response.hasOwnProperty('error')) {
                                    this.smsInfoHasError = true;
                                    this.smsInfoError = response;
                                } else {
                                    this.smsInfoHasError = false;
                                    this.smsInfoError = {};
                                }

                                this.paymentInfo = response;
                                $log.debug('Pobrane informacje: ', response);
                                this.smsInfoProcessing = false;
                            },
                            (error) => {
                                $log.debug('Error: ', error);
                            }
                        );
                    },
                    (response) => {
                        $log.debug('Transakcja nie została zaktualizowana: ', response);
                    }
                );
            }
        };

        $scope.$watch(() => {
            return this.data.sms;
        }, () => {
            this.getPaymentInfo();
        });

        $scope.$watch(() => {
            return this.data.cryptocurrency;
        }, () => {
            this.getPaymentInfo();
        });

    }
}

export const MultiStepsFormComponent = {
    templateUrl: './views/app/components/multi-steps-form/multi-steps-form.component.html',
    controller: MultiStepsFormController,
    controllerAs: 'multiStepsFormController',
    bindings: {}
};
