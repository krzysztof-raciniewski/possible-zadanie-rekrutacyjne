class ContactFormController {
    constructor($log, $scope, ToastService, vcRecaptchaService, ContactModel) {
        'ngInject';
        this.ToastService = ToastService;
        this.$scope = $scope;
        this.$log = $log;
        this.ContactModel = ContactModel;

        this.vcRecaptchaService = vcRecaptchaService;
        this.reCaptchaKey = '6LdNJwcUAAAAAAoA0xWh6lBWgltFDcJi4DM__xhK';
        this.reCaptchaTheme = 'light'; // dark or light
        this.reCaptchaSize = 'normal'; // normal or compact
        this.reCaptchaType = 'image'; // audio or image
        this.response = null;
        this.widgetId = null;

        this.emptyData = {
            fullname: '',
            email: '',
            content: ''
        };

        this.data = angular.copy(this.emptyData);
    }

    sendMessageAction() {
        if (this.contactForm.$valid) {
            this.ContactModel.sendMessage(this.data).then(
                (response) => {
                    this.ToastService.show('Wiadomość została wysłana. Dziękujemy.');
                    this.data = angular.copy(this.emptyData);
                    this.contactForm.$setUntouched();
                },
                (response) => {
                    if (response.status == 403) {
                        this.ToastService.error('W ciągu jednej minuty można wysłać maksymalnie dwie wiadomości. Spróbuj ponownie za minutę.');
                    } else {
                        this.ToastService.error('Nie udało się wysłać wiadomości');
                    }
                }
            );
        } else {
            this.$log.debug(this.contactForm);
        }
    }
}

export const ContactFormComponent = {
    templateUrl: './views/app/components/contact-form/contact-form.component.html',
    controller: ContactFormController,
    controllerAs: 'contactFormController',
    scope: {
        form: "=contactForm"
    }
};
