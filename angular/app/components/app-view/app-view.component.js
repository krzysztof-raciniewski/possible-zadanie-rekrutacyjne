class AppViewController {
    constructor($mdToast, ToastService, $window) {
        'ngInject';
        this.$window = $window;
        this.$mdToast = $mdToast;
        this.ToastService = ToastService;
    }
}

export const AppViewComponent = {

    templateUrl: './views/app/components/app-view/app-view.component.html',
    controller: AppViewController,
    controllerAs: 'vm',
    bindings: {}
};
