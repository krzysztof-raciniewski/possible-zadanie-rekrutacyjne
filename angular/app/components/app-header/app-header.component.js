class AppHeaderController {
    constructor($rootScope, $sce, $translate, $state) {
        'ngInject';
        this.$sce = $sce;
        this.$rootScope = $rootScope;
        this.$state = $state;
        this.$translate = $translate;
        this.language = $translate.use();
    }

    changeLanguageAction(language) {
        this.language = language;
        this.$translate.use(language);
        this.$state.reload();
    }
}

export const AppHeaderComponent = {
    templateUrl: './views/app/components/app-header/app-header.component.html',
    controller: AppHeaderController,
    controllerAs: 'appHeaderController',
    bindings: {}
};
