export class ScrollToTopDirective {
    constructor() {
	    'ngInject';
        this.restrict = 'A';
		this.scope = {};
    }

    // optional compile function
    compile(tElement) {
    }

    // optional link function
    link(scope, $element) {
      $element.on('click', function() {
      	let $body = $("body");
        $body.animate({scrollTop: $body.offset().top}, "slow");
      });
  }

}