import { RoutesConfig } from "./config/routes.config";
import { LoadingBarConfig } from "./config/loading_bar.config";
import { ThemeConfig } from "./config/theme.config";
import { GoogleMapsConfig } from "./config/googleMaps.config";
import { IconsConfig } from "./config/icons.config";
import { AppTranslates } from './config/appTranslates.config';
import { EnvironmentsConfig } from './config/environments.config';
import { LogProviderConfiguration } from './config/logProvider.config';
import { CompileProviderConfiguration } from './config/compileProvider.config';

angular.module('app.config')
    .config(RoutesConfig)
    .config(LoadingBarConfig)
    .config(ThemeConfig)
    .config(IconsConfig)
    .config(AppTranslates)
    .config(GoogleMapsConfig)
    .config(EnvironmentsConfig)
    .config(LogProviderConfiguration)
    .config(CompileProviderConfiguration);
