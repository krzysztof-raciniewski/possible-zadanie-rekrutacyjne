export function AppTranslates($translateProvider, $windowProvider, $httpProvider) {
    'ngInject';

    /**********************************
     *                  ENGLISH
     *********************************/

    $translateProvider.translations('EN', {
        /*
         * Global translations 
         */
        SELECT_LANGUAGE: 'Select language',
        /*
         * Countries
         */
        Polska: 'Poland',
        Niemcy: 'Germany',
        'Hiszpania': 'Spain',
        /*
         * Main menu translates
         */
        MENU_MAIN_PAGE: 'Main Page',
        MENU_ABOUT_US: 'About us',
        MENU_CONTACT: 'Contact',
        MENU_RULES: 'Rules',
        MENU_PARTNERSHIP: 'Cooperation',
        /*
         * partnership
         */
        PARTNERSHIP_PAGE_QUESTION: 'Cooperate with us',
        PARTNERSHIP_PAGE_ANSWER: 'Bitmobile wants to develop environment cryptocurrency in Poland and around the world. The main idea is to sell applications in the form of a license. You will receive an application ready to make! Decide what commission you offer to your customers. The application you receive will be completely integrated with the major stock exchanges in the world. Sales can be done by one or more premium SMS services. The offer is determined individually, alone decide which functions you want to have in your application. If you have an idea for additional functionalities that facilitate the operation of the system, we will implement your idea! Use the contact form and ask us what offer we have prepared for you.',
        PARTNERSHIP_PAGE_ASK_FOR_DETAILED_OFFER: 'Ask for a detailed offer',
        PARTNERSHIP_PAGE_SELL_CRYPTOCURRENCIES: 'Sell cryptocurrency',
        PARTNERSHIP_PAGE_FOR_SMS: 'for sms',
        PARTNERSHIP_PAGE_TITLE: 'Start cooperation with us',
        /*
         * About us page translates
         */
        ABOUT_US_PAGE_TITLE: 'Informations about our company',
        ABOUT_US_PAGE_CONTENT: 'We are a company which for many years engaged in operations related to arranging mikropłatnościach. Bitmobile is explicit and legitimate business based in Warsaw. We offer our services in Poland and around the world, taking care of every customer satisfaction.',
        CONTACT_PAGE_TITLE: 'Contact form',
        CONTACT_PAGE_CONTENT: 'You have a question or suggestion for us? Write it using the contact form.',
        CONTACT_PAGE_FULLNAME: 'Your full name',
        CONTACT_PAGE_FULLNAME_IS_REQUIRED: 'Your full name is required',
        CONTACT_PAGE_YOUR_EMAIL: 'E-mail address',
        CONTACT_PAGE_YOUR_EMAIL_IS_REQUIRED: 'E-mail address is required',
        CONTACT_PAGE_MESSAGE_CONTENT: 'Message content',
        CONTACT_PAGE_MESSAGE_CONTENT_IS_REQUIRED: 'Message content is required',
        CONTACT_PAGE_SEND_MESSAGE: 'Send message',
        /*
         * Main page translations
         */
        WHAT_BITCOIN_IS_QUESTION: 'What Bitcoin is?',
        WHAT_BITCOIN_IS_ANSWER: 'Bitcoin is based on a consensus network, which allows the new payment system and completely digital money. This is the first decentralized payment network peer-to-peer powered by its users, without a central authority or intermediaries. From the user\'s perspective, Bitcoin is more or less like a web cash. Bitcoin can also be seen as the greatest of the existing system based on the rule of triple record (triple-entry bookkeeping). After what time Bitcoiny appear in my wallet? Bitcoin transactions are realizownę almost immediately but in order to minimize the fees we collect them wszyskie and realizes once an hour.',
        WHICH_WALLET_CHOOSE_QUESTION: 'Which wallet choose?',
        WHICH_WALLET_CHOOSE_ANSWER: 'The choice of bitcoin wallet is a very important step. We recommend several popular and safe portfolios: Bitcoin Core Multibit, Electrum, Hive, Armory, Blockchain Wallet, Coinbase, Coinkite.',
        WHAT_MAKES_US_DIFFRENT_QUESTION: 'What makes us diffrent?',
        WHAT_MAKES_US_DIFFRENT_ANSWER: 'Coins4SMS.com offers you',
        BOOST_IN_MULTIPLE_CURRENCIES: 'Boost in multiple currencies',
        BOOST_IN_MULTIPLE_CURRENCIES_EXPLANATION: 'With Coins4SMS.com you can recharge your wallet several types cryptocurrency (BTC, LTC, ETH, LSK)',
        SECURE_TRANSACTIONS: 'Secure transactions',
        SECURE_TRANSACTIONS_EXPLANATION: 'Secure transactions Our website meets all safety standards required to mediate mikropłatnościach Bitcoin.',
        PROFESSIONAL_SUPPORT: 'Professional support',
        PROFESSIONAL_SUPPORT_EXPLANATION: 'You have a problem with the service application? Do you want to file a complaint? Use our contact form and wait for our answer.',
        /*
         * Multi steps form translations
         */
        MULTI_STEPS_FORM_PREVIOUS_STEP: 'Previous step',
        MULTI_STEPS_FORM_NEXT_STEP: 'Next step',
        MULTI_STEPS_FORM_START_NEW_TRANSACTION: 'Start new transaction',
        MULTI_STEPS_FORM_MAKE_A_PAYMENT: 'Make a payment',
        MULTI_STEPS_FORM_COMPLETE_THE_TRANSACTION: 'Complete the transaction',
        MULTI_STEPS_FORM_COUNTRY: 'Country',
        MULTI_STEPS_FORM_COUNTRY_REQUIRED: 'Select your country',
        MULTI_STEPS_FORM_OPERATOR: 'Operator',
        MULTI_STEPS_FORM_OPERATOR_REQUIRED: 'Select your operator',
        MULTI_STEPS_FORM_CURRENCY_TYPE: 'Currency type',
        MULTI_STEPS_FORM_CURRENCY_TYPE_REQUIRED: 'Select currency type',
        MULTI_STEPS_FORM_PHONE_NUMBER: 'Phone number',
        MULTI_STEPS_FORM_PHONE_NUMBER_REQUIRED: 'Phone number is required',
        MULTI_STEPS_FORM_PHONE_NUMBER_BAD_PATTERN: 'The phone number can only contain numbers',
        MULTI_STEPS_FORM_DECLARATION_PART1: 'I declare that I have read the',
        MULTI_STEPS_FORM_DECLARATION_PART2: '',
        MULTI_STEPS_FORM_DECLARATION_RULES: 'rules',
        MULTI_STEPS_FORM_PRICE_PER_SMS: 'Price per SMS',
        MULTI_STEPS_FORM_PRICE_PER_SMS_REQUIRED: 'Price per SMS is required',
        MULTI_STEPS_FORM_WALLET_ADDRESS: 'Wallet address',
        MULTI_STEPS_FORM_WALLET_ADDRESS_REQUIRED: 'Wallet address is required',
        MULTI_STEPS_FORM_EMAIL_ADDRESS: 'E-mail address',
        MULTI_STEPS_FORM_EMAIL_ADDRESS_BAD_FORMAT: 'E-mail address has invalid format ',
        MULTI_STEPS_FORM_SEND_TEXT_MESSAGE_TO_THE_NUMBER: 'Send text message to the number ',
        MULTI_STEPS_FORM_WITH_CONTENT: 'with content ',
        MULTI_STEPS_FORM_YOU_RECEIVE_MORE_OR_LESS: 'You receive more or less',
        MULTI_STEPS_FORM_TO_YOUR_ACCOUNT: 'to your account',
        MULTI_STEPS_FORM_SMS_EXPLANATION: 'In response you will get a one-time code that must be used in the next step. The price for SMS is the gross amount.',
        MULTI_STEPS_FORM_SERVICE_PROVIDER_IS: 'Service provider is',
        MULTI_STEPS_FORM_SERVED_OPERATORS: 'Available operators',
        MULTI_STEPS_FORM_COMPLAINT_FORM: 'Complaint form',
        MULTI_STEPS_FORM_SERVICE_REGULATIONS: 'Service regulations',
        MULTI_STEPS_FORM_CRYPTOCURRENCY_VALUE_TOO_SMALL: 'The calculated amount cryptocurrency for this value sms is too small. Select a higher value SMS.',
        MULTI_STEPS_FORM_CODE_IS_VALID_MESSAGE: 'The code is valid. In a few minutes you will receive powering your wallet. Your transaction ID is',
        MULTI_STEPS_FORM_SAVE_ID_MESSAGE: 'Save identifier may be useful in case of complaints',
        MULTI_STEPS_FORM_ENTER_ONE_TIME_CODE: 'Enter the one-time code',
        MULTI_STEPS_FORM_CODE_IS_INCORRECT: 'Code is incorrect',
        MULTI_STEPS_FORM_ENTER_THE_CODE_AGAIN_MESSAGE: 'Enter the code again, whitespace and capitalization is important!',
        MULTI_STEPS_FORM_TRY_ENTERING_THE_CODE_AGAIN: 'Try entering the code again!',
        MULTI_STEPS_FORM_CHECK_CODE: 'Check code',
        HOW_IT_WORKS_QUESTION: 'How it works?',
        HOW_IT_WORKS_ANSWER: 'It\'s easy, form has 3 parts' ,
        HOW_IT_WORKS_STEP_1_NAME: 'Create new transaction',
        HOW_IT_WORKS_STEP_1_EXPLANATION: 'In this step select your country, mobile operator and currency type you want to purchase. Before you start accept our rules. After filling all required fields transition to the next step will be unlocked.',
        HOW_IT_WORKS_STEP_2_NAME: 'Make payment',
        HOW_IT_WORKS_STEP_2_EXPLANATION: 'In this step you can select sms value, next you should see details to send a premium message.',
        HOW_IT_WORKS_STEP_3_NAME: 'Check your code',
        HOW_IT_WORKS_STEP_3_EXPLANATION: 'After sending the SMS you will receive one-time code. In step 3 you will be able to past that code and complete the transaction. After completed transaction we send the currency to your wallet.'
    });


    /**********************************
     *                  POLISH
     *********************************/

    $translateProvider.translations('PL', {
        /*
         * Global translations 
         */
        SELECT_LANGUAGE: 'Wybierz język',
        /*
         * Countries
         */
        Polska: 'Polska',
        Niemcy: 'Niemcy',
        'Hiszpania': 'Hiszpania',
        /*
         * Main menu translates
         */
        MENU_MAIN_PAGE: 'Strona główna',
        MENU_ABOUT_US: 'O nas',
        MENU_CONTACT: 'Kontakt',
        MENU_RULES: 'Regulamin',
        MENU_PARTNERSHIP: 'Współpraca',

        /*
         * partnership
         */
        PARTNERSHIP_PAGE_QUESTION: 'Co możemy Ci zaoferować?',
        PARTNERSHIP_PAGE_ANSWER: 'Bitmobile chce rozwijać środowisko kryptowalut w Polsce i na całym świecie. Główną ideą firmy jest sprzedaż aplikacji w formie licencyjnej. Otrzymasz od nas aplikację gotową do zarabiania! Sam ustalaj i decyduj jakie prowizje zaoferujesz swoim klientom. Aplikacja którą otrzymasz będzie całkowicie zintegrowana z największymi giełdami BTC. Sprzedaż może się odbywać za pomocą jednego, lub kilku dostawców usługi premium SMS. Oferta ustalana jest indywidualnie, sam zdecydujesz które funkcjonalności chciałbyś mieć w swojej aplikacji, a którę są zbędne. Jeżeli masz pomysł na dodatkowe funkcjonalności, które miałyby ułatwić obsługę systemu, zrealizujemy Twój pomysł! Skorzystaj z formularza kontaktowego i zapytaj się jaką ofertę przygotowaliśmy właśnie dla Ciebie.',
        PARTNERSHIP_PAGE_ASK_FOR_DETAILED_OFFER: 'Zadaj nam pytanie o ofertę szczegółową',
        PARTNERSHIP_PAGE_SELL_CRYPTOCURRENCIES: 'Sprzedawaj kryptowaluty',
        PARTNERSHIP_PAGE_FOR_SMS: 'za sms',
        PARTNERSHIP_PAGE_TITLE: 'Rozpocznij z nami współpracę',
        /*
         * About us page translations
         */
        ABOUT_US_PAGE_TITLE: 'Informacje o naszej firmie',
        ABOUT_US_PAGE_CONTENT: 'Jesteśmy spółką, która od wielu lat zajmuje się operacjami związanymi z pośredniczeniem w mikropłatnościach. Bitmobile jest jawną i legalną działalnością z siedzibą w Warszawie. Swoje usługi oferujemy w Polsce jak i na całym świecie, dbając o zadowolenie każdego klienta.',
        CONTACT_PAGE_TITLE: 'Formularz kontaktowy',
        CONTACT_PAGE_CONTENT: 'Masz do nas pytanie lub sugestię? Napisz do nas za pomocą formularza kontaktowego.',
        CONTACT_PAGE_FULLNAME: 'Twoje imię i nazwisko',
        CONTACT_PAGE_FULLNAME_IS_REQUIRED: 'Twoje imię i nazwisko jest wymagane',
        CONTACT_PAGE_YOUR_EMAIL: 'Adres e-mail',
        CONTACT_PAGE_YOUR_EMAIL_IS_REQUIRED: 'Adres e-mail jest wymagany',
        CONTACT_PAGE_MESSAGE_CONTENT: 'Treść wiadomości',
        CONTACT_PAGE_MESSAGE_CONTENT_IS_REQUIRED: 'Treść wiadomości jest wymagana',
        CONTACT_PAGE_SEND_MESSAGE: 'Wyślij wiadomość',
        /*
         * Main page translations
         */
        WHAT_BITCOIN_IS_QUESTION: 'Czym jest Bitcoin?',
        WHAT_BITCOIN_IS_ANSWER: 'Bitcoin jest opartą na konsensusie siecią, która umożliwia nowy system płatności i zupełnie cyfrowe pieniądze. Jest to pierwsza zdecentralizowana sieć płatności peer-to-peer zasilana przez jej użytkowników, bez władzy centralnej czy pośredników. Z perspektywy użytkownika Bitcoin jest mniej więcej jak internetowa gotówka. Bitcoin może być również postrzegany jako największy z istniejących system oparty na regule potrójnego zapisu (triple entry bookkeeping). Po jakim czasie Bitcoiny pojawią się na moim portfelu? Transakcje bitcoinami realizownę są prawie natychmiast ale w celu zminimalizowania opłat nasz system gromadzi je wszyskie i realizuje raz na godzinę.',
        WHICH_WALLET_CHOOSE_QUESTION: 'Który portfel wybrać?',
        WHICH_WALLET_CHOOSE_ANSWER: 'Wybór portfel bitcoin jest bardzo ważnym krokiem. My polecamy kilka popularnych i bezpiecznych portfeli: Bitcoin Core, Multibit, Electrum, Hive, Armory, Blockchain Wallet, Coinbase, Coinkite.',
        WHAT_MAKES_US_DIFFRENT_QUESTION: 'Co nas wyróżnia?',
        WHAT_MAKES_US_DIFFRENT_ANSWER: 'Coins4SMS.com oferuje',
        BOOST_IN_MULTIPLE_CURRENCIES: 'Doładowania w wielu walutach kryptograficznch',
        BOOST_IN_MULTIPLE_CURRENCIES_EXPLANATION: 'Dzięki aplikacji Coins4SMS.com możesz doładować swój portfel kilkoma rodzajami kryptowaluty(BTC, LTC, ETH, LSK)',
        SECURE_TRANSACTIONS: 'Bezpieczne transakcje',
        SECURE_TRANSACTIONS_EXPLANATION: 'Nasz portal spełnia wszystkie standardy bezpieczeństwa wymagane do pośredniczenia w mikropłatnościach Bitcoin..',
        PROFESSIONAL_SUPPORT: 'Profesjonalne wsparcie',
        PROFESSIONAL_SUPPORT_EXPLANATION: 'Masz problem z obsługą aplikacji? Chcesz zgłosić reklamację? Skorzystaj z naszego formularza kontaktowego i czekaj na naszą odpowiedź.',
        /*
         * Multi steps form translations
         */
        MULTI_STEPS_FORM_PREVIOUS_STEP: 'Poprzedni krok',
        MULTI_STEPS_FORM_NEXT_STEP: 'Następny krok',
        MULTI_STEPS_FORM_START_NEW_TRANSACTION: 'Rozpocznij nową transakcję',
        MULTI_STEPS_FORM_MAKE_A_PAYMENT: 'Dokonaj płatności',
        MULTI_STEPS_FORM_COMPLETE_THE_TRANSACTION: 'Zakończ transakcję',
        MULTI_STEPS_FORM_COUNTRY: 'Kraj',
        MULTI_STEPS_FORM_COUNTRY_REQUIRED: 'Wybierz swój kraj',
        MULTI_STEPS_FORM_OPERATOR: 'Operator',
        MULTI_STEPS_FORM_OPERATOR_REQUIRED: 'Wybierz swojego operatora',
        MULTI_STEPS_FORM_CURRENCY_TYPE: 'Typ waluty',
        MULTI_STEPS_FORM_CURRENCY_TYPE_REQUIRED: 'Wybierz typ waluty',
        MULTI_STEPS_FORM_PHONE_NUMBER: 'Numer telefonu',
        MULTI_STEPS_FORM_PHONE_NUMBER_REQUIRED: 'Podaj swój numer telefonu',
        MULTI_STEPS_FORM_PHONE_NUMBER_BAD_PATTERN: 'Numer telefonu może składać się tylko z liczb',
        MULTI_STEPS_FORM_DECLARATION_PART1: 'Oświadczam, że zapoznałem się z',
        MULTI_STEPS_FORM_DECLARATION_PART2: 'serwisu Coins4SMS.com',
        MULTI_STEPS_FORM_DECLARATION_RULES: 'regulaminem',
        MULTI_STEPS_FORM_PRICE_PER_SMS: 'Cena za sms',
        MULTI_STEPS_FORM_PRICE_PER_SMS_REQUIRED: 'Cena za sms jest wymagana, wybierz ją z listy',
        MULTI_STEPS_FORM_WALLET_ADDRESS: 'Adres portfela',
        MULTI_STEPS_FORM_WALLET_ADDRESS_REQUIRED: 'Adres portfela jest wymagany',
        MULTI_STEPS_FORM_EMAIL_ADDRESS: 'Adres e-mail',
        MULTI_STEPS_FORM_EMAIL_ADDRESS_BAD_FORMAT: 'Adres e-mail jest w niepoprawnym formacie ',
        MULTI_STEPS_FORM_SEND_TEXT_MESSAGE_TO_THE_NUMBER: 'Wyślij wiadomość SMS pod numer ',
        MULTI_STEPS_FORM_WITH_CONTENT: 'o treści ',
        MULTI_STEPS_FORM_YOU_RECEIVE_MORE_OR_LESS: 'Otrzymasz około',
        MULTI_STEPS_FORM_TO_YOUR_ACCOUNT: 'na swoje konto',
        MULTI_STEPS_FORM_SMS_EXPLANATION: 'Po wysłaniu wiadomości SMS otrzymasz w odpowiedzi jednorazowy kod, który będziesz musiał wykorzystać w następnym kroku.',
        MULTI_STEPS_FORM_SERVICE_PROVIDER_IS: 'Usługę premium SMS dostarcza ',
        MULTI_STEPS_FORM_SERVED_OPERATORS: 'Obsługiwani operatorzy',
        MULTI_STEPS_FORM_COMPLAINT_FORM: 'Formularz reklamacyjny',
        MULTI_STEPS_FORM_SERVICE_REGULATIONS: 'Regulamin usługi',
        MULTI_STEPS_FORM_CRYPTOCURRENCY_VALUE_TOO_SMALL: 'Obliczona wartość kryptowaluty jest zbyt niska, wybierz sms o większej wartości',
        MULTI_STEPS_FORM_CODE_IS_VALID_MESSAGE: 'Kod jest prawidłowy, za kilka minut powinieneś otrzymać doładowanie swojego portfela. Identyfikator Twojej transakcji to',
        MULTI_STEPS_FORM_SAVE_ID_MESSAGE: 'Zapisz identyfikator, przyda się w przypadku reklamacji',
        MULTI_STEPS_FORM_ENTER_ONE_TIME_CODE: 'Wprowadź kod jednorazowy',
        MULTI_STEPS_FORM_CODE_IS_INCORRECT: 'Kod jest niepoprwany',
        MULTI_STEPS_FORM_ENTER_THE_CODE_AGAIN_MESSAGE: 'Wprowadź kod jeszcze raz, pamiętaj o tym, że białe znaki są istotne!',
        MULTI_STEPS_FORM_TRY_ENTERING_THE_CODE_AGAIN: 'Spróbuj wprowadzić kod ponownie',
        MULTI_STEPS_FORM_CHECK_CODE: 'Sprawdź kod',
        HOW_IT_WORKS_QUESTION: 'Jak to działa?',
        HOW_IT_WORKS_ANSWER: 'To proste, powyższy formularz składa się z 3 kroków',
        HOW_IT_WORKS_STEP_1_NAME: 'Rozpoczynanie nowej transakcji',
        HOW_IT_WORKS_STEP_1_EXPLANATION: 'W tym kroku wybierasz swój kraj, operatora sieci komórkowej i typ waluty jaką chciałbyś nabyć. Przed rozpoczęciem nowej transakcji wymagane jest zaakceptowanie regulaminu. Po wypełnieniu wszystkich wymaganych informacji w kroku nr 1 odblokowany zostanie przycisk przejścia do kolejnego kroku.',
        HOW_IT_WORKS_STEP_2_NAME: 'Płatność',
        HOW_IT_WORKS_STEP_2_EXPLANATION: 'W tym kroku ustalasz wartość wiadomości SMS jaką chcesz wysłać i adres portfela na który wyślemy wyliczoną wartość kryptowaluty. W kroku drugim należy wysłać SMS o określonej wartości pod wskazany numer specjalny.',
        HOW_IT_WORKS_STEP_3_NAME: 'Sprawdzanie otrzymanego kodu',
        HOW_IT_WORKS_STEP_3_EXPLANATION: 'Po wysłaniu wiadomości SMS otrzymasz kod jednorazowy. W kroku nr 3 będziesz mógł przepisać otrzymany kod jednorazowy, żeby dokończyć transakcję i otrzymać określoną ilość kryptowaluty na swój portfel.'

    });

    // if (this.$rootScope.language) {
    //     console.log('JEST: ' + $rootScope.language);
    // }
    // var rootScope = $injector.get('$rootScope');


    $translateProvider.preferredLanguage('PL');
    $translateProvider.useSanitizeValueStrategy('escape');


}
