export function RoutesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
    'ngInject';
    $locationProvider.html5Mode(true);

    let getView = (viewName) => {
        return `./views/app/pages/${viewName}/${viewName}.page.html`;
    };

    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('app', {
            abstract: true,
            views: {
                header: {
                    templateUrl: getView('header')
                },
                footer: {
                    templateUrl: getView('footer')
                },
                main: {}
            }
        })
        .state('app.index', {
            url: '/',
            views: {
                'main@': {
                    templateUrl: getView('index')
                }
            }
        })
        .state('app.dotpay', {
            url: '/dotpay',
            views: {
                'main@': {
                    templateUrl: getView('dotpay')
                }
            }
        })
        .state('app.fortumo', {
            url: '/fortumo',
            views: {
                'main@': {
                    templateUrl: getView('fortumo')
                }
            }
        })
        .state('app.about', {
            url: '/about_us',
            views: {
                'main@': {
                    templateUrl: getView('about')
                }
            }
        })
        .state('app.contact', {
            url: '/contact',
            views: {
                'main@': {
                    templateUrl: getView('contact')
                }
            }
        })
        .state('app.partnership', {
            url: '/partnership',
            views: {
                'main@': {
                    templateUrl: getView('partnership')
                }
            }
        });
}
