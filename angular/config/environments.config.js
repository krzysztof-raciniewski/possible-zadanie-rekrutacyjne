export function EnvironmentsConfig(envServiceProvider) {
    'ngInject';

    // set the domains and variables for each environment 
    envServiceProvider.config({
        domains: {
            development: ['localhost', 'coins4sms.dev'],
            production: ['coins4sms.com']
        },
        vars: {
            development: {
                debug: true
            },
            production: {
                debug: false
            }
        }
    });

    // run the environment check, so the comprobation is made 
    // before controllers and services are built 
    envServiceProvider.check();

}
