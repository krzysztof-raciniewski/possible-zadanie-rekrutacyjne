export function CompileProviderConfiguration($compileProvider, envServiceProvider) {
    'ngInject';
    envServiceProvider.check();

    if (envServiceProvider.is('development')) {
        $compileProvider.debugInfoEnabled(true);
    } else {
        $compileProvider.debugInfoEnabled(false);
    }
}
