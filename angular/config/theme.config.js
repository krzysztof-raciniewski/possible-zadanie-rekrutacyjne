export function ThemeConfig($mdThemingProvider) {
    'ngInject';
    /* For more info, visit https://material.angularjs.org/#/Theming/01_introduction */
    $mdThemingProvider.theme('default')
        .primaryPalette('light-blue', {
            default: '600'
        })
        .accentPalette('grey')
        .warnPalette('red');

    $mdThemingProvider.theme('publicForm')
        .primaryPalette('yellow', {
            default: '500'
        })
        .accentPalette('purple')
        .warnPalette('yellow');

    $mdThemingProvider.theme('warn');
}
