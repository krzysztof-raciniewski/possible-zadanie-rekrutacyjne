export function IconsConfig($mdIconProvider) {
    'ngInject';
    $mdIconProvider
        .iconSet('menu:black', 'img/icons/ic_menu_black_24px.svg', 24)
        .defaultIconSet('img/icons/ic_menu_black_24px.svg', 24);
}
