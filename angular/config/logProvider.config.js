export function LogProviderConfiguration($logProvider, envServiceProvider) {
    'ngInject';

    if (envServiceProvider.is('development')) {
        $logProvider.debugEnabled(true);
    } else {
        $logProvider.debugEnabled(false);
    }
}
