export function GoogleMapsConfig(uiGmapGoogleMapApiProvider) {
    'ngInject';
    /* For more info, visit https://material.angularjs.org/#/Theming/01_introduction */
    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyDTjAQg21gEz1cmu981n-bnT6dJmMyyV8s',
        v: '3.20', //defaults to latest 3.X anyhow
        libraries: 'visualization'
    });
}
