import { AppHeaderComponent } from "./app/components/app-header/app-header.component";
import { AppViewComponent } from "./app/components/app-view/app-view.component";
import { ContactFormComponent } from "./app/components/contact-form/contact-form.component";
import { AboutUsGoogleMapComponent } from "./app/components/about-us-google-map/about-us-google-map.component";
import { MultiStepsFormComponent } from "./app/components/multi-steps-form/multi-steps-form.component";

angular.module('app.components')
    .component('appHeader', AppHeaderComponent)
    .component('appView', AppViewComponent)
    .component('contactForm', ContactFormComponent)
    .component('aboutUsGoogleMap', AboutUsGoogleMapComponent)
    .component('multiStepsForm', MultiStepsFormComponent);
