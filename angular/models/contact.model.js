export class ContactModel {
    constructor(API, ToastService) {
        'ngInject';

        let ContactModel = API.service('contact');

        ContactModel.sendMessage = function(data) {
            return ContactModel.post(data);
        };

        API.extendModel('sms', function(model) {
            return model;
        });

        return ContactModel;

    }
}
