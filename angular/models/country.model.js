export class CountryModel {
    constructor(API, ToastService) {
        'ngInject';

        let CountryModel = API.service('countries');

        /**
         * Get all countries
         * @returns {*|{method, params, headers}}
         */
        CountryModel.getAll = function() {
            return CountryModel.getList();
        };

        /**
         * Get all available operators for country
         */
        CountryModel.getOperators = (countryId) => {
            return CountryModel.one(countryId).one('operators').getList();
        };

        /**
         * Model methods
         */
        API.extendModel('countries', function(model) {
            return model;
        });

        return CountryModel;

    }
}
