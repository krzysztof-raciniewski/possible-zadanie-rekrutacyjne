export class CryptocurrencyModel {
    constructor(API, ToastService) {
        'ngInject';

        let CryptocurrencyModel = API.service('cryptocurrencies');

        /**
         * Get all countries
         * @returns {*|{method, params, headers}}
         */
        CryptocurrencyModel.getAll = function() {
            return CryptocurrencyModel.getList();
        };

        /**
         * Model methods
         */
        API.extendModel('cryptocurrencies', function(model) {
            return model;
        });

        return CryptocurrencyModel;

    }
}
