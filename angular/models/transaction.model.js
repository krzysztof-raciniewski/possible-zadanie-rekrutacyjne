export class TransactionModel {
    constructor(API, ToastService) {
        'ngInject';

        let TransactionModel = API.service('transactions');


        TransactionModel.createNew = (data) => {
            return TransactionModel.post(data);
        };

        TransactionModel.update = (transactionId, data) => {
            return TransactionModel.one(transactionId).put(data);
        };

        TransactionModel.checkCode = (transactionId, code) => {
            return TransactionModel.one(transactionId).one('checkcode', code).get();
        };

        TransactionModel.getInfo = (transactionId, smsId) => {
            return TransactionModel.one(transactionId).one('info', smsId).get();
        };

        /**
         * Model methods
         */
        API.extendModel('transactions', function(model) {
            return model;
        });

        return TransactionModel;

    }
}
