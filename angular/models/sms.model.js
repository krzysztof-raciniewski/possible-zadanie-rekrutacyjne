export class SmsModel {
    constructor(API, ToastService) {
        'ngInject';

        let SmsModel = API.service('sms');

        /**
         * Get all countries
         * @returns {*|{method, params, headers}}
         */
        SmsModel.getAllForTransaction = function(transactionId) {
            return SmsModel.one(transactionId).getList();
        };

        /**
         * Model methods
         */
        API.extendModel('sms', function(model) {
            return model;
        });

        return SmsModel;

    }
}
