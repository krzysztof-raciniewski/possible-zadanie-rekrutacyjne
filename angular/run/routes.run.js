export function RoutesRun($log, $http, $translate, $state) {
    'ngInject';

    $http.get('https://ipinfo.io/').then(
        (response) => {
            var countryCode = response.data.country;
            $log.debug(countryCode);
            if (countryCode == 'PL') {
                $translate.use('PL');
                $state.reload();
            } else {
                $translate.use('EN');
                $state.reload();
            }
        },
        () => {
            $translate.use('EN');
            $state.reload();
        }
    );

}
