import { register } from "./utils/register";
import { ScrollToTopDirective } from "./directives/scrollToTop.directive";

angular.module('app.directives');

register('app.directives').directive('scrollToTop', ScrollToTopDirective);