export class APIService {
    constructor(Restangular, ToastService, $window) {
        'ngInject';
        //content negotiation
        let headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/x.laravel.v1+json'
        };

        return Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer
                .setBaseUrl('/api/public')
                .setDefaultHeaders(headers)
                .setErrorInterceptor(function (response) {
                    if (response.status === 422 || response.status === 401) {
                        for (let error in response.data.errors) {
                            return ToastService.error(response.data.errors[error][0]);
                        }
                    }
                    if (response.status === 500) {
                        return ToastService.error(response.statusText);
                    }
                    if (response.status === 404) {
                        return ToastService.error('Nie odnaleziono zasobu!');
                    }
                })
                .addFullRequestInterceptor(function (element, operation, what, url, headers) {
              
                });
        });
    }
}
