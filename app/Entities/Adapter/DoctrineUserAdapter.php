<?php
/**
 * Created by IntelliJ IDEA.
 * User: raciniak
 * Date: 23.08.16
 * Time: 13:23
 */

namespace App\Entities\Adapter;

use App\Entities\User;
use Doctrine\ORM\EntityManagerInterface;
use Tymon\JWTAuth\Providers\User\UserInterface;

class DoctrineUserAdapter implements UserInterface
{
    protected $em;

    public function __construct(User $user, EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getBy($key, $value)
    {
        /*
         * Note that I'm ignoring $key because it's almost always 'id', but if you
         * have a different jwt.identifier (that isn't your entity identifier) you'll need
         * to do something more complex here, potentially involving a repository.
         *
         * Also the entity class is hard-set in this example, but you could read it
         * from your config or have a setter to make it a more general solution.
         */
        return $this->em->find('App\Entities\User', $value);
    }
}