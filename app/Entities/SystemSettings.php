<?php
declare(strict_types=1);

namespace App\Entities;

use DOctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Operator *
 * @ORM\Table(name="system_settings")
 * @ORM\Entity
 * @Gedmo\Loggable
 */
class SystemSettings
{
    /**
     * @var integer *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * Payment job interval in seconds
     * @var string *
     * @ORM\Column(name="payment_job_interval", type="integer", nullable=false)
     * @Gedmo\Versioned
     */
    protected $paymentJobInterval;

    /**
     * @var boolean
     * @ORM\Column(name="wallet_allerts_active", type="boolean", nullable=false)
     */
    protected $walletAllertsActive;

    /**
     * commision in percent
     * @var float *
     * @ORM\Column(name="sale_commission", type="decimal", precision=5, scale=2, nullable=false)
     * @Gedmo\Versioned
     */
    protected $sale_commission;

    /**
     * @var float
     * @ORM\Column(name="btc_wallet_full_value", type="decimal", precision=5, scale=2, nullable=false)
     * @Gedmo\Versioned
     */
    protected $btcWalletFullValue;

    /**
     * @var float
     * @ORM\Column(name="btc_wallet_alert_value", type="decimal", precision=5, scale=2, nullable=false)
     * @Gedmo\Versioned
     */
    protected $btcWalletAlertValue;

    /**
     * @var float
     * @ORM\Column(name="lsk_wallet_full_value", type="decimal", precision=5, scale=2, nullable=false)
     * @Gedmo\Versioned
     */
    protected $lskWalletFullValue;

    /**
     * @var float
     * @ORM\Column(name="lsk_wallet_alert_value", type="decimal", precision=5, scale=2, nullable=false)
     * @Gedmo\Versioned
     */
    protected $lskWalletAlertValue;

    /**
     * @var float
     * @ORM\Column(name="eth_wallet_full_value", type="decimal", precision=5, scale=2, nullable=false)
     * @Gedmo\Versioned
     */
    protected $ethWalletFullValue;

    /**
     * @var float
     * @ORM\Column(name="eth_wallet_alert_value", type="decimal", precision=5, scale=2, nullable=false)
     * @Gedmo\Versioned
     */
    protected $ethWalletAlertValue;

    /**
     * @var float
     * @ORM\Column(name="ltc_wallet_full_value", type="decimal", precision=5, scale=2, nullable=false)
     * @Gedmo\Versioned
     */
    protected $ltcWalletFullValue;

    /**
     * @var float
     * @ORM\Column(name="ltc_wallet_alert_value", type="decimal", precision=5, scale=2, nullable=false)
     * @Gedmo\Versioned
     */
    protected $ltcWalletAlertValue;

    /**
     * @var string
     * @ORM\Column(name="contact_email", type="string", nullable=false)
     * @Gedmo\Versioned
     */
    protected $contactEmail;

    /**
     * @var string
     * @ORM\Column(name="notifications_email", type="string", nullable=false)
     * @Gedmo\Versioned
     */
    protected $notificationsEmail;

    /**
     * @var string
     * @ORM\Column(name="owner_name", type="string", nullable=false)
     * @Gedmo\Versioned
     */
    protected $ownerName;

    /**
     * @var string
     * @ORM\Column(name="domain_name", type="string", nullable=false)
     * @Gedmo\Versioned
     */
    protected $domainName;

    /**
     * @var string
     * @ORM\Column(name="application_name", type="string", nullable=false)
     * @Gedmo\Versioned
     */
    protected $applicationName;

    /**
     * @var boolean
     * @ORM\Column(name="payment_process_active", type="boolean", nullable=false)
     */
    protected $paymentProcessActive;

    /**
     * @var \DateTime $created
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var \DateTime $updated
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPaymentJobInterval()
    {
        return $this->paymentJobInterval;
    }

    /**
     * @param mixed $paymentJobInterval
     */
    public function setPaymentJobInterval($paymentJobInterval)
    {
        $this->paymentJobInterval = $paymentJobInterval;
    }

    /**
     * @return float
     */
    public function getSaleCommission()
    {
        return (float)$this->sale_commission;
    }

    /**
     * @param float $sale_commission
     */
    public function setSaleCommission(float $sale_commission)
    {
        $this->sale_commission = $sale_commission;
    }

    /**
     * @return float
     */
    public function getBtcWalletFullValue()
    {
        return (float)$this->btcWalletFullValue;
    }

    /**
     * @param float $btcWalletFullValue
     */
    public function setBtcWalletFullValue(float $btcWalletFullValue)
    {
        $this->btcWalletFullValue = $btcWalletFullValue;
    }

    /**
     * @return float
     */
    public function getBtcWalletAlertValue()
    {
        return (float)$this->btcWalletAlertValue;
    }

    /**
     * @param float $btcWalletAlertValue
     */
    public function setBtcWalletAlertValue(float $btcWalletAlertValue)
    {
        $this->btcWalletAlertValue = $btcWalletAlertValue;
    }

    /**
     * @return float
     */
    public function getLskWalletFullValue()
    {
        return (float)$this->lskWalletFullValue;
    }

    /**
     * @param float $lskWalletFullValue
     */
    public function setLskWalletFullValue(float $lskWalletFullValue)
    {
        $this->lskWalletFullValue = $lskWalletFullValue;
    }

    /**
     * @return float
     */
    public function getLskWalletAlertValue()
    {
        return (float)$this->lskWalletAlertValue;
    }

    /**
     * @param float $lskWalletAlertValue
     */
    public function setLskWalletAlertValue(float $lskWalletAlertValue)
    {
        $this->lskWalletAlertValue = $lskWalletAlertValue;
    }

    /**
     * @return float
     */
    public function getEthWalletFullValue()
    {
        return (float)$this->ethWalletFullValue;
    }

    /**
     * @param float $ethWalletFullValue
     */
    public function setEthWalletFullValue(float $ethWalletFullValue)
    {
        $this->ethWalletFullValue = $ethWalletFullValue;
    }

    /**
     * @return float
     */
    public function getEthWalletAlertValue()
    {
        return (float)$this->ethWalletAlertValue;
    }

    /**
     * @param float $ethWalletAlertValue
     */
    public function setEthWalletAlertValue(float $ethWalletAlertValue)
    {
        $this->ethWalletAlertValue = $ethWalletAlertValue;
    }

    /**
     * @return float
     */
    public function getLtcWalletFullValue()
    {
        return (float)$this->ltcWalletFullValue;
    }

    /**
     * @param float $ltcWalletFullValue
     */
    public function setLtcWalletFullValue(float $ltcWalletFullValue)
    {
        $this->ltcWalletFullValue = $ltcWalletFullValue;
    }

    /**
     * @return float
     */
    public function getLtcWalletAlertValue()
    {
        return (float)$this->ltcWalletAlertValue;
    }

    /**
     * @param float $ltcWalletAlertValue
     */
    public function setLtcWalletAlertValue(float $ltcWalletAlertValue)
    {
        $this->ltcWalletAlertValue = $ltcWalletAlertValue;
    }

    /**
     * @return string
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * @param string $contactEmail
     */
    public function setContactEmail(string $contactEmail)
    {
        $this->contactEmail = $contactEmail;
    }

    /**
     * @return string
     */
    public function getNotificationsEmail()
    {
        return $this->notificationsEmail;
    }

    /**
     * @param string $notificationsEmail
     */
    public function setNotificationsEmail(string $notificationsEmail)
    {
        $this->notificationsEmail = $notificationsEmail;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getOwnerName()
    {
        return $this->ownerName;
    }

    /**
     * @param string $ownerName
     */
    public function setOwnerName(string $ownerName)
    {
        $this->ownerName = $ownerName;
    }

    /**
     * @return string
     */
    public function getDomainName()
    {
        return $this->domainName;
    }

    /**
     * @param string $domainName
     */
    public function setDomainName(string $domainName)
    {
        $this->domainName = $domainName;
    }

    /**
     * @return boolean
     */
    public function isPaymentProcessActive()
    {
        return $this->paymentProcessActive;
    }

    public function getPaymentProcessStatus() {
        return $this->paymentProcessActive;
    }

    /**
     * @param boolean $paymentProcessActive
     */
    public function setPaymentProcessActive(bool $paymentProcessActive)
    {
        $this->paymentProcessActive = $paymentProcessActive;
    }

    /**
     * @return string
     */
    public function getApplicationName(): string
    {
        return $this->applicationName;
    }

    /**
     * @param string $applicationName
     */
    public function setApplicationName(string $applicationName)
    {
        $this->applicationName = $applicationName;
    }

    /**
     * @return boolean
     */
    public function isWalletAllertsActive(): bool
    {
        return $this->walletAllertsActive;
    }

    /**
     * @param boolean $walletAllertsActive
     */
    public function setWalletAllertsActive(bool $walletAllertsActive)
    {
        $this->walletAllertsActive = $walletAllertsActive;
    }


}