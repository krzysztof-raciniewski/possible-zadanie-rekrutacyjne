<?php
declare(strict_types=1);

namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use DOctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Operator *
 * @ORM\Table(name="payment_orders")
 * @ORM\Entity
 * @Gedmo\Loggable
 */
class PaymentOrder implements \JsonSerializable
{

    /**
     * @var integer *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var Transaction
     * @OneToOne(targetEntity="Transaction", mappedBy="paymentOrder")
     */
    protected $transaction;

    /**
     * @ManyToMany(targetEntity="PaymentOrderLog", inversedBy="paymentOrdersSuccesed")
     * @JoinTable(name="payment_order_success_logs")
     */
    protected $paymentOrderSuccessLogs;

    /**
     * @ManyToMany(targetEntity="PaymentOrderLog", inversedBy="paymentOrdersFailures")
     * @JoinTable(name="payment_order_failure_logs")
     */
    protected $paymentOrderFailuresLogs;

    /**
     * @var float
     * @ORM\Column(name="value", type="decimal", precision=10, scale=7, nullable=false)
     * @Gedmo\Versioned
     */
    protected $value;

    /**
     * @var string *
     * @ORM\Column(name="currency", type="string", nullable=false)
     * @Gedmo\Versioned
     */
    protected  $currency;

    /**
     * @var string *
     * @ORM\Column(name="wallet", type="string", nullable=false)
     * @Gedmo\Versioned
     */
    protected $wallet;

    /**
     * @var boolean *
     * @ORM\Column(name="payed", type="boolean", nullable=false, options={"default":false})
     * @Gedmo\Versioned
     */
    protected $payed;

    /**
     * @var \DateTime $created
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var \DateTime $updated
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    /**
     * @var \DateTime $updated
     * @ORM\Column(name="payment_date", type="datetime", nullable=true)
     */
    protected $paymentDate;

    /**
     * @var integer
     * @ORM\Column(name="trials_number", type="integer", options={"default":0, "unsigned":true})
     */
    protected $trialsNumber;

    public function __construct()
    {
        $this->paymentOrderSuccessLogs = new ArrayCollection();
        $this->paymentOrderFailuresLogs = new ArrayCollection();
        $this->trialsNumber = 0;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param mixed $transaction
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return floatval($this->value);
    }

    /**
     * @param float $value
     */
    public function setValue(float $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getWallet(): string
    {
        return $this->wallet;
    }

    /**
     * @param string $wallet
     */
    public function setWallet(string $wallet)
    {
        $this->wallet = $wallet;
    }

    /**
     * @return boolean
     */
    public function isPayed(): bool
    {
        return $this->payed;
    }

    /**
     * @param boolean $payed
     */
    public function setPayed(bool $payed)
    {
        $this->payed = $payed;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return \DateTime
     */
    public function getPaymentDate(): \DateTime
    {
        return $this->paymentDate;
    }

    /**
     * @param \DateTime $paymentDate
     */
    public function setPaymentDate(\DateTime $paymentDate)
    {
        $this->paymentDate = $paymentDate;
    }

    /**
     * @return ArrayCollection
     */
    public function getPaymentOrderSuccessLogs()
    {
        return $this->paymentOrderSuccessLogs;
    }

    /**
     * @param $paymentOrder
     */
    public function setPaymentOrderSuccessLogs($paymentOrder)
    {
        $this->getPaymentOrderSuccessLogs()->add($paymentOrder);
    }

    /**
     * @return ArrayCollection
     */
    public function getPaymentOrderFailuresLogs()
    {
        return $this->paymentOrderFailuresLogs;
    }

    /**
     * @param $paymentOrder
     */
    public function setPaymentOrderFailuresLogs($paymentOrder)
    {
        $this->getPaymentOrderFailuresLogs()->add($paymentOrder);
    }

    /**
     * @return int
     */
    public function getTrialsNumber(): int
    {
        return $this->trialsNumber;
    }

    /**
     * @param int $trialsNumber
     */
    public function setTrialsNumber(int $trialsNumber)
    {
        $this->trialsNumber = $trialsNumber;
    }

    /**
     * Increment trials number
     */
    public function incrementTrialsNumber()
    {
        $this->setTrialsNumber($this->getTrialsNumber()+1);
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'value' => $this->getValue(),
            'currency' => $this->getCurrency(),
            'wallet' => $this->getWallet(),
            'payed' => $this->isPayed(),
            'created' => $this->getCreated()->format('Y-m-d H:m:s'),
            'updated' => $this->getUpdated()->format('Y-m-d H:m:s'),
            'trialsNumber' => $this->getTrialsNumber(),
            'transactionId' => $this->getTransaction()->getId(),
            'paymentDate' => ($this->paymentDate !== null ? $this->getPaymentDate()->format('Y-m-d H:m:s') : ''),
            'paymentOrderFailuresLogs' => $this->getPaymentOrderFailuresLogs()->toArray()
        ];
    }
}