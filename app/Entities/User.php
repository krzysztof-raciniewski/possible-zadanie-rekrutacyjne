<?php
declare(strict_types=1);

namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use DOctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use LaravelDoctrine\ORM\Auth\Authenticatable;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Users *
 * @ORM\Table(name="users")
 * @ORM\Entity
 * @Gedmo\Loggable
 */
class User implements \Illuminate\Contracts\Auth\Authenticatable, \JsonSerializable
{
    use Authenticatable;

    /**
     * @var integer *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string *
     * @ORM\Column(name="email", type="string", nullable=false, unique=true)
     * @Gedmo\Versioned
     */
    protected $email;

    /**
     * @var string *
     * @ORM\Column(name="first_name", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $first_name;

    /**
     * @var string *
     * @ORM\Column(name="last_name", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $last_name;

    /**
     * @var string *
     * @ORM\Column(name="image", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $image;

    /**
     * @OneToMany(targetEntity="SystemLog", mappedBy="user")
     */
    protected $systemLogs;

    /**
     * @var \DateTime $created
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    public function __construct()
    {
        $this->systemLogs = new ArrayCollection();
    }

    /**
     * @return int *
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     */
    public function setFirstName(string $first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     */
    public function setLastName(string $last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @return mixed
     */
    public function getSystemLogs()
    {
        return $this->systemLogs;
    }

    /**
     * @param mixed $systemLogs
     */
    public function setSystemLogs($systemLogs)
    {
        $this->systemLogs = $systemLogs;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image)
    {
        $this->image = $image;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'image' => $this->image
        ];
    }
}