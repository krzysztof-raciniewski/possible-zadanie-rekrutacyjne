<?php
declare(strict_types=1);

namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use DOctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToMany;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Operator *
 * @ORM\Table(name="payment_orders_logs")
 * @ORM\Entity
 * @Gedmo\Loggable
 */
class PaymentOrderLog implements \JsonSerializable
{
    /**
     * @var integer *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var PaymentOrder
     * @ManyToMany(targetEntity="PaymentOrder", mappedBy="paymentOrderSuccessLogs")
     */
    protected $paymentOrdersSuccesed;

    /**
     * @var PaymentOrder
     * @ManyToMany(targetEntity="PaymentOrder", mappedBy="paymentOrderFailuresLogs")
     */
    protected $paymentOrdersFailures;

    /**
     * @var string
     * @ORM\Column(name="message", type="string", nullable=true);
     */
    protected $message;

    /**
     * @var \DateTime $created
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var \DateTime $updated
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    /**
     * PaymentOrderLog constructor.
     */
    public function __construct()
    {
        $this->paymentOrdersSuccesed = new ArrayCollection();
        $this->paymentOrdersFailures = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getPaymentOrdersSuccesed(): ArrayCollection
    {
        return $this->paymentOrdersSuccesed;
    }

    /**
     * @param PaymentOrder $paymentOrder
     */
    public function setPaymentOrdersSuccesed(PaymentOrder $paymentOrder)
    {
        $this->getPaymentOrdersSuccesed()->add($paymentOrder);
    }

    /**
     * @return ArrayCollection
     */
    public function getPaymentOrdersFailures(): ArrayCollection
    {
        return $this->paymentOrdersFailures;
    }

    /**
     * @param PaymentOrder $paymentOrder
     */
    public function setPaymentOrdersFailures(PaymentOrder $paymentOrder)
    {
        $this->getPaymentOrdersFailures()->add($paymentOrder);
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }


    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'message' => $this->getMessage(),
            'created' => $this->getCreated()->format('Y-m-d H:i:s'),
            'updated' => $this->getUpdated()->format('Y-m-d H:i:s')
        ];
    }
}