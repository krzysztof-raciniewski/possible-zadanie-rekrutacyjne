<?php
declare(strict_types=1);

namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use DOctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Operator *
 * @ORM\Table(name="operators")
 * @ORM\Entity
 * @Gedmo\Loggable
 */
class Operator implements \JsonSerializable
{

    public function __construct()
    {
        $this->textMessages = new ArrayCollection();
    }

    /**
     * @var integer *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string *
     * @ORM\Column(name="name", type="string", nullable=true, unique=true)
     * @Gedmo\Versioned
     */
    protected $name;

    /**
     * @ORM\ManyToMany(targetEntity="Sms", mappedBy="operators")
     */
    protected $textMessages;

    /**
     * @var \DateTime $created
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var \DateTime $updated
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getTextMessages()
    {
        return $this->textMessages;
    }

    /**
     * @param mixed $textMessages
     */
    public function setTextMessages($textMessages)
    {
        $this->textMessages = $textMessages;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }



    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->getName(),
            'updated' => $this->getUpdated() instanceof \DateTime ? $this->getUpdated()->format('Y:m:d H:i:s') : '',
            'created' => $this->getCreated() instanceof  \DateTime ? $this->getCreated()->format('Y:m:d H:i:s') : '',
            'sms_assignments' => $this->getTextMessages()->count()
        ];
    }
}