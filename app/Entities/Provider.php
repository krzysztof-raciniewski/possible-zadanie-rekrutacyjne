<?php
declare(strict_types = 1);

namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use DOctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Provider *
 * @ORM\Table(name="providers")
 * @ORM\Entity
 * @Gedmo\Loggable
 */
class Provider implements  \JsonSerializable
{

    /**
     *
     * @var integer *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     *
     * @var string *
     * @ORM\Column(name="name", type="string", nullable=false, unique=true)
     * @Gedmo\Versioned
     */
    protected $name;

    /**
     * @var string *
     * @ORM\Column(name="www", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $www;

    /**
     * @var string *
     * @ORM\Column(name="image", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $image;

    /**
     * @var string *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected $description;

    /**
     * @var string *
     * @ORM\Column(name="secret", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $secret;

    /**
     * @var string
     * @ORM\Column(name="rules_url", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $rules_url;

    /**
     * @var
     * @ORM\Column(name="complaint_form_url", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $complaint_form_url;

    /**
     * @var
     * @ORM\Column(name="public_provider_information", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected $public_provider_information;

    /**
     * @OneToMany(targetEntity="ProviderConfiguration", mappedBy="provider")
     */
    private $configurations;

    /**
     * @var boolean *
     * @ORM\Column(name="configured", type="boolean", nullable=false, options={"default": false})
     * @Gedmo\Versioned
     */
    private $configured;

    /**
     * @var boolean *
     * @ORM\Column(name="enabled", type="boolean", nullable=false, options={"default": false})
     * @Gedmo\Versioned
     */
    private $enabled;

    /**
     * @var boolean *
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default": false})
     * @Gedmo\Versioned
     */
    private $active;

    /**
     * @var \DateTime $created
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var \DateTime $updated
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    /**
     * @var \DateTime $last_active
     * @ORM\Column(name="last_active", type="datetime", nullable=true)
     */
    protected $last_active;

    public function __construct()
    {
        $this->configurations = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getWww()
    {
        return $this->www;
    }

    /**
     * @param string $www
     */
    public function setWww($www)
    {
        $this->www = $www;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getConfiguration()
    {
        return $this->configurations;
    }

    /**
     * @param mixed $configuration
     */
    public function setConfiguration($configuration)
    {
        $this->configurations = $configuration;
    }

    /**
     * @return boolean
     */
    public function isConfigured()
    {
        return $this->configured;
    }

    /**
     * @param boolean $configured
     * @return bool
     */
    public function setConfigured($configured)
    {
        $this->configured = $configured;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getPublicProviderInformation()
    {
        return $this->public_provider_information;
    }

    /**
     * @param mixed $public_provider_information
     */
    public function setPublicProviderInformation($public_provider_information)
    {
        $this->public_provider_information = $public_provider_information;
    }

    /**
     * @return mixed
     */
    public function getComplaintFormUrl()
    {
        return $this->complaint_form_url;
    }

    /**
     * @param mixed $complaint_form_url
     */
    public function setComplaintFormUrl($complaint_form_url)
    {
        $this->complaint_form_url = $complaint_form_url;
    }

    /**
     * @return mixed
     */
    public function getRulesUrl()
    {
        return $this->rules_url;
    }

    /**
     * @param string $rules_url
     */
    public function setRulesUrl($rules_url)
    {
        $this->rules_url = $rules_url;
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param string $secret
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
    }

    /**
     * @return \DateTime
     */
    public function getLastActive()
    {
        return $this->last_active;
    }

    /**
     *
     */
    public function setLastActiveNow()
    {
        $this->last_active = new \DateTime("now");
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'www' => $this->getWww(),
            'image' => $this->getImage(),
            'description' => $this->getDescription(),
            'secret' => $this->getSecret(),
            'rules_url' => $this->getRulesUrl(),
            'complaint_from_url' => $this->getComplaintFormUrl(),
            'public_provider_information' => $this->getPublicProviderInformation(),
            'configured' => $this->isConfigured(),
            'enabled' => $this->isEnabled(),
            'active' => $this->isActive(),
            'created' => $this->created->format('Y-m-d H:i:s'),
            'updated' => $this->updated->format('Y-m-d H:i:s'),
            'last_active' => $this->last_active !== null ? $this->last_active->format('Y-m-d H:i:s') : ''
        ];
    }
}