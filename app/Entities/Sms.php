<?php
declare(strict_types = 1);

namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use DOctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Provider *
 * @ORM\Table(name="sms_definitions")
 * @ORM\Entity
 * @Gedmo\Loggable
 */
class Sms implements \JsonSerializable
{

    /**
     * @var integer *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Gedmo\Versioned
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="ProviderConfiguration", inversedBy="sms")
     * @JoinColumn(name="provider_configuration_id", referencedColumnName="id")
     */
    private $providerConfiguration;

    /**
     * @var string
     * @ORM\Column(name="la_number", type="string", nullable=false)
     * @Gedmo\Versioned
     */
    private $laNumber;

    /**
     * @var string
     * @ORM\Column(name="command", type="string", nullable=false)
     * @Gedmo\Versioned
     */
    private $command;

    /**
     * @var string
     * @ORM\Column(name="token", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    private $token;

    /**
     * @var string
     * @ORM\Column(name="service_id", type="string", nullable=false)
     * @Gedmo\Versioned
     */
    private $serviceId;

    /**
     * @var boolean
     * @ORM\Column(name="testing", type="boolean", nullable=false, options={"default"=true})
     * @Gedmo\Versioned
     */
    private $testing;

    /**
     * @var float
     * @ORM\Column(name="net_price", type="decimal", precision=5, scale=2, nullable=false)
     * @Gedmo\Versioned
     */
    private $netPrice;

    /**
     * @var string
     * @ORM\Column(name="gross_price", type="decimal", precision=5, scale=2, nullable=false)
     * @Gedmo\Versioned
     */
    private $grossPrice;

    /**
     * @var string
     * @ORM\Column(name="brokerage", type="decimal", precision=5, scale=2, nullable=false)
     * @Gedmo\Versioned
     */
    private $brokerage;

    /**
     * @var boolean
     * @ORM\Column(name="enabled", type="boolean", nullable=false, options={"default"=false})
     * @Gedmo\Versioned
     */
    private $enabled;

    /**
     * @ManyToOne(targetEntity="Country", inversedBy="textMessages")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;

    /**
     * @ManyToOne(targetEntity="Currency", inversedBy="textMessages")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     */
    private $currency;

    /**
     * @ORM\ManyToMany(targetEntity="Operator", inversedBy="textMessages")
     * @ORM\JoinTable(name="sms_operators")
     */
    private $operators;

    /**
     * @var \DateTime $created
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    public function __construct()
    {
        $this->operators = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProviderConfiguration()
    {
        return $this->providerConfiguration;
    }

    /**
     * @param mixed $providerConfiguration
     */
    public function setProviderConfiguration($providerConfiguration)
    {
        $this->providerConfiguration = $providerConfiguration;
    }

    /**
     * @return mixed
     */
    public function getLaNumber()
    {
        return $this->laNumber;
    }

    /**
     * @param mixed $laNumber
     */
    public function setLaNumber($laNumber)
    {
        $this->laNumber = $laNumber;
    }

    /**
     * @return string
     */
    public function getCommand(): string
    {
        return $this->command;
    }

    /**
     * @param string $command
     */
    public function setCommand(string $command)
    {
        $this->command = $command;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token !== null ? $this->token : '';
    }

    /**
     * @param string $token
     */
    public function setToken(string $token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getServiceId(): string
    {
        return $this->serviceId !== null ? $this->serviceId : '';
    }

    /**
     * @param string $serviceId
     */
    public function setServiceId(string $serviceId)
    {
        $this->serviceId = $serviceId;
    }

    /**
     * @return boolean
     */
    public function isTesting(): bool
    {
        return $this->testing !== null ? $this->testing : false;
    }

    /**
     * @param boolean $testing
     */
    public function setTesting(bool $testing)
    {
        $this->testing = $testing;
    }

    /**
     * @return string
     */
    public function getNetPrice(): string
    {
        return (string)$this->netPrice;
    }

    /**
     * @param float $netPrice
     */
    public function setNetPrice(float $netPrice)
    {
        $this->netPrice = $netPrice;
    }

    /**
     * @return string
     */
    public function getGrossPrice(): string
    {
        return $this->grossPrice;
    }

    /**
     * @param string $grossPrice
     */
    public function setGrossPrice(string $grossPrice)
    {
        $this->grossPrice = $grossPrice;
    }

    /**
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     * @param Currency $currency
     */
    public function setCurrency(Currency $currency)
    {
        $this->currency = $currency;
    }


    /**
     * @return boolean
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;
    }

    /**
     * @return ArrayCollection
     */
    public function getOperators()
    {
        return $this->operators;
    }

    /**
     * @param mixed $operators
     */
    public function setOperators($operators)
    {
        $this->operators = $operators;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        $result = [];

        !$this->id          ?: $result['id']            = $this->getId();
        !$this->laNumber    ?: $result['la_number']     = $this->getLaNumber();
        !$this->command     ?: $result['command']       = $this->getCommand();
        !$this->token       ?: $result['token']         = $this->getToken();
        !$this->serviceId  ?: $result['service_id']    = $this->getServiceId();
        !$this->country     ?: $result['country']       = $this->getCountry()->getCode();
        !$this->currency    ?: $result['currency']      = $this->getCurrency()->getCode();
        !$this->brokerage    ?: $result['brokerage']      = $this->getBrokerage();
        !$this->operators   ?: $result['operators']     = $this->getOperators()->toArray();
        !$this->testing     ?: $result['testing']       = $this->isTesting();
        !$this->netPrice   ?: $result['net_price']     = $this->getNetPrice();
        !$this->grossPrice ?: $result['gross_price']   = $this->getGrossPrice();
        !$this->enabled     ?: $result['enabled']       = $this->isEnabled();
        !$this->created     ?: $result['created']       = $this->getCreated()->format('Y-m-d H:i:s');
        !$this->updated     ?: $result['updated']       = $this->getUpdated()->format('Y-m-d H:i:s');

        return $result;
    }

    /**
     * @return float
     */
    public function getBrokerage(): float
    {
        return (float)$this->brokerage;
    }

    /**
     * @param string $brokerage
     */
    public function setBrokerage(string $brokerage)
    {
        $this->brokerage = $brokerage;
    }
}