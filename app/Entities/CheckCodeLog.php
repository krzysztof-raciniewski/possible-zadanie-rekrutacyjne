<?php
declare(strict_types = 1);

namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use DOctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * CheckCodeLog *
 * @ORM\Table(name="check_code_logs")
 * @ORM\Entity
 * @Gedmo\Loggable
 */
class CheckCodeLog implements \JsonSerializable
{
    /**
     *
     * @var integer *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Gedmo\Versioned
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Transaction", inversedBy="checkCodeLogs", cascade={"persist"})
     * @JoinColumn(name="transaction_id", referencedColumnName="id")
     */
    private $transaction;

    /**
     * @var string
     * @ORM\Column(name="code", type="string", nullable=false)
     */
    private $code;

    /**
     * @var boolean
     * @ORM\Column(name="is_Valid", type="boolean", nullable=false)
     */
    private $valid;

    /**
     * @var string
     * @ORM\Column(name="ip_address", type="string", nullable=false)
     */
    private $ipAddress;

    /**
     * @var \DateTime $created
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param mixed $transaction
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }

    /**
     * @return boolean
     */
    public function isValid(): bool
    {
        return $this->valid;
    }

    /**
     * @param boolean $valid
     */
    public function setValid(bool $valid)
    {
        $this->valid = $valid;
    }

    /**
     * @return string
     */
    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     */
    public function setIpAddress(string $ipAddress)
    {
        $this->ipAddress = $ipAddress;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'code' => $this->getCode(),
            'is_valid' => $this->isValid(),
            'ip_address' => $this->getIpAddress(),
            'created' => $this->getCreated()->format('Y-m-d H:i:s')
        ];
    }
}