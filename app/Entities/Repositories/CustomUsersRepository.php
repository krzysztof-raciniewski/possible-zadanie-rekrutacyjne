<?php
declare(strict_types=1);

namespace App\Entities\Repositories;


use App\Entities\Repositories\RepositoriesInterfaces\UsersRepository;
use Doctrine\ORM\EntityRepository;

/**
 * Class CustomUsersRepository
 * @package App\Entities\Repositories
 */
class CustomUsersRepository extends EntityRepository implements UsersRepository
{

}