<?php
declare(strict_types = 1);

namespace App\Entities\Repositories;

use App\Entities\Country;
use App\Entities\Operator;
use App\Entities\Provider;
use App\Entities\Repositories\RepositoriesInterfaces\SmsRepository;
use App\Entities\Sms;
use App\Entities\Transaction;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Class CustomSmsRepository
 * @package App\Entities\Repositories
 */
class CustomSmsRepository extends EntityRepository implements SmsRepository
{

    public function getAllAvailable(Transaction $transaction)
    {
        $countriesRepository = $this->getEntityManager()->getRepository(Country::class);
        $operatorsRepository = $this->getEntityManager()->getRepository(Operator::class);
        $providersRepository = $this->getEntityManager()->getRepository(Provider::class);

        $operator = $operatorsRepository->findOneByName($transaction->getOperator());
        $country = $countriesRepository->findOneByCode($transaction->getCountry());
        $provider = $providersRepository->findOneByName($transaction->getProvider());

        $rsm = new ResultSetMapping;
        $rsm->addEntityResult(Sms::class, 'ss');
        $rsm->addFieldResult('ss', 'id', 'id');
        $rsm->addFieldResult('ss', 'la_number', 'laNumber');
        $rsm->addFieldResult('ss', 'command', 'command');
        $rsm->addFieldResult('ss', 'net_price', 'netPrice');
        $rsm->addFieldResult('ss', 'gross_price', 'grossPrice');
        $rsm->addMetaResult('ss', 'currency_id', 'currency_id');

        $entityManager = $this->getEntityManager();
        $query = $entityManager->createNativeQuery('
            SELECT
              ss.id, p.id as provider_id, ss.country_id, ss.command, ss.currency_id, ss.gross_price, ss.net_price, ss.la_number, ss.command
            FROM sms_definitions ss LEFT JOIN providers_configurations pc ON ss.provider_configuration_id = pc.id LEFT JOIN providers p ON pc.provider_id = p.id
            WHERE p.id = ? AND ss.country_id = ? AND ss.enabled = 1 AND ss.id IN (SELECT so.sms_id FROM sms_operators so WHERE so.operator_id = ?) 
            GROUP BY ss.id
            ORDER BY ss.net_price DESC;
        ', $rsm);
        $query->setParameter(1, $provider->getId());
        $query->setParameter(2, $country->getId());
        $query->setParameter(3, $operator->getId());
        $results = $query->getResult();

        return $results;
    }
}