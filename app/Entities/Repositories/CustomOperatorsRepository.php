<?php
declare(strict_types = 1);

namespace App\Entities\Repositories;


use App\Entities\Country;
use App\Entities\Operator;
use App\Entities\Repositories\RepositoriesInterfaces\OperatorsRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Class CustomOperatorsRepository
 * @package App\Entities\Repositories
 */
class CustomOperatorsRepository extends EntityRepository implements OperatorsRepository
{
    /**
     * @return array
     */
    public function getAllIds(): array
    {
        $outputArray = [];
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('o.id')
            ->from(Operator::class, 'o')
            ->orderBy('o.id', 'ASC');

        $query = $qb->getQuery();
        $results = $query->getResult(\Doctrine\ORM\Query::HYDRATE_SCALAR);
        foreach ($results as $result) {
            $outputArray[] = $result['id'];
        }
        return $outputArray;
    }

    /**
     * @param array $ids
     * @return \App\Entities\Operator[]|array
     */
    public function findAllIds(array $ids): array
    {
        return parent::findBy(['id' => $ids]);
    }

    public function getAvailableOperatorsForCountry(Country $country)
    {
        $rsm = new ResultSetMapping;
        $rsm->addEntityResult(Operator::class, 'c');
        $rsm->addFieldResult('c', 'id', 'id');
        $rsm->addFieldResult('c', 'name', 'name');

        $entityManager = $this->getEntityManager();
        $query = $entityManager->createNativeQuery('
                SELECT
                  o.id,
                  o.name
                FROM operators o
                WHERE o.id IN (SELECT s.operator_id
                               FROM sms_operators s
                               WHERE s.sms_id IN (SELECT ss.id
                                                  FROM sms_definitions ss
                                                  WHERE ss.country_id = ? AND ss.enabled = TRUE))
                               GROUP BY o.id;
        ', $rsm);
        $query->setParameter(1, $country->getId());
        $results = $query->getResult();

        return $results;
    }
}