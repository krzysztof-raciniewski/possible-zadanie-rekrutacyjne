<?php
declare(strict_types=1);

namespace App\Entities\Repositories;


use App\Entities\Repositories\RepositoriesInterfaces\PaymentLogsRepository;
use Doctrine\ORM\EntityRepository;

/**
 * Class CustomPaymentReportsRepository
 * @package App\Entities\Repositories
 */
class CustomPaymentReportsRepository extends EntityRepository implements PaymentLogsRepository
{

}