<?php

namespace App\Entities\Repositories\RepositoriesInterfaces;
use App\Entities\Transaction;

/**
 * Interface SmsRepository
 * @package App\Entities\Repositories\RepositoriesInterfaces
 */
interface SmsRepository
{
    public function getAllAvailable(Transaction $transaction);
}