<?php

namespace App\Entities\Repositories\RepositoriesInterfaces;
use App\Entities\Country;

/**
 * Interface OperatorsRepository
 * @package App\Entities\Repositories\RepositoriesInterfaces
 */
interface OperatorsRepository
{
    public function getAllIds(): array;
    public function findAllIds(array $ids): array;
    public function getAvailableOperatorsForCountry(Country $country);

}