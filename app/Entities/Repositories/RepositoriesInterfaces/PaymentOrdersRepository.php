<?php

namespace App\Entities\Repositories\RepositoriesInterfaces;

/**
 * Interface PaymentOrdersRepository
 * @package App\Entities\Repositories\RepositoriesInterfaces
 */
interface PaymentOrdersRepository
{
    /**
     * @param $start
     * @param $length
     * @param array $search
     * @param array $columns
     * @param array $order
     * @param bool $getAll
     * @return mixed
     */
    public function getPaymentsPage($start, $length, array $search, array $columns, array $order, bool $getAll = false);

    /**
     * @return mixed
     */
    public function getPaymentsCount();

    /**
     * @param $search
     * @param $columns
     * @param $order
     * @return mixed
     */
    public function getPayments($search, $columns, $order);

    /**
     * @return mixed
     */
    public function getUnsuccessfulPaymentsCount();

    /**
     * @param $count
     * @return mixed
     */
    public function getLast($count);

    /**
     * @param $count
     * @return mixed
     */
    public function getLastCompleted($count);
}