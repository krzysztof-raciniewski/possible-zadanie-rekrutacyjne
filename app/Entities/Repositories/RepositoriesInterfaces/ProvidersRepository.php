<?php

namespace App\Entities\Repositories\RepositoriesInterfaces;
use App\Entities\Country;
use App\Entities\Operator;

/**
 * Interface ProvidersRepository
 * @package App\Entities\Repositories\RepositoriesInterfaces
 */
interface ProvidersRepository
{
    /**
     * @param array $columns
     * @return mixed
     */
    public function findAll($columns = []);

    /**
     * @param Country $country
     * @param Operator $operator
     * @return array
     */
    public function findEnabledProvidersWithCountryAndOperator(Country $country, Operator $operator): array;
}