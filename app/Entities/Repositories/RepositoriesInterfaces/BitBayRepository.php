<?php

namespace App\Entities\Repositories\RepositoriesInterfaces;
use App\Api\Bitbay\BitbayExceptions\BitBayNotConfiguredException;
use App\Entities\BitBay;

/**
 * Interface BitBayRepository
 * @package App\Entities\Repositories\RepositoriesInterfaces
 */
interface BitBayRepository
{
    /**
     * Get bitbay configuration object
     * @return BitBay
     * @throws BitBayNotConfiguredException
     */
    public function getConfiguration(): BitBay;
}