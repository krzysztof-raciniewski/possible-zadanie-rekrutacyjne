<?php

namespace App\Entities\Repositories\RepositoriesInterfaces;

/**
 * Interface TransactionsRepository
 * @package App\Entities\Repositories\RepositoriesInterfaces
 */
interface TransactionsRepository
{
    /**
     * @param $start
     * @param $length
     * @param array $search
     * @param array $columns
     * @param array $order
     * @param bool $getAll
     * @return mixed
     */
    public function getTransactionsPage($start, $length, array $search, array $columns, array $order, bool $getAll = false);

    /**
     * @return mixed
     */
    public function getTransactionsCount();

    /**
     * @param $search
     * @param $columns
     * @param $order
     * @return mixed
     */
    public function getTransactions($search, $columns, $order);

    /**
     * @param $count
     * @return mixed
     */
    public function getLast($count);

    /**
     * @param $count
     * @return mixed
     */
    public function getLastCompleted($count);
}