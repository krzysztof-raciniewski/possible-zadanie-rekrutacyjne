<?php

namespace App\Entities\Repositories\RepositoriesInterfaces;

/**
 * Interface CountriesRepository
 * @package App\Entities\Repositories\RepositoriesInterfaces
 */
interface CountriesRepository
{
    public function getAvailableCountries();
}