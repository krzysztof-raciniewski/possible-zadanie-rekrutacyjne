<?php
declare(strict_types=1);

namespace App\Entities\Repositories;


use App\Entities\Repositories\RepositoriesInterfaces\PaymentLogsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\PaymentOrdersRepository;
use App\Entities\Repositories\RepositoriesInterfaces\SystemSettingsRepository;
use Doctrine\ORM\EntityRepository;

/**
 * Class CustomPaymentOrdersRepository
 * @package App\Entities\Repositories
 */
class CustomPaymentOrdersRepository extends EntityRepository implements PaymentOrdersRepository
{
    /**
     * @param $start
     * @param $length
     * @param array $search
     * @param array $columns
     * @param array $order
     * @param bool $getAll
     * @return mixed
     */
    public function getPaymentsPage($start, $length, array $search, array $columns, array $order, bool $getAll = false)
    {
        $pattern = $search['value'];
        $parameters = [];
        if (count($order) > 0) {
            $orderColumn = $columns[$order[0]['column']]['data'];
            $orderType = $order[0]['dir'];
        }

        $dql = "SELECT p FROM \App\Entities\PaymentOrder p ";
        if ($pattern !== '') {
            $parameters['pattern1'] = '%' . $pattern . '%';
            $parameters['pattern2'] = $pattern . '%';
            $parameters['pattern3'] = '%' . $pattern;

            $counter = 0;
            foreach($columns as $column) {
                if($column['searchable'] === 'true') {
                    $counter++;
                    if ($counter === 1) {
                        $dql .= "WHERE p.".$column['data']." LIKE :pattern1 OR p.".$column['data']." LIKE :pattern2 OR p.".$column['data']." LIKE :pattern3 ";
                    } else {
                        $dql .= "OR p.".$column['data']." LIKE :pattern1 OR p.".$column['data']." LIKE :pattern2 OR p.".$column['data']." LIKE :pattern3 ";
                    }
                }
            }
        }

        if (isset($orderColumn) && isset($orderType)) {
            $dql .= "ORDER BY p." . $orderColumn . " " . $orderType;
        }

        $query = $this->getEntityManager()->createQuery($dql);

        if (!$getAll) {
            $query->setFirstResult($start);
            $query->setMaxResults($length);
        }

        $query->setParameters($parameters);
        return $query->getResult();
    }

    /**
     * @return mixed
     */
    public function getPaymentsCount()
    {
        $dql = "SELECT COUNT(p.id) AS pcount FROM \App\Entities\PaymentOrder p";
        $query = $this->getEntityManager()->createQuery($dql);

        $result = $query->getResult();
        return $result[0]['pcount'];
    }

    /**
     * @return mixed
     */
    public function getUnsuccessfulPaymentsCount()
    {
        $dql = "SELECT COUNT(p.id) AS pcount FROM \App\Entities\PaymentOrder p WHERE p.payed = false AND p.trialsNumber > 0";
        $query = $this->getEntityManager()->createQuery($dql);

        $result = $query->getResult();
        return $result[0]['pcount'];
    }

    /**
     * @param $count
     * @return mixed
     */
    public function getLast($count)
    {
        $dql = "SELECT p FROM \App\Entities\PaymentOrder p ORDER BY p.created DESC";
        $query = $this->getEntityManager()
            ->createQuery($dql)
            ->setMaxResults($count);

        return $query->getResult();
    }

    /**
     * @param $count
     * @return mixed
     */
    public function getLastCompleted($count)
    {
        $dql = "SELECT p FROM \App\Entities\PaymentOrder p WHERE p.payed = TRUE ORDER BY p.created DESC";
        $query = $this->getEntityManager()
            ->createQuery($dql)
            ->setMaxResults($count);

        return $query->getResult();
    }

    /**
     * @param $search
     * @param $columns
     * @param $order
     * @return mixed
     */
    public function getPayments($search, $columns, $order)
    {
        return $this->getPaymentsPage(0, 0, $search, $columns, $order, true);
    }
}