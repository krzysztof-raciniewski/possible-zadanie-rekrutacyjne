<?php
declare(strict_types=1);

namespace App\Entities\Repositories;


use App\Entities\Country;
use App\Entities\Operator;
use App\Entities\Provider;
use App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Class CustomProviderRepository
 * @package App\Entities\Repositories
 */
class CustomProviderRepository extends EntityRepository implements ProvidersRepository
{
    /**
     * Get enabled, configured and active provider
     * @return Provider
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function findFirstActive() {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb -> select('p')
            ->from(Provider::class, 'p')
            ->where('p.active = true')
            ->orderBy('p.name', 'ASC');

        $query = $qb->getQuery();
        return $query->getSingleResult();
    }

    /**
     * @return Provider[]
     */
//    public function findAllConfiguredAndEnabled() {
//        $qb = $this->getEntityManager()->createQueryBuilder();
//        $qb -> select('p')
//            ->from(Provider::class, 'p')
//            ->where($qb->expr()->andX(
//                $qb->expr()->eq('p.configured', true),
//                $qb->expr()->eq('p.enabled', true)
//            ))
//            ->orderBy('p.name', 'ASC');
//
//        $query = $qb->getQuery();
//        return $query->getResult();
//    }

    /**
     * @param bool $status
     * @return Provider[]
     */
    public function findByActive(bool $status) {
        return parent::findByActive($status);
    }

    public function findAll($columns = []) {
        if(count($columns)>0) {
            foreach($columns as &$column) {
                $column = 'p.'.$column;
            }
        } else {
            return parent::findAll();
        }

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb -> select($columns)
            ->from(Provider::class, 'p')
            ->orderBy('p.id', 'ASC');

        $query = $qb->getQuery();
        return $query->getResult(\Doctrine\ORM\Query::HYDRATE_SCALAR);
    }

    /**
     * @param Country $country
     * @param Operator $operator
     * @return array
     */
    public function findEnabledProvidersWithCountryAndOperator(Country $country, Operator $operator): array {

        $rsm = new ResultSetMapping;
        $rsm->addEntityResult(Provider::class, 'p');
        $rsm->addFieldResult('p', 'id', 'id');
        $rsm->addFieldResult('p', 'name', 'name');

        $entityManager = $this->getEntityManager();
        $query = $entityManager->createNativeQuery('
                SELECT
                  p.*
                FROM sms_definitions ss LEFT JOIN providers_configurations pc ON ss.provider_configuration_id = pc.id LEFT JOIN providers p ON pc.provider_id = p.id
                WHERE p.enabled = TRUE AND ss.country_id = ? AND ss.enabled = 1 AND ss.id IN (SELECT so.sms_id FROM sms_operators so WHERE so.operator_id = ?)
                GROUP BY p.id'
            , $rsm);
        $query->setParameter(1, $country->getId());
        $query->setParameter(2, $operator->getId());
        $results = $query->getResult();

        return $results;
    }
}