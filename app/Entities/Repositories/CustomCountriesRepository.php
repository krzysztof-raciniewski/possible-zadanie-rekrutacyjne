<?php
declare(strict_types=1);

namespace App\Entities\Repositories;


use App\Entities\Country;
use App\Entities\Repositories\RepositoriesInterfaces\CountriesRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Class CountriesRepository
 * @package App\Entities\Repositories
 */
class CustomCountriesRepository extends EntityRepository implements CountriesRepository
{

    /**
     * Get only available countries for client
     * @return array
     */
    public function getAvailableCountries()
    {
        $rsm = new ResultSetMapping;
        $rsm->addEntityResult(Country::class, 'c');
        $rsm->addFieldResult('c', 'id', 'id');
        $rsm->addFieldResult('c', 'code', 'code');
        $rsm->addFieldResult('c', 'name', 'name');

        $entityManager = $this->getEntityManager();
        $query = $entityManager->createNativeQuery('
            SELECT c.id, c.code, c.name 
            FROM countries c 
            WHERE c.id IN (
              SELECT s.country_id FROM sms_definitions s WHERE s.enabled = TRUE GROUP BY s.country_id)', $rsm
            );
        $results = $query->getResult();

        return $results;
    }
}