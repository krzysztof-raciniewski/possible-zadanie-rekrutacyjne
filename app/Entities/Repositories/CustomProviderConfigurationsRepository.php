<?php
declare(strict_types=1);

namespace App\Entities\Repositories;


use App\Entities\Repositories\RepositoriesInterfaces\ProviderConfigurationsRepository;
use Doctrine\ORM\EntityRepository;

/**
 * Class ProviderConfigurationsRepository
 * @package App\Entities\Repositories
 */
class CustomProviderConfigurationsRepository extends EntityRepository implements ProviderConfigurationsRepository
{

}