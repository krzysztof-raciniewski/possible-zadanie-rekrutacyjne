<?php
declare(strict_types = 1);

namespace App\Entities\Repositories;


use App\Entities\Repositories\RepositoriesInterfaces\TransactionsRepository;
use Doctrine\ORM\EntityRepository;

/**
 * Class CustomTransactionsRepository
 * @package App\Entities\Repositories
 */
class CustomTransactionsRepository extends EntityRepository implements TransactionsRepository
{

    /**
     * @param $start
     * @param $length
     * @param array $search
     * @param array $columns
     * @param array $order
     * @param bool $getAll
     * @return mixed
     */
    public function getTransactionsPage($start, $length, array $search, array $columns, array $order, bool $getAll = false)
    {
        $pattern = $search['value'];
        $parameters = [];
        if (count($order) > 0) {
            $orderColumn = $columns[$order[0]['column']]['data'];
            $orderType = $order[0]['dir'];
        }

        $dql = "SELECT t FROM \App\Entities\TRANSACTION t ";
        if ($pattern !== '') {
            $parameters['pattern1'] = '%' . $pattern . '%';
            $parameters['pattern2'] = $pattern . '%';
            $parameters['pattern3'] = '%' . $pattern;

            $counter = 0;
            foreach ($columns as $column) {
                if ($column['searchable'] === 'true') {
                    $counter++;
                    if ($counter === 1) {
                        $dql .= "WHERE t." . $column['data'] . " LIKE :pattern1 OR t." . $column['data'] . " LIKE :pattern2 OR t." . $column['data'] . " LIKE :pattern3 ";
                    } else {
                        $dql .= "OR t." . $column['data'] . " LIKE :pattern1 OR t." . $column['data'] . " LIKE :pattern2 OR t." . $column['data'] . " LIKE :pattern3 ";
                    }
                }
            }
        }

        if (isset($orderColumn) && isset($orderType)) {
            $dql .= "ORDER BY t." . $orderColumn . " " . $orderType;
        }

        $query = $this->getEntityManager()->createQuery($dql);

        if (!$getAll) {
            $query->setFirstResult($start);
            $query->setMaxResults($length);
        }

        $query->setParameters($parameters);
        return $query->getResult();
    }

    /**
     * @return mixed
     */
    public function getTransactionsCount()
    {
        $dql = "SELECT COUNT(t.id) AS tcount FROM \App\Entities\TRANSACTION t";
        $query = $this->getEntityManager()->createQuery($dql);

        $result = $query->getResult();
        return $result[0]['tcount'];
    }

    /**
     * @param $search
     * @param $columns
     * @param $order
     * @return mixed
     */
    public function getTransactions($search, $columns, $order)
    {
        return $this->getTransactionsPage(0, 0, $search, $columns, $order, true);
    }

    /**
     * @param $count
     * @return mixed
     */
    public function getLast($count)
    {
        $dql = "SELECT t FROM \App\Entities\Transaction t ORDER BY t.created DESC";
        $query = $this->getEntityManager()
            ->createQuery($dql)
            ->setMaxResults($count);

        return $query->getResult();
    }

    /**
     * @param $count
     * @return mixed
     */
    public function getLastCompleted($count)
    {
        $dql = "SELECT t FROM \App\Entities\Transaction t LEFT JOIN t.paymentOrder p WHERE p.payed = TRUE ORDER BY t.created DESC";
        $query = $this->getEntityManager()
            ->createQuery($dql)
            ->setMaxResults($count);

        return $query->getResult();
    }
}