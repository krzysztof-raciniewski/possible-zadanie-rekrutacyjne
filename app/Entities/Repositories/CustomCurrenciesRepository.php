<?php
declare(strict_types=1);

namespace App\Entities\Repositories;


use App\Entities\Repositories\RepositoriesInterfaces\CountriesRepository;
use App\Entities\Repositories\RepositoriesInterfaces\CurrenciesRepository;
use Doctrine\ORM\EntityRepository;

/**
 * Class CountriesRepository
 * @package App\Entities\Repositories
 */
class CustomCurrenciesRepository extends EntityRepository implements CurrenciesRepository
{

}