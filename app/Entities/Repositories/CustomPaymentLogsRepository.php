<?php
declare(strict_types=1);

namespace App\Entities\Repositories;


use App\Entities\Repositories\RepositoriesInterfaces\PaymentLogsRepository;
use Doctrine\ORM\EntityRepository;

/**
 * Class CustomPaymentLogsRepository
 * @package App\Entities\Repositories
 */
class CustomPaymentLogsRepository extends EntityRepository implements PaymentLogsRepository
{

}