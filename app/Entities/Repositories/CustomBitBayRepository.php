<?php
declare(strict_types=1);

namespace App\Entities\Repositories;


use App\Api\Bitbay\BitbayExceptions\BitBayNotConfiguredException;
use App\Entities\BitBay;
use App\Entities\Repositories\RepositoriesInterfaces\BitBayRepository;
use Doctrine\ORM\EntityRepository;

/**
 * Class CustomBitBayRepository
 * @package App\Entities\Repositories
 */
class CustomBitBayRepository extends EntityRepository implements BitBayRepository
{
    /**
     * Get bitbay configuration object
     * @return BitBay
     * @throws BitBayNotConfiguredException
     */
    public function getConfiguration(): BitBay
    {
        $configurations = $this->findAll();

        if(count($configurations) === 0) {
            throw new BitBayNotConfiguredException();
        }

        return $configurations[0];
    }
}