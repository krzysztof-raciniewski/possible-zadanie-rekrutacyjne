<?php
declare(strict_types=1);

namespace App\Entities\Repositories;


use App\Entities\Repositories\RepositoriesInterfaces\PaymentLogsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\SystemSettingsRepository;
use Doctrine\ORM\EntityRepository;

/**
 * Class CustomSystemSettingsRepository
 * @package App\Entities\Repositories
 */
class CustomSystemSettingsRepository extends EntityRepository implements SystemSettingsRepository
{

}