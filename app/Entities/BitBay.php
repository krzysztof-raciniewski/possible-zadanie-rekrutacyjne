<?php
declare(strict_types=1);

namespace App\Entities;

use App\Enums\CryptoCurrency;
use DOctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Users *
 * @ORM\Table(name="bitbay_configuration")
 * @ORM\Entity
 * @Gedmo\Loggable
 */
class BitBay implements \JsonSerializable
{
    /**
     * @var integer *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string *
     * @ORM\Column(name="bitbay_key", type="string", nullable=false)
     * @Gedmo\Versioned
     */
    protected $key;

    /**
     * @var string *
     * @ORM\Column(name="bitbay_secret", type="string", nullable=false)
     * @Gedmo\Versioned
     */
    protected $secret;

    /**
     * @var float
     * @ORM\Column(name="btc_min_payment", type="decimal", precision=10, scale=7, nullable=true)
     * @Gedmo\Versioned
     */
    protected $btcMinPayment;

    /**
     * @var float
     * @ORM\Column(name="lsk_min_payment", type="decimal", precision=10, scale=7, nullable=true)
     * @Gedmo\Versioned
     */
    protected $lskMinPayment;

    /**
     * @var float
     * @ORM\Column(name="ltc_min_payment", type="decimal", precision=10, scale=7, nullable=true)
     * @Gedmo\Versioned
     */
    protected $ltcMinPayment;

    /**
     * @var float
     * @ORM\Column(name="eth_min_payment", type="decimal", precision=10, scale=7, nullable=true)
     * @Gedmo\Versioned
     */
    protected $ethMinPayment;

    /**
     * @var float
     * @ORM\Column(name="btc_payment_fee", type="decimal", precision=10, scale=7, nullable=true)
     * @Gedmo\Versioned
     */
    protected $btcPaymentFee;

    /**
     * @var float
     * @ORM\Column(name="lsk_payment_fee", type="decimal", precision=10, scale=7, nullable=true)
     * @Gedmo\Versioned
     */
    protected $lskPaymentFee;

    /**
     * @var float
     * @ORM\Column(name="ltc_payment_fee", type="decimal", precision=10, scale=7, nullable=true)
     * @Gedmo\Versioned
     */
    protected $ltcPaymentFee;

    /**
     * @var float
     * @ORM\Column(name="eth_payment_fee", type="decimal", precision=10, scale=7, nullable=true)
     * @Gedmo\Versioned
     */
    protected $ethPaymentFee;

    /**
     * @var \DateTime $created
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey(string $key)
    {
        $this->key = $key;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSecret(): string
    {
        return $this->secret;
    }

    /**
     * @param string $secret
     */
    public function setSecret(string $secret)
    {
        $this->secret = $secret;
    }

    /**
     * @return float
     */
    public function getBtcMinPayment(): float
    {
        return (float)$this->btcMinPayment;
    }

    /**
     * @param float $btcMinPayment
     */
    public function setBtcMinPayment(float $btcMinPayment)
    {
        $this->btcMinPayment = $btcMinPayment;
    }

    /**
     * @return float
     */
    public function getLskMinPayment(): float
    {
        return (float)$this->lskMinPayment;
    }

    /**
     * @param float $lskMinPayment
     */
    public function setLskMinPayment(float $lskMinPayment)
    {
        $this->lskMinPayment = $lskMinPayment;
    }

    /**
     * @return float
     */
    public function getLtcMinPayment(): float
    {
        return (float)$this->ltcMinPayment;
    }

    /**
     * @param float $ltcMinPayment
     */
    public function setLtcMinPayment(float $ltcMinPayment)
    {
        $this->ltcMinPayment = $ltcMinPayment;
    }

    /**
     * @return float
     */
    public function getEthMinPayment(): float
    {
        return (float)$this->ethMinPayment;
    }

    /**
     * @param float $ethMinPayment
     */
    public function setEthMinPayment(float $ethMinPayment)
    {
        $this->ethMinPayment = $ethMinPayment;
    }

    /**
     * @return float
     */
    public function getBtcPaymentFee()
    {
        return (float)$this->btcPaymentFee;
    }

    /**
     * @param float $btcPaymentFee
     */
    public function setBtcPaymentFee(float $btcPaymentFee)
    {
        $this->btcPaymentFee = $btcPaymentFee;
    }

    /**
     * @return float
     */
    public function getLskPaymentFee()
    {
        return (float)$this->lskPaymentFee;
    }

    /**
     * @param float $lskPaymentFee
     */
    public function setLskPaymentFee(float $lskPaymentFee)
    {
        $this->lskPaymentFee = $lskPaymentFee;
    }

    /**
     * @return float
     */
    public function getLtcPaymentFee()
    {
        return (float)$this->ltcPaymentFee;
    }

    /**
     * @param float $ltcPaymentFee
     */
    public function setLtcPaymentFee(float $ltcPaymentFee)
    {
        $this->ltcPaymentFee = $ltcPaymentFee;
    }

    /**
     * @return float
     */
    public function getEthPaymentFee()
    {
        return (float)$this->ethPaymentFee;
    }

    /**
     * @param float $ethPaymentFee
     */
    public function setEthPaymentFee(float $ethPaymentFee)
    {
        $this->ethPaymentFee = $ethPaymentFee;
    }

    /**
     * @param $cryptocurrency
     * @return float
     */
    public function getPaymentFeeForCryptocurrency($cryptocurrency): float
    {
        switch($cryptocurrency) {
            case CryptoCurrency::BITCOIN:
                return $this->getBtcPaymentFee();
                break;
            case CryptoCurrency::LITECOIN:
                return $this->getLtcPaymentFee();
                break;
            case CryptoCurrency::LISK:
                return $this->getLskPaymentFee();
                break;
            case CryptoCurrency::ETHEREUM:
                return $this->getEthPaymentFee();
        }

        return (float)0;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'key' => $this->getKey(),
            'secret' => $this->getSecret()
        ];
    }
}