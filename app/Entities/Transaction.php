<?php
declare(strict_types = 1);

namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use DOctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Operator *
 * @ORM\Table(name="transactions")
 * @ORM\Entity
 * @Gedmo\Loggable
 */
class Transaction implements \JsonSerializable
{

    /**
     * @var string *
     * @ORM\Column(name="id", type="string", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    public $id;

    /**
     * @var string *
     * @ORM\Column(name="country", type="string", nullable=false)
     * @Gedmo\Versioned
     */
    protected $country;

    /**
     * @var string *
     * @ORM\Column(name="operator", type="string", nullable=false)
     * @Gedmo\Versioned
     */
    protected $operator;

    /**
     * @var string *
     * @ORM\Column(name="cryptocurrency", type="string", nullable=false)
     * @Gedmo\Versioned
     */
    protected $cryptocurrency;

    /**
     * @var string *
     * @ORM\Column(name="phone", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $phone;

    /**
     * @var string *
     * @ORM\Column(name="provider", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $provider;

    /**
     * @var float *
     * @ORM\Column(name="sms_net_price", type="decimal", precision=5, scale=2, nullable=true)
     * @Gedmo\Versioned
     */
    protected $smsNetPrice;

    /**
     * @var float *
     * @ORM\Column(name="sms_gross_price", type="decimal", precision=5, scale=2, nullable=true)
     * @Gedmo\Versioned
     */
    protected $smsGrossPrice;

    /**
     * @var string *
     * @ORM\Column(name="sms_brokerage", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $smsBrokerage;

    /**
     * @var string *
     * @ORM\Column(name="sms_currency", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $smsCurrency;

    /**
     * @var string *
     * @ORM\Column(name="wallet", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $wallet;

    /**
     * @var string *
     * @ORM\Column(name="email", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $email;

    /**
     * @var string *
     * @ORM\Column(name="sms_command", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $smsCommand;

    /**
     * @var string *
     * @ORM\Column(name="sms_la_number", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $smsLaNumber;

    /**
     * @var float *
     * @ORM\Column(name="system_commission", type="decimal", precision=5, scale=2, nullable=false)
     * @Gedmo\Versioned
     */
    protected $systemCommission;

    /**
     * @var float *
     * @ORM\Column(name="currency_exchange_rate", type="decimal", precision=10, scale=2, nullable=true)
     * @Gedmo\Versioned
     */
    protected $currencyExchangeRate;

    /**
     * @var float
     * @ORM\Column(name="value", type="decimal", precision=10, scale=7, nullable=true)
     * @Gedmo\Versioned
     */
    protected $calculatedSum;

    /**
     * @var string *
     * @ORM\Column(name="ip_address", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $ipAddress;

    /**
     * @OneToOne(targetEntity="PaymentOrder", inversedBy="transaction")
     * @JoinColumn(name="payment_order_id", referencedColumnName="id")
     */
    protected $paymentOrder;

    /**
     * @var \DateTime $created
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var \DateTime $updated
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @OneToMany(targetEntity="CheckCodeLog", mappedBy="transaction", cascade={"persist"})
     */
    private $checkCodeLogs;

    public function __construct()
    {
        $this->checkCodeLogs = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country ? $this->country : '';
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param string $operator
     */
    public function setOperator(string $operator)
    {
        $this->operator = $operator;
    }

    /**
     * @return string
     */
    public function getCryptocurrency()
    {
        return $this->cryptocurrency;
    }

    /**
     * @param string $cryptocurrency
     */
    public function setCryptocurrency(string $cryptocurrency)
    {
        $this->cryptocurrency = $cryptocurrency;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param string $provider
     */
    public function setProvider(string $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @return float
     */
    public function getSmsNetPrice()
    {
        return floatval($this->smsNetPrice);
    }

    /**
     * @param float $smsNetPrice
     */
    public function setSmsNetPrice(float $smsNetPrice)
    {
        $this->smsNetPrice = $smsNetPrice;
    }

    /**
     * @return float
     */
    public function getSmsGrossPrice()
    {
        return floatval($this->smsGrossPrice);
    }

    /**
     * @param float $smsGrossPrice
     */
    public function setSmsGrossPrice(float $smsGrossPrice)
    {
        $this->smsGrossPrice = $smsGrossPrice;
    }

    /**
     * @return string
     */
    public function getWallet()
    {
        return $this->wallet;
    }

    /**
     * @param string $wallet
     */
    public function setWallet(string $wallet)
    {
        $this->wallet = $wallet;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getSmsCommand()
    {
        return $this->smsCommand;
    }

    /**
     * @param string $smsCode
     */
    public function setSmsCommand(string $smsCode)
    {
        $this->smsCommand = $smsCode;
    }

    /**
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     */
    public function setIpAddress(string $ipAddress)
    {
        $this->ipAddress = $ipAddress;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return mixed
     */
    public function getCheckCodeLogs()
    {
        return $this->checkCodeLogs;
    }

    /**
     * @param mixed $checkCodeLogs
     */
    public function setCheckCodeLogs($checkCodeLogs)
    {
        $this->checkCodeLogs = $checkCodeLogs;
    }

    /**
     * @return string
     */
    public function getSmsCurrency()
    {
        return $this->smsCurrency;
    }

    /**
     * @param string $smsCurrency
     */
    public function setSmsCurrency(string $smsCurrency)
    {
        $this->smsCurrency = $smsCurrency;
    }

    /**
     * @return string
     */
    public function getSmsBrokerage()
    {
        return $this->smsBrokerage;
    }

    /**
     * @param string $smsBrokerage
     */
    public function setSmsBrokerage(string $smsBrokerage)
    {
        $this->smsBrokerage = $smsBrokerage;
    }

    /**
     * @return string
     */
    public function getSmsLaNumber()
    {
        return $this->smsLaNumber;
    }

    /**
     * @param string $smsLaNumber
     */
    public function setSmsLaNumber(string $smsLaNumber)
    {
        $this->smsLaNumber = $smsLaNumber;
    }

    /**
     * @return mixed
     */
    public function getPaymentOrder()
    {
        return $this->paymentOrder;
    }

    /**
     * @param mixed $paymentOrder
     */
    public function setPaymentOrder($paymentOrder)
    {
        $this->paymentOrder = $paymentOrder;
    }

    /**
     * @return float
     */
    public function getSystemCommission()
    {
        return $this->systemCommission ? $this->systemCommission : 0;
    }

    /**
     * @param float $systemCommission
     */
    public function setSystemCommission(float $systemCommission)
    {
        $this->systemCommission = $systemCommission;
    }

    /**
     * @return float
     */
    public function getCalculatedSum()
    {
        return $this->calculatedSum;
    }

    /**
     * @param float $calculatedSum
     */
    public function setCalculatedSum(float $calculatedSum)
    {
        $this->calculatedSum = $calculatedSum;
    }

    /**
     * @return float
     */
    public function getCurrencyExchangeRate()
    {
        return (float)$this->currencyExchangeRate;
    }

    /**
     * @param float $currencyExchangeRate
     */
    public function setCurrencyExchangeRate(float $currencyExchangeRate)
    {
        $this->currencyExchangeRate = $currencyExchangeRate;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'country' => $this->getCountry(),
            'operator' => $this->getOperator(),
            'cryptocurrency' => $this->getCryptocurrency(),
            'phone' => $this->getPhone(),
            'provider' => $this->getProvider(),
            'smsNetPrice' => $this->getSmsNetPrice(),
            'smsGrossPrice' => $this->getSmsGrossPrice(),
            'smsBrokerage' => $this->getSmsBrokerage(),
            'smsCurrency' => $this->getSmsCurrency(),
            'wallet' => $this->getWallet(),
            'email' => $this->getEmail(),
            'smsCommand' => $this->getSmsCommand(),
            'smsLaNumber' => $this->getSmsLaNumber(),
            'ipAddress' => $this->getIpAddress(),
            'systemCommission' => $this->getSystemCommission(),
            'calculatedSum' => $this->getCalculatedSum(),
            'paymentOrder' => $this->getPaymentOrder(),
            'currencyExchangeRate' => $this->getCurrencyExchangeRate(),
            'checkCodeLogs' => $this->getCheckCodeLogs()->toArray(),
            'created' => $this->getCreated()->format('Y-m-d H:i:s'),
            'updated' => $this->getUpdated()->format('Y-m-d H:i:s')

        ];
    }
}