<?php

namespace App\Providers;

use App\Services\BitBayWalletAllertService;
use App\Services\ServicesInterfaces\BitBayWalletAllertServiceInterface;
use App\Services\ServicesInterfaces\PublicControllerServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class ServicesProvider extends ServiceProvider
{

    public function __construct(Application $app)
    {
        parent::__construct($app);

        $this->defer = true;
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\ServicesInterfaces\PublicControllerServiceInterface', 'App\Services\PublicControllerService');
        $this->app->singleton(BitBayWalletAllertServiceInterface::class, function ($app) {
            return new BitBayWalletAllertService(
                $app->make(EntityManagerInterface::class)
            );
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            PublicControllerServiceInterface::class,
        ];
    }
}
