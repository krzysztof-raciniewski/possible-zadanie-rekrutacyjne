<?php

namespace App\Providers;

use App\Entities\BitBay;
use App\Entities\Country;
use App\Entities\Currency;
use App\Entities\Operator;
use App\Entities\PaymentLog;
use App\Entities\PaymentOrder;
use App\Entities\PaymentReport;
use App\Entities\Provider;
use App\Entities\ProviderConfiguration;
use App\Entities\Repositories\CustomBitBayRepository;
use App\Entities\Repositories\CustomCountriesRepository;
use App\Entities\Repositories\CustomCurrenciesRepository;
use App\Entities\Repositories\CustomOperatorsRepository;
use App\Entities\Repositories\CustomPaymentLogsRepository;
use App\Entities\Repositories\CustomPaymentOrdersRepository;
use App\Entities\Repositories\CustomPaymentReportsRepository;
use App\Entities\Repositories\CustomProviderConfigurationsRepository;
use App\Entities\Repositories\CustomProviderRepository;
use App\Entities\Repositories\CustomSmsRepository;
use App\Entities\Repositories\CustomSystemSettingsRepository;
use App\Entities\Repositories\CustomTransactionsRepository;
use App\Entities\Repositories\CustomUsersRepository;
use App\Entities\Repositories\RepositoriesInterfaces\BitBayRepository;
use App\Entities\Repositories\RepositoriesInterfaces\CountriesRepository;
use App\Entities\Repositories\RepositoriesInterfaces\CurrenciesRepository;
use App\Entities\Repositories\RepositoriesInterfaces\OperatorsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\PaymentLogsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\PaymentOrdersRepository;
use App\Entities\Repositories\RepositoriesInterfaces\PaymentReportsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\ProviderConfigurationsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository;
use App\Entities\Repositories\RepositoriesInterfaces\SmsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\SystemSettingsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\TransactionsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\UsersRepository;
use App\Entities\Sms;
use App\Entities\SystemSettings;
use App\Entities\Transaction;
use App\Entities\User;
use Illuminate\Support\ServiceProvider;

class DoctrineRepositoriesProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UsersRepository::class, function($app) {
            return new CustomUsersRepository(
                $app['em'],
                $app['em']->getClassMetaData(User::class)
            );
        });

        $this->app->bind(ProvidersRepository::class, function($app) {
            return new CustomProviderRepository(
                $app['em'],
                $app['em']->getClassMetaData(Provider::class)
            );
        });

        $this->app->bind(ProviderConfigurationsRepository::class, function($app) {
            return new CustomProviderConfigurationsRepository(
                $app['em'],
                $app['em']->getClassMetaData(ProviderConfiguration::class)
            );
        });

        $this->app->bind(PaymentLogsRepository::class, function($app) {
            return new CustomPaymentLogsRepository(
                $app['em'],
                $app['em']->getClassMetaData(PaymentLog::class)
            );
        });

        $this->app->bind(PaymentReportsRepository::class, function($app) {
            return new CustomPaymentReportsRepository(
                $app['em'],
                $app['em']->getClassMetaData(PaymentReport::class)
            );
        });

        $this->app->bind(SmsRepository::class, function($app) {
            return new CustomSmsRepository(
                $app['em'],
                $app['em']->getClassMetaData(Sms::class)
            );
        });

        $this->app->bind(BitBayRepository::class, function($app) {
            return new CustomBitBayRepository(
                $app['em'],
                $app['em']->getClassMetaData(BitBay::class)
            );
        });

        $this->app->bind(CountriesRepository::class, function($app) {
            return new CustomCountriesRepository(
                $app['em'],
                $app['em']->getClassMetaData(Country::class)
            );
        });

        $this->app->bind(CurrenciesRepository::class, function($app) {
            return new CustomCurrenciesRepository(
                $app['em'],
                $app['em']->getClassMetaData(Currency::class)
            );
        });

        $this->app->bind(OperatorsRepository::class, function($app) {
            return new CustomOperatorsRepository(
                $app['em'],
                $app['em']->getClassMetaData(Operator::class)
            );
        });

        $this->app->bind(TransactionsRepository::class, function($app) {
            return new CustomTransactionsRepository(
                $app['em'],
                $app['em']->getClassMetaData(Transaction::class)
            );
        });

        $this->app->bind(SystemSettingsRepository::class, function($app) {
            return new CustomSystemSettingsRepository(
                $app['em'],
                $app['em']->getClassMetaData(SystemSettings::class)
            );
        });

        $this->app->bind(PaymentOrdersRepository::class, function($app) {
            return new CustomPaymentOrdersRepository(
                $app['em'],
                $app['em']->getClassMetaData(PaymentOrder::class)
            );
        });
    }

    public function provides()
    {
        return [
            UsersRepository::class,
            ProvidersRepository::class,
            PaymentReportsRepository::class,
            PaymentLogsRepository::class,
            ProviderConfigurationsRepository::class,
            SmsRepository::class,
            BitBayRepository::class,
            CountriesRepository::class,
            CurrenciesRepository::class,
            OperatorsRepository::class,
            TransactionsRepository::class,
            SystemSettingsRepository::class,
            PaymentOrdersRepository::class
        ];
    }
}
