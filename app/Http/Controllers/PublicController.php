<?php

namespace App\Http\Controllers;

use App\Api\Sms\Factory\ProviderFactory;
use App\Entities\CheckCodeLog;
use App\Entities\Country;
use App\Entities\Operator;
use App\Entities\Provider;
use App\Entities\Repositories\RepositoriesInterfaces\CountriesRepository;
use App\Entities\Repositories\RepositoriesInterfaces\OperatorsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository;
use App\Entities\Repositories\RepositoriesInterfaces\SmsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\SystemSettingsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\TransactionsRepository;
use App\Entities\Sms;
use App\Entities\SystemSettings;
use App\Entities\Transaction;
use App\Enums\CryptoCurrency;
use Dingo\Api\Http\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Services\PublicControllerService;
use Illuminate\Support\Facades\Mail;


class PublicController extends Controller
{
    /** @var SystemSettingsRepository $systemSettingsRepository */
    protected $systemSettingsRepository;

    /** @var CountriesRepository $countriesRepository */
    private $countriesRepository;

    /** @var OperatorsRepository $operatorsRepository */
    private $operatorsRepository;

    /** @var SmsRepository $smsRepository */
    private $smsRepository;

    /** @var  TransactionsRepository $transactionsRepository */
    private $transactionsRepository;

    /** @var  ProvidersRepository $providersRepository */
    private $providersRepository;

    /** @var  PublicControllerService $publicControllerService */
    private $publicControllerService;

    private $em;

    /**
     * PublicController constructor.
     * @param CountriesRepository $countriesRepository
     * @param OperatorsRepository $operatorsRepository
     * @param SmsRepository $smsRepository
     * @param TransactionsRepository $transactionsRepository
     * @param ProvidersRepository $providersRepository
     * @param PublicControllerService $publicControllerService
     * @param SystemSettingsRepository $systemSettingsRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(CountriesRepository $countriesRepository,
                                OperatorsRepository $operatorsRepository,
                                SmsRepository $smsRepository,
                                TransactionsRepository $transactionsRepository,
                                ProvidersRepository $providersRepository,
                                PublicControllerService $publicControllerService,
                                SystemSettingsRepository $systemSettingsRepository,
                                EntityManagerInterface $entityManager)
    {
        $this->countriesRepository = $countriesRepository;
        $this->operatorsRepository = $operatorsRepository;
        $this->smsRepository = $smsRepository;
        $this->transactionsRepository = $transactionsRepository;
        $this->providersRepository = $providersRepository;
        $this->publicControllerService = $publicControllerService;
        $this->systemSettingsRepository = $systemSettingsRepository;
        $this->em = $entityManager;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCountries()
    {
        return response()->json($this->countriesRepository->getAvailableCountries());
    }

    /**
     * Get available cryptocurrencies for select input on public page
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCryptocurrencies()
    {
        return response()->json((new CryptoCurrency())->getArray());
    }

    /**
     * Get available operators for country. This endpoint is used by public site(select input)
     * @param $countryId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function getOperators($countryId)
    {
        /** @var Country $countryObject */
        $countryObject = $this->countriesRepository->findOneById($countryId);

        if (!$countryObject) {
            return response('Country not exists', 404);
        }

        return response()->json($this->operatorsRepository->getAvailableOperatorsForCountry($countryObject));
    }

    /**
     * @param $transactionId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function getSms($transactionId)
    {
        /** @var Transaction $transaction */
        $transaction = $this->transactionsRepository->findOneById($transactionId);
        if (!$transaction) {
            return response()->json([]);
        }

        /** @var Country $countryObject */
        $countryObject = $this->countriesRepository->findOneByCode($transaction->getCountry());
        $operatorObject = $this->operatorsRepository->findOneByName($transaction->getOperator());

        if (!$countryObject || !$operatorObject) {
            return response('Country or operator not exists', 404);
        }

        /*
         * Set provider for transaction
         */
        if (!$transaction->getProvider()) {
            $transaction = $this->publicControllerService->setProviderForTransaction($transaction, $countryObject, $operatorObject);
        }

        $smsList = $this->smsRepository->getAllAvailable($transaction);

        return response()->json($smsList != null ? $smsList : []);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTransaction(Request $request)
    {
        $this->validate($request, [
            'country' => 'required',
            'operator' => 'required',
            'cryptocurrency' => 'required'
        ]);

        /** @var Country $countryObject */
        $countryObject = $this->countriesRepository->findOneById($request->get('country'));
        /** @var Operator $operatorObject */
        $operatorObject = $this->operatorsRepository->findOneById($request->get('operator'));

        /** @var SystemSettings $systemSettings */
        $systemSettings = $this->systemSettingsRepository->findAll()[0];

        if (!$countryObject || !$operatorObject) {
            return response('Country or operator not exists', 404);
        }

        $transaction = new Transaction();
        $transaction->setCountry($countryObject->getCode());
        $transaction->setOperator($operatorObject->getName());
        $transaction->setCryptocurrency($request->get('cryptocurrency'));
        $transaction->setSystemCommission($systemSettings->getSaleCommission());
        $transaction->setIpAddress($request->ip());

        $this->em->persist($transaction);
        $this->em->flush();

        return response()->json($transaction);
    }

    /**
     * @param Request $request
     * @param $transactionId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateTransaction(Request $request, $transactionId)
    {
        $this->validate($request, [
            'country' => 'required',
            'operator' => 'required',
            'cryptocurrency' => 'required',
        ]);

        /** @var Sms $smsObject */
        $smsObject = null;
        /** @var Country $countryObject */
        $countryObject = null;
        /** @var Operator $operatorObject */
        $operatorObject = null;

        /** @var Country $countryObject */
        $countryObject = $this->countriesRepository->findOneById($request->get('country'));
        /** @var Operator $operatorObject */
        $operatorObject = $this->operatorsRepository->findOneById($request->get('operator'));

        if (!$countryObject || !$operatorObject) {
            return response('Country or operator not exists', 404);
        }
        if ($request->has('sms')) {
            $smsObject = $this->smsRepository->findOneById($request->get('sms'));
        }

        /** @var Transaction $transaction */
        $transaction = $this->transactionsRepository->findOneById($transactionId);
        $transaction->setCountry($countryObject->getCode());
        $transaction->setOperator($operatorObject->getName());
        $transaction->setCryptocurrency($request->get('cryptocurrency'));
//        $transaction->setPhone($request->get('phone'));

        if ($smsObject) {
            $transaction->setSmsGrossPrice($smsObject->getGrossPrice());
            $transaction->setSmsNetPrice($smsObject->getNetPrice());
            $transaction->setSmsCommand($smsObject->getCommand());
            $transaction->setSmsLaNumber($smsObject->getLaNumber());
            $transaction->setSmsCurrency($smsObject->getCurrency()->getCode());
            $transaction->setSmsBrokerage($smsObject->getBrokerage());
        }

        if ($request->has('wallet')) {
            $transaction->setWallet($request->get('wallet'));
        }

//        if ($request->has('email')) {
//            $transaction->setEmail($request->get('email'));
//        }

        $transaction->setIpAddress($request->ip());

        $this->em->persist($transaction);
        $this->em->flush();

        return response()->json($transaction);
    }

    /**
     * @param Request $request
     * @param $transactionId
     * @param $code
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function checkCode(Request $request, $transactionId, $code)
    {
        /** @var Transaction $transaction */
        $transaction = $this->transactionsRepository->findOneById($transactionId);
        if (!$transaction) {
            return response('Transaction does not exist', 404);
        }

        // Log check code tying
        $checkCodeLog = new CheckCodeLog();
        $checkCodeLog->setCode($code);
        $checkCodeLog->setTransaction($transaction);
        $checkCodeLog->setIpAddress($request->ip());

        $sms = $this->smsRepository->findOneBy([
            'command' => $transaction->getSmsCommand(),
            'laNumber' => $transaction->getSmsLaNumber(),
            'netPrice' => $transaction->getSmsNetPrice(),
            'grossPrice' => $transaction->getSmsGrossPrice()
        ]);

        $provider = ProviderFactory::make($transaction->getProvider(), $sms);

        $error = false;
        $codeIsValid = false;
        try {
            $codeIsValid = $provider->checkCode($code);
        } catch (\Exception $e) {
            $error = true;
        }

        $checkCodeLog->setValid($codeIsValid);
        $this->em->persist($checkCodeLog);
        $this->em->flush();

        if ($error) {
            return response('BadCode', 200);
        }

        if ($codeIsValid === true) {
            /*
             * Make payment order for transaction
             */
            $this->publicControllerService->makePayment($transaction, $sms);
            return response('CodeIsValid', 200);
        } else {
            return response('BadCode', 200);
        }
    }

    /**
     * @param $transactionId
     * @param $smsId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function getTransactionInfo($transactionId, $smsId)
    {

        /** @var Transaction $transaction */
        $transaction = $this->transactionsRepository->findOneById($transactionId);
        if (!$transaction) {
            return response('Transaction not found', 404);
        }

        /** @var Provider $provider */
        $provider = $this->providersRepository->findOneByName($transaction->getProvider());
        if (!$provider) {
            return response('Provider not found', 404);
        }

        /** @var Sms $sms */
        $sms = $this->smsRepository->findOneById($smsId);
        if (!$sms) {
            return response('Sms not found', 404);
        }

        $calculatedSum = $this->publicControllerService->calculateCryptocurrencyValue($transaction, $sms);

        $sumStatus = $this->publicControllerService->sumIsValid($transaction, $calculatedSum);

        if (is_array($sumStatus)) {
            return response()->json(
                $sumStatus
            );
        } else {
            if ($sumStatus === true) {
                $transaction->setCalculatedSum($calculatedSum);
                $this->em->persist($transaction);
                $this->em->flush();

                $operators_names = [];
                foreach ($sms->getOperators()->toArray() as $operator) {
                    $operators_names[] = $operator->getName();
                }

                return response()->json([
                    'provider' => [
                        'name' => $provider->getName(),
                        'complaint_form_url' => $provider->getComplaintFormUrl(),
                        'rules_url' => $provider->getRulesUrl()
                    ],
                    'sms' => [
                        'net_price' => $sms->getNetPrice(),
                        'gross_price' => $sms->getGrossPrice(),
                        'command' => $sms->getCommand(),
                        'number' => $sms->getLaNumber(),
                        'operators' => $operators_names
                    ],
                    'cryptocurrency' => [
                        'code' => $transaction->getCryptocurrency(),
                        'value' => $calculatedSum
                    ]
                ]);
            }
        }

        return response('Error', 400);
    }

    /**
     * @param Request $request
     */
    public function contactAction(Request $request)
    {
        /** @var SystemSettings $systemSettings */
        $systemSettings = $this->systemSettingsRepository->findAll()[0];
        $this->validate($request, [
            'fullname' => 'required',
            'email' => 'required|email',
            'content' => 'required'
        ]);

        $data = [
            'sender_email' => $request->get('email'),
            'sender_name' => $request->get('fullname'),
            'message' => $request->get('content'),
            'application_name' => $systemSettings->getApplicationName(),
            'domain_name' => $systemSettings->getDomainName()
        ];

        Mail::send('emails.contact', ['data' => $data], function ($m) use ($data, $systemSettings) {
//            $m->from($data['sender_email'], $data['sender_name']);

            $m->to($systemSettings->getContactEmail(), $systemSettings->getApplicationName())->subject('Kontakt z zaplikacji ' . $systemSettings->getApplicationName());
        });
    }
}
