<?php

namespace App\Http\Controllers;

use App\Entities\Repositories\RepositoriesInterfaces\PaymentOrdersRepository;
use App\Entities\Repositories\RepositoriesInterfaces\TransactionsRepository;
use Dingo\Api\Http\Request;
use Doctrine\ORM\EntityManagerInterface;


class PaymentsController extends Controller
{
    /** @var PaymentOrdersRepository $paymentOrdersRepository */
    private $paymentOrdersRepository;
    /** @var EntityManagerInterface $em */
    private $em;

    public function __construct(PaymentOrdersRepository $paymentOrdersRepository, EntityManagerInterface $em)
    {
        $this->paymentOrdersRepository = $paymentOrdersRepository;
        $this->em = $em;
    }

    public function getList(Request $request)
    {
        $this->validate($request, [
            'draw' => 'required',
            'order' => 'required',
            'columns' => 'required',
            'start' => 'required',
            'length' => 'required',
            'search' => 'required'
        ]);

        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search');
        $columns = $request->get('columns');
        $order = $request->get('order');

        $payments = $this->paymentOrdersRepository->getPaymentsPage($start, $length, $search, $columns, $order);
        $filtered = count($this->paymentOrdersRepository->getPayments($search, $columns, $order));
        $count = $this->paymentOrdersRepository->getPaymentsCount();

        return response()->json(
            [
                "draw" => $draw,
                "recordsTotal" => $count,
                "recordsFiltered" => $filtered,
                "data" => $payments
            ]
        );
    }

    /**
     * @param $paymentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetails($paymentId) {
        $payment = $this->paymentOrdersRepository->findOneById($paymentId);

        return response()->json($payment);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllCount() {
        $count = count($this->paymentOrdersRepository->findAll());

        return response()->json([
            'count' => $count
        ]);
    }

    /**
     * @param $count
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLast($count) {
        $transactions = $this->paymentOrdersRepository->getLast($count);
        return response()->json($transactions);
    }

    /**
     * @param $count
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLastCompleted($count) {
        $transactions = $this->paymentOrdersRepository->getLastCompleted($count);
        return response()->json($transactions);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUnsuccessfulCount() {
        $count = $this->paymentOrdersRepository->getUnsuccessfulPaymentsCount();

        return response()->json([
            'count' => $count
        ]);
    }
}
