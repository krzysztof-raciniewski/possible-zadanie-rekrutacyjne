<?php

namespace App\Http\Controllers\Auth;

use App\Entities\User;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use JWTAuth;
use LaravelDoctrine\ORM\Facades\EntityManager;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required|min:8',
        ]);

        $credentials = $request->only('email', 'password');

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->error('Invalid credentials', 401);
            }
        } catch (\JWTException $e) {
            return response()->error('Could not create token', 500);
        }

        $user = Auth::user();

        return response()->success(compact('user', 'token'));
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name'       => 'required|min:3',
            'email'      => 'required|email',
            'password'   => 'required|min:8',
        ]);

        $user = new User();
        $user->setEmail($request->email);
        $user->setPassword(bcrypt($request->password));
        $user->setFirstName('');
        $user->setLastName('');
        $user->setLogin($request->name);

        EntityManager::persist($user);
        EntityManager::flush();

        $token = JWTAuth::fromUser($user);

        return response()->success(compact('user', 'token'));
    }
}
