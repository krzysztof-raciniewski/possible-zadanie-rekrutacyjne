<?php

namespace App\Http\Controllers;

use App\Entities\Country;
use App\Entities\Repositories\RepositoriesInterfaces\CountriesRepository;
use App\Entities\Repositories\RepositoriesInterfaces\CurrenciesRepository;
use Dingo\Api\Http\Request;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;

class CountryController extends Controller
{
    private $countriesRepository;
    private $em;

    /**
     * ProviderController constructor.
     * @param EntityManagerInterface $em
     * @param CountriesRepository $countriesRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        CountriesRepository $countriesRepository)
    {
        $this->countriesRepository = $countriesRepository;
        $this->em = $em;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function get()
    {
        return response()->json(
            $this->countriesRepository->findAll()
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
                'code' => 'required'
            ]);

            $country = new Country();
            $country->setName($request['name']);
            $country->setCode(strtoupper($request['code']));

            $this->em->persist($country);
            $this->em->flush();

            return response()->json(
                $country
            );
        } catch (UniqueConstraintViolationException $e) {
            return response('Code exists', 400);
        }
    }

    /**
     * @param Request $request
     * @param $country_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request, $country_id)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
                'code' => 'required'
            ]);

            /** @var Country $country */
            $country = $this->countriesRepository->findOneById($country_id);
            $country->setName($request['name']);
            $country->setCode(strtoupper($request['code']));

            $this->em->persist($country);
            $this->em->flush();

            return response()->json(
                $country
            );
        } catch (UniqueConstraintViolationException $e) {
            return response('Code exists', 400);
        }
    }

    public function delete($country_id)
    {
        try {
            /** @var Country $country */
            $country = $this->countriesRepository->findOneById($country_id);

            $this->em->remove($country);
            $this->em->flush();

            return response('Ok', 200);
        } catch (ForeignKeyConstraintViolationException $e) {
            return response('Country has assignments', 400);
        }
    }
}
