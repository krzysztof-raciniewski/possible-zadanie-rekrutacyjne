<?php

namespace App\Http\Controllers;

use App\Entities\Operator;
use App\Entities\Repositories\RepositoriesInterfaces\OperatorsRepository;
use Dingo\Api\Http\Request;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;

class OperatorController extends Controller
{
    private $operatorsRepository;
    private $em;

    /**
     * ProviderController constructor.
     * @param EntityManagerInterface $em
     * @param OperatorsRepository $operatorsRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        OperatorsRepository $operatorsRepository)
    {
        $this->operatorsRepository = $operatorsRepository;
        $this->em = $em;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function get()
    {
        return response()->json(
            $this->operatorsRepository->findAll()
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
            ]);

            $operator = new Operator();
            $operator->setName($request['name']);

            $this->em->persist($operator);
            $this->em->flush();

            return response()->json(
                $operator
            );
        } catch(UniqueConstraintViolationException $e) {
            return response('Operator exists', 400);
        }
    }

    /**
     * @param Request $request
     * @param $operator_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request, $operator_id)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
            ]);

            /** @var Operator $operator */
            $operator = $this->operatorsRepository->findOneById($operator_id);
            $operator->setName($request['name']);

            $this->em->persist($operator);
            $this->em->flush();

            return response()->json(
                $operator
            );
        } catch(UniqueConstraintViolationException $e) {
            return response('Operator exists', 400);
        }
    }

    public function delete($operator_id)
    {
        /** @var Operator $operator */
        $operator = $this->operatorsRepository->findOneById($operator_id);

        $this->em->remove($operator);
        $this->em->flush();

        return response('Ok', 200);
    }
}
