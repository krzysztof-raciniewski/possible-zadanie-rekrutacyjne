<?php

namespace App\Http\Controllers;

use App\Entities\Currency;
use App\Entities\Repositories\RepositoriesInterfaces\CurrenciesRepository;
use Dingo\Api\Http\Request;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;

class CurrencyController extends Controller
{
    private $currenciesRepository;
    private $em;

    /**
     * ProviderController constructor.
     * @param EntityManagerInterface $em
     * @param CurrenciesRepository $currenciesRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        CurrenciesRepository $currenciesRepository)
    {
        $this->currenciesRepository = $currenciesRepository;
        $this->em = $em;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function get()
    {
        return response()->json(
            $this->currenciesRepository->findAll()
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
                'code' => 'required'
            ]);

            $currency = new Currency();
            $currency->setName($request['name']);
            $currency->setCode(strtoupper($request['code']));

            $this->em->persist($currency);
            $this->em->flush();

            return response()->json(
                $currency
            );
        } catch (UniqueConstraintViolationException $e) {
            return response('Code exists', 400);
        }
    }

    /**
     * @param Request $request
     * @param $currency_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request, $currency_id)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
                'code' => 'required'
            ]);

            /** @var Currency $currency */
            $currency = $this->currenciesRepository->findOneById($currency_id);
            $currency->setName($request['name']);
            $currency->setCode(strtoupper($request['code']));

            $this->em->persist($currency);
            $this->em->flush();

            return response()->json(
                $currency
            );
        } catch (UniqueConstraintViolationException $e) {
            return response('Code exists', 400);
        }
    }

    public function delete($currency_id)
    {
        try {
            /** @var Currency $currency */
            $currency = $this->currenciesRepository->findOneById($currency_id);

            $this->em->remove($currency);
            $this->em->flush();

            return response('Ok', 200);
        } catch (ForeignKeyConstraintViolationException $e) {
            return response('Currency has assignments', 400);
        }
    }
}
