<?php

namespace App\Http\Controllers;

use App\Entities\BitBay;
use App\Entities\Repositories\RepositoriesInterfaces\BitBayRepository;
use Dingo\Api\Http\Request;
use Doctrine\ORM\EntityManagerInterface;

class BitBayConfigurationController extends Controller
{
    private $bitbay;
    private $em;

    public function __construct(EntityManagerInterface $em, BitBayRepository $bitbay)
    {
        $this->bitbay = $bitbay;
        $this->em = $em;
    }

    /**
     * Get bitbay configuration
     * @return \Illuminate\Http\JsonResponse
     */
    public function getConfiguration()
    {
        return response()->json($this->bitbay->findAll()[0]);
    }

    /**
     * Set bitbay configuration
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setConfiguration(Request $request)
    {
        $this->validate($request, [
            'key' => 'required',
            'secret' => 'required',
        ]);

        /** @var BitBay $configuration */
        $configuration = $this->bitbay->findAll()[0];
        $configuration->setKey($request->get('key'));
        $configuration->setSecret($request->get('secret'));
        $this->em->persist($configuration);
        $this->em->flush();
        return response()->json([]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLimits()
    {
        /** @var BitBay $bitBayConfiguration */
        $bitBayConfiguration = $this->bitbay->findAll()[0];

        return response()->json([
            'btcMinPayment' => $bitBayConfiguration->getBtcMinPayment(),
            'lskMinPayment' => $bitBayConfiguration->getLskMinPayment(),
            'ltcMinPayment' => $bitBayConfiguration->getLtcMinPayment(),
            'ethMinPayment' => $bitBayConfiguration->getEthMinPayment()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function setLimits(Request $request)
    {
        $this->validate($request, [
            'btcMinPayment' => 'required',
            'lskMinPayment' => 'required',
            'ltcMinPayment' => 'required',
            'ethMinPayment' => 'required',
        ]);
        /** @var BitBay $bitBayConfiguration */
        $bitBayConfiguration = $this->bitbay->findAll()[0];

        $bitBayConfiguration->setBtcMinPayment((float)$request->get('btcMinPayment'));
        $bitBayConfiguration->setLskMinPayment((float)$request->get('lskMinPayment'));
        $bitBayConfiguration->setLtcMinPayment((float)$request->get('ltcMinPayment'));
        $bitBayConfiguration->setEthMinPayment((float)$request->get('ethMinPayment'));

        $this->em->persist($bitBayConfiguration);
        $this->em->flush();

        return response('Ok', 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFees()
    {
        /** @var BitBay $bitBayConfiguration */
        $bitBayConfiguration = $this->bitbay->findAll()[0];

        return response()->json([
            'btcPaymentFee' => $bitBayConfiguration->getBtcPaymentFee(),
            'lskPaymentFee' => $bitBayConfiguration->getLskPaymentFee(),
            'ltcPaymentFee' => $bitBayConfiguration->getLtcPaymentFee(),
            'ethPaymentFee' => $bitBayConfiguration->getEthPaymentFee()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function setFees(Request $request)
    {
        $this->validate($request, [
            'btcPaymentFee' => 'required',
            'lskPaymentFee' => 'required',
            'ltcPaymentFee' => 'required',
            'ethPaymentFee' => 'required',
        ]);
        /** @var BitBay $bitBayConfiguration */
        $bitBayConfiguration = $this->bitbay->findAll()[0];

        $bitBayConfiguration->setBtcPaymentFee((float)$request->get('btcPaymentFee'));
        $bitBayConfiguration->setLskPaymentFee((float)$request->get('lskPaymentFee'));
        $bitBayConfiguration->setLtcPaymentFee((float)$request->get('ltcPaymentFee'));
        $bitBayConfiguration->setEthPaymentFee((float)$request->get('ethPaymentFee'));

        $this->em->persist($bitBayConfiguration);
        $this->em->flush();

        return response('Ok', 200);
    }
}
