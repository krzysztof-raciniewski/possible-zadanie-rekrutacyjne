<?php

namespace App\Http\Controllers;

use App\Entities\Repositories\RepositoriesInterfaces\TransactionsRepository;
use Dingo\Api\Http\Request;
use Doctrine\ORM\EntityManagerInterface;

use App\Http\Requests;

class TransactionsController extends Controller
{
    /** @var TransactionsRepository $transactionsRepository */
    private $transactionsRepository;
    /** @var EntityManagerInterface $em */
    private $em;

    public function __construct(TransactionsRepository $transactionsRepository, EntityManagerInterface $em)
    {
        $this->transactionsRepository = $transactionsRepository;
        $this->em = $em;
    }

    public function getList(Request $request)
    {
        $this->validate($request, [
            'draw' => 'required',
            'order' => 'required',
            'columns' => 'required',
            'start' => 'required',
            'length' => 'required',
            'search' => 'required'
        ]);

        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->get('search');
        $columns = $request->get('columns');
        $order = $request->get('order');

        $transactions = $this->transactionsRepository->getTransactionsPage($start, $length, $search, $columns, $order);
        $filtered = count($this->transactionsRepository->getTransactions($search, $columns, $order));
        $count = $this->transactionsRepository->getTransactionsCount();

        return response()->json(
            [
                "draw" => $draw,
                "recordsTotal" => $count,
                "recordsFiltered" => $filtered,
                "data" => $transactions
            ]
        );
    }

    /**
     * @param $count
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLast($count) {
        $transactions = $this->transactionsRepository->getLast($count);
        return response()->json($transactions);
    }

    /**
     * @param $count
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLastCompleted($count) {
        $transactions = $this->transactionsRepository->getLastCompleted($count);
        return response()->json($transactions);
    }

    /**
     * @param $transactionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetails($transactionId) {
        $transaction = $this->transactionsRepository->findOneById($transactionId);

        return response()->json($transaction);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllCount() {
        $count = count($this->transactionsRepository->findAll());

        return response()->json([
            'count' => $count
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNewCount() {
        $count = count($this->transactionsRepository->findByPaymentOrder(null));

        return response()->json([
            'count' => $count
        ]);
    }
}
