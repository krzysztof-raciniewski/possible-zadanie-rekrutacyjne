<?php

namespace App\Http\Controllers;

use App\Api\Sms\FortumoProvider;
use App\Api\Sms\ProviderException\FortumoInvalidParametersException;
use App\Api\Sms\ProviderLogger\ProviderLoggerFactory;
use App\Enums\ProviderName;
use Doctrine\ORM\EntityManagerInterface;

class FortumoController extends Controller
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Get payment confirmation from Fortumo SMS provider
     */
    public function paymentsReceiverMethod() {
        try {
            $fortumo = new FortumoProvider($_GET);
            if (!$fortumo->checkSignature()) {
                return response('Error: Invalid signature', 404);
            }

            $providerLogger = ProviderLoggerFactory::create(ProviderName::FORTUMO);
            $providerLogger->logPayment($_GET);

            return response('Coins4SMS.com - dziekujemy, otrzymalismy Twoja wplate', 200);
        } catch(FortumoInvalidParametersException $e) {
            return response('Error', 404);
        }
    }

    /**
     * Get report from Fortumo SMS provider
     */
    public function reportsReceiverMethod() {
        try {
            $fortumo = new FortumoProvider($_GET);
            if (!$fortumo->checkSignature()) {
                return response('Error: Invalid signature', 404);
            }

            $providerLogger = ProviderLoggerFactory::create(ProviderName::FORTUMO);
            $providerLogger->logReport($_GET);

            return response('OK', 200);
        } catch (FortumoInvalidParametersException $e) {
            return response('Error', 404);
        }
    }
}
