<?php

namespace App\Http\Controllers;

use App\Entities\Provider;
use App\Entities\ProviderConfiguration;
use App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository;
use App\Entities\Repositories\RepositoriesInterfaces\SmsRepository;
use Dingo\Api\Http\Request;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\PersistentCollection;
use Symfony\Bundle\FrameworkBundle\Templating\Helper\RequestHelper;

class SmsController extends Controller
{
    private $providersRespository;
    private $smsRepository;
    private $em;

    /**
     * ProviderController constructor.
     * @param EntityManagerInterface $em
     * @param ProvidersRepository $providersRepository
     */
    public function __construct(EntityManagerInterface $em, ProvidersRepository $providersRepository, SmsRepository $smsRepository)
    {
        $this->providersRespository = $providersRepository;
        $this->smsRepository = $smsRepository;
        $this->em = $em;
    }


//    public function getSmsDefinitionsForProvider($id)
//    {
//        /** @var Provider $provider */
//        $provider = $this->providersRespository->findOneById($id);
//        if(!$provider) {
//            return response('Provider not found', 404);
//        }
//
//        /** @var ProviderConfiguration $providerConfiguration */
//        $providerConfiguration = $provider->getConfiguration()->get(0);
//
//        /** @var PersistentCollection $smsPersistanceCollection */
//        $smsPersistanceCollection = $providerConfiguration->getSms();
//        return response()->json(
//            $smsPersistanceCollection->toArray()
//        );
//    }

}
