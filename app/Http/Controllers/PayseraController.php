<?php

namespace App\Http\Controllers;

use App\Api\Sms\PayseraProvider;
use App\Entities\PaymentLog;
use Doctrine\ORM\EntityManagerInterface;

class PayseraController extends Controller
{
    public function callback() {
        response()->isOk();
    }
}
