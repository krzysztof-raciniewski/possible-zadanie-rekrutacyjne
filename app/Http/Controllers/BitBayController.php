<?php

namespace App\Http\Controllers;

use App\Api\Bitbay\BitbayApi;
use App\Api\Bitbay\BitbayExceptions\BitBayException;
use App\Api\Bitbay\BitbayExceptions\InvalidCurrencyNameException;
use App\Api\Bitbay\Enum\CurrencyName;

class BitBayController extends Controller
{
    private $api;

    public function __construct(BitbayApi $api)
    {
        $this->api = $api;
    }

    /**
     * Get all info
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAmount()
    {
        return response()->json($this->api->getInfo());
    }

    /**
     * Get info about currency in wallet
     * @param $currency
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAmountForCurrency($currency)
    {

        if (!$currency) {
            return response()->json($this->api->getInfo());
        }

        $currency = strtoupper($currency);

        switch ($currency) {
            case CurrencyName::BTC:
                return response()->json($this->api->getInfoAboutBTC());
                break;
            case CurrencyName::LSK:
                return response()->json($this->api->getInfoAboutLSK());
                break;
            case CurrencyName::LTC:
                return response()->json($this->api->getInfoAboutLTC());
                break;
            case CurrencyName::ETH:
                return response()->json($this->api->getInfoAboutETH());
                break;
            default:
                return response()->json($this->api->getInfo());
                break;
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMyActiveOrders()
    {
        return response()->json($this->api->getMyActiveOrders());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMyInactiveOrders()
    {
        return response()->json($this->api->getMyInactiveOrders());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMyOrders()
    {
        return response()->json($this->api->getMyOrders());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHistory()
    {

        try {
            return response()->json($this->api->getHistory());
        } catch (BitBayException $e) {
            return response()->json([]);
        }
    }

    /**
     * @param $currency
     * @param int $limit
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHistoryForCurrency($currency, $limit = 10)
    {
        $limit = (int)$limit;

        $currency = strtoupper($currency);

        try {
            switch ($currency) {
                case CurrencyName::BTC:
                    return response()->json($this->api->getHistory(CurrencyName::BTC, $limit));
                    break;
                case CurrencyName::LSK:
                    return response()->json($this->api->getHistory(CurrencyName::LSK, $limit));
                    break;
                case CurrencyName::LTC:
                    return response()->json($this->api->getHistory(CurrencyName::LTC, $limit));
                    break;
                case CurrencyName::ETH:
                    return response()->json($this->api->getHistory(CurrencyName::ETH, $limit));
                    break;
                default:
                    return response()->json($this->api->getHistory(CurrencyName::PLN, $limit));
                    break;
            }
        } catch (InvalidCurrencyNameException $e) {
            return response()->json([]);
        }
    }


    /**
     * @param string $market
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function getTransactionsHistory($market = '')
    {
        $market = strtoupper($market);

        if($market == '') {
            $market = null;
        }

        try {
            return response()->json($this->api->getTransactionsHistory($market));
        } catch (BitBayException $e) {
            return response()->json([]);
        }
    }

    /**
     * @param string $ordercurrency
     * @param string $paymentcurrency
     * @return \Illuminate\Http\JsonResponse
     * @internal param string $market
     */
    public
    function getAllOffersFromMarket($ordercurrency = CurrencyName::BTC, $paymentcurrency = CurrencyName::PLN)
    {
        $ordercurrency = strtoupper($ordercurrency);
        $paymentcurrency = strtoupper($paymentcurrency);

        try {
            return response()->json($this->api->getOpenOffersFromMarket($ordercurrency, $paymentcurrency));
        } catch (BitBayException $e) {
            return response()->json([]);
        }
    }

}
