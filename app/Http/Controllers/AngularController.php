<?php

namespace App\Http\Controllers;

class AngularController extends Controller
{
    public function serveApp()
    {
        return view('index', ['random' => \Carbon\Carbon::now()->getTimestamp()]);
    }

    public function serveAdminApp() {
        return view('admin');
    }

    public function unsupported()
    {
        return view('unsupported_browser');
    }
}
