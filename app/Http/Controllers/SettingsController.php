<?php

namespace App\Http\Controllers;

use App\Entities\Repositories\RepositoriesInterfaces\SystemSettingsRepository;
use App\Entities\SystemSettings;
use Dingo\Api\Http\Request;
use Doctrine\ORM\EntityManagerInterface;

class SettingsController extends Controller
{
    /** @var EntityManagerInterface $em */
    private $em;

    /** @var SystemSettingsRepository $systemSettingsRepository */
    private $systemSettingsRepository;

    /**
     * SettingsController constructor.
     * @param EntityManagerInterface $em
     * @param SystemSettingsRepository $systemSettingsRepository
     */
    public function __construct(EntityManagerInterface $em, SystemSettingsRepository $systemSettingsRepository)
    {
        $this->systemSettingsRepository = $systemSettingsRepository;
        $this->em = $em;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEmails()
    {
        /** @var SystemSettings $settings */
        $settings = $this->systemSettingsRepository->findAll()[0];

        return response()->json([
            'contact_email' => $settings->getContactEmail(),
            'notifications_email' => $settings->getNotificationsEmail()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setEmails(Request $request)
    {
        $this->validate($request, [
            'contact_email' => 'required',
            'notifications_email' => 'required',
        ]);

        /** @var SystemSettings $settings */
        $settings = $this->systemSettingsRepository->findAll()[0];
        $settings->setContactEmail($request->get('contact_email'));
        $settings->setNotificationsEmail($request->get('notifications_email'));
        $this->em->persist($settings);
        $this->em->flush();

        return response()->json([
            'contact_email' => $settings->getContactEmail(),
            'notifications_email' => $settings->getNotificationsEmail()
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWallet()
    {
        /** @var SystemSettings $settings */
        $settings = $this->systemSettingsRepository->findAll()[0];

        return response()->json([
            'btcWalletFullValue' => $settings->getBtcWalletFullValue(),
            'lskWalletFullValue' => $settings->getLskWalletFullValue(),
            'ltcWalletFullValue' => $settings->getLtcWalletFullValue(),
            'ethWalletFullValue' => $settings->getEthWalletFullValue(),
            'btcWalletAlertValue' => $settings->getBtcWalletAlertValue(),
            'ltcWalletAlertValue' => $settings->getLtcWalletAlertValue(),
            'lskWalletAlertValue' => $settings->getLskWalletAlertValue(),
            'ethWalletAlertValue' => $settings->getEthWalletAlertValue()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setWallet(Request $request)
    {
        $this->validate($request, [
            'btcWalletFullValue' => 'required',
            'lskWalletFullValue' => 'required',
            'ltcWalletFullValue' => 'required',
            'ethWalletFullValue' => 'required',
            'btcWalletAlertValue' => 'required',
            'lskWalletAlertValue' => 'required',
            'ltcWalletAlertValue' => 'required',
            'ethWalletAlertValue' => 'required',
        ]);

        /** @var SystemSettings $settings */
        $settings = $this->systemSettingsRepository->findAll()[0];
        $settings->setBtcWalletFullValue($request->get('btcWalletFullValue'));
        $settings->setLskWalletFullValue($request->get('lskWalletFullValue'));
        $settings->setLtcWalletFullValue($request->get('ltcWalletFullValue'));
        $settings->setEthWalletFullValue($request->get('ethWalletFullValue'));
        $settings->setBtcWalletAlertValue($request->get('btcWalletAlertValue'));
        $settings->setLskWalletAlertValue($request->get('lskWalletAlertValue'));
        $settings->setLtcWalletAlertValue($request->get('ltcWalletAlertValue'));
        $settings->setEthWalletAlertValue($request->get('ethWalletAlertValue'));
        $this->em->persist($settings);
        $this->em->flush();

        return response()->json([
            'btcWalletFullValue' => $settings->getBtcWalletFullValue(),
            'lskWalletFullValue' => $settings->getLskWalletFullValue(),
            'ltcWalletFullValue' => $settings->getLtcWalletFullValue(),
            'ethWalletFullValue' => $settings->getEthWalletFullValue(),
            'btcWalletAlertValue' => $settings->getBtcWalletAlertValue(),
            'ltcWalletAlertValue' => $settings->getLtcWalletAlertValue(),
            'lskWalletAlertValue' => $settings->getLskWalletAlertValue(),
            'ethWalletAlertValue' => $settings->getEthWalletAlertValue()
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCommission()
    {
        /** @var SystemSettings $settings */
        $settings = $this->systemSettingsRepository->findAll()[0];

        return response()->json([
            'sale_commission' => $settings->getSaleCommission(),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setCommission(Request $request)
    {
        $this->validate($request, [
            'sale_commission' => 'required|min:0|max:99'
        ]);

        /** @var SystemSettings $settings */
        $settings = $this->systemSettingsRepository->findAll()[0];
        $settings->setSaleCommission($request->get('sale_commission'));
        $this->em->persist($settings);
        $this->em->flush();

        return response()->json([
            'sale_commission' => $settings->getSaleCommission(),
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQueueStatus() {
        /** @var SystemSettings $systemSettings */
        $systemSettings = $this->systemSettingsRepository->findAll()[0];

        return response()->json([
            'status' => $systemSettings->isPaymentProcessActive()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setQueueStatus(Request $request) {
        $this->validate($request, [
           'status' => 'required'
        ]);

        $status = $request->get('status') === 'true';

        /** @var SystemSettings $systemSettings */
        $systemSettings = $this->systemSettingsRepository->findAll()[0];

        $systemSettings->setPaymentProcessActive($status);
        $this->em->persist($systemSettings);
        $this->em->flush();

        return response()->json([
            'status' => $status
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWalletAllertsStatus() {
        /** @var SystemSettings $systemSettings */
        $systemSettings = $this->systemSettingsRepository->findAll()[0];

        return response()->json([
            'status' => $systemSettings->isWalletAllertsActive()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setWalletAllertsStatus(Request $request) {
        $this->validate($request, [
            'status' => 'required'
        ]);

        $status = $request->get('status') === 'true';

        /** @var SystemSettings $systemSettings */
        $systemSettings = $this->systemSettingsRepository->findAll()[0];

        $systemSettings->setWalletAllertsActive($status);
        $this->em->persist($systemSettings);
        $this->em->flush();

        return response()->json([
            'status' => $status
        ]);
    }
}
