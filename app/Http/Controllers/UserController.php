<?php

namespace App\Http\Controllers;

use App\Entities\Repositories\RepositoriesInterfaces\UsersRepository;

use App\Entities\User;
use Dingo\Api\Http\Request;
use Doctrine\ORM\EntityManagerInterface;
use JWTAuth;

class UserController extends Controller
{
    /** @var EntityManagerInterface $em */
    protected $em;

    /** @var UsersRepository $usersRepository */
    private $usersRepository;

    /**
     * UserController constructor.
     * @param UsersRepository $usersRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(UsersRepository $usersRepository, EntityManagerInterface $em)
    {
        $this->usersRepository = $usersRepository;
        $this->em = $em;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCurrentUserInformation()
    {
        $user = JWTAuth::parseToken()->authenticate();

        if (!$user) {
            return response()->json(['user_not_found'], 404);
        }

        // the token is valid and we have found the user via the sub claim
        return response()->json($user);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changeUserData(Request $request) {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
        ]);

        /** @var User $user */
        $user = JWTAuth::parseToken()->authenticate();

        if (!$user) {
            return response()->json(['user_not_found'], 404);
        }

        $user->setFirstName($request->get('first_name'));
        $user->setLastName($request->get('last_name'));
        $this->em->persist($user);
        $this->em->flush();

        return response()->json($user);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList()
    {
        $users = $this->usersRepository->findAll();

        return response()->json($users);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function changeCurrentUserPassword(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required',
            'repeated_password' => 'required'
        ]);

        $old_password = $request->get('old_password');
        $hash = bcrypt($request->get('old_password'));
        /** @var User $user */
        $user = JWTAuth::parseToken()->authenticate();

        if (!$user) {
            return response('user_not_found', 404);
        }

        if( !password_verify($old_password, $user->getPassword())) {
            return response('bad_password', 422);
        }

        $password = $request->get('password');
        $repeatedPassword = $request->get('repeated_password');

        if( $password !== $repeatedPassword ) {
            return response('password_are_not_the_same', 422);
        }

        $user->setPassword(bcrypt($password));
        $this->em->persist($user);
        $this->em->flush();

        return response('Ok', 200);
    }
}
