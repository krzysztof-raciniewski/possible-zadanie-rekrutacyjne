<?php

namespace App\Http\Controllers;

use App\Entities\Provider;
use App\Entities\ProviderConfiguration;
use App\Entities\Repositories\RepositoriesInterfaces\CountriesRepository;
use App\Entities\Repositories\RepositoriesInterfaces\CurrenciesRepository;
use App\Entities\Repositories\RepositoriesInterfaces\OperatorsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository;
use App\Entities\Repositories\RepositoriesInterfaces\SmsRepository;
use App\Entities\Sms;
use Dingo\Api\Http\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\PersistentCollection;

class ProviderController extends Controller
{
    private $providersRespository;
    private $smsRepository;
    private $countriesRepository;
    private $currenciesRepository;
    private $operatorsRepository;
    private $em;

    /**
     * ProviderController constructor.
     * @param EntityManagerInterface $em
     * @param SmsRepository $smsRepository
     * @param CountriesRepository $countriesRepository
     * @param CurrenciesRepository $currenciesRepository
     * @param OperatorsRepository $operatorsRepository
     * @param ProvidersRepository $providersRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        SmsRepository $smsRepository,
        CountriesRepository $countriesRepository,
        CurrenciesRepository $currenciesRepository,
        OperatorsRepository $operatorsRepository,
        ProvidersRepository $providersRepository)
    {
        $this->providersRespository = $providersRepository;
        $this->smsRepository = $smsRepository;
        $this->countriesRepository = $countriesRepository;
        $this->currenciesRepository = $currenciesRepository;
        $this->operatorsRepository = $operatorsRepository;
        $this->em = $em;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProvidersList()
    {
        return response()->json(
            $this->providersRespository->findAll(
                [
                    'id', 'name', 'www', 'image', 'description', 'configured', 'enabled', 'active', 'updated', 'last_active'
                ]
            )
        );
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProviderById($id)
    {
        $provider = $this->providersRespository->findById($id);

        if ($provider) {
            return response()->json(
                $provider
            );
        } else {
            return response('Provider not found', 404);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateProviderById(Request $request, $id)
    {
        $this->validate($request, [
            'image' => 'required',
            'www' => 'required'
        ]);

        /** @var Provider $provider */
        $provider = $this->providersRespository->findOneById($id);

        if (!$provider) {
            return response('Provider not found', 404);
        }

        $provider->setImage($request->get('image'));
        $provider->setWww($request->get('www'));
        $provider->setDescription($request->get('description'));
        $provider->setRulesUrl($request->get('rules_url'));
        $provider->setComplaintFormUrl($request->get('complaint_from_url'));

        $this->em->persist($provider);
        $this->em->flush();

        return response()->json($provider);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function changeProviderStatus($id) {
        /** @var Provider $provider */
        $provider = $this->providersRespository->findOneById($id);

        if (!$provider) {
            return response('Provider not found', 404);
        }

        $provider->setEnabled(!$provider->isEnabled());
        $this->em->persist($provider);
        $this->em->flush();

        return response('Ok', 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function getConfigurationForProvider($id)
    {
        /** @var Provider $provider */
        $provider = $this->providersRespository->findOneById($id);
        if (!$provider) {
            return response('Provider not found', 404);
        }

        return response()->json(
            $provider->getConfiguration()->toArray()
        );
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function setConfigurationForProvider(Request $request, $id)
    {
        /** @var Provider $provider */
        $provider = $this->providersRespository->findOneById($id);
        if (!$provider) {
            return response('Provider not found', 404);
        }
        /** @var ProviderConfiguration $configuration */
        $configuration = $provider->getConfiguration()->get(0);
        $configuration->setLogin($request->get('login'));
        $configuration->setPassword($request->get('password'));

        $this->em->persist($configuration);
        $this->em->flush();

        return response()->json(
            $configuration
        );
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function getSmsDefinitionsForProvider($id)
    {
        /** @var Provider $provider */
        $provider = $this->providersRespository->findOneById($id);
        if (!$provider) {
            return response('Provider not found', 404);
        }

        /** @var ProviderConfiguration $providerConfiguration */
        $providerConfiguration = $provider->getConfiguration()->get(0);

        /** @var PersistentCollection $smsPersistanceCollection */
        $smsPersistanceCollection = $providerConfiguration->getSms();
        return response()->json(
            $smsPersistanceCollection->toArray()
        );
    }

    /**
     * Remove sms from provider configuration
     * @param $provider_id
     * @param $sms_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function removeSmsDefinitionsFromProvider($provider_id, $sms_id)
    {
        /** @var Provider $provider */
        $provider = $this->providersRespository->findOneById($provider_id);
        if (!$provider) {
            return response('Provider not found', 404);
        }

        $smsObject = $this->smsRepository->findOneById($sms_id);
        if (!$smsObject) {
            return response('Sms not found', 404);
        }

        /** @var ProviderConfiguration $providerConfiguration */
        $providerConfiguration = $provider->getConfiguration()->get(0);

        /** @var ArrayCollection $smsCollection */
        $smsCollection = $providerConfiguration->getSms();

        if (!$smsCollection->contains($smsObject)) {
            return response('Sms not found in provider configuration', 404);
        }

        $smsCollection->removeElement($smsObject);
        $this->em->remove($smsObject);
        $this->em->flush();
        return response('Ok', 200);
    }

    /**
     * @param Request $request
     * @param $provider_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function addSms(Request $request, $provider_id)
    {
        $this->validate($request, [
            'la_number' => 'required',
            'command' => 'required',
            'country' => 'required',
            'currency' => 'required',
            'enabled' => 'required',
            'net_price' => 'required',
            'gross_price' => 'required',
            'brokerage' => 'required'
        ]);

        /** @var Provider $provider */
        $provider = $this->providersRespository->findOneById($provider_id);
        if (!$provider) {
            return response('Provider not found', 404);
        }

        $country = $this->countriesRepository->findOneByCode($request->get('country'));
        if (!$country) {
            return response('Country not found', 404);
        }

        $currency = $this->currenciesRepository->findOneByCode($request->get('currency'));
        if (!$currency) {
            return response('Currency not found', 404);
        }

        $operators_array = explode(",",$request->get('operators'));
        $operators = $this->operatorsRepository->findAllIds($operators_array);

        $enabled = $request->get('enabled');
        $testing = $request->get('testing');

        $newSms = new Sms();
        $newSms->setLaNumber($request->get('la_number'));
        $newSms->setCommand($request->get('command'));
        $newSms->setCountry($country);
        $newSms->setCurrency($currency);
        $newSms->setNetPrice($request->get('net_price'));
        $newSms->setGrossPrice($request->get('gross_price'));
        $newSms->setServiceId($request->get('service_id', ''));
        $newSms->setBrokerage($request->get('brokerage'));
        $newSms->setToken($request->get('token', ''));
        $newSms->setEnabled($enabled === 'true' || $enabled === true);
        $newSms->setTesting($testing === 'true' || $testing === true);
        $newSms->setProviderConfiguration($provider->getConfiguration()->get(0));
        $this->em->persist($newSms);

        // Add operators
        $newSms->getOperators()->clear();

        foreach($operators as $operator) {
            $newSms->getOperators()->add($operator);
        }

        $this->em->flush();

        return response()->json($newSms);
    }

    /**
     * @param Request $request
     * @param $provider_id
     * @param $sms_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function editSms(Request $request, $provider_id, $sms_id)
    {
        $this->validate($request, [
            'la_number' => 'required',
            'command' => 'required',
            'country' => 'required',
            'currency' => 'required',
            'enabled' => 'required',
            'net_price' => 'required',
            'gross_price' => 'required',
            'brokerage' => 'required'
        ]);

        /** @var Provider $provider */
        $provider = $this->providersRespository->findOneById($provider_id);
        if (!$provider) {
            return response('Provider not found', 404);
        }

        /** @var Sms $sms */
        $sms = $this->smsRepository->findOneById($sms_id);
        if(!$sms) {
            return response('Sms not found', 404);
        }

        $country = $this->countriesRepository->findOneByCode($request->get('country'));
        if (!$country) {
            return response('Country not found', 404);
        }

        $currency = $this->currenciesRepository->findOneByCode($request->get('currency'));
        if (!$currency) {
            return response('Currency not found', 404);
        }
        $operators_array = explode(",",$request->get('operators'));
        $operators = $this->operatorsRepository->findAllIds($operators_array);

        $sms->setLaNumber($request->get('la_number'));
        $sms->setCommand($request->get('command'));
        $sms->setCountry($country);
        $sms->setCurrency($currency);
        $sms->setNetPrice($request->get('net_price'));
        $sms->setGrossPrice($request->get('gross_price'));
        $sms->setServiceId($request->get('service_id', ''));
        $sms->setBrokerage($request->get('brokerage'));
        $sms->setToken($request->get('token', ''));
        $sms->setEnabled($request->get('enabled') === 'true');
        $sms->setTesting($request->get('testing') === 'true');
        $sms->setProviderConfiguration($provider->getConfiguration()->get(0));

        // Add operators
        $sms->getOperators()->clear();

        foreach($operators as $operator) {
            $sms->getOperators()->add($operator);
        }

        $this->em->persist($sms);
        $this->em->flush();

        return response()->json($sms);
    }

    /**
     * @param $provider_id
     * @param $sms_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function changeSmsStatus($provider_id, $sms_id)
    {
        $provider = $this->providersRespository->findOneById($provider_id);
        if (!$provider) {
            return response('Provider not found', 404);
        }
        /** @var Sms $sms */
        $sms = $this->smsRepository->findOneById($sms_id);
        if(!$sms) {
            return response('Sms not found', 404);
        }

        $sms->setEnabled(!$sms->isEnabled());
        $this->em->persist($sms);
        $this->em->flush();

        return response('Ok', 200);
    }

    /**
     * @param $provider_id
     * @param $sms_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function getSmsFromProvider($provider_id, $sms_id)
    {
        $provider = $this->providersRespository->findOneById($provider_id);
        if (!$provider) {
            return response('Provider not found', 404);
        }
        /** @var Sms $sms */
        $sms = $this->smsRepository->findOneById($sms_id);
        if(!$sms) {
            return response('Sms not found', 404);
        }

        return response()->json($sms);
    }
}
