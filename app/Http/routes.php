<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
|  .-. . . .-. .   .-. .-.     .-. .-. . . .-. .-. .-.
|  |-' | | |(  |    |  |       |(  | | | |  |  |-  `-.
|  '   `-' `-' `-' `-' `-'     ' ' `-' `-'  '  `-' `-'
|
*/
$laravelAdminPanelRoutes = function () {
    Route::get('/', 'AngularController@serveAdminApp');
    Route::get('{all}', 'AngularController@serveAdminApp');
    Route::get('/transactions/detail/{all}', 'AngularController@serveAdminApp');
    Route::get('/transactions/{all}', 'AngularController@serveAdminApp');
    Route::get('/payments/{all}', 'AngularController@serveAdminApp');
    Route::get('/payments/list/{all}', 'AngularController@serveAdminApp');
    Route::get('/statistics/{all}', 'AngularController@serveAdminApp');
    Route::get('/providers/{all}', 'AngularController@serveAdminApp');
    Route::get('/operators/{all}', 'AngularController@serveAdminApp');
    Route::get('/countries/{all}', 'AngularController@serveAdminApp');
    Route::get('/bitbay/{all}', 'AngularController@serveAdminApp');
    Route::get('/settings/{all}', 'AngularController@serveAdminApp');
    Route::get('/settings/payment/{all}', 'AngularController@serveAdminApp');
    Route::get('/page/{all}', 'AngularController@serveAdminApp');
    Route::get('/currencies/{all}', 'AngularController@serveAdminApp');
    Route::get('/providers/{all}', 'AngularController@serveAdminApp');
    Route::get('/providers/configuration/{all}', 'AngularController@serveAdminApp');
};

Route::group(['domain' => 'admin.coins4sms.dev'], $laravelAdminPanelRoutes);
Route::group(['domain' => 'admin.coins4sms.com'], $laravelAdminPanelRoutes);

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'AngularController@serveApp');
    Route::get('/about_us', 'AngularController@serveApp');
    Route::get('/contact', 'AngularController@serveApp');
    Route::get('/partnership', 'AngularController@serveApp');
    Route::get('/unsupported-browser', 'AngularController@unsupported');
    Route::get('/testexception', 'TestController@test');
});

/*
 *
 * .-. . . .-. .   .-. .-.     .-. .-. .-.
 * |-' | | |(  |    |  |       |-| |-'  |
 * '   `-' `-' `-' `-' `-'     ` ' '   `-'
 */
$api->group(['middleware' => 'api.throttle', 'limit' => 5, 'expires' => 5], function ($api) {
    $api->post('auth/login', 'Auth\AuthController@login');
});

$api->group(['middleware' => 'api.throttle', 'limit' => 10, 'expires' => 1], function ($api) {
    $api->post('public/transactions', 'PublicController@createTransaction');
});

$api->group(['middleware' => 'api.throttle', 'limit' => 2, 'expires' => 1], function ($api) {
    $api->post('public/contact', 'PublicController@contactAction');
});

$api->group(['middleware' => ['api']], function ($api) {
    $api->get('public/countries', 'PublicController@getCountries');
    $api->get('public/cryptocurrencies', 'PublicController@getCryptocurrencies');
    $api->get('public/countries/{countryId}/operators', 'PublicController@getOperators');
    $api->get('public/sms/{transactionId}', 'PublicController@getSms');
    $api->put('public/transactions/{transactionId}', 'PublicController@updateTransaction');
    $api->get('public/transactions/{transactionId}/checkcode/{code}', 'PublicController@checkCode');
    $api->get('public/transactions/{transactionId}/info/{smsId}', 'PublicController@getTransactionInfo');
});


/*
 * .-. .-. .-. . . .-. .-. .-.     .-. .-. .-.
 * `-. |-  |   | | |(  |-  |  )    |-| |-'  |
 * `-' `-' `-' `-' ' ' `-' `-'     ` ' '   `-'
 */
$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    /*
     * /api/user
     */
    $api->get('user/current/', 'UserController@getCurrentUserInformation');
    $api->put('user/current/', 'UserController@changeUserData');
    $api->put('user/current/changepassword', 'UserController@changeCurrentUserPassword');
    $api->get('user/list', 'UserController@getList');

    /*
     * /api/bitbay/amount
     */
    $api->get('bitbay/amount/', 'BitBayController@getAmount');
    $api->get('bitbay/amount/{currency?}', 'BitBayController@getAmountForCurrency');

    /*
     * /api/bitbay/orders
     */
    $api->get('bitbay/orders/', 'BitBayController@getMyOrders');
    $api->get('bitbay/orders/active', 'BitBayController@getMyActiveOrders');
    $api->get('bitbay/orders/inactive', 'BitBayController@getMyInactiveOrders');
    $api->get('bitbay/history/', 'BitBayController@getHistory');
    $api->get('bitbay/history/{limit?}', 'BitBayController@getHistory');
    $api->get('bitbay/history/{currency}/{limit?}', 'BitBayController@getHistoryForCurrency');

    /*
     * /api/bitbay/transactions
     */
    $api->get('bitbay/transactions/{market?}', 'BitBayController@getTransactionsHistory');

    /*
     * /api/bitbay/offers
     */
    $api->get('bitbay/offers/{ordercurrency?}/{paymentcurrency}', 'BitBayController@getAllOffersFromMarket');
    $api->get('bitbay/offers/', 'BitBayController@getAllOffersFromMarket');

    /*
     * /api/configuration/bitbay
     */
    $api->get('/configuration/bitbay', 'BitBayConfigurationController@getConfiguration');
    $api->get('/configuration/bitbay/limits', 'BitBayConfigurationController@getLimits');
    $api->get('/configuration/bitbay/fees', 'BitBayConfigurationController@getFees');
    $api->put('/configuration/bitbay/fees', 'BitBayConfigurationController@setFees');
    $api->put('/configuration/bitbay/limits', 'BitBayConfigurationController@setLimits');

    /*
     * /api/providers
     */
    $api->get('/providers', 'ProviderController@getProvidersList');
    $api->get('/providers/{id}', 'ProviderController@getProviderById');
    $api->put('/providers/{id}', 'ProviderController@updateProviderById');

    $api->get('/providers/{id}/configuration', 'ProviderController@getConfigurationForProvider');
    $api->put('/providers/{id}/configuration', 'ProviderController@setConfigurationForProvider');
    $api->put('/providers/{id}/changestatus', 'ProviderController@changeProviderStatus');

    $api->get('/providers/{id}/sms', 'ProviderController@getSmsDefinitionsForProvider');
    $api->post('/providers/{id}/sms', 'ProviderController@addSms');
    $api->delete('/providers/{provider_id}/sms/{sms_id}', 'ProviderController@removeSmsDefinitionsFromProvider');
    $api->put('/providers/{provider_id}/sms/{sms_id}/changestatus', 'ProviderController@changeSmsStatus');
    $api->get('/providers/{provider_id}/sms/{sms_id}', 'ProviderController@getSmsFromProvider');
    $api->put('/providers/{provider_id}/sms/{sms_id}', 'ProviderController@editSms');

    /*
     * /api/operators
     */
    $api->get('/operators', 'OperatorController@get');
    $api->post('/operators', 'OperatorController@add');
    $api->put('/operators/{operator_id}', 'OperatorController@edit');
    $api->delete('/operators/{operator_id}', 'OperatorController@delete');

    /*
     * /api/currencies
     */
    $api->get('/currencies', 'CurrencyController@get');
    $api->post('/currencies', 'CurrencyController@add');
    $api->put('/currencies/{currency_id}', 'CurrencyController@edit');
    $api->delete('/currencies/{currency_id}', 'CurrencyController@delete');

    /*
     * /api/currencies
     */
    $api->get('/countries', 'CountryController@get');
    $api->post('/countries', 'CountryController@add');
    $api->put('/countries/{currency_id}', 'CountryController@edit');
    $api->delete('/countries/{currency_id}', 'CountryController@delete');

    /*
     * /api/settings/emails
     */
    $api->get('/settings/emails', 'SettingsController@getEmails');
    $api->put('/settings/emails', 'SettingsController@setEmails');

    /*
     * /api/settings/emails
     */
    $api->get('/settings/wallet', 'SettingsController@getWallet');
    $api->put('/settings/wallet', 'SettingsController@setWallet');

    /*
     * /api/settings/emails
     */
    $api->get('/settings/commission', 'SettingsController@getCommission');
    $api->put('/settings/commission', 'SettingsController@setCommission');

    /*
     * /api/settings/payment/status
     */
    $api->get('/settings/paymentqueue/status', 'SettingsController@getQueueStatus');
    $api->put('/settings/paymentqueue/status', 'SettingsController@setQueueStatus');

    /*
     * /api/settings/walletallerts/status
     */
    $api->get('/settings/walletallerts/status', 'SettingsController@getWalletAllertsStatus');
    $api->put('/settings/walletallerts/status', 'SettingsController@setWalletAllertsStatus');

    /*
     * /api/transactions
     */
    $api->get('/transactions', 'TransactionsController@getList');
    $api->get('/transactions/last/{count}', 'TransactionsController@getLast');
    $api->get('/transactions/last/completed/{count}', 'TransactionsController@getLastCompleted');
    $api->get('/transactions/{transactionId}', 'TransactionsController@getDetails');
    $api->get('/transactions/all/count', 'TransactionsController@getAllCount');
    $api->get('/transactions/new/count', 'TransactionsController@getNewCount');

    /*
     * /api/payments
     */
    $api->get('/payments', 'PaymentsController@getList');
    $api->get('/payments/last/{count}', 'PaymentsController@getLast');
    $api->get('/payments/last/completed/{count}', 'PaymentsController@getLastCompleted');
    $api->get('/payments/{paymentId}', 'PaymentsController@getDetails');
    $api->get('/payments/all/count', 'PaymentsController@getAllCount');
    $api->get('/payments/unsuccessful/count', 'PaymentsController@getUnsuccessfulCount');

});
