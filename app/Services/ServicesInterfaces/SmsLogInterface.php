<?php
/**
 * Created by IntelliJ IDEA.
 * User: raciniak
 * Date: 02.09.16
 * Time: 10:25
 */

namespace App\Services\ServicesInterfaces;


interface SmsLogInterface
{
    public function logFortumoPayment(array $params);
    public function logFortumoReport(array $params);
}