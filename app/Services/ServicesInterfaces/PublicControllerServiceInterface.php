<?php
/**
 * Created by IntelliJ IDEA.
 * User: raciniak
 * Date: 02.09.16
 * Time: 10:25
 */

namespace App\Services\ServicesInterfaces;


use App\Entities\Country;
use App\Entities\Operator;
use App\Entities\Sms;
use App\Entities\Transaction;

interface PublicControllerServiceInterface
{
    public function setProviderForTransaction(Transaction $transaction, Country $country, Operator $operator): Transaction;
    public function calculateCryptocurrencyValue(Transaction $transaction, Sms $sms): float;
    public function makePayment(Transaction $transaction, Sms $sms);
}