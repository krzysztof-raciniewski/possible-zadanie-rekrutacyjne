<?php

namespace App\Services\ServicesInterfaces;


interface BitBayWalletAllertServiceInterface
{
    public function checkWalletsAmountsAndSendEmail(): bool;
}