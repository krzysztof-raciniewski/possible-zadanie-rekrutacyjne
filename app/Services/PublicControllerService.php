<?php
declare(strict_types=1);

namespace App\Services;


use App\Api\CryptocurrencyPrice\CryptocurrencyPriceProviderFactory;
use App\Api\Sms\ProviderLogger\Exceptions\ProviderNotFoundException;
use App\Entities\BitBay;
use App\Entities\Country;
use App\Entities\Operator;
use App\Entities\PaymentOrder;
use App\Entities\Provider;
use App\Entities\Repositories\RepositoriesInterfaces\BitBayRepository;
use App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository;
use App\Entities\Repositories\RepositoriesInterfaces\SystemSettingsRepository;
use App\Entities\Sms;
use App\Entities\SystemSettings;
use App\Entities\Transaction;
use App\Enums\CryptoCurrency;
use App\Services\ServicesInterfaces\PublicControllerServiceInterface;
use Doctrine\ORM\EntityManagerInterface;

class PublicControllerService implements PublicControllerServiceInterface
{
    private $bitBayRepository;
    private $providersRepository;
    private $systemSettingsRepository;
    private $em;

    public function __construct(ProvidersRepository $providersRepository,
                                SystemSettingsRepository $systemSettingsRepository,
                                BitBayRepository $bitBayRepository,
                                EntityManagerInterface $em)
    {
        $this->providersRepository = $providersRepository;
        $this->systemSettingsRepository = $systemSettingsRepository;
        $this->bitBayRepository = $bitBayRepository;
        $this->em = $em;
    }

    /**
     * Random value from array
     * @param array $array
     * @return mixed|string
     * @throws ProviderNotFoundException
     */
    private function randomFromArray(array $array): string {
        if( count($array) === 0 ) {
            return '';
        }
        $index = array_rand($array);

        $providerId = $array[$index];
        /** @var Provider $providerObject */
        $providerObject = $this->providersRepository->findOneById($providerId);

        if(!$providerObject) {
            throw new ProviderNotFoundException();
        }

        return $providerObject->getName();
    }

    /**
     * Random provider for transaction
     * @param Transaction $transaction
     * @param Country $country
     * @param Operator $operator
     * @return Transaction
     */
    public function setProviderForTransaction(Transaction $transaction, Country $country, Operator $operator): Transaction
    {
        /** @var Provider[] $providers */
        $providers = $this->providersRepository->findEnabledProvidersWithCountryAndOperator($country, $operator);
        $ids = [];
        foreach($providers as $provider) {
            $ids[] = $provider->getId();
        }

        if(count($ids) === 0) {
            return $transaction;
        }

        $providerName = $this->randomFromArray($ids);
        /** @var Provider $provider */
        $provider = $this->providersRepository->findOneByName($providerName);
        $transaction->setProvider((string)$provider->getName());

        $provider->setLastActiveNow();

        $this->em->persist($provider);
        $this->em->persist($transaction);
        $this->em->flush();

        return $transaction;
    }

    /**
     * Calculate cryptocurrency value for sms
     * @param Transaction $transaction
     * @param Sms $sms
     * @return float
     */
    public function calculateCryptocurrencyValue(Transaction $transaction, Sms $sms): float
    {
        $cryptocurrencyPriceProvider = CryptocurrencyPriceProviderFactory::create();
        /** @var SystemSettings $systemSettings */
        $systemSettings = $this->systemSettingsRepository->findAll()[0];

        $cryptocurrency = $transaction->getCryptocurrency();
        $smsNetPrice = (float)$sms->getNetPrice();
        $smsBrokerage = (float)$sms->getBrokerage();

        $saleCommission = $systemSettings->getSaleCommission();

        // Get exchange rate for cryptocurrency
        $cryptocurrency_price = $cryptocurrencyPriceProvider->getPrice($cryptocurrency, $sms->getCurrency()->getCode());
        $providerAllocation = $smsNetPrice*($smsBrokerage/100);

        $systemAllocation = ($saleCommission/100)*($smsNetPrice-$providerAllocation);
        $client_part = $smsNetPrice - ($providerAllocation + $systemAllocation);
        $value = $client_part/$cryptocurrency_price;

        /** @var BitBay $bitBayConfiguration */
        $bitBayConfiguration = $this->bitBayRepository->findAll()[0];
        $cryptocurrencyPaymentFee = $bitBayConfiguration->getPaymentFeeForCryptocurrency($cryptocurrency);
        $value -= $cryptocurrencyPaymentFee;

        // Save cryptocurrency exchange rate
        $transaction->setCurrencyExchangeRate($cryptocurrency_price);

        $this->em->persist($transaction);
        $this->em->flush();

        return $value;
    }

    /**
     * Make payment order method
     * @param $transaction
     * @param $sms
     */
    public function makePayment(Transaction $transaction, Sms $sms)
    {
        $value = $this->calculateCryptocurrencyValue($transaction, $sms);
        $paymentOrder = new PaymentOrder();
        $paymentOrder->setPayed(false);
        $paymentOrder->setValue($value);
        $paymentOrder->setCurrency($transaction->getCryptocurrency());
        $paymentOrder->setWallet($transaction->getWallet());
        $paymentOrder->setTransaction($transaction);
        $transaction->setPaymentOrder($paymentOrder);
        $this->em->persist($paymentOrder);
        $this->em->persist($transaction);
        $this->em->flush();

    }

    /**
     * @param Transaction $transaction
     * @param $calculatedSum
     * @return array|bool
     */
    public function sumIsValid(Transaction $transaction, $calculatedSum)
    {
        /** @var BitBay $bitBayConfiguration */
        $bitBayConfiguration = $this->bitBayRepository->findAll()[0];
        $minValueForCryptocurrency = null;
        switch($transaction->getCryptocurrency()) {
            case CryptoCurrency::BITCOIN:
                $minValueForCryptocurrency = $bitBayConfiguration->getBtcMinPayment();
                break;
            case CryptoCurrency::ETHEREUM:
                $minValueForCryptocurrency = $bitBayConfiguration->getEthMinPayment();
                break;
            case CryptoCurrency::LISK:
                $minValueForCryptocurrency = $bitBayConfiguration->getLskMinPayment();
                break;
            case CryptoCurrency::LITECOIN:
                $minValueForCryptocurrency = $bitBayConfiguration->getLtcMinPayment();
                break;
        }

        if( $minValueForCryptocurrency === null ) {
            return true;
        }

        if( (float)$minValueForCryptocurrency > (float)$calculatedSum ) {
            return [
                'error' => 'AmountIsTooSmall',
                'amount' => $calculatedSum,
                'min' => $minValueForCryptocurrency
            ];
        } else {
            return true;
        }
    }
}