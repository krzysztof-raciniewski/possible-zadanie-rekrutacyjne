<?php

namespace App\Services;

use App\Api\Bitbay\BitbayApi;
use App\Entities\SystemSettings;
use App\Enums\CryptoCurrency;
use App\Services\ServicesInterfaces\BitBayWalletAllertServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Mail;

class BitBayWalletAllertService implements BitBayWalletAllertServiceInterface
{
    /** @var \Doctrine\Common\Persistence\ObjectRepository $systemSettingsRepository */
    private $systemSettingsRepository;

    /** @var  SystemSettings $systemSettings */
    private $systemSettings;

    /** @var  BitbayApi $bitBayApi */
    private $bitBayApi;

    public function __construct(EntityManagerInterface $em)
    {
        $this->systemSettingsRepository = $em->getRepository(SystemSettings::class);
        $this->systemSettings = $this->systemSettingsRepository->findAll()[0];
        $this->bitBayApi = new BitbayApi();
    }

    public function checkWalletsAmountsAndSendEmail(): bool
    {
        if( !$this->systemSettings->isWalletAllertsActive() ) {
            return false;
        }

        $walletsInfo = $this->bitBayApi->getInfo();
        $btcBalance = $walletsInfo['balances'][CryptoCurrency::BITCOIN]['available'];
        $ltcBalance = $walletsInfo['balances'][CryptoCurrency::LITECOIN]['available'];
        $lskBalance = $walletsInfo['balances'][CryptoCurrency::LISK]['available'];
        $ethBalance = $walletsInfo['balances'][CryptoCurrency::ETHEREUM]['available'];

        $btcMinValue = $this->systemSettings->getBtcWalletAlertValue();
        $ltcMinValue = $this->systemSettings->getLtcWalletAlertValue();
        $lskMinValue = $this->systemSettings->getLskWalletAlertValue();
        $ethMinValue = $this->systemSettings->getEthWalletAlertValue();

        $message = "Przypominam o niskim stanie kryptowaluty w portfelu: <br/>";
        $index = 1;
        if($btcBalance <= $btcMinValue) {
            $message .= $index . '. BTC - aktualny stan konta: ' . $btcBalance . '<br />';
            $index++;
        }
        if($ltcBalance <= $ltcMinValue) {
            $message .= $index . '. LTC - aktualny stan konta: ' . $ltcBalance . '<br />';
            $index++;
        }
        if($lskBalance <= $lskMinValue) {
            $message .= $index . '. LSK - aktualny stan konta: ' . $lskBalance . '<br />';
            $index++;
        }
        if($ethBalance <= $ethMinValue) {
            $message .= $index . '. ETH - aktualny stan konta: ' . $ethBalance . '<br />';
            $index++;
        }

        if($index > 1) {
            $this->sendAllertEmail($message);
            return true;
        }

        return false;
    }

    private function sendAllertEmail(string $message)
    {
        /** @var SystemSettings $systemSettings */
        $systemSettings = $this->systemSettingsRepository->findAll()[0];
        $data = [
            'sender_name' => $systemSettings->getApplicationName(),
            'sender_email' => 'postmaster@coins4sms.com',
            'message' => $message,
            'application_name' => $systemSettings->getApplicationName(),
            'domain_name' => $systemSettings->getDomainName()
        ];

        Mail::send('emails.reminder', ['data' => $data], function ($m) use ($data, $systemSettings) {
            $m->to($systemSettings->getNotificationsEmail(), $systemSettings->getApplicationName())->subject('Mało kryptowaluty w portfelu! ');
        });
    }

}