<?php

namespace App\Console;

use App\Console\Commands\CryptocurrencyPaymentListPayed;
use App\Console\Commands\CryptocurrencyPaymentListStats;
use App\Console\Commands\CryptocurrencyPaymentListUnpaid;
use App\Console\Commands\CryptocurrencyPaymentRun;
use App\Console\Commands\Inspire;
use App\Console\Commands\MoneyAmountCheck;
use Illuminate\Console\Scheduling\Schedule;

use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Inspire::class,
        CryptocurrencyPaymentRun::class,
        CryptocurrencyPaymentListUnpaid::class,
        CryptocurrencyPaymentListPayed::class,
        CryptocurrencyPaymentListStats::class,
        MoneyAmountCheck::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*
         * Run payment procedure
         */
        $schedule->command('payment:run')->everyMinute();

        /*
         * Run check money procedure
         */
        $schedule->command('money:check')->everyThirtyMinutes();
    }
}
