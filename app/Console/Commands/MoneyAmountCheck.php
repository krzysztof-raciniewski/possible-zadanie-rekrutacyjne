<?php

namespace App\Console\Commands;

use App\Services\ServicesInterfaces\BitBayWalletAllertServiceInterface;
use Illuminate\Console\Command;

class MoneyAmountCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'money:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check money amount and send alert e-mail if necessary';

    protected  $bitBayWalletAllertService;

    public function __construct(BitBayWalletAllertServiceInterface $bitBayWalletAllertService)
    {
        parent::__construct();
        $this->bitBayWalletAllertService = $bitBayWalletAllertService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return $this->bitBayWalletAllertService->checkWalletsAmountsAndSendEmail();
    }
}
