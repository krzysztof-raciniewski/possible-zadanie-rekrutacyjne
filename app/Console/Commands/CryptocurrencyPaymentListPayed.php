<?php

namespace App\Console\Commands;

use App\Entities\PaymentOrder;
use App\Entities\Repositories\RepositoriesInterfaces\PaymentOrdersRepository;
use Illuminate\Console\Command;

class CryptocurrencyPaymentListPayed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:list:paid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List only paid orders';

    protected $paymentOrdersRepository;

    /**
     * Create a new command instance.
     *
     * @param PaymentOrdersRepository $paymentOrdersRepository
     */
    public function __construct(PaymentOrdersRepository $paymentOrdersRepository)
    {
        parent::__construct();
        $this->paymentOrdersRepository = $paymentOrdersRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var PaymentOrder[] $orders */
        $orders = $this->paymentOrdersRepository->findByPayed(true);
        if( count($orders) > 0 ) {
            $this->info('--------------------------------');
            $this->info('Ilość obsłużonych wypłat: ' . count($orders));
            $this->info('--------------------------------');
        } else {
            $this->error('-----------------------------------------');
            $this->error('Brak obsłużonych wypłat na konta klientów');
            $this->error('-----------------------------------------');
        }
        foreach ($orders as $order) {
            $this->comment('Wypłacone środki na portfel: ' . $order->getWallet());
            $this->info('Wartość: ' . $order->getValue());
            $this->info('Kryptowaluta: ' . $order->getCurrency());
            $this->info('Data dodania do kolejki: ' . $order->getCreated()->format('Y:m:d H:i:s'));
            $this->info('Data wypłaty: ' . $order->getPaymentDate()->format('Y:m:d H:i:s'));
        }

        return;
    }
}
