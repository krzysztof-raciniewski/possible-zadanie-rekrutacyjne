<?php

namespace App\Console\Commands;

use App\Api\Bitbay\BitbayApi;
use App\Api\Bitbay\BitbayExceptions\BitBayException;
use App\Api\Bitbay\BitbayExceptions\InvalidAmountException;
use App\Api\Bitbay\BitbayExceptions\InvalidWalletAddressException;
use App\Entities\PaymentOrder;
use App\Entities\PaymentOrderLog;
use App\Entities\Repositories\RepositoriesInterfaces\PaymentOrdersRepository;
use App\Entities\Repositories\RepositoriesInterfaces\SystemSettingsRepository;
use App\Entities\SystemSettings;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Console\Command;

class CryptocurrencyPaymentRun extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run payment queue operation';

    /** @var PaymentOrdersRepository $paymentOrdersRepository */
    protected $paymentOrdersRepository;

    /** @var  SystemSettingsRepository $systemSettingsRepository */
    protected $systemSettingsRepository;

    /** @var  EntityManagerInterface $em */
    protected $em;

    /**
     * Create a new command instance.
     *
     * @param PaymentOrdersRepository $paymentOrdersRepository
     * @param SystemSettingsRepository $systemSettingsRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(PaymentOrdersRepository $paymentOrdersRepository, SystemSettingsRepository $systemSettingsRepository, EntityManagerInterface $em)
    {
        parent::__construct();

        $this->paymentOrdersRepository = $paymentOrdersRepository;
        $this->systemSettingsRepository = $systemSettingsRepository;
        $this->em = $em;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var SystemSettings $systemSettings */
        $systemSettings = $this->systemSettingsRepository->findAll()[0];

        if (!$systemSettings->isPaymentProcessActive()) {
            $this->error('Płatności są wyłączone w systemie');
            return;
        } else {
            $this->info('Uruchamiam proces wypłat');
        }

        $bitBayApi = new BitbayApi();
        $payments = $this->paymentOrdersRepository->findByPayed(false);
        $errorMessage = '';

        if (count($payments) === 0) {
            $this->comment('Brak zadań do wykonania');
        }

        /** @var PaymentOrder $payment */
        foreach ($payments as $payment) {
            try {
                $paymentStatus = $bitBayApi->makeTransfer($payment->getCurrency(), $payment->getValue(), $payment->getWallet());
            } catch (InvalidAmountException $e) {
                $paymentStatus = false;
                $errorMessage = 'BitBay: Invalid amount: ' . $payment->getValue();
            } catch (InvalidWalletAddressException $e) {
                $paymentStatus = false;
                $errorMessage = 'BitBay: Invalid wallet address';
            } catch (BitBayException $e) {
                $paymentStatus = false;
                $errorMessage = 'BitBay: Exception';
            }

            $paymentLog = new PaymentOrderLog();

            /*
             * Send cryptocurrency procedure
             */
            if (is_array($paymentStatus)) {
                if ($paymentStatus['success'] === 1) {
                    $payment->setPaymentOrderSuccessLogs($paymentLog);
                    $paymentLog->setPaymentOrdersSuccesed($payment);
                    $payment->setPaymentDate(new \DateTime());
                    $payment->setPayed(true);
                    $this->info('Transakcja o identyfikatorze ' . $payment->getTransaction()->getId() . ' zakończona powodzeniem');
                } else {
                    $payment->setPaymentOrderFailuresLogs($paymentLog);
                    $paymentLog->setPaymentOrdersFailures($payment);
                    $payment->setPayed(false);
                    $this->error('Transakcja o identyfikatorze ' . $payment->getTransaction()->getId() . ' zakończona niepowodzeniem');
                }
            } else {
                $payment->setPaymentOrderFailuresLogs($paymentLog);
                $paymentLog->setPaymentOrdersFailures($payment);
                $payment->setPayed(false);
                $this->error('Transakcja o identyfikatorze ' . $payment->getTransaction()->getId() . ' zakończona niepowodzeniem');
            }

            $paymentStatus['system_message'] = $errorMessage;
            $paymentLog->setMessage(json_encode($paymentStatus));
            $payment->incrementTrialsNumber();
            $this->em->persist($paymentLog);
            $this->em->persist($payment);
            $this->em->flush();
        }

        return;
    }
}
