<?php

namespace App\Console\Commands;

use App\Entities\PaymentOrder;
use App\Entities\Repositories\RepositoriesInterfaces\PaymentOrdersRepository;
use Illuminate\Console\Command;

class CryptocurrencyPaymentListUnpaid extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:list:unpaid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List unapid orders';

    protected $paymentOrdersRepository;

    /**
     * Create a new command instance.
     *
     * @param PaymentOrdersRepository $paymentOrdersRepository
     */
    public function __construct(PaymentOrdersRepository $paymentOrdersRepository)
    {
        parent::__construct();
        $this->paymentOrdersRepository = $paymentOrdersRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var PaymentOrder[] $orders */
        $orders = $this->paymentOrdersRepository->findByPayed(false);
        if( count($orders) > 0 ) {
            $this->error('--------------------------------');
            $this->error('Ilość niewypłaconych środków: ' . count($orders));
            $this->error('--------------------------------');
        } else {
            $this->info('-----------------------------------------');
            $this->info('Wszystkie zamówienia wypłaty są obsłużone');
            $this->info('-----------------------------------------');
        }

        foreach ($orders as $order) {
            $this->comment(PHP_EOL.'Niewypłacone środki na portfel: ' . $order->getWallet());
            $this->info('Wartość: ' . $order->getValue(). ' ');
            $this->info('Kryptowaluta: ' . $order->getCurrency(). ' ');
            $this->info('Data dodania do kolejki: ' . $order->getCreated()->format('Y:m:d H:i:s'). ' ');
        }
    }
}
