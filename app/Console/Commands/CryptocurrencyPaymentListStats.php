<?php

namespace App\Console\Commands;

use App\Entities\PaymentOrder;
use App\Entities\Repositories\RepositoriesInterfaces\PaymentOrdersRepository;
use Illuminate\Console\Command;

class CryptocurrencyPaymentListStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:list:stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show payment statistics';

    protected $paymentOrdersRepository;

    /**
     * Create a new command instance.
     *
     * @param PaymentOrdersRepository $paymentOrdersRepository
     */
    public function __construct(PaymentOrdersRepository $paymentOrdersRepository)
    {
        parent::__construct();
        $this->paymentOrdersRepository = $paymentOrdersRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var PaymentOrder[] $orders */
        $orders_paid = $this->paymentOrdersRepository->findByPayed(true);
        $orders_unpaid = $this->paymentOrdersRepository->findByPayed(false);

        $this->info('================================================================');
        $this->info('==                                                            ==');
        $this->info('==                     Statystyki wypłat                      ==');
        $this->info('==                                                            ==');
        $this->info('================================================================');
        $this->warn('==                                                              ');
        $this->warn('==   Ilość wszystkich operacji:                                 ');
        $this->warn('==                                                              ');
        $this->warn('==   '. (count($orders_paid)+count($orders_unpaid))              );
        $this->warn('==                                                              ');
        $this->warn('==   Ilość wszystkich operacji obsłużonych:                     ');
        $this->warn('==                                                              ');
        $this->warn('==   '. (count($orders_paid))                                    );
        $this->warn('==                                                              ');
        $this->warn('==   Ilość wszystkich operacji nieobsłużonych:                  ');
        $this->warn('==                                                              ');
        $this->warn('==   '. (count($orders_unpaid))                                  );
        $this->warn('==                                                              ');
        $this->info('================================================================');

        return;
    }
}
