<?php

namespace App\Enums;

class SystemLogType extends Enum
{
    const PROFILE_DATA_CHANGE = 'profile_data_change';
    const PASSWORD_CHANGE = 'user_password_change';
    const SETTINGS_CHANGE = 'settings_change';
    /*
     * .
     * .
     * .
     * .
     * .
     * .
     * .
     * .
     * .
     * .
     * .
     * .
     * .
     * .
     */
}