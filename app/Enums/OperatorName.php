<?php

namespace App\Enums;

class OperatorName extends Enum
{
    const TMOBILE = 'T-mobile';
    const PLUS = 'Plus GSM';
    const ORANGE = 'Orange';
    const PLAY = 'Play';
    /*
     * .
     * . Add another countries here
     * .
     *
     */
}