<?php

namespace App\Enums;

class CountryShortcut extends Enum
{
    const POLAND = 'PL';
    const GERMANY = 'DE';
    const SPAIN = 'ES';
    /*
     * .
     * . Add another countries here
     * .
     *
     */
}