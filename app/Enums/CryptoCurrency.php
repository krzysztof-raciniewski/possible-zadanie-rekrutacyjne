<?php

namespace App\Enums;

class CryptoCurrency extends Enum
{
    const BITCOIN = 'BTC';
    const LISK = 'LSK';
    const LITECOIN = 'LTC';
    const ETHEREUM = 'ETH';
    /*
     * .
     * . Add another cryptocurrencies here
     * .
     *
     */
}