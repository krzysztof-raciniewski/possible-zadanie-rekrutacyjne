<?php
declare(strict_types = 1);

namespace App\Enums;


use Exception;
use ReflectionClass;

/**
 * Abstract enum class
 * Class Enum
 * @package App\Api\Bitbay\Enum
 */
abstract class Enum
{
    final public function __construct()
    {

    }

    public function getArray(): array
    {
        $c = new ReflectionClass($this);
        $constants = $c->getConstants();

        $result = [];
        foreach($constants as $key => $constant) {
            $result[] = [
                'code' => $constant,
                'name' => ucfirst(strtolower($key))
            ];
        }

        return $result;
    }

    public function getValues(): array
    {
        $c = new ReflectionClass($this);
        $constants = $c->getConstants();

        $result = [];
        foreach($constants as $key => $constant) {
            $result[] = $constant;
        }

        return $result;
    }
}