<?php

namespace App\Enums;

use ReflectionClass;

class ProviderName extends Enum
{
    const FORTUMO = 'Fortumo';
    const TXTNATION = 'TxtNation';
    const PAYSERA = 'PaySera';
    const CASHBILL = 'CashBill';
    const EBILL = 'Ebill';
    const JUSTPAY = 'JUSTPAY';
    const MICROSMS = 'MicroSms';
    const TPAY = 'TPAY';
    const DOTPAY = 'DotPay';

    public function getArray(): array
    {
        $c = new ReflectionClass($this);
        $constants = $c->getConstants();
        return $constants;
    }
}