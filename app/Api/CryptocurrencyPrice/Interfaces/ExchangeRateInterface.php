<?php
declare(strict_types=1);

namespace App\Api\CryptocurrencyPrice\Interfaces;


interface ExchangeRateInterface
{
    public function getPrice(string $fromCurrency, string $toCurrency): float;
}