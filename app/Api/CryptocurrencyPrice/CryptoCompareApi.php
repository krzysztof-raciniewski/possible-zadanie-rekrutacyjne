<?php
declare(strict_types=1);

namespace App\Api\CryptocurrencyPrice;


use App\Api\CryptocurrencyPrice\Exceptions\CryptoCompareApiException;
use App\Api\CryptocurrencyPrice\Exceptions\CryptoCompareApiNoResultsException;
use App\Api\CryptocurrencyPrice\Exceptions\ExchangeRateRequestException;
use App\Api\CryptocurrencyPrice\Interfaces\ExchangeRateInterface;
use GuzzleHttp\Client;

class CryptoCompareApi implements ExchangeRateInterface
{

    private $baseUrl = 'https://min-api.cryptocompare.com/data/price';

    private $client;

    /**
     * CryptoCompareApi constructor.
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => $this->baseUrl,
            'timeout' => 2.0,
        ]);
    }


    private function makeParamsString(string $fromCurrency, string $toCurrency): string {
        $from = strtoupper($fromCurrency);
        $to = strtoupper($toCurrency);

        return '?fsym='.$from.'&tsyms='.$to;
    }

    /**
     * Make get request to API
     * @param string $fromCurrency
     * @param string $toCurrency
     * @return mixed
     * @throws CryptoCompareApiException
     * @throws ExchangeRateRequestException
     */
    private function makeRequest(string $fromCurrency, string $toCurrency) {

        $uri = $this->makeParamsString($fromCurrency, $toCurrency);
        $response = $this->client->get($uri);
        if( $response->getStatusCode() !== 200 ) {
            throw new CryptoCompareApiException();
        }

        $json = \GuzzleHttp\json_decode($response->getBody()->getContents());

        if(count($json) === 0) {
            throw new CryptoCompareApiNoResultsException();
        }

        return $json;
    }

    /**
     * Get currency price from result
     * @param string $fromCurrency
     * @param string $toCurrency
     * @return float
     */
    public function getPrice(string $fromCurrency, string $toCurrency): float
    {
        $result = $this->makeRequest($fromCurrency, $toCurrency);
        $objectParam = strtoupper($toCurrency);

        return $result->$objectParam;
    }
}