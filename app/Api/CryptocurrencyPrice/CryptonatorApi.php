<?php
declare(strict_types=1);

namespace App\Api\CryptocurrencyPrice;


use App\Api\CryptocurrencyPrice\Exceptions\CryptoCompareApiException;
use App\Api\CryptocurrencyPrice\Exceptions\CryptoCompareApiNoResultsException;
use App\Api\CryptocurrencyPrice\Exceptions\CryptonatorApiException;
use App\Api\CryptocurrencyPrice\Exceptions\ExchangeRateRequestException;
use App\Api\CryptocurrencyPrice\Interfaces\ExchangeRateInterface;
use GuzzleHttp\Client;

class CryptonatorApi implements ExchangeRateInterface
{

    private $baseUrl = 'https://api.cryptonator.com/api/ticker/';

    private $client;

    /**
     * CryptoCompareApi constructor.
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => $this->baseUrl,
            'timeout' => 2.0,
        ]);
    }

    /**
     * @param string $fromCurrency
     * @param string $toCurrency
     * @return string
     */
    private function makeParamsString(string $fromCurrency, string $toCurrency): string {
        $from = strtolower($fromCurrency);
        $to = strtolower($toCurrency);

        return $from.'-'.$to;
    }

    /**
     * Make get request to API
     * @param string $fromCurrency
     * @param string $toCurrency
     * @return mixed
     * @throws CryptonatorApiException
     */
    private function makeRequest(string $fromCurrency, string $toCurrency) {

        $uri = $this->makeParamsString($fromCurrency, $toCurrency);
        $response = $this->client->get($uri);
        if( $response->getStatusCode() !== 200 ) {
            throw new CryptonatorApiException();
        }

        $json = \GuzzleHttp\json_decode($response->getBody()->getContents());

        if(count($json) === 0) {
            throw new CryptonatorApiException();
        }

        if(!property_exists($json,'ticker')) {
            throw new CryptonatorApiException();
        }

        return $json->ticker;
    }

    /**
     * Get currency price from result
     * @param string $fromCurrency
     * @param string $toCurrency
     * @return float
     * @throws CryptonatorApiException
     */
    public function getPrice(string $fromCurrency, string $toCurrency): float
    {
        $result = $this->makeRequest($fromCurrency, $toCurrency);

        if( !property_exists($result, 'price') ) {
            throw new CryptonatorApiException();
        }

        return (float)$result->price;
    }
}