<?php
/**
 * Created by IntelliJ IDEA.
 * User: raciniak
 * Date: 27.08.16
 * Time: 19:38
 */

namespace App\Api\Bitbay;


use App\Api\Bitbay\BitbayExceptions\AccountLockedException;
use App\Api\Bitbay\BitbayExceptions\BicOrSwiftIsRequiredException;
use App\Api\Bitbay\BitbayExceptions\BitBayConnectionException;
use App\Api\Bitbay\BitbayExceptions\BitBayException;
use App\Api\Bitbay\BitbayExceptions\InvalidAmountException;
use App\Api\Bitbay\BitbayExceptions\InvalidCurrencyNameException;
use App\Api\Bitbay\BitbayExceptions\InvalidMarketException;
use App\Api\Bitbay\BitbayExceptions\InvalidMethodNameException;
use App\Api\Bitbay\BitbayExceptions\InvalidMethodParametersException;
use App\Api\Bitbay\BitbayExceptions\InvalidMomentParameterException;
use App\Api\Bitbay\BitbayExceptions\InvalidOrderIdException;
use App\Api\Bitbay\BitbayExceptions\InvalidOrderTypeException;
use App\Api\Bitbay\BitbayExceptions\InvalidPaymentCurrencyNameException;
use App\Api\Bitbay\BitbayExceptions\InvalidPublicKeyException;
use App\Api\Bitbay\BitbayExceptions\InvalidSignException;
use App\Api\Bitbay\BitbayExceptions\InvalidTransactionTypeException;
use App\Api\Bitbay\BitbayExceptions\InvalidWalletAddressException;
use App\Api\Bitbay\BitbayExceptions\LackOfPermissionForThisActionException;
use App\Api\Bitbay\BitbayExceptions\NoOrdersWithSpecifiedCurrenciesException;
use App\Api\Bitbay\BitbayExceptions\WalletEmptyException;
use App\Api\Bitbay\Enum\ErrorCode;

class BitbayApiResponseValidator
{
    /**
     * @param string $response
     * @return array
     * @throws BitBayException
     */
    public static function valid($response): array
    {

        if( $response === false ) {
            throw new BitBayConnectionException();
        }

        $json = json_decode($response, true);

        if (!$response) {
            throw new BitBayException();
        }

        if (array_key_exists('code', $json)) {
            switch ($json['code']) {
                case ErrorCode::INVALID_METHOD_PARAMETERS:
                    throw new InvalidMethodParametersException();
                case ErrorCode::INVALID_ORDER_TYPE:
                    throw new InvalidOrderTypeException();
                    break;
                case ErrorCode::NO_ORDERS_WITH_SPECIFIED_CURRENCIES:
                    throw new NoOrdersWithSpecifiedCurrenciesException();
                    break;
                case ErrorCode::INVALID_PAYMENT_CURRENCY_NAME:
                    throw new InvalidPaymentCurrencyNameException();
                    break;
                case ErrorCode::INVALID_TRANSACTION_TYPE:
                    throw new InvalidTransactionTypeException();
                    break;
                case ErrorCode::INVALID_ORDER_ID:
                    throw new InvalidOrderIdException();
                    break;
                case ErrorCode::NO_ENOUGHT_MONEY_OR_CRYPTO:
                    throw new WalletEmptyException();
                    break;
                case ErrorCode::INVALID_CURRENCY_NAME:
                    throw new InvalidCurrencyNameException();
                    break;
                case ErrorCode::INVALID_PUBLIC_KEY:
                    throw new InvalidPublicKeyException();
                    break;
                case ErrorCode::INVALID_SIGN:
                    throw new InvalidSignException();
                    break;
                case ErrorCode::INVALID_MOMENT_PARAMETER:
                    throw new InvalidMomentParameterException();
                    break;
                case ErrorCode::INVALID_METHOD:
                    throw new InvalidMethodNameException();
                    break;
                case ErrorCode::INVALID_PERMISSION_FOR_KEY:
                    throw new LackOfPermissionForThisActionException();
                    break;
                case ErrorCode::ACCOUNT_LOCKED:
                    throw new AccountLockedException();
                    break;
                case ErrorCode::BIC_OR_SWIFT_REQUIRED:
                    throw new BicOrSwiftIsRequiredException();
                    break;
                case ErrorCode::INVALID_MARKET_NAME:
                    throw new InvalidMarketException();
            }
        }

        if (array_key_exists('error', $json)) {
            if ($json['error'] === 'NotValidAmount') {
                throw new InvalidAmountException();
            }
            if ($json['error'] === 'not valid address') {
                throw new InvalidWalletAddressException();
            }
        }

        return $json;
    }
}