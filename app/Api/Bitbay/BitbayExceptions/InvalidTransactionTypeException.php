<?php

namespace App\Api\Bitbay\BitbayExceptions;


class InvalidTransactionTypeException extends BitBayException
{
}