<?php

namespace App\Api\Bitbay\BitbayExceptions;


class InvalidOrderIdException extends BitBayException
{
}