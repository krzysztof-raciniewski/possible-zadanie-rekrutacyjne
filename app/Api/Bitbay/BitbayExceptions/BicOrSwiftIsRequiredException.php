<?php

namespace App\Api\Bitbay\BitbayExceptions;


class BicOrSwiftIsRequiredException extends BitBayException
{
}