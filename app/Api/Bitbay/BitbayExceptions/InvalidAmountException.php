<?php

namespace App\Api\Bitbay\BitbayExceptions;


class InvalidAmountException extends BitBayException
{
}