<?php

namespace App\Api\Bitbay\BitbayExceptions;


class AccountLockedException extends BitBayException
{
}