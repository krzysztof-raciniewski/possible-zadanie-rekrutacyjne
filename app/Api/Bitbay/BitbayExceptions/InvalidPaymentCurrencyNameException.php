<?php

namespace App\Api\Bitbay\BitbayExceptions;


class InvalidPaymentCurrencyNameException extends BitBayException
{
}