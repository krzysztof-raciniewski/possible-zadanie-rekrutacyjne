<?php

namespace App\Api\Bitbay\BitbayExceptions;


class InvalidCurrencyNameException extends BitBayException
{
}