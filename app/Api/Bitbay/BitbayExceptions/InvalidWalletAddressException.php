<?php

namespace App\Api\Bitbay\BitbayExceptions;


class InvalidWalletAddressException extends BitBayException
{
}