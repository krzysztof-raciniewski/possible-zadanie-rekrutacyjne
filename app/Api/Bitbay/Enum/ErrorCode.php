<?php
declare(strict_types=1);

namespace App\Api\Bitbay\Enum;
use App\Enums\Enum;

/**
 * All errors const from documentation:
 * https://github.com/BitBayNet/API
 * Class ErrorCode
 * @package Api\Bitbay\Enum
 */
class ErrorCode extends Enum
{
    const
        INVALID_METHOD_PARAMETERS = 400,
        INVALID_ORDER_TYPE = 401,
        NO_ORDERS_WITH_SPECIFIED_CURRENCIES = 402,
        INVALID_PAYMENT_CURRENCY_NAME = 403,
        INVALID_TRANSACTION_TYPE = 404,
        INVALID_ORDER_ID = 405,
        NO_ENOUGHT_MONEY_OR_CRYPTO = 406,
        INVALID_CURRENCY_NAME = 408,
        INVALID_PUBLIC_KEY = 501,
        INVALID_SIGN = 502,
        INVALID_MOMENT_PARAMETER = 503,
        INVALID_METHOD = 504,
        INVALID_PERMISSION_FOR_KEY = 505,
        ACCOUNT_LOCKED = 506,
        BIC_OR_SWIFT_REQUIRED = 509,
        INVALID_MARKET_NAME = 510;
}