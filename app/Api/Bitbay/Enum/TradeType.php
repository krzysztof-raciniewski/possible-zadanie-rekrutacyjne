<?php
declare(strict_types=1);

namespace App\Api\Bitbay\Enum;

use App\Enums\Enum;

class TradeType extends Enum
{
    const
        // Buy request
        BUY = 'bid',
        // Sell request
        SELL = 'ask';
}