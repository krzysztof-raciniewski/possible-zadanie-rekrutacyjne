<?php
declare(strict_types=1);

namespace App\Api\Bitbay\Enum;

use App\Enums\Enum;

class CurrencyName extends Enum
{
    const
        PLN = 'PLN',
        USD = 'USD',
        EUR = 'EUR',
        BTC = 'BTC',
        ETH = 'ETH',
        LSK = 'LSK',
        LTC = 'LTC';

}