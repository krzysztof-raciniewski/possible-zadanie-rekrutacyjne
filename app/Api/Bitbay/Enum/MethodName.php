<?php
declare(strict_types=1);
namespace App\Api\Bitbay\Enum;

use App\Enums\Enum;

class MethodName extends Enum
{
    const
        INFO = 'info',
        TRADE = 'trade',
        CANCEL = 'cancel',
        ORDERBOOK = 'orderbook',
        ORDERS = 'orders',
        TRANSFER = 'transfer',
        WITHDRAW = 'withdraw',
        HISTORY = 'history',
        TRANSACTIONS = 'transactions';

}