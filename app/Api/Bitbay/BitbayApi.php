<?php
declare(strict_types = 1);

namespace App\Api\Bitbay;


use App;
use App\Api\Bitbay\BitbayExceptions\InvalidAmountException;
use App\Api\Bitbay\Enum\CurrencyName;
use App\Api\Bitbay\Enum\MethodName;
use App\Entities\Repositories\RepositoriesInterfaces\BitBayRepository;

class BitbayApi
{
    private static $API_URL = 'https://bitbay.net/API/Trading/tradingApi.php';
    private $bitBayConfiguration;
    private $bitBayRepository;

    /**
     * BitbayApi constructor.
     * @throws App\Api\Bitbay\BitbayExceptions\BitBayNotConfiguredException
     */
    public function __construct()
    {
        /** @var BitBayRepository bitBayRepository */
        $this->bitBayRepository = App::make(BitBayRepository::class);
        $this->bitBayConfiguration = $this->bitBayRepository->getConfiguration();
    }

    /**
     * Send request base method
     * @param string $methodName
     * @param array $params
     * @param array $headers
     * @param bool $setDefaultParams
     * @param bool $setDefaultHeaders
     * @return array
     * @throws \App\Api\Bitbay\BitbayExceptions\BitBayException
     */
    public function sendRequest(string $methodName, array $params = array(), array $headers = array(), bool $setDefaultParams = true, bool $setDefaultHeaders = true): array
    {
        // Add require params
        if ($setDefaultParams) {
            $params['method'] = $methodName;
            $params['moment'] = time();
        }

        // Create required sign from secret
        $post = http_build_query($params, '', '&');
        $sign = hash_hmac('sha512', $post, $this->bitBayConfiguration->getSecret());

        // Set required headers
        if ($setDefaultHeaders) {
            $headers = array(
                'API-Key: ' . $this->bitBayConfiguration->getKey(),
                'API-Hash: ' . $sign,
            );
        }

        // Make request
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, self::$API_URL);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($curl);

        return BitbayApiResponseValidator::valid($result);
    }

    /**
     * Send post request
     * @param string $methodName
     * @param array $params
     * @param array $headers
     * @param bool $setDefaultParams
     * @param bool $setDefaultHeaders
     * @return array
     * @throws \App\Api\Bitbay\BitbayExceptions\BitBayException
     */
    public function sendPostRequest(string $methodName = '', array $params = array(), array $headers = array(), bool $setDefaultParams = true, bool $setDefaultHeaders = true) : array
    {
        return $this->sendRequest($methodName, $params, $headers, $setDefaultParams, $setDefaultHeaders);
    }

    /**
     * Get informations about wallets from BitBay.pl
     * @param array $params
     * @return array
     * @throws \App\Api\Bitbay\BitbayExceptions\BitBayException
     */
    public function getInfo(array $params = array()): array
    {
        return $this->sendPostRequest(MethodName::INFO, $params);
    }

    /**
     * Get informations about BTC wallet from BitBay.pl
     * @return array
     * @throws \App\Api\Bitbay\BitbayExceptions\BitBayException
     */
    public function getInfoAboutBTC(): array
    {
        return $this->sendPostRequest(MethodName::INFO, ['currency' => CurrencyName::BTC]);
    }

    /**
     * Get informations about LTC wallet from BitBay.pl
     * @return array
     * @throws \App\Api\Bitbay\BitbayExceptions\BitBayException
     */
    public function getInfoAboutLTC(): array
    {
        return $this->sendPostRequest(MethodName::INFO, ['currency' => CurrencyName::LTC]);
    }

    /**
     * Get informations about ETH wallet from BitBay.pl
     * @return array
     * @throws \App\Api\Bitbay\BitbayExceptions\BitBayException
     */
    public function getInfoAboutETH(): array
    {
        return $this->sendPostRequest(MethodName::INFO, ['currency' => CurrencyName::ETH]);
    }

    /**
     * Get informations about LSK wallet from BitBay.pl
     * @return array
     * @throws \App\Api\Bitbay\BitbayExceptions\BitBayException
     */
    public function getInfoAboutLSK(): array
    {
        return $this->sendPostRequest(MethodName::INFO, ['currency' => CurrencyName::LSK]);
    }

    /**
     * @param array $params
     * @return array
     * @throws \App\Api\Bitbay\BitbayExceptions\BitBayException
     */
    public function trade(array $params = array()): array
    {
        return $this->sendPostRequest(MethodName::TRADE, $params);
    }

    /**
     * @param int $offer_id
     * @return array
     * @throws \App\Api\Bitbay\BitbayExceptions\BitBayException
     */
    public function cancel(int $offer_id): array
    {
        return $this->sendPostRequest(MethodName::CANCEL, [
            'id' => $offer_id
        ]);
    }

    /**
     * @param string $order_currency
     * @param string $payment_currency
     * @return array
     * @throws \App\Api\Bitbay\BitbayExceptions\BitBayException
     */
    public function getOpenOffersFromMarket(string $order_currency = CurrencyName::PLN, string $payment_currency = CurrencyName::PLN): array
    {
        return $this->sendPostRequest(MethodName::ORDERBOOK, [
            'order_currency' => $order_currency,
            'payment_currency' => $payment_currency
        ]);
    }

    /**
     * @param int|null $limit
     * @return array
     * @throws \App\Api\Bitbay\BitbayExceptions\BitBayException
     */
    public function getMyActiveOrders(int $limit = null): array
    {
        $params = [];

        if($limit !== null) {
            $params['limit'] = $limit;
        }

        $result = $this->sendPostRequest(MethodName::ORDERS, $params);

        // Remove inactive orders
        foreach ($result as $key => $order) {
            if ($order['status'] === 'inactive') {
                unset($result[$key]);
            }
        }

        return $result;
    }

    /**
     * @param int|null $limit
     * @return array
     * @throws \App\Api\Bitbay\BitbayExceptions\BitBayException
     */
    public function getMyInactiveOrders(int $limit = null): array
    {
        $params = [];

        if($limit !== null) {
            $params['limit'] = $limit;
        }

        $result = $this->sendPostRequest(MethodName::ORDERS, $params);

        // Remove active orders
        foreach ($result as $key => $order) {
            if ($order['status'] === 'active') {
                unset($result[$key]);
            }
        }

        return $result;
    }

    /**
     * @param int|null $limit
     * @return array
     */
    public function getMyOrders(int $limit = null): array
    {
        $params = [];

        if($limit !== null) {
            $params['limit'] = $limit;
        }

        return $this->sendPostRequest(MethodName::ORDERS, $params);
    }

    /**
     * Convert float to string
     * @param $floatAsString
     * @return string
     */
    private function convertFloat($floatAsString)
    {
        $norm = strval(floatval($floatAsString));
        $e = strrchr($norm, 'E');
        if ($e === false) {
            return $norm;
        }

        return number_format((float)$norm, -intval(substr($e, 1)));
    }

    /**
     * Make transfer to client account
     * @param string $currency Currency name
     * @param float|int $quantity Currency quantity
     * @param string $address Wallet address
     * @return array
     * @throws \App\Api\Bitbay\BitbayExceptions\BitBayException
     */
    public function makeTransfer(string $currency, float $quantity, string $address = '1AWvKXjK7PSmrXQRNKrbBMB5ZqDPBpwr1N'): array
    {
        if ($quantity <= 0) {
            throw new InvalidAmountException();
        }
        $value = $this->convertFloat((string)$quantity);

        return $this->sendPostRequest(MethodName::TRANSFER, [
            'currency' => $currency,
            'quantity' => $value,
            'address' => $address
        ]);
    }

    /**
     * @param string $currency
     * @param int $limit
     * @return array
     */
    public function getHistory(string $currency = CurrencyName::BTC, int $limit = 10): array
    {
            return $this->sendPostRequest(MethodName::HISTORY, [
                'currency' => $currency,
                'limit' => $limit
            ]);
    }

    /**
     * @param string|null $market
     * @return array
     */
    public function getTransactionsHistory(string $market = null): array
    {
        if($market === '') {
            $market = null;
        }

        $params = [];

        if ($market) {
            $params['market'] = $market;
        }

        return $this->sendPostRequest(MethodName::TRANSACTIONS, $params);
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->bitBayConfiguration->getKey();
    }

    /**
     * @param string $key
     */
    public function setKey(string $key)
    {
        $this->bitBayConfiguration->setKey($key);
    }

    /**
     * @param string $secret
     */
    public function setSecret(string $secret)
    {
        $this->bitBayConfiguration->setSecret($secret);
    }

}