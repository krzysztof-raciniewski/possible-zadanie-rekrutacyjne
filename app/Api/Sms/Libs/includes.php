<?php

if (!class_exists('WebToPay')) {
    include __DIR__ . '/WebToPay.php';
    include __DIR__ . '/WebToPayException.php';
    include __DIR__ . '/WebToPay/Exception/Callback.php';
    include __DIR__ . '/WebToPay/Exception/Configuration.php';
    include __DIR__ . '/WebToPay/Exception/Validation.php';
    include __DIR__ . '/WebToPay/Sign/SignCheckerInterface.php';
    include __DIR__ . '/WebToPay/Sign/SS1SignChecker.php';
    include __DIR__ . '/WebToPay/Sign/SS2SignChecker.php';
    include __DIR__ . '/WebToPay/CallbackValidator.php';
    include __DIR__ . '/WebToPay/Factory.php';
    include __DIR__ . '/WebToPay/PaymentMethod.php';
    include __DIR__ . '/WebToPay/PaymentMethodCountry.php';
    include __DIR__ . '/WebToPay/PaymentMethodGroup.php';
    include __DIR__ . '/WebToPay/PaymentMethodList.php';
    include __DIR__ . '/WebToPay/PaymentMethodListProvider.php';
    include __DIR__ . '/WebToPay/RequestBuilder.php';
    include __DIR__ . '/WebToPay/SmsAnswerSender.php';
    include __DIR__ . '/WebToPay/Util.php';
    include __DIR__ . '/WebToPay/WebClient.php';
    include __DIR__ . '/WebToPay/UrlBuilder.php';
}