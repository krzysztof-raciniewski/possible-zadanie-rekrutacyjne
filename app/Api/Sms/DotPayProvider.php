<?php
declare(strict_types = 1);


namespace App\Api\Sms;


use App\Api\Sms\CustomInterface\ProviderInterface;
use App\Api\Sms\ProviderException\DotPayBadServerException;
use App\Entities\ProviderConfiguration;
use App\Entities\Sms;

class DotPayProvider extends Provider implements ProviderInterface
{
    private $type = 'sms';
    private $sms;

    /**
     * DotPayProvider constructor.
     * @param Sms $sms
     */
    public function __construct(Sms $sms)
    {
        $this->baseUrl = 'https://ssl.dotpay.pl/check_code.php';
        $command = $sms->getCommand();
        $commandExplode = explode('.', $command);
        $this->serviceId = $commandExplode[1];

        /** @var ProviderConfiguration $providerConfiguration */
        $providerConfiguration = $sms->getProviderConfiguration();
        $this->userId = $providerConfiguration->getLogin();
        $this->type = $providerConfiguration->getType();
        $this->sms = $sms;
    }

    public function checkCode(string $code): bool
    {
        $params = [];
        $params['check'] = $code;
        $params['code'] = $this->serviceId;
        $params['id'] = $this->userId;
        $params['type'] = $this->type;
        $params['del'] = (int)$this->isProduction();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->baseUrl);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $this->response = curl_exec($ch);
        curl_close($ch);

        $data = explode("\n", $this->response);

        $status = $data[0];

        if( count($data) > 2 ) {
            $message = $data[2];

            if($message === 'Bad server') {
                throw new DotPayBadServerException();
            }
        }

        if( (int)$status === 1 ) {
            return true;
        }

        return false;
    }
}