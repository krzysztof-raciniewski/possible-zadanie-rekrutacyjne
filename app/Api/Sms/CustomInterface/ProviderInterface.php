<?php
declare(strict_types = 1);

namespace App\Api\Sms\CustomInterface;

interface ProviderInterface
{
    public function checkCode(string $code): bool;
    public function setBaseUrl(string $baseUrl);
    public function setServiceId(string $serviceId);
    public function setSUserId(string $userId);
}