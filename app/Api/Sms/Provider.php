<?php
declare(strict_types = 1);

namespace App\Api\Sms;

/**
 * Abstract class for providers implementation
 * Class Provider
 * @package App\Api\Sms
 */
abstract class Provider
{
    protected $userId;
    protected $serviceId;
    protected $smsContent;
    protected $smsNumber;
    protected $baseUrl;
    protected $hash;
    protected $response;
    protected $token;

    // Disable testing environment default
    private $testing = false;

    /**
     * @param string $baseUrl
     */
    public function setBaseUrl(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @param string $serviceId
     */
    public function setServiceId(string $serviceId)
    {
        $this->serviceId = $serviceId;
    }

    /**
     * @return string
     */
    public function getServiceId(): string {
        return $this->serviceId;
    }

    /**
     * @param string $userId
     */
    public function setSUserId(string $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash(string $hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param null $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * @return mixed
     */
    public function getSmsNumber()
    {
        return $this->smsNumber;
    }

    /**
     * @param mixed $smsNumber
     */
    public function setSmsNumber($smsNumber)
    {
        $this->smsNumber = $smsNumber;
    }

    /**
     * @return mixed
     */
    public function getSmsContent()
    {
        return $this->smsContent;
    }

    /**
     * @param mixed $smsContent
     */
    public function setSmsContent($smsContent)
    {
        $this->smsContent = $smsContent;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    public function setTestingEnvironment() {
        $this->testing = true;
    }

    public function setProductionEnvironment() {
        $this->testing = false;
    }

    public function isTesting(): bool {
        return $this->testing === true;
    }

    public function isProduction(): bool {
        return $this->testing === false;
    }
}