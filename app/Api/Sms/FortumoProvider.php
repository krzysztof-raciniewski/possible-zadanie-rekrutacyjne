<?php
declare(strict_types = 1);


namespace App\Api\Sms;


use App;
use App\Api\Sms\ProviderException\FortumoInvalidParametersException;
use App\Entities\Repositories\RepositoriesInterfaces\SmsRepository;
use App\Entities\Sms;

class FortumoProvider
{

    private $smsRepository;
    private $sms;

    /**
     * FortumoProvider constructor.
     * @param $params
     * @throws FortumoInvalidParametersException
     */
    public function __construct($params)
    {
        if(!$this->checkParams($params)) {
            throw new FortumoInvalidParametersException();
        }

        $this->smsRepository = App::make(SmsRepository::class);
        $this->params = $params;
        ksort($this->params);

        // Find SMS in database
        $this->sms = $this->getSmsConfiguration();
    }

    /**
     * Check request signature
     * @return bool
     */
    public function checkSignature(): bool
    {
        $str = '';

        foreach ($this->params as $k => $v) {
            if ($k != 'sig') {
                $str .= "$k=$v";
            }
        }

        $str .= $this->sms->getToken();
        $signature = md5($str);

        return ($this->params['sig'] == $signature);
    }

    private function checkParams($params)
    {
        $params_keys = [
            'billing_type',
            'country',
            'currency',
            'keyword',
            'message',
            'message_id',
            'operator',
            'price',
            'price_wo_vat',
            'sender',
            'service_id',
            'shortcode',
            'sig',
            'status',
            'test'
        ];

        foreach ($params_keys as $param) {
            if (!array_key_exists($param, $params)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get sms entity
     * @return Sms
     * @throws FortumoInvalidParametersException
     */
    private function getSmsConfiguration(): Sms
    {
        if (!array_key_exists('service_id', $this->params)) {
            throw new FortumoInvalidParametersException();
        }

        return $this->smsRepository->findOneBy(
            [
                'service_id' => $this->params['service_id']
            ]
        );
    }

}