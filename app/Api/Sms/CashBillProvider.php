<?php
declare(strict_types = 1);


namespace App\Api\Sms;


use App\Api\Sms\CustomInterface\ProviderInterface;
use App\Entities\Sms;

class CashBillProvider extends Provider implements ProviderInterface
{

    /**
     * CashBillPrivider constructor.
     * @param Sms $sms
     */
    public function __construct(Sms $sms)
    {
        $this->baseUrl = 'https://sms.cashbill.pl/code/';
        $this->token = $sms->getToken();
    }

    public function checkCode(string $code): bool
    {
        $connection = curl_init();
        curl_setopt_array($connection, array(
            CURLOPT_URL => $this->baseUrl . $this->token . '/' . $code,
            CURLOPT_RETURNTRANSFER => true
        ));
        $this->response = json_decode(curl_exec($connection), true);

        if(isset($this->response['error'])) {
            return false;
        }

        return $this->response['active'];
    }
}