<?php
declare(strict_types = 1);

namespace App\Api\Sms\Randomizer;


use App\Entities\Provider;
use App\Entities\Repositories\CustomProviderRepository;
use App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository;
use Doctrine\ORM\EntityManagerInterface;

class SmsProviderRandomizer
{
    /**
     * @var CustomProviderRepository
     */
//    private $providerRepository;
//
//    private $em;
//
//    /**
//     * SmsProviderRandomizer constructor.
//     * @param ProvidersRepository $providersRepository
//     * @param EntityManagerInterface $em
//     */
//    public function __construct(ProvidersRepository $providersRepository, EntityManagerInterface $em)
//    {
//        /** @var CustomProviderRepository providerRepository */
//        $this->providerRepository = $providersRepository;
//
//        $this->em = $em;
//    }
//
//    /**
//     * Get active provider at this moment
//     * @return Provider
//     * @throws \Doctrine\ORM\NonUniqueResultException
//     * @throws \Doctrine\ORM\NoResultException
//     */
//    public function getActive(): Provider
//    {
//        return $this->providerRepository->findFirstActive();
//    }
//
//    public function switch()
//    {
//        /** @var Provider[] $providers */
//        $providers = $this->providerRepository->findAllConfiguredAndEnabled();
//
//        for ($i = 0, $max = count($providers); $i < $max; $i++) {
//            if ($providers[$i]->isActive()) {
//                $providers[$i]->setActive(false);
//                $providers[($i + 1) % $max]->setActive(true);
//                $this->em->persist($providers[$i]);
//                $this->em->persist($providers[($i + 1) % $max]);
//                break;
//            }
//        }
//
//        $this->em->flush();
//    }
}