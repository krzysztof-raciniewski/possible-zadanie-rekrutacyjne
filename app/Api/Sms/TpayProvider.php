<?php
declare(strict_types = 1);


namespace App\Api\Sms;


use App\Api\Sms\CustomInterface\ProviderInterface;

class TpayProvider extends Provider implements ProviderInterface
{

    public function __construct()
    {
        $this->baseUrl = 'http://sms.transferuj.pl/widget/verifyCode.php';
        $this->hash = 'xzwbu';
    }

    public function checkCode(string $code, string $number = null): bool
    {
        $postFields = array();
        $postFields['tfCodeToCheck'] = $code;
        $postFields['tfHash'] = $this->hash;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->baseUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        $response = curl_exec($ch);
        curl_close($ch);

        $data = explode("\n", $response);
        $status = $data[0];

        return (int)$status === 1;
    }

}