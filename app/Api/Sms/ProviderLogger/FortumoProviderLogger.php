<?php
declare(strict_types = 1);

namespace App\Api\Sms\ProviderLogger;


use App;
use App\Api\Sms\ProviderLogger\Interfaces\ProviderLoggerInterface;
use App\Entities\PaymentLog;
use App\Entities\PaymentReport;
use App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository;

class FortumoProviderLogger extends ProviderLogger implements ProviderLoggerInterface
{

    /**
     *
     * Fortumo real get params:
     *
     *  {
     *      "billing_type":"MO",
     *      "country":"PL",
     *      "currency":"PLN",
     *      "keyword":"TXT BITMOBILE",
     *      "message":"TEEEEEEEEEEST",
     *      "message_id":"4ef1744aacb8a1abb5cde7a54da4cf2b",
     *      "operator":"Cyfrowy Polsat",
     *      "price":"2.46",
     *      "price_wo_vat":"2.0",
     *      "sender":"570380486",
     *      "service_id":"9a07067d6b3c1bdcd2275688eba85daa",
     *      "shortcode":"7268",
     *      "sig":"285633833b4af2f9d1d8dcbb2198f1f4",
     *      "status":"pending",
     *      "test":"true"
     *  }
     *
     *
     * @param array $params
     * @return Int
     */
    public function logPayment(array $params): int
    {
        $string_params = json_encode($params);

        $paymentLog = new PaymentLog();
        $paymentLog->setJson($string_params);
        $paymentLog->setBillingType($params['billing_type']);
        $paymentLog->setCountry($params['country']);
        $paymentLog->setCurrency($params['currency']);
        $paymentLog->setCommand($params['keyword']);
        $paymentLog->setMessage($params['message']);
        $paymentLog->setMessageId($params['message_id']);
        $paymentLog->setOperator($params['operator']);
        $paymentLog->setNetPrice((float)$params['price_wo_vat']);
        $paymentLog->setGrossPrice((float)$params['price']);
        $paymentLog->setPhoneNumber($params['sender']);
        $paymentLog->setServiceId($params['service_id']);
        $paymentLog->setLaNumber($params['shortcode']);
        $paymentLog->setSig($params['sig']);
        $paymentLog->setStatus($params['status']);
        $paymentLog->setTesting((boolean)$params['test']);
        $paymentLog->setProvider($this->getProvider());

        $this->em->persist($paymentLog);
        $this->em->flush();

        return $paymentLog->getId();
    }

    /**
     *
     * {
     *      "status":"OK",
     *      "sig":"67d3dbc5411d01273a7fbef4cfc88286",
     *      "currency":"PLN",
     *      "price_wo_vat":"2.0",
     *      "message_id":"742837193eed7d425d54416c56c8ca22",
     *      "billing_type":"MO",
     *      "message":"TO JEST TEEEEEST",
     *      "service_id":"9a07067d6b3c1bdcd2275688eba85daa",
     *      "sender":"570380486",
     *      "price":"2.46",
     *      "test":"true",
     *      "country":"PL",
     *      "keyword":"TXT BITMOBILE",
     *      "shortcode":"7268",
     *      "operator":"T-Mobile"
     * }
     *
     * @param array $params
     * @return int
     */
    public function logReport(array $params): int
    {
        $string_params = json_encode($params);

        $paymentReport = new PaymentReport();
        $paymentReport->setJson($string_params);
        $paymentReport->setBillingType($params['billing_type']);
        $paymentReport->setCountry($params['country']);
        $paymentReport->setCurrency($params['currency']);
        $paymentReport->setCommand($params['keyword']);
        $paymentReport->setMessage($params['message']);
        $paymentReport->setMessageId($params['message_id']);
        $paymentReport->setOperator($params['operator']);
        $paymentReport->setNetPrice((float)$params['price_wo_vat']);
        $paymentReport->setGrossPrice((float)$params['price']);
        $paymentReport->setPhoneNumber($params['sender']);
        $paymentReport->setServiceId($params['service_id']);
        $paymentReport->setLaNumber($params['shortcode']);
        $paymentReport->setSig($params['sig']);
        $paymentReport->setStatus($params['status']);
        $paymentReport->setTesting((boolean)$params['test']);
        $paymentReport->setProvider($this->getProvider());

        $this->em->persist($paymentReport);
        $this->em->flush();

        return $paymentReport->getId();
    }
}