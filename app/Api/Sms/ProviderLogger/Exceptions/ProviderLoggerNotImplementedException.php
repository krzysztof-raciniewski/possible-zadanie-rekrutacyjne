<?php
declare(strict_types = 1);

namespace App\Api\Sms\ProviderLogger\Exceptions;


class ProviderLoggerNotImplementedException extends \Exception
{

}