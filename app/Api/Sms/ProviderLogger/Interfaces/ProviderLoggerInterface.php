<?php
declare(strict_types = 1);


namespace App\Api\Sms\ProviderLogger\Interfaces;


interface ProviderLoggerInterface
{
    public function __construct(string $name);
    public function getName(): string;
    public function logPayment(array $params);
    public function logReport(array $params);
}