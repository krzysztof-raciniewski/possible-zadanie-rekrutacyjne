<?php
declare(strict_types = 1);

namespace App\Api\Sms\ProviderLogger;


use App;
use App\Api\Sms\ProviderLogger\Exceptions\ProviderNotFoundException;
use App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository;
use Doctrine\ORM\EntityManagerInterface;

abstract class ProviderLogger
{
    protected $name;
    protected $providersRepository;
    protected $em;
    protected $provider;

    /**
     * ProviderLogger constructor.
     * @param string $name
     * @throws Exceptions\ProviderNotFoundException
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->providersRepository = App::make(ProvidersRepository::class);
        $this->em = App::make(EntityManagerInterface::class);

        $this->provider = $this->providersRepository->findOneByName($this->name);

        if( !$this->provider ) {
            throw new ProviderNotFoundException();
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function getProvider(): App\Entities\Provider {
        return $this->provider;
    }

}