<?php
declare(strict_types = 1);


namespace App\Api\Sms\ProviderLogger;


use App\Api\Sms\ProviderLogger\Exceptions\ProviderLoggerNotImplementedException;
use App\Enums\ProviderName;

class ProviderLoggerFactory
{
    /**
     * @param $name
     * @return FortumoProviderLogger|null
     * @throws ProviderLoggerNotImplementedException
     */
    public static function create($name)
    {
        $logger = null;
        switch($name) {
            case ProviderName::FORTUMO:
                $logger = new FortumoProviderLogger($name);
                break;
        }

        if( !$logger ) {
            throw new ProviderLoggerNotImplementedException();
        } else {
            return $logger;
        }

    }
}