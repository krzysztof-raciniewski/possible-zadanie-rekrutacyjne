<?php
declare(strict_types = 1);

namespace App\Api\Sms;

require_once __DIR__ . '/Libs/includes.php';

use WebToPay;
use WebToPayException;


/**
 * Contains static methods for most used scenarios.
 */
class PayseraProvider
{
    private static $project_id = '87155';
    private static $password = '38739afb686a3ce7ade0e9b69cd9c393';

    /**
     * @param array $method
     * @return array
     * @throws \WebToPayException
     */
    public function checkResponse(array $method): array
    {
        return WebToPay::validateAndParseData($method, self::$project_id, self::$password);
    }

    public function redirectToPayment() {
        function get_self_url() {
            $s = substr(strtolower($_SERVER['SERVER_PROTOCOL']), 0,
                strpos($_SERVER['SERVER_PROTOCOL'], '/'));

            if (!empty($_SERVER["HTTPS"])) {
                $s .= ($_SERVER["HTTPS"] === "on") ? "s" : "";
            }

            $s .= '://'.$_SERVER['HTTP_HOST'];

            if (!empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] != '80') {
                $s .= ':'.$_SERVER['SERVER_PORT'];
            }

            $s .= dirname($_SERVER['SCRIPT_NAME']);

            return $s;
        }

        try {
            $self_url = get_self_url();

            WebToPay::redirectToPayment(array(
                'projectid'     => 87155,
                'sign_password' => '38739afb686a3ce7ade0e9b69cd9c393',
                'orderid'       => 2,
                'amount'        => 10,
                'currency'      => 'PLN',
                'country'       => 'PL',
                'accepturl'     => $self_url.'paysera/accept',
                'cancelurl'     => $self_url.'paysera/cancel',
                'callbackurl'   => $self_url.'paysera/callback',
                'test'          => 1,
            ));
        } catch (WebToPayException $e) {
            // handle exception
        }
    }

}