<?php
declare(strict_types = 1);

namespace App\Api\Sms;


use App\Api\Sms\CustomInterface\ProviderInterface;
use App\Api\Sms\ProviderException\MicroSmsConnectionException;
use App\Api\Sms\ProviderException\MicroSmsInvalidCodeFormatException;
use App\Entities\ProviderConfiguration;
use App\Entities\Sms;

class MicroSmsProvider extends Provider implements ProviderInterface
{
    /** @var  Sms $sms */
    private $sms;

    public function __construct(Sms $sms)
    {
        /** @var ProviderConfiguration $providerConfiguration */
        $providerConfiguration = $sms->getProviderConfiguration();
        $this->baseUrl = 'http://microsms.pl/api/v2/index.php';
        $this->userId = $providerConfiguration->getLogin();
        $this->serviceId = $sms->getServiceId();
        $this->sms = $sms;
    }

    public function checkCode(string $code): bool
    {
        $url = $this->baseUrl . '?userid=' . $this->userId . '&number=' . $this->sms->getLaNumber() . '&code=' . $code . '&serviceid=' . $this->serviceId;
        $api = @file_get_contents($url);

        if (!isset($api)) {
            throw new MicroSmsConnectionException();
        }

        $this->response = json_decode($api, true);

        if (array_key_exists('data', $this->response)) {
            if(array_key_exists('status', $this->response['data'])) {
                if( $this->response['data']['status'] === 1) {
                    return true;
                }
            }
        }

        return false;
    }
}