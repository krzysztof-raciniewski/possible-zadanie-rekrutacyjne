<?php
declare(strict_types=1);

namespace App\Api\Sms\Factory;


use App\Api\Sms\CashBillProvider;
use App\Api\Sms\CustomInterface\ProviderInterface;
use App\Api\Sms\DotPayProvider;
use App\Api\Sms\EbillProvider;
use App\Api\Sms\Factory\Exceptions\ProviderNotSupportedException;
use App\Api\Sms\JustPayProvider;
use App\Api\Sms\MicroSmsProvider;
use App\Entities\Sms;
use App\Enums\ProviderName;

class ProviderFactory
{
    /**
     * @param string $providerName
     * @param Sms $sms
     * @return ProviderInterface
     * @throws ProviderNotSupportedException
     */
    public static function make(string $providerName, Sms $sms): ProviderInterface {
        $validProviders = (new ProviderName())->getValues();

        if( !in_array($providerName, $validProviders) ) {
            throw new ProviderNotSupportedException();
        }

        switch($providerName) {
            case ProviderName::CASHBILL:
                return new CashBillProvider($sms);
                break;
            case ProviderName::DOTPAY:
                return new DotPayProvider($sms);
                break;
            case ProviderName::EBILL:
                return new EbillProvider($sms);
                break;
            case ProviderName::JUSTPAY:
                return new JustPayProvider($sms);
                break;
            case ProviderName::MICROSMS:
                return new MicroSmsProvider($sms);
                break;
        }

        throw new ProviderNotSupportedException();
    }
}