<?php
declare(strict_types = 1);

namespace App\Api\Sms;


use App\Api\Sms\CustomInterface\ProviderInterface;
use App\Api\Sms\ProviderException\MicroSmsConnectionException;
use App\Entities\Sms;

class EbillProvider extends Provider implements ProviderInterface
{
    public function __construct(Sms $sms)
    {
        $this->hash = $sms->getToken();
        $this->serviceId = $sms->getServiceId();
        $this->baseUrl = 'http://smsgw.ebill.pl/sms_codes/check_code.php';
    }

    public function checkCode(string $code): bool
    {
        $url = $this->baseUrl . '?hash=' . $this->hash . '&keyword_id=' . $this->serviceId . '&code=' . $code;

        if ($this->isTesting()) {
            $url .= '&mark=0';
        } else {
            $url .= '&mark=1';
        }

        $this->response = @file_get_contents($url);

        if (!isset($this->response)) {
            throw new MicroSmsConnectionException();
        }

        $data = explode("\n", $this->response);
        $status = $data[0];

        return (int)$status === 1;
    }


}