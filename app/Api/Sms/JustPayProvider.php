<?php
declare(strict_types = 1);


namespace App\Api\Sms;


use App\Api\Sms\CustomInterface\ProviderInterface;
use App\Entities\ProviderConfiguration;
use App\Entities\Sms;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;


class JustPayProvider extends Provider implements ProviderInterface
{
    private $login;
    private $password;

    public function __construct(Sms $sms)
    {
        $this->baseUrl = 'http://www.justpay.pl/smscodes/rpc';

        /** @var ProviderConfiguration $providerConfiguration */
        $providerConfiguration = $sms->getProviderConfiguration();
        $this->login = $providerConfiguration->getLogin();
        $this->password = $providerConfiguration->getPassword();
        $this->serviceId = $sms->getCommand();
        $this->smsNumber = $sms->getLaNumber();
    }

    // Todo check if XML RPC is installed
    public function checkCode(string $code, string $number = ''): bool
    {
        $request = xmlrpc_encode_request('validate',
            array($this->serviceId, $this->smsNumber, $this->login, $this->password, $code));
        $context = stream_context_create(array(
                'http' => array(
                    'method' => 'POST',
                    'header' => 'Content-Type: text/xml',
                    'content' => $request
                )
            )
        );
        $file = file_get_contents($this->baseUrl, false, $context);
        $this->response = xmlrpc_decode($file);

        if (xmlrpc_is_fault($this->response)) {
            return false;
        }
        return $this->response['status'] === 'OK';
    }
}