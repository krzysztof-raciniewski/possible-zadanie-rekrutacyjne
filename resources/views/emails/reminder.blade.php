@extends('emails.layouts.danger')

@section('title', 'Wiadomość z aplikacji')

@section('message')
    <p>{!! $data['message'] !!}</p>
@endsection