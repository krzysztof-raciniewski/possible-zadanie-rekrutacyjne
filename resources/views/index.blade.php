<!doctype html>
<html ng-app="app">
<head>
    <title>Coins4SMS.com - Kup Bitcoin, Litecoin, Ethereum, Lisk za SMS</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="application-name" content="Coins4SMS.com"/>
    <meta name="author" content="Bitmobile sp. z.o.o."/>
    <meta name="robots" content="All"/>
    <meta name="description"
          content="Coins4SMS.com to platforma zakupowa umożliwiająca zakup kryptowalut takich jak Bitcoin, Litecoin, Lisk i Ethereum za premium SMS. Wymień SMS na BTC, LTC, ETH i LSK bezpiecznie, łatwo i szybko. Formularz zamówienia składa się z trzech prostych kroków, w pierwszym kroku wybierasz kraj i operatora swojej sieci komórkowej, dodatkowo możesz wybrać typ waluty jaką chcesz kupić, system umożliwia zakup kryptowalut takich jak: Bitcoin, Litecoin, Ethereum, Lisk. W kroku nr 2 podajesz adres swojego wirtualnego portfela i wybierasz wartość wiadomości SMS, wysyłasz SMS premium pod wskazany numer o wygenerowanej treści i przechodzisz do kroku trzeciego, który jest krokiem ostatnim. W tym kroku podajesz numer kodu jednorazowego, który otrzymasz w wiadomości zwrotnej po wysłaniu SMS. Jeżeli wprowadziłeś prawidłowy kod, aplikacja automatycznie i natychmiast wysyła walutę w celu zasilenia Twojego portfela."/>
    <meta name="keywords"
          content="bitcoin, litecoin, ethereum, lisk, sms,za sms, for sms, btc for sms, eth for sms, ltc for sms, lsk for sms, btc za sms, eth za sms, ltc za sms, lsk za sms, btc za sms, ltc za sms, eth za sms, lsk za sms, btc for sms, lsk for sms, eth for sms, ltc for sms"/>
    <meta name="rating" content="General"/>
    <meta name="dcterms.title" content="Coins4SMS.com - Kupuj kryptowaluty prosto i bezpieczenie za SMS"/>
    <meta name="dcterms.contributor" content="Bitmobile sp. z.o.o."/>
    <meta name="dcterms.creator" content="Bitmobile sp. z.o.o."/>
    <meta name="dcterms.publisher" content="Bitmobile sp. z.o.o."/>
    <meta name="dcterms.description"
          content="Coins4SMS.com to platforma zakupowa umożliwiająca zakup kryptowalut takich jak Bitcoin, Litecoin, Lisk i Ethereum za premium SMS. "/>
    <meta name="dcterms.rights" content="Bitmobile sp. z.o.o."/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Coins4SMS.com - Kupuj kryptowaluty prosto i bezpieczenie za SMS"/>
    <meta property="og:description"
          content="Coins4SMS.com to platforma zakupowa umożliwiająca zakup kryptowalut takich jak Bitcoin, Litecoin, Lisk i Ethereum za premium SMS. "/>
    <meta property="twitter:title" content="Coins4SMS.com - Kupuj kryptowaluty prosto i bezpieczenie za SMS"/>
    <meta property="twitter:description"
          content="Coins4SMS.com to platforma zakupowa umożliwiająca zakup kryptowalut takich jak Bitcoin, Litecoin, Lisk i Ethereum za premium SMS. "/>
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
    <meta name="theme-color" content="#0690B7">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i&subset=latin-ext" rel="stylesheet">
    <!-- Include the JS ReCaptcha API -->
    <script src="//www.google.com/recaptcha/api.js?render=explicit&onload=vcRecaptchaApiLoaded" async defer></script>
    <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTjAQg21gEz1cmu981n-bnT6dJmMyyV8s"></script>
    <!--[if lte IE 10]>
    <script type="text/javascript">document.location.href = '/unsupported-browser'</script>
    <![endif]-->
    @if (App::environment() == 'local')
        <base href="http://coins4sms.dev/">
    @else
        <base href="https://coins4sms.com/">
    @endif

</head>
<body>

<app-view></app-view>

<script async defer src="{!! elixir('js/final.js') !!}?{!! $random !!}"></script>
<script>
    (function () {
        var link = document.createElement("link");
        link.href = "{!! elixir('css/final.css') !!}?{!! $random !!}";
        link.type = "text/css";
        link.rel = "stylesheet";
        document.body.appendChild(link);
    })();
</script>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-84828165-1', 'auto');
    ga('send', 'pageview');
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter41040834 = new Ya.Metrika({
                    id:41040834,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/41040834" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
