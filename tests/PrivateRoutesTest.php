<?php


use App\Api\Bitbay\Enum\CurrencyName;

class PrivateRoutesTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testCurrentUserEntpoint()
    {
        $response = $this->call('GET', '/api/user/current');
        $this->assertEquals(401, $response->status());
    }

    public function testBitbayAmountEntpoint()
    {
        $response = $this->call('GET', '/api/bitbay/amount');
        $this->assertEquals(401, $response->status());
    }

    public function testBitbayAmountCurrencyEntpoint()
    {
        $response = $this->call('GET', '/api/bitbay/amount/' . CurrencyName::BTC);
        $this->assertEquals(401, $response->status());
    }

    public function testBitbayOrdersEntpoint()
    {
        $response = $this->call('GET', '/api/bitbay/orders/');
        $this->assertEquals(401, $response->status());
    }

    public function testBitbayOrdersActiveEntpoint()
    {
        $response = $this->call('GET', '/api/bitbay/orders/active');
        $this->assertEquals(401, $response->status());
    }

    public function testBitbayOrdersInactiveEntpoint()
    {
        $response = $this->call('GET', '/api/bitbay/orders/inactive');
        $this->assertEquals(401, $response->status());
    }

    public function testBitbayHistoryWithEntpoint()
    {
        $response = $this->call('GET', '/api/bitbay/history');
        $this->assertEquals(401, $response->status());
    }

    public function testBitbayHistoryForCurrencyEntpoint()
    {
        $response = $this->call('GET', '/api/bitbay/history/'.CurrencyName::BTC);
        $this->assertEquals(401, $response->status());
    }

    public function testBitbayTransactionsEntpoint()
    {
        $response = $this->call('GET', '/api/bitbay/transactions/'.CurrencyName::BTC);
        $this->assertEquals(401, $response->status());
    }

    public function testBitbayOffersEntpoint()
    {
        $response = $this->call('GET', '/api/bitbay/offers/');
        $this->assertEquals(401, $response->status());
    }
}
