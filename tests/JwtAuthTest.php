<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class JwtAuthTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
        $this->initializeTestsForUserOperations();
    }

    /**
     * Test successful login with JWT.
     */
    public function testSuccessfulLogin()
    {
        /** @var  $userRepository */
        $userRepository = $this->em->getRepository(\App\Entities\User::class);

        $user = $userRepository->findAll()[0];

        $this->post('/api/auth/login', [
            'email'    => $user->getEmail(),
            'password' => 'haslo1234',
        ])
        ->seeApiSuccess()
        ->seeJson(['errors' => false])
        ->seeJsonKey('token')
        ->dontSee('"password"');

        $this->em->clear();
    }

    /**
     * Test failed login with JWT.
     */
    public function testFailedLoginWithIncorrectPassword()
    {
        /** @var  $userRepository */
        $userRepository = $this->em->getRepository(\App\Entities\User::class);

        $user = $userRepository->findAll()[0];

        $this->post('/api/auth/login', [
            'email'    => $user->getEmail(),
            'password' => str_random(10),
        ])
        ->seeApiError(401)
        ->dontSee($user->getEmail())
        ->dontSee('"token"');
    }

    /**
     * Test failed login with JWT.
     */
    public function testFailedLoginWithIncorrectEmail()
    {
        /** @var  $userRepository */
        $userRepository = $this->em->getRepository(\App\Entities\User::class);

        $user = $userRepository->findAll()[0];

        $this->post('/api/auth/login', [
            'email'    => 'bademail@xox.com',
            'password' => str_random(10),
        ])
            ->seeApiError(401)
            ->dontSee($user->getEmail())
            ->dontSee('"token"');
    }

}
