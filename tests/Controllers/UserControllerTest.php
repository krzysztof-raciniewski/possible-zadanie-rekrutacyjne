<?php

class UserControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->initializeTestsForUserOperations();
    }

    public function testGetCurrentUserInfoEndpoint()
    {
        $response = $this->call('GET', '/api/user/current', [], [], [], $this->headers);

        $this->assertEquals(200, $response->status());

        $this->seeJson([
            'email' => $this->user->getEmail(),
            'first_name' => $this->user->getFirstName(),
            'last_name' => $this->user->getLastName(),
            'image' => $this->user->getImage(),
            'id' => $this->user->getId()
        ]);
    }

    public function testGetCurrentUserInfoWithoutHeadersEndpoint()
    {
        $response = $this->call('GET', '/api/user/current');

        $this->assertEquals(401, $response->status());

        $this->seeJson(
            [
                "message" => "Failed to authenticate because of bad credentials or an invalid authorization header.",
                "status_code" => 401
            ]
        );
    }

    public function testGetCurrentUserInfoWithInvalidTokenEndpoint()
    {
        $response = $this->call('GET', '/api/user/current', [], [], [], ['HTTP_Authorization' => 'Bearer ' . 'badtoken']);

        $this->assertEquals(401, $response->status());

        $this->seeJson(
            [
                "message" => "Wrong number of segments",
                "status_code" => 401
            ]
        );
    }

    public function testGetCurrentUserInfoWithInvalidToken2Endpoint()
    {
        $response = $this->call('GET', '/api/user/current', [], [], [],
            ['HTTP_Authorization' => 'Bearer ' . 'ayJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdCIsImlhdCI6MTQ3MzQ0MzU0MiwiZXhwIjoxNDczNDQ3MTQyLCJuYmYiOjE0NzM0NDM1NDIsImp0aSI6IjhkNjc5NGU3MTRjNWFiZDI5OWY1YTFmYzFlZTUzMzk1In0.MrvRwO1nVdS4lunNA047hWgOjGtk6JQEtU8PQy01cNM']);

        $this->assertEquals(401, $response->status());

        $this->seeJsonStructure(
            [
                "message",
                "status_code"
            ]
        );
    }
}
