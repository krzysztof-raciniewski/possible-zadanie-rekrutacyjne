<?php

use App\Api\Bitbay\Enum\CurrencyName;

class BitbayControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->initializeTestsForUserOperations();
    }

    public function testGetBitbayAmountWithoutParamsEndpoint()
    {
        $response = $this->call('GET', '/api/bitbay/amount', [], [], [], $this->headers);

//        $this->assertEquals(200, $response->status());

        $this->assertResponseStatus(200);

        $this->seeJson(
            [
                "addresses" => [
                    "BTC" => "1BcGBvk8XwZARvh4mCbffr9T3oRKwKNbm3",
                    "ETH" => "0x475eed1c5dd4a6e32a3b62d15d8c8db54f9e230d",
                    "LSK" => "8814452300831666598L",
                    "LTC" => "LUVjF5KodpYuEh2H7C97Euvv62xSWAQPYQ"
                ]
            ]
        );

        $this->seeJsonKey('balances');
        $this->seeJson(['success' => 1]);


        $this->seeJsonStructure(
            [
                "success",
                "balances" => [
                    "PLN" => ["available", "locked"],
                    "EUR" => ["available", "locked"],
                    "USD" => ["available", "locked"],
                    "BTC" => ["available", 'locked'],
                    "LTC" => ["available", "locked"],
                    "ETH" => ["available", "locked"],
                    "LSK" => ["available", "locked"]
                ],
                "addresses" => [
                    "BTC", "LTC", "ETH", "LSK"]
            ]

        );

        $content = $response->getContent();
        $json = json_decode($content);

        $balances = $json->balances;

        $this->assertNotNull($balances);
        $this->assertTrue(array_key_exists(CurrencyName::ETH, $balances));
        $this->assertTrue(array_key_exists(CurrencyName::LSK, $balances));
        $this->assertTrue(array_key_exists(CurrencyName::BTC, $balances));
        $this->assertTrue(array_key_exists(CurrencyName::EUR, $balances));
        $this->assertTrue(array_key_exists(CurrencyName::PLN, $balances));
        $this->assertTrue(array_key_exists(CurrencyName::USD, $balances));
        $this->assertTrue(array_key_exists(CurrencyName::LTC, $balances));
    }

    public function testGetBitbayAmountWithBTCParam()
    {
        $response = $this->call('GET', '/api/bitbay/amount/BTC', [], [], [], $this->headers);

        $this->assertEquals(200, $response->status());

        $this->assertResponseStatus(200);

        $this->seeJson(
            [
                "addresses" => [
                    "BTC" => "1BcGBvk8XwZARvh4mCbffr9T3oRKwKNbm3",
                    "ETH" => "0x475eed1c5dd4a6e32a3b62d15d8c8db54f9e230d",
                    "LSK" => "8814452300831666598L",
                    "LTC" => "LUVjF5KodpYuEh2H7C97Euvv62xSWAQPYQ"
                ]
            ]
        );

        $this->seeJsonKey('balances');
        $this->seeJson(['success' => 1]);


        $this->seeJsonStructure(
            [
                "success",
                "balances" => [
                    "BTC" => ["available", 'locked'],
                ],
                "addresses" => [
                    "BTC", "LTC", "ETH", "LSK"]
            ]
        );

        $content = $response->getContent();
        $json = json_decode($content);

        $balances = $json->balances;

        $this->assertNotNull($balances);
        $this->assertFalse(array_key_exists(CurrencyName::ETH, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::LSK, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::EUR, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::PLN, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::USD, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::LTC, $balances));
    }

    public function testGetBitbayAmountWithLSKaram()
    {
        $response = $this->call('GET', '/api/bitbay/amount/' . CurrencyName::LSK, [], [], [], $this->headers);

        $this->assertEquals(200, $response->status());

        $this->assertResponseStatus(200);

        $this->seeJson(
            [
                "addresses" => [
                    "BTC" => "1BcGBvk8XwZARvh4mCbffr9T3oRKwKNbm3",
                    "ETH" => "0x475eed1c5dd4a6e32a3b62d15d8c8db54f9e230d",
                    "LSK" => "8814452300831666598L",
                    "LTC" => "LUVjF5KodpYuEh2H7C97Euvv62xSWAQPYQ"
                ]
            ]
        );

        $this->seeJsonKey('balances');
        $this->seeJson(['success' => 1]);


        $this->seeJsonStructure(
            [
                "success",
                "balances" => [
                    "LSK" => ["available", 'locked'],
                ],
                "addresses" => [
                    "BTC", "LTC", "ETH", "LSK"]
            ]
        );

        $content = $response->getContent();
        $json = json_decode($content);

        $balances = $json->balances;

        $this->assertNotNull($balances);
        $this->assertFalse(array_key_exists(CurrencyName::ETH, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::BTC, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::EUR, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::PLN, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::USD, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::LTC, $balances));
    }

    public function testGettBitbayAmountWithLTCParam()
    {
        $response = $this->call('GET', '/api/bitbay/amount/LTC', [], [], [], $this->headers);

        $this->assertEquals(200, $response->status());

        $this->assertResponseStatus(200);

        $this->seeJson(
            [
                "addresses" => [
                    "BTC" => "1BcGBvk8XwZARvh4mCbffr9T3oRKwKNbm3",
                    "ETH" => "0x475eed1c5dd4a6e32a3b62d15d8c8db54f9e230d",
                    "LSK" => "8814452300831666598L",
                    "LTC" => "LUVjF5KodpYuEh2H7C97Euvv62xSWAQPYQ"
                ]
            ]
        );

        $this->seeJsonKey('balances');
        $this->seeJson(['success' => 1]);


        $this->seeJsonStructure(
            [
                "success",
                "balances" => [
                    "LTC" => ["available", 'locked'],
                ],
                "addresses" => [
                    "BTC", "LTC", "ETH", "LSK"]
            ]
        );

        $content = $response->getContent();
        $json = json_decode($content);

        $balances = $json->balances;

        $this->assertFalse(array_key_exists(CurrencyName::ETH, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::LSK, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::BTC, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::EUR, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::PLN, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::USD, $balances));
    }

    public function testGettBitbayAmountWithETHParam()
    {
        $response = $this->call('GET', '/api/bitbay/amount/ETH', [], [], [], $this->headers);

        $this->assertEquals(200, $response->status());

        $this->assertResponseStatus(200);

        $this->seeJson(
            [
                "addresses" => [
                    "BTC" => "1BcGBvk8XwZARvh4mCbffr9T3oRKwKNbm3",
                    "ETH" => "0x475eed1c5dd4a6e32a3b62d15d8c8db54f9e230d",
                    "LSK" => "8814452300831666598L",
                    "LTC" => "LUVjF5KodpYuEh2H7C97Euvv62xSWAQPYQ"
                ]
            ]
        );

        $this->seeJsonKey('balances');
        $this->seeJson(['success' => 1]);


        $this->seeJsonStructure(
            [
                "success",
                "balances" => [
                    "ETH" => ["available", 'locked'],
                ],
                "addresses" => [
                    "BTC", "LTC", "ETH", "LSK"]
            ]
        );

        $content = $response->getContent();
        $json = json_decode($content);

        $balances = $json->balances;

        $this->assertFalse(array_key_exists(CurrencyName::LSK, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::BTC, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::EUR, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::PLN, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::USD, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::LTC, $balances));
    }

    public function testGettBitbayAmountWithETHSmallLettersParam()
    {
        $response = $this->call('GET', '/api/bitbay/amount/eth', [], [], [], $this->headers);

        $this->assertEquals(200, $response->status());

        $this->assertResponseStatus(200);

        $this->seeJson(
            [
                "addresses" => [
                    "BTC" => "1BcGBvk8XwZARvh4mCbffr9T3oRKwKNbm3",
                    "ETH" => "0x475eed1c5dd4a6e32a3b62d15d8c8db54f9e230d",
                    "LSK" => "8814452300831666598L",
                    "LTC" => "LUVjF5KodpYuEh2H7C97Euvv62xSWAQPYQ"
                ]
            ]
        );

        $this->seeJsonKey('balances');
        $this->seeJson(['success' => 1]);


        $this->seeJsonStructure(
            [
                "success",
                "balances" => [
                    "ETH" => ["available", 'locked'],
                ],
                "addresses" => [
                    "BTC", "LTC", "ETH", "LSK"]
            ]
        );

        $content = $response->getContent();
        $json = json_decode($content);

        $balances = $json->balances;

        $this->assertFalse(array_key_exists(CurrencyName::LSK, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::BTC, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::EUR, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::PLN, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::USD, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::LTC, $balances));
    }

    public function testGettBitbayAmountWithBTCSmallLettersParam()
    {
        $response = $this->call('GET', '/api/bitbay/amount/btc', [], [], [], $this->headers);

        $this->assertEquals(200, $response->status());

        $this->assertResponseStatus(200);

        $this->seeJson(
            [
                "addresses" => [
                    "BTC" => "1BcGBvk8XwZARvh4mCbffr9T3oRKwKNbm3",
                    "ETH" => "0x475eed1c5dd4a6e32a3b62d15d8c8db54f9e230d",
                    "LSK" => "8814452300831666598L",
                    "LTC" => "LUVjF5KodpYuEh2H7C97Euvv62xSWAQPYQ"
                ]
            ]
        );

        $this->seeJsonKey('balances');
        $this->seeJson(['success' => 1]);


        $this->seeJsonStructure(
            [
                "success",
                "balances" => [
                    "BTC" => ["available", 'locked'],
                ],
                "addresses" => [
                    "BTC", "LTC", "ETH", "LSK"]
            ]
        );

        $content = $response->getContent();
        $json = json_decode($content);

        $balances = $json->balances;

        $this->assertFalse(array_key_exists(CurrencyName::LSK, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::ETH, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::EUR, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::PLN, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::USD, $balances));
        $this->assertFalse(array_key_exists(CurrencyName::LTC, $balances));
    }

    public function testGetMyOrdersEndpoint()
    {
        $this->call('GET', '/api/bitbay/orders', [], [], [], $this->headers);
        $this->assertResponseStatus(200);

        $this->seeJsonStructure(
            [
                [
                    "current_price",
                    "order_currency",
                    "order_date",
                    "order_id",
                    "payment_currency",
                    "start_price",
                    "start_units",
                    "status",
                    "type",
                    "units"
                ]
            ]
        );
    }

    public function testGetMyActiveOrdersEndpoint()
    {
        $response = $this->call('GET', '/api/bitbay/orders/active', [], [], [], $this->headers);
        $this->assertResponseStatus(200);

        $content = $response->getContent();
        $json = json_decode($content);

        if (count($json) === 0) {
            $this->seeJsonStructure(
                [

                ]
            );
        } else {
            $this->seeJsonStructure(
                [
                    [
                        "current_price",
                        "order_currency",
                        "order_date",
                        "order_id",
                        "payment_currency",
                        "start_price",
                        "start_units",
                        "status",
                        "type",
                        "units"
                    ]
                ]
            );
        }
    }

    public function testGetMyInactiveOrdersEndpoint()
    {
        $response = $this->call('GET', '/api/bitbay/orders/inactive', [], [], [], $this->headers);
        $this->assertResponseStatus(200);

        $content = $response->getContent();
        $json = json_decode($content);

        if (count($json) === 0) {
            $this->seeJsonStructure(
                [

                ]
            );
        } else {
            $this->seeJsonStructure(
                [
                    [
                        "current_price",
                        "order_currency",
                        "order_date",
                        "order_id",
                        "payment_currency",
                        "start_price",
                        "start_units",
                        "status",
                        "type",
                        "units"
                    ]
                ]
            );
        }
    }

    public function testGetHistoryEndpoint()
    {
        $response = $this->call('GET', '/api/bitbay/history', [], [], [], $this->headers);
        $this->assertResponseStatus(200);

        $content = $response->getContent();
        $json = json_decode($content);

        if (count($json) === 0) {
            $this->seeJsonStructure(
                [

                ]
            );
        } else {
            $this->seeJsonStructure(
                [
                    [
                        "id",
                        "amount",
                        "balance_after",
                        "currency",
                        "operation_type",
                        "time",
                        "comment"
                    ]
                ]
            );

            $this->seeJsonValue(CurrencyName::BTC);
        }
    }

    public function testGetHistoryForBTCEndpoint()
    {
        $response = $this->call('GET', '/api/bitbay/history/' . CurrencyName::BTC, [], [], [], $this->headers);
        $this->assertResponseStatus(200);

        $content = $response->getContent();
        $json = json_decode($content);

        if (count($json) === 0) {
            $this->seeJsonStructure(
                [

                ]
            );
        } else {
            $this->seeJsonStructure(
                [
                    [
                        "id",
                        "amount",
                        "balance_after",
                        "currency",
                        "operation_type",
                        "time",
                        "comment"
                    ]
                ]
            );

            $this->seeJsonValue(CurrencyName::BTC);
        }
    }

    public function testGetHistoryForLTCEndpoint()
    {
        $response = $this->call('GET', '/api/bitbay/history/' . CurrencyName::LTC, [], [], [], $this->headers);
        $this->assertResponseStatus(200);

        $content = $response->getContent();
        $json = json_decode($content);

        if (count($json) === 0) {
            $this->seeJsonStructure(
                [

                ]
            );
        } else {
            $this->seeJsonStructure(
                [
                    [
                        "id",
                        "amount",
                        "balance_after",
                        "currency",
                        "operation_type",
                        "time",
                        "comment"
                    ]
                ]
            );

//            $this->seeJsonValue(CurrencyName::LTC);
        }
    }

    public function testGetHistoryForLSKEndpoint()
    {
        $response = $this->call('GET', '/api/bitbay/history/' . CurrencyName::LSK, [], [], [], $this->headers);
        $this->assertResponseStatus(200);

        $content = $response->getContent();
        $json = json_decode($content);

        if (count($json) === 0) {
            $this->seeJsonStructure(
                [

                ]
            );
        } else {
            $this->seeJsonStructure(
                [
                    [
                        "id",
                        "amount",
                        "balance_after",
                        "currency",
                        "operation_type",
                        "time",
                        "comment"
                    ]
                ]
            );

//            $this->seeJsonValue(CurrencyName::LSK);
        }
    }

    public function testGetHistoryForBTCWithLimitEndpoint()
    {
        $limit = 3;
        $response = $this->call('GET', '/api/bitbay/history/' . CurrencyName::BTC . '/' . $limit, [], [], [], $this->headers);
        $this->assertResponseStatus(200);

        $content = $response->getContent();
        $json = json_decode($content);

        if (count($json) === 0) {
            $this->seeJsonStructure(
                [

                ]
            );
        } else {
            $this->seeJsonStructure(
                [
                    [
                        "id",
                        "amount",
                        "balance_after",
                        "currency",
                        "operation_type",
                        "time",
                        "comment"
                    ]
                ]
            );

//            $this->seeJsonValue(CurrencyName::BTC);

            $this->assertCount($limit, $json);
        }
    }

    public function testGetTransactionsHistoryEndpoint()
    {
        $response = $this->call('GET', '/api/bitbay/transactions/', [], [], [], $this->headers);
        $this->assertResponseStatus(200);

        $content = $response->getContent();
        $json = json_decode($content);

        if (count($json) === 0) {
            $this->seeJsonStructure(
                [

                ]
            );
        } else {
            $this->seeJsonStructure(
                [
                    [
                        "datee",
                        "type",
                        "market",
                        "amount",
                        "rate",
                        "price"
                    ]
                ]
            );

//            $this->seeJsonValue(CurrencyName::BTC . '-' . CurrencyName::PLN);

        }
    }

    public function testGetTransactionsHistoryForBTC_PLNMarketEndpoint()
    {
        $response = $this->call('GET', '/api/bitbay/transactions/' . CurrencyName::BTC . '-' . CurrencyName::PLN, [], [], [], $this->headers);
        $this->assertResponseStatus(200);

        $content = $response->getContent();
        $json = json_decode($content);

        if (count($json) === 0) {
            $this->seeJsonStructure(
                [

                ]
            );
        } else {
            $this->seeJsonStructure(
                [
                    [
                        "date",
                        "type",
                        "market",
                        "amount",
                        "rate",
                        "price"
                    ]
                ]
            );

            $this->seeJsonValue(CurrencyName::BTC . '-' . CurrencyName::PLN);
        }
    }

    public function testGetAllOffersFromMarketWithDefaultValues()
    {
        $response = $this->call('GET', '/api/bitbay/offers/', [], [], [], $this->headers);
        $this->assertResponseStatus(200);

        $content = $response->getContent();
        $json = json_decode($content);

        if (count($json) === 0) {
            $this->seeJsonStructure(
                [
                    'bids' => [],
                    'asks' => []
                ]
            );
        } else {
            $this->seeJsonStructure(
                [
                    'bids' => [
                        [
                            "currency",
                            "price",
                            "quantity",
                        ]
                    ],
                    'asks' => [
                        [
                            "currency",
                            "price",
                            "quantity",
                        ]
                    ]
                ]
            );

            $this->seeJsonValue(CurrencyName::PLN);
        }
    }

    public function testGetAllOffersFromBTCtoPLNMarket()
    {
        $response = $this->call('GET', '/api/bitbay/offers/' . CurrencyName::BTC . '/' . CurrencyName::PLN, [], [], [], $this->headers);
        $this->assertResponseStatus(200);

        $content = $response->getContent();
        $json = json_decode($content);

        if (count($json) === 0) {
            $this->seeJsonStructure(
                [
                    'bids' => [],
                    'asks' => []
                ]
            );
        } else {
            $this->seeJsonStructure(
                [
                    'bids' => [
                        [
                            "currency",
                            "price",
                            "quantity",
                        ]
                    ],
                    'asks' => [
                        [
                            "currency",
                            "price",
                            "quantity",
                        ]
                    ]
                ]
            );

            $this->seeJsonValue(CurrencyName::PLN);
        }
    }

    public function testGetAllOffersFromBTCtoUSDMarket()
    {
        $response = $this->call('GET', '/api/bitbay/offers/' . CurrencyName::BTC . '/' . CurrencyName::USD, [], [], [], $this->headers);
        $this->assertResponseStatus(200);

        $content = $response->getContent();
        $json = json_decode($content);

        if (count($json) === 0) {
            $this->seeJsonStructure(
                [
                    'bids' => [],
                    'asks' => []
                ]
            );
        } else {
            $this->seeJsonStructure(
                [
                    'bids' => [
                        [
                            "currency",
                            "price",
                            "quantity",
                        ]
                    ],
                    'asks' => [
                        [
                            "currency",
                            "price",
                            "quantity",
                        ]
                    ]
                ]
            );

            $this->seeJsonValue(CurrencyName::USD);
        }
    }

}
