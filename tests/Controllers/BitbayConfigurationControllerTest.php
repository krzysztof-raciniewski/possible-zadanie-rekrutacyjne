<?php

use App\Api\Bitbay\Enum\CurrencyName;

class BitbayConfigurationControllerTest extends TestCase
{
    private $bitBayRepository;

    public function setUp()
    {
        parent::setUp();
        $this->initializeTestsForUserOperations();
        $this->bitBayRepository = $this->app->make(\App\Entities\Repositories\RepositoriesInterfaces\BitBayRepository::class);
    }

    public function testGetConfigurationEndpoint()
    {
        $response = $this->call('GET', '/api/configuration/bitbay', [], [], [], $this->headers);


        $this->assertResponseStatus(200);

        $this->seeJsonStructure(
            [
                'key', 'secret'
            ]
        );

        $bitBayConfig = $this->bitBayRepository->findAll()[0];

        $json = json_decode($response->getContent());

        self::assertEquals($json->key, $bitBayConfig->getKey());
        self::assertEquals($json->secret, $bitBayConfig->getSecret());
    }

    public function testSetConfigurationWithInvalidParamsEndpoint()
    {
        $this->call('PUT', '/api/configuration/bitbay', [], [], [], $this->headers);
        $this->assertResponseStatus(422);
        $this->seeJsonStructure(
            [
                'message', 'errors', 'status_code'
            ]
        );

    }

    public function testSetConfigurationEndpoint()
    {
        /** @var \App\Entities\BitBay $bitBayConfig */
        $bitBayConfig = $this->bitBayRepository->findAll()[0];

        $params = [
            'key' => $bitBayConfig->getKey(),
            'secret' => $bitBayConfig->getSecret(),
        ];
        $this->call('PUT', '/api/configuration/bitbay', $params, [], [], $this->headers);
        $this->assertResponseStatus(200);
    }

}
