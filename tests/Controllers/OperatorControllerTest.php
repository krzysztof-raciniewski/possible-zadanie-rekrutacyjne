<?php

use App\Entities\Repositories\RepositoriesInterfaces\OperatorsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository;

class OperatorControllerTest extends TestCase
{
    private $operatorsRepository;
    private $providersRepository;

    public function setUp()
    {
        parent::setUp();
        $this->initializeTestsForUserOperations();
        $this->operatorsRepository = $this->app->make(OperatorsRepository::class);
        $this->providersRepository = $this->app->make(ProvidersRepository::class);
    }

    public function testGetListEndpoint()
    {
        $response = $this->call('GET', '/api/operators', [], [], [], $this->headers);


        $this->assertResponseStatus(200);

        $this->seeJsonStructure(
            [
                [
                    'id', 'name', 'created', 'updated', 'sms_assignments'
                ]
            ]
        );

        $providers = $this->operatorsRepository->findAll();

        $json = json_decode($response->getContent());
        $this->assertCount(count($providers), $json);
    }

    /**
     * Add new operator
     */
    public function testAddNewOperatorEndpoint()
    {
        $operatorsBefore = count($this->operatorsRepository->findAll());
        $response = $this->call('POST', '/api/operators', ['name' => $this->faker->name], [], [], $this->headers);
        $operatorsAfter = count($this->operatorsRepository->findAll());

        $this->assertResponseStatus(200);
        $this->seeJsonStructure(
            [
                'id', 'name', 'created', 'updated', 'sms_assignments'
            ]
        );

        $this->assertGreaterThan($operatorsBefore, $operatorsAfter);
    }

    public function testAddNewOperatorWithTheSameNameEndpoint()
    {
        $operator = $this->operatorsRepository->findAll()[0];
        $operatorsBefore = count($this->operatorsRepository->findAll());
        $response = $this->call('POST', '/api/operators', ['name' => $operator->getName()], [], [], $this->headers);
        $operatorsAfter = count($this->operatorsRepository->findAll());

        $this->assertResponseStatus(400);
        $this->assertEquals($operatorsBefore, $operatorsAfter);
        $this->see('Operator exists');
    }

    /**
     * Edit operator name
     */
    public function testEditOperatorNameEndpoint()
    {
        /** @var \App\Entities\Operator $operator */
        $operator = $this->operatorsRepository->findAll()[0];
        $operatorsBefore = count($this->operatorsRepository->findAll());
        $response = $this->call('PUT', '/api/operators/' . $operator->getId(), ['name' => $operator->getName()], [], [], $this->headers);
        $operatorsAfter = count($this->operatorsRepository->findAll());

        $this->assertResponseStatus(200);

        $this->seeJsonStructure(
            [
                'id', 'name', 'created', 'updated', 'sms_assignments'
            ]
        );

        $this->assertEquals($operatorsBefore, $operatorsAfter);

        $this->see($operator->getName());
    }

    /**
     * Remove operator
     */
    public function testDeleteOperatorNameEndpoint()
    {
        /** @var \App\Entities\Operator $operator */
        $operator = $this->operatorsRepository->findAll()[0];
        $providersCountBefore = $this->providersRepository->findAll();
        $operatorsBefore = count($this->operatorsRepository->findAll());
        $response = $this->call('DELETE', '/api/operators/' . $operator->getId(), [], [], [], $this->headers);
        $operatorsAfter = count($this->operatorsRepository->findAll());
        $providersCountAfter = $this->providersRepository->findAll();


        $this->assertResponseStatus(200);

        $this->see('Ok');

        $this->assertLessThan($operatorsBefore, $operatorsAfter);
        $this->assertEquals($providersCountBefore, $providersCountAfter);
    }
}
