<?php

use App\Entities\Repositories\RepositoriesInterfaces\CountriesRepository;
use App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository;

class CountriesControllerTest extends TestCase
{
    private $countriesRepository;
    private $providersRepository;

    public function setUp()
    {
        parent::setUp();
        $this->initializeTestsForUserOperations();
        $this->countriesRepository = $this->app->make(CountriesRepository::class);
        $this->providersRepository = $this->app->make(ProvidersRepository::class);
    }

    public function testGetListEndpoint()
    {
        $response = $this->call('GET', '/api/countries', [], [], [], $this->headers);


        $this->assertResponseStatus(200);

        $this->seeJsonStructure(
            [
                [
                    'id', 'name', 'code', 'created', 'updated', 'sms_assignments'
                ]
            ]
        );

        $currencies = $this->countriesRepository->findAll();

        $json = json_decode($response->getContent());
        $this->assertCount(count($currencies), $json);
    }

    public function testAddNewCountryEndpoint()
    {
        $currenciesBefore = count($this->countriesRepository->findAll());
        $response = $this->call('POST', '/api/countries', ['name' => $this->faker->country, 'code' => $this->faker->countryCode], [], [], $this->headers);
        $this->assertResponseStatus(200);
        $this->seeJsonStructure(
            [
                'id', 'name', 'code', 'created', 'updated', 'sms_assignments'
            ]
        );

        $currenciesAfter = count($this->countriesRepository->findAll());


        $this->assertGreaterThan($currenciesBefore, $currenciesAfter);
    }

    public function testAddNewCountryWithTheSameNameEndpoint()
    {
        /** @var \App\Entities\Country $country */
        $country = $this->countriesRepository->findAll()[0];
        $currenciesBefore = count($this->countriesRepository->findAll());
        $response = $this->call('POST', '/api/countries', ['name' => $country->getName(), 'code' => $country->getCode()], [], [], $this->headers);
        $currenciesAfter = count($this->countriesRepository->findAll());

        $this->assertResponseStatus(400);
        $this->assertEquals($currenciesBefore, $currenciesAfter);
        $this->see('Code exists');
    }

    /**
     * Edit operator name
     */
    public function testEditCountryEndpoint()
    {
        /** @var \App\Entities\Country $country */
        $country = $this->countriesRepository->findAll()[0];
        $currenciesBefore = count($this->countriesRepository->findAll());
        $response = $this->call('PUT', '/api/countries/' . $country->getId(), ['name' => $country->getName(), 'code' => $country->getCode()], [], [], $this->headers);
        $currenciesAfter = count($this->countriesRepository->findAll());

        $this->assertResponseStatus(200);

        $this->seeJsonStructure(
            [
                'id', 'name', 'code', 'created', 'updated', 'sms_assignments'
            ]
        );

        $this->assertEquals($currenciesBefore, $currenciesAfter);
    }

    /**
     * Remove operator
     */
    public function testDeleteCountryWithoutAssignmentsEndpoint()
    {
        $country = null;
        /** @var \App\Entities\Country $country */
        $currencies = $this->countriesRepository->findAll();
        foreach($currencies as $country_obj) {
            if( $country_obj->getTextMessages()->count() === 0 ) {
                $country = $country_obj;
                break;
            }
        }
        if( !$country ) {
            $this->markTestSkipped();
        }

        $providersCountBefore = $this->providersRepository->findAll();
        $currenciesCountBefore = count($this->countriesRepository->findAll());
        $response = $this->call('DELETE', '/api/countries/' . $country->getId(), [], [], [], $this->headers);
        $currenciesCountAfter = count($this->countriesRepository->findAll());
        $providersCountAfter = $this->providersRepository->findAll();


        $this->assertResponseStatus(200);

        $this->see('Ok');

        $this->assertLessThan($currenciesCountBefore, $currenciesCountAfter);
        $this->assertEquals($providersCountBefore, $providersCountAfter);
    }

    /**
     * Remove operator
     */
    public function testDeleteCountryWithAssignmentsEndpoint()
    {
        $country = null;
        /** @var \App\Entities\Country $country */
        $currencies = $this->countriesRepository->findAll();
        foreach($currencies as $country_obj) {
            if( $country_obj->getTextMessages()->count() !== 0 ) {
                $country = $country_obj;
                break;
            }
        }
        if( !$country ) {
            $this->markTestSkipped();
        }

        $this->call('DELETE', '/api/countries/' . $country->getId(), [], [], [], $this->headers);

        $this->assertResponseStatus(400);
        $this->see('Country has assignments');
    }
}
