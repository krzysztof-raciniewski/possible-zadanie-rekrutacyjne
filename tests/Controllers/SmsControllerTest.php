<?php

use App\Api\Bitbay\Enum\CurrencyName;

class SmsControllerTest extends TestCase
{
    private $providersRepository;

    public function setUp()
    {
        parent::setUp();
        $this->initializeTestsForUserOperations();
        $this->providersRepository = $this->app->make(\App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository::class);
    }

}
