<?php

use App\Entities\Repositories\RepositoriesInterfaces\CurrenciesRepository;
use App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository;

class CurrenciesControllerTest extends TestCase
{
    private $currenciesRepository;
    private $providersRepository;

    public function setUp()
    {
        parent::setUp();
        $this->initializeTestsForUserOperations();
        $this->currenciesRepository = $this->app->make(CurrenciesRepository::class);
        $this->providersRepository = $this->app->make(ProvidersRepository::class);
    }

    public function testGetListEndpoint()
    {
        $response = $this->call('GET', '/api/currencies', [], [], [], $this->headers);


        $this->assertResponseStatus(200);

        $this->seeJsonStructure(
            [
                [
                    'id', 'name', 'code', 'created', 'updated', 'sms_assignments'
                ]
            ]
        );

        $currencies = $this->currenciesRepository->findAll();

        $json = json_decode($response->getContent());
        $this->assertCount(count($currencies), $json);
    }

    public function testAddNewCurrencyEndpoint()
    {
        $currenciesBefore = count($this->currenciesRepository->findAll());
        $response = $this->call('POST', '/api/currencies', ['name' => $this->faker->currencyCode, 'code' => $this->faker->currencyCode], [], [], $this->headers);
        $currenciesAfter = count($this->currenciesRepository->findAll());

        $this->assertResponseStatus(200);
        $this->seeJsonStructure(
            [
                'id', 'name', 'code', 'created', 'updated', 'sms_assignments'
            ]
        );

        $this->assertGreaterThan($currenciesBefore, $currenciesAfter);
    }

    public function testAddNewCurrencyWithTheSameNameEndpoint()
    {
        /** @var \App\Entities\Currency $currency */
        $currency = $this->currenciesRepository->findAll()[0];
        $currenciesBefore = count($this->currenciesRepository->findAll());
        $response = $this->call('POST', '/api/currencies', ['name' => $currency->getName(), 'code' => $currency->getCode()], [], [], $this->headers);
        $currenciesAfter = count($this->currenciesRepository->findAll());

        $this->assertResponseStatus(400);
        $this->assertEquals($currenciesBefore, $currenciesAfter);
        $this->see('Code exists');
    }

    /**
     * Edit operator name
     */
    public function testEditCurrencyEndpoint()
    {
        /** @var \App\Entities\Currency $currency */
        $currency = $this->currenciesRepository->findAll()[0];
        $currenciesBefore = count($this->currenciesRepository->findAll());
        $response = $this->call('PUT', '/api/currencies/' . $currency->getId(), ['name' => $currency->getName(), 'code' => $currency->getCode()], [], [], $this->headers);
        $currenciesAfter = count($this->currenciesRepository->findAll());

        $this->assertResponseStatus(200);

        $this->seeJsonStructure(
            [
                'id', 'name', 'code', 'created', 'updated', 'sms_assignments'
            ]
        );

        $this->assertEquals($currenciesBefore, $currenciesAfter);
    }

    /**
     * Remove operator
     */
    public function testDeleteCurrencyWithoutAssignmentsEndpoint()
    {
        $currency = null;
        /** @var \App\Entities\Currency $currency */
        $currencies = $this->currenciesRepository->findAll();
        foreach($currencies as $currency_obj) {
            if( $currency_obj->getTextMessages()->count() === 0 ) {
                $currency = $currency_obj;
                break;
            }
        }
        if( !$currency ) {
            $this->markTestSkipped();
        }

        $providersCountBefore = $this->providersRepository->findAll();
        $currenciesCountBefore = count($this->currenciesRepository->findAll());
        $response = $this->call('DELETE', '/api/currencies/' . $currency->getId(), [], [], [], $this->headers);
        $currenciesCountAfter = count($this->currenciesRepository->findAll());
        $providersCountAfter = $this->providersRepository->findAll();


        $this->assertResponseStatus(200);

        $this->see('Ok');

        $this->assertLessThan($currenciesCountBefore, $currenciesCountAfter);
        $this->assertEquals($providersCountBefore, $providersCountAfter);
    }

    /**
     * Remove operator
     */
    public function testDeleteCurrencyWithAssignmentsEndpoint()
    {
        $currency = null;
        /** @var \App\Entities\Currency $currency */
        $currencies = $this->currenciesRepository->findAll();
        foreach($currencies as $currency_obj) {
            if( $currency_obj->getTextMessages()->count() !== 0 ) {
                $currency = $currency_obj;
                break;
            }
        }
        if( !$currency ) {
            $this->markTestSkipped();
        }

        $this->call('DELETE', '/api/currencies/' . $currency->getId(), [], [], [], $this->headers);

        $this->assertResponseStatus(400);
        $this->see('Currency has assignments');
    }
}
