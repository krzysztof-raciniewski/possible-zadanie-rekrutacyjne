<?php

use App\Api\Bitbay\Enum\CurrencyName;

class ProvidersControllerTest extends TestCase
{
    /** @var  \App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository $providersRepository */
    private $providersRepository;

    /** @var  \App\Entities\Repositories\RepositoriesInterfaces\SmsRepository $smsRepository */
    private $currenciesRepository;

    /** @var  \App\Entities\Repositories\RepositoriesInterfaces\CurrenciesRepository */
    private $smsRepository;

    /** @var  \App\Entities\Repositories\RepositoriesInterfaces\CountriesRepository */
    private $countriesRepository;

    /** @var  \App\Entities\Repositories\RepositoriesInterfaces\OperatorsRepository */
    private $operatorsRepository;


    public function setUp()
    {
        parent::setUp();
        $this->initializeTestsForUserOperations();
        $this->providersRepository = $this->app->make(\App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository::class);
        $this->smsRepository = $this->app->make(\App\Entities\Repositories\RepositoriesInterfaces\SmsRepository::class);
        $this->currenciesRepository = $this->app->make(\App\Entities\Repositories\RepositoriesInterfaces\CurrenciesRepository::class);
        $this->countriesRepository = $this->app->make(\App\Entities\Repositories\RepositoriesInterfaces\CountriesRepository::class);
        $this->operatorsRepository = $this->app->make(\App\Entities\Repositories\RepositoriesInterfaces\OperatorsRepository::class);
    }

    public function testGetProvidersListEndpoint()
    {
        $response = $this->call('GET', '/api/providers', [], [], [], $this->headers);


        $this->assertResponseStatus(200);

        $this->seeJsonStructure(
            [
                [
                    'id', 'name', 'www', 'image', 'description', 'configured', 'enabled', 'active', 'updated', 'last_active'
                ]
            ]
        );

        $this->dontSee('secret');
        $this->dontSee('rules_url');
        $this->dontSee('complaint_from_url');
        $this->dontSee('public_provider_information');

        $providers = $this->providersRepository->findAll();

        $json = json_decode($response->getContent());
        self::assertCount(count($providers), $json);
    }

    public function testGetProviderByIdEndpoint()
    {
        /** @var \App\Entities\Provider $provider */
        $provider = $this->providersRepository->findAll()[0];

        $response = $this->call('GET', '/api/providers/' . $provider->getId(), [], [], [], $this->headers);

        $this->assertResponseStatus(200);

        $this->seeJsonStructure(
            [
                [
                    'id', 'name', 'www', 'image', 'description', 'configured', 'enabled', 'active', 'updated', 'last_active', 'secret', 'rules_url', 'complaint_from_url', 'public_provider_information'
                ]
            ]
        );

        $this->seeJson(

            [
                'id' => $provider->getId(),
                'name' => $provider->getName(),
                'www' => $provider->getWww(),
                'image' => $provider->getImage(),
                'description' => $provider->getDescription(),
                'configured' => $provider->isConfigured(),
                'enabled' => $provider->isEnabled(),
                'active' => $provider->isActive(),
                'secret' => $provider->getSecret(),
                'rules_url' => $provider->getRulesUrl(),
                'complaint_from_url' => $provider->getComplaintFormUrl(),
                'public_provider_information' => $provider->getPublicProviderInformation()
            ]

        );
    }

    public function testGetProviderByIdEndpointWithBadID()
    {
        $this->call('GET', '/api/providers/2505665465789448489465456', [], [], [], $this->headers);
        $this->assertResponseStatus(404);
    }

    public function testUpdateProviderByIdEndpointWithBadID()
    {
        $params = [
            'image' => 'afs',
            'www' => 'fgds',
            'description' => 'dsgsd'
        ];

        $this->call('PUT', '/api/providers/987234824', $params, [], [], $this->headers);
        $this->assertResponseStatus(404);


    }

    public function testUpdateProviderByIdEndpointWithBadParams()
    {
        /** @var \App\Entities\Provider $provider */
        $provider = $this->providersRepository->findAll()[0];

        $params = [

        ];

        $this->call('PUT', '/api/providers/' . $provider->getId(), $params, [], [], $this->headers);
        $this->assertResponseStatus(422);
    }

    public function testUpdateProviderEndpoint()
    {
        /** @var \App\Entities\Provider $provider */
        $provider = $this->providersRepository->findAll()[0];

        $params = [
            'www' => $provider->getWww(),
            'image' => $provider->getImage(),
            'description' => $provider->getDescription()
        ];

        $this->call('PUT', '/api/providers/' . $provider->getId(), $params, [], [], $this->headers);
        $this->assertResponseStatus(200);
    }

    public function testGetConfigurationForProviderEndpoint()
    {
        /** @var \App\Entities\Provider $provider */
        $provider = $this->providersRepository->findAll()[0];
        $response = $this->call('GET', '/api/providers/' . $provider->getId() . '/configuration', [], [], [], $this->headers);

        $this->assertResponseStatus(200);

        $this->seeJsonStructure(
            [
                [
                    'id', 'login', 'password', 'type', 'created', 'updated'
                ]
            ]
        );
    }

    public function testSetConfigurationForProviderEndpoint()
    {
        /** @var \App\Entities\Provider $provider */
        $provider = $this->providersRepository->findAll()[0];
        /** @var \App\Entities\ProviderConfiguration $config */
        $config = $provider->getConfiguration()->get(0);
        $params = [
            'login' => $config->getLogin(),
            'password' => $config->getPassword(),
            'type' => $config->getType()
        ];

        $response = $this->call('PUT', '/api/providers/' . $provider->getId() . '/configuration', $params, [], [], $this->headers);

        $this->assertResponseStatus(200);

        $this->seeJsonStructure(
            [
                'id', 'login', 'password', 'type', 'created', 'updated'
            ]
        );

        $this->seeJson([
            'login' => $config->getLogin(),
            'password' => $config->getPassword(),
            'type' => $config->getType(),
        ]);

//        $this->dontSee($config->getUpdated()->format('Y-m-d H:i:s'));

    }

    public function testGetSmsListForProviderEndpoint()
    {
        /** @var \App\Entities\Provider $provider */
        $provider = $this->providersRepository->findAll()[0];
        $response = $this->call('GET', '/api/providers/' . $provider->getId() . '/sms', [], [], [], $this->headers);

        $this->assertResponseStatus(200);

        $this->seeJsonStructure(
            [
                [
                    'id', 'la_number', 'command', 'token', 'country', 'currency', 'testing', 'net_price', 'gross_price', 'created', 'updated'
                ]
            ]
        );
        $json = json_decode($response->getContent());
        $smsCollection = $provider->getConfiguration()->get(0)->getSms();
        self::assertCount(count($smsCollection), $json);

    }

    public function testRemoveSmsFromProviderEndpoint()
    {
        /** @var \App\Entities\Provider $provider */
        $provider = $this->providersRepository->findAll()[0];
        /** @var \App\Entities\ProviderConfiguration $configuration */
        $configuration = $provider->getConfiguration()->get(0);
        /** @var \App\Entities\Sms $sms */
        $sms = $configuration->getSms()->get(0);

        $countBefore = $configuration->getSms()->count();
        $response = $this->call('DELETE', '/api/providers/' . $provider->getId() . '/sms/' . $sms->getId(), [], [], [], $this->headers);
        $countAfter = $configuration->getSms()->count();

        $this->assertResponseStatus(200);
        self::assertGreaterThan($countAfter, $countBefore);

    }

    public function testAddSmsToProviderEndpoint()
    {
        /** @var \App\Entities\Provider $provider */
        $provider = $this->providersRepository->findAll()[0];

        /** @var \App\Entities\Currency $currency */
        $currency = $this->currenciesRepository->findAll()[0];

        /** @var \App\Entities\Country $country */
        $country = $this->countriesRepository->findAll()[0];

        /** @var \App\Entities\Operator $operators */
        $operators = $this->operatorsRepository->getAllIds();


        $data = [
            'la_number' => '1234',
            'command' => 'TEST_COMMAND',
            'token' => 'service_id',
            'service_id' => 'service_id',
            'country' => $country->getCode(),
            'currency' => $currency->getCode(),
            'operators' => implode(",",$operators),
            'enabled' => true,
            'net_price' => 2.00,
            'gross_price' => 2.34,
            'testing' => false,
        ];

        $countBefore = count($this->smsRepository->findAll());
        $url = '/api/providers/' . $provider->getId() . '/sms';
        $response = $this->call('POST', '/api/providers/' . $provider->getId() . '/sms', $data, [], [], $this->headers);
        $countAfter = count($this->smsRepository->findAll());

        $this->assertResponseStatus(200);
        self::assertLessThan($countAfter, $countBefore);

        $this->seeJson([

            'la_number' => '1234',
            'command' => 'TEST_COMMAND',
            'token' => 'service_id',
            'service_id' => 'service_id',
            'country' => $country->getCode(),
            'currency' => $currency->getCode(),
            'enabled' => true

        ]);

        $json = json_decode($response->getContent());
        self::assertNotNull($json->id);

        $operatorsFromServer = $json->operators;

        self::assertEquals(count($operatorsFromServer), count($operators));

        foreach($operatorsFromServer as $operator) {
            self::assertTrue(in_array($operator->id, $operators));
        }
    }

    public function testAddSmsToProviderWithoutTokenAndServiceIdEndpoint()
    {
        /** @var \App\Entities\Provider $provider */
        $provider = $this->providersRepository->findAll()[0];

        /** @var \App\Entities\Currency $currency */
        $currency = $this->currenciesRepository->findAll()[0];

        /** @var \App\Entities\Country $country */
        $country = $this->countriesRepository->findAll()[0];

        $operators = $this->operatorsRepository->getAllIds();

        $data = [
            'la_number' => '1234',
            'command' => 'TEST_COMMAND',
            'country' => $country->getCode(),
            'currency' => $currency->getCode(),
            'enabled' => true,
            'net_price' => 2.00,
            'gross_price' => 2.34,
            'testing' => false,
            'operators' => implode(",",$operators)
        ];

        $countBefore = count($this->smsRepository->findAll());
        $response = $this->call('POST', '/api/providers/' . $provider->getId() . '/sms/', $data, [], [], $this->headers);
        $countAfter = count($this->smsRepository->findAll());

        $this->assertResponseStatus(200);
        self::assertLessThan($countAfter, $countBefore);

        $this->seeJson([
            'la_number' => '1234',
            'command' => 'TEST_COMMAND',
            'token' => '',
            'service_id' => '',
            'country' => $country->getCode(),
            'currency' => $currency->getCode(),
            'enabled' => true,
            'net_price' => (string)2.00,
            'gross_price' => (string)2.34
        ]);

        $json = json_decode($response->getContent());
        self::assertNotNull($json->id);

        $operatorsFromServer = $json->operators;

        self::assertEquals(count($operatorsFromServer), count($operators));

        foreach($operatorsFromServer as $operator) {
            self::assertTrue(in_array($operator->id, $operators));
        }
    }

    public function testChangeSmsStatusEndpoint()
    {
        /** @var \App\Entities\Provider $provider */
        $provider = $this->providersRepository->findAll()[0];

        /** @var \App\Entities\Sms $sms */
        $sms = $provider->getConfiguration()->get(0)->getSms()->get(0);
        $isEnabledBefore = $sms->isEnabled();
        $this->call('PUT', '/api/providers/' . $provider->getId() . '/sms/' . $sms->getId() . '/changestatus', [], [], [], $this->headers);
        $isEnabledAfter = $sms->isEnabled();

        $this->assertResponseStatus(200);

        self::assertNotEquals($isEnabledAfter, $isEnabledBefore);
    }

    public function testChangeProviderStatusEndpoint()
    {
        /** @var \App\Entities\Provider $provider */
        $provider = $this->providersRepository->findAll()[0];

        /** @var \App\Entities\Sms $sms */
        $isEnabledBefore = $provider->isEnabled();
        $this->call('PUT', '/api/providers/' . $provider->getId() . '/changestatus', [], [], [], $this->headers);
        $isEnabledAfter = $provider->isEnabled();

        $this->assertResponseStatus(200);

        self::assertNotEquals($isEnabledAfter, $isEnabledBefore);
    }

    public function testGetSmsInfoEndpoint()
    {
        /** @var \App\Entities\Provider $provider */
        $provider = $this->providersRepository->findAll()[0];

        /** @var \App\Entities\Sms $sms */
        $sms = $provider->getConfiguration()->get(0)->getSms()->get(0);
        $this->call('GET', '/api/providers/' . $provider->getId() . '/sms/' . $sms->getId() . '/', [], [], [], $this->headers);

        $this->assertResponseStatus(200);

        self::seeJson(
            [
                'net_price' => (string)$sms->getNetPrice(),
                'gross_price' => (string)$sms->getGrossPrice(),
                'la_number' => $sms->getLaNumber(),
                'command' => $sms->getCommand(),
                'country' => $sms->getCountry()->getCode(),
                'currency' => $sms->getCurrency()->getCode(),
                'service_id' => $sms->getServiceId(),
                'token' => $sms->getToken(),
                'enabled' => $sms->isEnabled()
            ]
        );

    }

    public function testEditSmsEndpoint()
    {
        /** @var \App\Entities\Provider $provider */
        $provider = $this->providersRepository->findAll()[0];

        /** @var \App\Entities\Sms $sms */
        $sms = $provider->getConfiguration()->get(0)->getSms()->get(0);

        /** @var \App\Entities\Currency $currency */
        $currency = $this->currenciesRepository->findAll()[0];

        /** @var \App\Entities\Country $country */
        $country = $this->countriesRepository->findAll()[0];

        $operators = $this->operatorsRepository->getAllIds();
        array_pop($operators);


        $data = [
            'la_number' => '1234',
            'command' => 'TEST_COMMAND',
            'token' => 'service_id',
            'service_id' => 'service_id',
            'country' => $country->getCode(),
            'currency' => $currency->getCode(),
            'operators' => implode(",",$operators),
            'enabled' => false,
            'net_price' => 2.00,
            'gross_price' => 2.34,
            'testing' => false,
        ];

        $response = $this->call('PUT', '/api/providers/' . $provider->getId() . '/sms/' . $sms->getId(), $data, [], [], $this->headers);

        $this->assertResponseStatus(200);

        $this->seeJson([

            'la_number' => '1234',
            'command' => 'TEST_COMMAND',
            'token' => 'service_id',
            'service_id' => 'service_id',
            'country' => $country->getCode(),
            'currency' => $currency->getCode(),
            'enabled' => false,
            'net_price' => (string)2.00,
            'gross_price' => (string)2.34

        ]);

        $json = json_decode($response->getContent());
        $operatorsFromServer = $json->operators;

        self::assertEquals(count($operatorsFromServer), count($operators));

        foreach($operatorsFromServer as $operator) {
            self::assertTrue(in_array($operator->id, $operators));
        }
    }

}
