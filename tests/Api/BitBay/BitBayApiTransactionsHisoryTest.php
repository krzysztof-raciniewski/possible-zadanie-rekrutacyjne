<?php

use App\Api\Bitbay\BitbayApi;
use App\Api\Bitbay\Enum\CurrencyName;

class BitBayApiTransactionsHistoryTest extends \TestCase
{
    public function testGetTransactionsHistoryRequestWithDefaultParams()
    {
            $api = new BitbayApi();
            $result = $api->getTransactionsHistory();

            if(count($result) > 0) {
                self::assertTrue(array_key_exists('date', $result[0]));
                self::assertTrue(array_key_exists('type', $result[0]));
                self::assertTrue(array_key_exists('market', $result[0]));
                self::assertTrue(array_key_exists('amount', $result[0]));
                self::assertTrue(array_key_exists('rate', $result[0]));
                self::assertTrue(array_key_exists('price', $result[0]));
            }
    }

    public function testGetTransactionsHistoryRequestWithValidMarketParam()
    {
        $api = new BitbayApi();
        $result = $api->getTransactionsHistory(CurrencyName::BTC.'-'.CurrencyName::PLN);

        if(count($result) > 0) {
            self::assertTrue(array_key_exists('date', $result[0]));
            self::assertTrue(array_key_exists('type', $result[0]));
            self::assertTrue(array_key_exists('market', $result[0]));
            self::assertTrue(array_key_exists('amount', $result[0]));
            self::assertTrue(array_key_exists('rate', $result[0]));
            self::assertTrue(array_key_exists('price', $result[0]));
        }
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidMarketException
     */
    public function testGetTransactionsHistoryRequestWithInvalidMarketParam()
    {
        $api = new BitbayApi();
        $api->getTransactionsHistory('invalid-market');
    }

}