<?php

use App\Api\Bitbay\BitbayApi;
use App\Api\Bitbay\Enum\CurrencyName;
use App\Api\Bitbay\Enum\TradeType;

class BitBayApiOrdersTest extends \TestCase
{
    /**
     * Test invalid trade request without params
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidMethodParametersException
     */
    public function testInvalidTradeRequestWithoutParams()
    {
        $api = new BitbayApi();
        $api->trade();
    }

    public function testTradeBuyRequestWithInvalidParams()
    {
        $api = new BitbayApi();
        $result = $api->trade([
            'type' => TradeType::BUY,
            'currency' => CurrencyName::BTC,
            'amount' => 0,
            'payment_currency' => CurrencyName::PLN,
            'rate' => 0
        ]);

        self::assertNotEmpty($result);
        self::assertTrue(array_key_exists('success', $result));
    }

    public function testTradeSellRequestWithInvalidParams()
    {
        $api = new BitbayApi();
        $result = $api->trade([
            'type' => TradeType::SELL,
            'currency' => CurrencyName::BTC,
            'amount' => 0,
            'payment_currency' => CurrencyName::PLN,
            'rate' => 0
        ]);

        self::assertNotEmpty($result);
        self::assertTrue(array_key_exists('success', $result));
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidOrderTypeException
     */
    public function testTradeRequestWithInvalidTradeTypeParam()
    {
        $api = new BitbayApi();
        $api->trade([
            'type' => 'badtype',
            'currency' => CurrencyName::BTC,
            'amount' => 0,
            'payment_currency' => CurrencyName::PLN,
            'rate' => 0
        ]);
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidCurrencyNameException
     */
    public function testTradeRequestWithInvalidCurrencyNameParam()
    {
        $api = new BitbayApi();
        $api->trade([
            'type' => TradeType::BUY,
            'currency' => 'badcurrency',
            'amount' => 0,
            'payment_currency' => CurrencyName::PLN,
            'rate' => 0
        ]);
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidPaymentCurrencyNameException
     */
    public function testTradeRequestWithInvalidPaymentCurrencyParam()
    {
        $api = new BitbayApi();
        $api->trade([
            'type' => TradeType::BUY,
            'currency' => CurrencyName::BTC,
            'amount' => 0,
            'payment_currency' => 'invalidcurrency',
            'rate' => 0
        ]);
    }

    public function testSellRequestWithValidParams()
    {
        self::markTestIncomplete('Remove this to run sell request');
        $api = new BitbayApi();
        $result = $api->trade([
            'type' => TradeType::SELL,
            'currency' => CurrencyName::BTC,
            'amount' => 2,
            'payment_currency' => CurrencyName::PLN,
            'rate' => 4000
        ]);

        self::assertNotEmpty($result);
    }

    public function testGetMyOrdersRequestWithDefaultParams()
    {
            $api = new BitbayApi();
            $result = $api->getMyOrders(10);
            if( count($result) > 0 ) {
                foreach($result as $order) {
                    self::assertTrue(array_key_exists('order_currency', $order));
                    self::assertTrue(array_key_exists('order_date', $order));
                    self::assertTrue(array_key_exists('payment_currency', $order));
                    self::assertTrue(array_key_exists('type', $order));
                    self::assertTrue(array_key_exists('status', $order));
                    self::assertTrue(array_key_exists('units', $order));
                    self::assertTrue(array_key_exists('start_units', $order));
                    self::assertTrue(array_key_exists('current_price', $order));
                    self::assertTrue(array_key_exists('start_price', $order));
                    self::assertTrue(array_key_exists('order_id', $order));

                    self::assertTrue(is_int($order['order_id']));
                }
            }
    }

    /**
     * @depends testGetMyOrdersRequestWithDefaultParams
     */
    public function testGetMyActiveOrdersRequestWithDefaultParams()
    {
        $api = new BitbayApi();

        $trades = $api->getMyActiveOrders(10);

        self::assertTrue(true);

        foreach($trades as $trade) {
            self::assertEquals($trade['status'], 'active');
        }
    }

    /**
     * @depends testGetMyActiveOrdersRequestWithDefaultParams
     */
    public function testGetMyInactiveOrdersRequestWithDefaultParams()
    {
        $api = new BitbayApi();

        $trades = $api->getMyInactiveOrders(10);

        foreach($trades as $trade) {
            self::assertEquals($trade['status'], 'inactive');
        }
    }

    /**
     * @expectedException App\Api\Bitbay\BitbayExceptions\BitBayException
     */
    public function testTradeCancelRequestWithInvalidParam()
    {
        $api = new BitbayApi();
        $api->cancel(-1);
    }

    /**
     * @expectedException App\Api\Bitbay\BitbayExceptions\InvalidOrderIdException
     */
    public function testTradeCancelRequestWithInvalidOfferIdParam()
    {
        $api = new BitbayApi();
        $api->cancel(0);
    }

    /**
     * @depends testSellRequestWithValidParams
     */
    public function testGetTradeHistoryAndRemoveAllTradesByIdParam()
    {
        $api = new BitbayApi();
        $trades = $api->getMyActiveOrders(10);

        foreach($trades as $trade) {
            $response = $api->cancel($trade['order_id']);
            self::assertNotEmpty($response);
            self::assertEquals($response['success'], 1);
            self::assertEquals($response['order_id'], $trade['order_id']);
        }
    }
}