<?php

use App\Api\Bitbay\BitbayApi;
use App\Api\Bitbay\Enum\CurrencyName;

class BitBayApiHistoryTest extends \TestCase
{
    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidCurrencyNameException
     */
    public function testGetHistoryRequestWithNotValidPLNAmountParam()
    {
        $api = new BitbayApi();
        $result = $api->getHistory(CurrencyName::PLN, 10);
        self::assertNotEmpty($result);
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidCurrencyNameException
     */
    public function testGetHistoryRequestWithNotValidUSDAmountParam()
    {
        $api = new BitbayApi();
        $result = $api->getHistory(CurrencyName::USD, 10);
        self::assertNotEmpty($result);
    }

    public function testGetHistoryRequestWithValidBTCAmountParam()
    {
        $api = new BitbayApi();
        $result = $api->getHistory(CurrencyName::BTC, 10);
        self::assertNotEmpty($result);
    }

    public function testGetHistoryRequestWithDefaultParams()
    {
        $api = new BitbayApi();
        $result = $api->getHistory();
        self::assertNotEmpty($result);
    }

}