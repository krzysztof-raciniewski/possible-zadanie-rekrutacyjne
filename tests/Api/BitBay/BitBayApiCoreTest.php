<?php
declare(strict_types=1);

use App\Api\Bitbay\BitbayApi;

class BitBayApiCoreTest extends TestCase
{
    /**
     * Valid api request
     */
    public function testValidRequestToApiMethod()
    {
        $api = new BitbayApi();
        $result = $api->sendRequest('info', ['test' => 'asdfad']);
        self::assertNotEmpty($result);

        self::assertTrue(array_key_exists('success', $result));
        self::assertTrue(array_key_exists('addresses', $result));
        self::assertFalse(array_key_exists('message', $result));
        self::assertEquals($result['success'], 1);
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidMethodNameException
     */
    public function testInvalidRequestToApiMethod()
    {
        $api = new BitbayApi();
        $api->sendRequest('badmethodname');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidPublicKeyException
     */
    public function testRequestWithInvalidKey() {
        $api = new BitbayApi();
        $api -> setKey('badkey');
        $api->sendRequest('info');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidSignException
     */
    public function testRequestWithInvalidSecret() {
        $api = new BitbayApi();
        $api -> setSecret('badsecret');
        $api->sendRequest('info');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidPublicKeyException
     */
    public function testRequestWithoutDefaultdHeaders() {
        $api = new BitbayApi();
        $api->sendRequest('badmethodname', array(), array(), true, false);
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidMomentParameterException
     */
    public function testRequestWithoutDefaultParams() {
        $api = new BitbayApi();
        $api->sendRequest('badmethodname', array(), array(), false, true);
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidMomentParameterException
     */
    public function testRequestWithoutDefaultParamsAndHeaders() {
        $api = new BitbayApi();
        $api->sendRequest('badmethodname', array(), array(), false, true);
    }

}