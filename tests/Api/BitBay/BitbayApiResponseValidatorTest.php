<?php
declare(strict_types=1);

use App\Api\Bitbay\BitbayApiResponseValidator;

class BitbayApiResponseValidatorTest extends TestCase
{
    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidMethodParametersException
     */
    public function testInvalidMethodParametersExceptionThrown() {
        BitbayApiResponseValidator::valid('{"code":400}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidOrderTypeException
     */
    public function testInvalidOrderTypeExceptionThrown() {
        BitbayApiResponseValidator::valid('{"code":401}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\NoOrdersWithSpecifiedCurrenciesException
     */
    public function testNoOrdersWithSpecifiedCurrenciesExceptionThrown() {
        BitbayApiResponseValidator::valid('{"code":402}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidPaymentCurrencyNameException
     */
    public function testInvalidPaymentCurrencyNameExceptionThrown() {
        BitbayApiResponseValidator::valid('{"code":403}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidTransactionTypeException
     */
    public function testInvalidTransactionTypeExceptionThrown() {
        BitbayApiResponseValidator::valid('{"code":404}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidOrderIdException
     */
    public function testInvalidOrderIdExceptionThrown() {
        BitbayApiResponseValidator::valid('{"code":405}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\WalletEmptyException
     */
    public function testWalletEmptyExceptionThrown() {
        BitbayApiResponseValidator::valid('{"code":406}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidCurrencyNameException
     */
    public function testInvalidCurrencyNameExceptionThrown() {
        BitbayApiResponseValidator::valid('{"code":408}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidPublicKeyException
     */
    public function testInvalidPublicKeyExceptionThrown() {
        BitbayApiResponseValidator::valid('{"code":501}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidSignException
     */
    public function testInvalidSignExceptionThrown() {
        BitbayApiResponseValidator::valid('{"code":502}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidMomentParameterException
     */
    public function testInvalidMomentParameterExceptionThrown() {
        BitbayApiResponseValidator::valid('{"code":503}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidMethodNameException
     */
    public function testInvalidMethodExceptionThrown() {
        BitbayApiResponseValidator::valid('{"code":504}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\LackOfPermissionForThisActionException
     */
    public function testLackOfPermissionForThisActionExceptionThrown() {
        BitbayApiResponseValidator::valid('{"code":505}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\AccountLockedException
     */
    public function testAccountLockedExceptionThrown() {
        BitbayApiResponseValidator::valid('{"code":506}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\BicOrSwiftIsRequiredException
     */
    public function testBicOrSwiftIsRequiredExceptionThrown() {
        BitbayApiResponseValidator::valid('{"code":509}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidMarketException
     */
    public function testInvalidMarketExceptionThrown() {
        BitbayApiResponseValidator::valid('{"code":510}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidWalletAddressException
     */
    public function testInvalidWalletNumberExceptionThrown() {
        BitbayApiResponseValidator::valid('{"error":"not valid address"}');
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidAmountException
     */
    public function testNotValidAmountExceptionThrown() {
        BitbayApiResponseValidator::valid('{"error":"NotValidAmount"}');
    }
}