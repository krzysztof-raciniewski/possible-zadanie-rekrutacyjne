<?php

use App\Api\Bitbay\BitbayApi;
use App\Api\Bitbay\Enum\CurrencyName;

class BitBayApiOrderbookTest extends \TestCase
{

    public function testTradeOrderbookRequestWithValidParams()
    {
        try {
            $api = new BitbayApi();
            $result = $api->getOpenOffersFromMarket(CurrencyName::BTC, CurrencyName::PLN);
            self::assertNotEmpty($result);
            self::assertTrue(array_key_exists('bids', $result));
            self::assertTrue(array_key_exists('asks', $result));
            self::assertGreaterThan(0, count($result['bids']));
            self::assertGreaterThan(0, count($result['asks']));

            self::assertEquals($result['bids'][0]['currency'], CurrencyName::PLN);
        } catch (\App\Api\Bitbay\BitbayExceptions\NoOrdersWithSpecifiedCurrenciesException $e) {
            self::markTestIncomplete('No orders with specified currencies');
        }
    }


    public function testTradeOrderbookRequestWithDefaultParams()
    {
        try {
            $api = new BitbayApi();
            $result = $api->getOpenOffersFromMarket(CurrencyName::BTC, CurrencyName::PLN);
            self::assertNotEmpty($result);
            self::assertTrue(array_key_exists('bids', $result));
            self::assertTrue(array_key_exists('asks', $result));
            self::assertGreaterThan(0, count($result['bids']));
            self::assertGreaterThan(0, count($result['asks']));

            self::assertEquals($result['bids'][0]['currency'], CurrencyName::PLN);
        } catch (\App\Api\Bitbay\BitbayExceptions\NoOrdersWithSpecifiedCurrenciesException $e) {
            self::markTestIncomplete('No orders with specified currencies');
        }
    }

    public function testTradeOrderbookRequestBTCtoLTC()
    {
        try {
            $api = new BitbayApi();
            $result = $api->getOpenOffersFromMarket(CurrencyName::BTC, CurrencyName::LTC);
            self::assertNotEmpty($result);
            self::assertTrue(array_key_exists('bids', $result));
            self::assertTrue(array_key_exists('asks', $result));
            self::assertGreaterThan(0, count($result['bids']));
            self::assertGreaterThan(0, count($result['asks']));

            self::assertEquals($result['bids'][0]['currency'], CurrencyName::LTC);
        } catch(\App\Api\Bitbay\BitbayExceptions\NoOrdersWithSpecifiedCurrenciesException $e) {
            self::markTestIncomplete('No orders with specified currencies');
        }
    }

    public function testTradeOrderbookRequestBTCtoUSD()
    {
        try {
            $api = new BitbayApi();
            $result = $api->getOpenOffersFromMarket(CurrencyName::BTC, CurrencyName::USD);
            self::assertNotEmpty($result);
            self::assertTrue(array_key_exists('bids', $result));
            self::assertTrue(array_key_exists('asks', $result));
            self::assertGreaterThan(0, count($result['bids']));
            self::assertGreaterThan(0, count($result['asks']));

            self::assertEquals($result['bids'][0]['currency'], CurrencyName::USD);
        } catch(\App\Api\Bitbay\BitbayExceptions\NoOrdersWithSpecifiedCurrenciesException $e) {
            self::markTestIncomplete('No orders with specified currencies');
        }
    }

}