<?php
declare(strict_types=1);

use App\Api\Bitbay\BitbayApi;
use App\Api\Bitbay\Enum\CurrencyName;

class BitBayApiInfoTest extends TestCase
{
    /**
     * Get information about wallets from BitBay.pl - test
     */
    public function testGetInfoApiWithDefaultParametersMethod()
    {
        $api = new BitbayApi();
        $result = $api->getInfo();
        self::assertNotEmpty($result);

        self::assertTrue(array_key_exists('success', $result));
        self::assertTrue(array_key_exists('addresses', $result));
        self::assertFalse(array_key_exists('message', $result));

        self::assertTrue(array_key_exists('balances', $result));

        $balances = $result['balances'];

        self::assertTrue(array_key_exists(CurrencyName::PLN, $balances));
        self::assertTrue(array_key_exists(CurrencyName::EUR, $balances));
        self::assertTrue(array_key_exists(CurrencyName::USD, $balances));
        self::assertTrue(array_key_exists(CurrencyName::BTC, $balances));
        self::assertTrue(array_key_exists(CurrencyName::ETH, $balances));
        self::assertTrue(array_key_exists(CurrencyName::LSK, $balances));
        self::assertTrue(array_key_exists(CurrencyName::LTC, $balances));

        self::assertEquals($result['success'], 1);
    }

    /**
     * Get information about wallets from BitBay.pl - custom params test
     */
    public function testGetInfoApiWithCustomPLNParameterMethod()
    {
        $api = new BitbayApi();
        $result = $api->getInfo(['currency' => CurrencyName::PLN]);
        self::assertNotEmpty($result);

        self::assertTrue(array_key_exists('success', $result));
        self::assertTrue(array_key_exists('addresses', $result));
        self::assertFalse(array_key_exists('message', $result));

        self::assertTrue(array_key_exists('balances', $result));

        $balances = $result['balances'];

        self::assertTrue(array_key_exists(CurrencyName::PLN, $balances));
        self::assertFalse(array_key_exists(CurrencyName::EUR, $balances));
        self::assertFalse(array_key_exists(CurrencyName::USD, $balances));
        self::assertFalse(array_key_exists(CurrencyName::BTC, $balances));
        self::assertFalse(array_key_exists(CurrencyName::ETH, $balances));
        self::assertFalse(array_key_exists(CurrencyName::LSK, $balances));
        self::assertFalse(array_key_exists(CurrencyName::LTC, $balances));

        self::assertEquals($result['success'], 1);
    }

    /**
     * Get information about wallets from BitBay.pl - custom params test
     */
    public function testGetInfoApiWithCustomUSDParameterMethod()
    {
        $api = new BitbayApi();
        $result = $api->getInfo(['currency' => CurrencyName::USD]);
        self::assertNotEmpty($result);
        self::assertTrue(array_key_exists('success', $result));
        self::assertTrue(array_key_exists('addresses', $result));
        self::assertFalse(array_key_exists('message', $result));
        self::assertTrue(array_key_exists('balances', $result));

        $balances = $result['balances'];

        self::assertTrue(array_key_exists(CurrencyName::USD, $balances));
        self::assertFalse(array_key_exists(CurrencyName::PLN, $balances));
        self::assertFalse(array_key_exists(CurrencyName::EUR, $balances));
        self::assertFalse(array_key_exists(CurrencyName::BTC, $balances));
        self::assertFalse(array_key_exists(CurrencyName::ETH, $balances));
        self::assertFalse(array_key_exists(CurrencyName::LSK, $balances));
        self::assertFalse(array_key_exists(CurrencyName::LTC, $balances));

        self::assertEquals($result['success'], 1);
    }
}