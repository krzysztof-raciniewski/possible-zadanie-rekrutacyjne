<?php

use App\Api\Bitbay\BitbayApi;
use App\Api\Bitbay\Enum\CurrencyName;

class BitBayApiTransferTest extends \TestCase
{

    /**
     * Bitbay commission = 0.00035 for BTC
     */
    private static $TESTING_BTC_WALLET_NUMBER = '1AWvKXjK7PSmrXQRNKrbBMB5ZqDPBpwr1N';
    private static $TESTING_ETH_WALLET_NUMBER = '0xB30d4008101147ABdb49e0DC5aBe688Fe9A1c92A';
    private static $TESTING_LTC_WALLET_NUMBER = 'LPWLuatVtH9KDNdq9Mt361VNLYVcR1CHpK';
    private static $TESTING_LSK_WALLET_NUMBER = '4518342334760175674L';

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidAmountException
     */
    public function testMakeBTCTransferRequestWithZeroAmountParam()
    {
        $this->markTestSkipped('Remove this to run method');
        $api = new BitbayApi();
        $api->makeTransfer(CurrencyName::BTC, 0, self::$TESTING_BTC_WALLET_NUMBER);
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidAmountException
     */
    public function testMakeBTCTransferRequestWithNegativeAmountParam()
    {
        $this->markTestSkipped('Remove this to run method');
        $api = new BitbayApi();
        $api->makeTransfer(CurrencyName::BTC, -0.0001, self::$TESTING_BTC_WALLET_NUMBER);
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidAmountException
     */
    public function testMakeBTCTransferRequestWithTooLargeAmmountParam()
    {
        $this->markTestSkipped('Remove this to run method');
        $api = new BitbayApi();
        $api->makeTransfer(CurrencyName::BTC, PHP_INT_MAX, self::$TESTING_BTC_WALLET_NUMBER);
    }

    /**
     * @expectedException \App\Api\Bitbay\BitbayExceptions\InvalidWalletAddressException
     */
    public function testMakeBTCTransferRequestWithInvalidWalletNumber() {
        $this->markTestSkipped('Remove this to run method');
        $api = new BitbayApi();
        $result = $api->makeTransfer(CurrencyName::BTC, 0.0001, 'bad_wallet');
        $this->assertNotEmpty($result);
    }


    public function testMakeBTCTransferRequestWithValidAmountParam()
    {
//        $this->markTestSkipped('Remove this to run method');
        try {
            $api = new BitbayApi();
            $result = $api->makeTransfer(CurrencyName::BTC, 0.00109999999, self::$TESTING_BTC_WALLET_NUMBER);
            $this->assertNotEmpty($result);
            $this->assertTrue(array_key_exists('success', $result));
            $this->assertEquals($result['success'], 1);
            $this->assertTrue(array_key_exists('insert_id', $result));
            $this->assertGreaterThan(0, $result['insert_id']);
        } catch( \App\Api\Bitbay\BitbayExceptions\WalletEmptyException $e ) {
            $this->markTestIncomplete('No enought money in the wallet to test transfer operation');
        }
    }

    public function testMakeETHTransferRequestWithValidAmountParam()
    {
        $this->markTestSkipped('Remove this to run method');
        try {
            $api = new BitbayApi();
            $result = $api->makeTransfer(CurrencyName::ETH, 0.03, self::$TESTING_ETH_WALLET_NUMBER);
            $this->assertNotEmpty($result);
            $this->assertTrue(array_key_exists('success', $result));
            $this->assertEquals($result['success'], 1);
            $this->assertTrue(array_key_exists('insert_id', $result));
            $this->assertGreaterThan(0, $result['insert_id']);
        } catch( \App\Api\Bitbay\BitbayExceptions\WalletEmptyException $e ) {
            $this->markTestIncomplete('No enought money in the wallet to test transfer operation');
        }
    }

    public function testMakeLTCTransferRequestWithValidAmountParam()
    {
        $this->markTestSkipped('Remove this to run method');
        try {
            $api = new BitbayApi();
            $result = $api->makeTransfer(CurrencyName::LTC, 0.1, self::$TESTING_LTC_WALLET_NUMBER);
            $this->assertNotEmpty($result);
            $this->assertTrue(array_key_exists('success', $result));
            $this->assertEquals($result['success'], 1);
            $this->assertTrue(array_key_exists('insert_id', $result));
            $this->assertGreaterThan(0, $result['insert_id']);
        } catch( \App\Api\Bitbay\BitbayExceptions\WalletEmptyException $e ) {
            $this->markTestIncomplete('No enought money in the wallet to test transfer operation');
        }
    }

    public function testMakeLSKTransferRequestWithValidAmountParam()
    {
        $this->markTestSkipped('Remove this to run method');
        try {
            $api = new BitbayApi();
            $result = $api->makeTransfer(CurrencyName::LSK, 0.00000003, self::$TESTING_LSK_WALLET_NUMBER);
            $this->assertNotEmpty($result);
            $this->assertTrue(array_key_exists('success', $result));
            $this->assertEquals($result['success'], 1);
            $this->assertTrue(array_key_exists('insert_id', $result));
            $this->assertGreaterThan(0, $result['insert_id']);
        } catch( \App\Api\Bitbay\BitbayExceptions\WalletEmptyException $e ) {
            $this->markTestIncomplete('No enought money in the wallet to test transfer operation');
        }
    }
}