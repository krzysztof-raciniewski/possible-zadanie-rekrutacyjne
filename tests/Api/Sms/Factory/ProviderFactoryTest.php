<?php
declare(strict_types = 1);

use App\Api\Sms\Factory\ProviderFactory;
use App\Enums\ProviderName;

class ProviderFactoryTest extends TestCase
{
    private $smsRepository;
    private $sms;
    protected $em;

    public function setUp()
    {
        parent::setUp();
        $this->initializeTestsForUserOperations();
        $this->smsRepository = $this->app->make(\App\Entities\Repositories\RepositoriesInterfaces\SmsRepository::class);
        $this->sms = $this->smsRepository->findAll()[0];
    }

    public function testProviderFactoryWithValidParams()
    {
        // Microsms
        $provider = ProviderFactory::make(ProviderName::MICROSMS, $this->sms);
        $this->assertInstanceOf(\App\Api\Sms\MicroSmsProvider::class, $provider);
        $this->assertInstanceOf(\App\Api\Sms\CustomInterface\ProviderInterface::class, $provider);

        // JustPay
        $provider = ProviderFactory::make(ProviderName::JUSTPAY, $this->sms);
        $this->assertInstanceOf(\App\Api\Sms\JustPayProvider::class, $provider);
        $this->assertInstanceOf(\App\Api\Sms\CustomInterface\ProviderInterface::class, $provider);


        // Ebill
        $provider = ProviderFactory::make(ProviderName::EBILL, $this->sms);
        $this->assertInstanceOf(\App\Api\Sms\EbillProvider::class, $provider);
        $this->assertInstanceOf(\App\Api\Sms\CustomInterface\ProviderInterface::class, $provider);


        // CashBill
        $provider = ProviderFactory::make(ProviderName::CASHBILL, $this->sms);
        $this->assertInstanceOf(\App\Api\Sms\CashBillProvider::class, $provider);
        $this->assertInstanceOf(\App\Api\Sms\CustomInterface\ProviderInterface::class, $provider);


        // DotPay
        $provider = ProviderFactory::make(ProviderName::DOTPAY, $this->sms);
        $this->assertInstanceOf(\App\Api\Sms\DotPayProvider::class, $provider);
        $this->assertInstanceOf(\App\Api\Sms\CustomInterface\ProviderInterface::class, $provider);

    }

    /**
     * @expectedException App\Api\Sms\Factory\Exceptions\ProviderNotSupportedException
     */
    public function testProviderFactoryWithInvalidParams() {
        ProviderFactory::make('BadProviderName', $this->sms);
    }


}