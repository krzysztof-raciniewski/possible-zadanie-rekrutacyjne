<?php
declare(strict_types = 1);


use App\Api\Sms\Randomizer\SmsProviderRandomizer;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class SmsProviderRandomizerTest extends TestCase
{
    /** @var  \App\Entities\Repositories\CustomProviderRepository */
    protected $repository;

    /** @var  \Doctrine\ORM\EntityManagerInterface */
    protected $em;

    public function setUp()
    {
        parent::setUp();
        $this->repository = $this->app->make(\App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository::class);
        $this->em = App::make('Doctrine\ORM\EntityManagerInterface');
    }

    public function testGetActiveProvider()
    {
        try {
            $smsProviderRandomizer = $this->app->make(SmsProviderRandomizer::class);
            $provider = $smsProviderRandomizer->getActive();

            self::assertInstanceOf(\App\Entities\Provider::class, $provider);

            self::assertTrue($provider->isEnabled());
            self::assertTrue($provider->isConfigured());
            self::assertTrue($provider->isActive());
        } catch (NoResultException $e) {
            self::fail('No result exception');
        } catch (NonUniqueResultException $e) {
            self::fail('No unique result exception');
        }
    }

    public function testSetNextActiveProvider()
    {
        /** @var SmsProviderRandomizer $smsProviderRandomizer */
        $smsProviderRandomizer = $this->app->make(SmsProviderRandomizer::class);

        for ($i = 0; $i < 100; $i++) {
            $providerBefore = $smsProviderRandomizer->getActive();
            $smsProviderRandomizer->switch();
            $providerAfter = $smsProviderRandomizer->getActive();
            self::assertNotSame($providerBefore, $providerAfter);
        }

        $allConfiguredAndEnabledProviders = $this->repository->findAllConfiguredAndEnabled();

        // Set first provider as active
        for ($i = 0, $max = count($allConfiguredAndEnabledProviders); $i < $max; $i++) {
            $i === 0 ?
                $allConfiguredAndEnabledProviders[$i]->setActive(true)
                :
                $allConfiguredAndEnabledProviders[$i]->setActive(false);
        }

        for ($i = 0, $max = count($allConfiguredAndEnabledProviders)-1; $i < $max; $i++) {
            self::assertTrue($allConfiguredAndEnabledProviders[$i]->isActive());
            self::assertFalse($allConfiguredAndEnabledProviders[$i+1]->isActive());
            $smsProviderRandomizer->switch();
            self::assertFalse($allConfiguredAndEnabledProviders[$i]->isActive());
            self::assertTrue($allConfiguredAndEnabledProviders[$i+1]->isActive());
        }
    }

    /**
     * @depends testSetNextActiveProvider
     */
    public function testOnlyOneProviderShouldBeActive() {
        $providers = $this->repository->findByActive(true);
        self::assertCount(1, $providers);
    }

    /**
     * @depends testOnlyOneProviderShouldBeActive
     */
    public function testOnlyConfiguredAndEnabledProvidersShouldBeActive() {
        /** @var SmsProviderRandomizer $smsProviderRandomizer */
        $smsProviderRandomizer = $this->app->make(SmsProviderRandomizer::class);

        $allConfiguredAndEnabledProviders = $this->repository->findAllConfiguredAndEnabled();

        $max = count($allConfiguredAndEnabledProviders);

        // Set first provider as active
        for ($i = 0; $i < $max; $i++) {
            $i === 0 ?
                $allConfiguredAndEnabledProviders[$i]->setActive(true)
                :
                $allConfiguredAndEnabledProviders[$i]->setActive(false);
        }

        // Set second provider are not configured
        $allConfiguredAndEnabledProviders[1]->setConfigured(false);
        $this->em->persist($allConfiguredAndEnabledProviders[1]);
        $this->em->flush();

        self::assertCount($max-1, $this->repository->findAllConfiguredAndEnabled());

        for ($i = 0; $i < 100; $i++) {
            $smsProviderRandomizer->switch();
            self::assertFalse($allConfiguredAndEnabledProviders[1]->isConfigured());
            self::assertFalse($allConfiguredAndEnabledProviders[1]->isActive());
        }

        // Set third provider are not configured
        $allConfiguredAndEnabledProviders[2]->setEnabled(false);
        $this->em->persist($allConfiguredAndEnabledProviders[2]);
        $this->em->flush();

        self::assertCount($max-2, $this->repository->findAllConfiguredAndEnabled());

        for ($i = 0; $i < 100; $i++) {
            $smsProviderRandomizer->switch();
            self::assertFalse($allConfiguredAndEnabledProviders[1]->isConfigured());
            self::assertFalse($allConfiguredAndEnabledProviders[1]->isActive());
            self::assertFalse($allConfiguredAndEnabledProviders[1]->isConfigured());
            self::assertFalse($allConfiguredAndEnabledProviders[1]->isActive());
        }

        $allConfiguredAndEnabledProviders[1]->setConfigured(true);
        $allConfiguredAndEnabledProviders[1]->setEnabled(true);
        $allConfiguredAndEnabledProviders[2]->setConfigured(true);
        $allConfiguredAndEnabledProviders[2]->setEnabled(true);
        $this->em->flush();
    }
}