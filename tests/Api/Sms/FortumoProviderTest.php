<?php
declare(strict_types = 1);


use App\Api\Sms\FortumoProvider;

class FortumoProviderTest extends TestCase
{

    public function testCheckSignatureWithValidQueryMethod()
    {
        $params = [
            "billing_type" => "MO",
            "country" => "PL",
            "currency" => "PLN",
            "keyword" => "TXT BITMOBILE",
            "message" => "TEEEEEEEEEEST",
            "message_id" => "4ef1744aacb8a1abb5cde7a54da4cf2b",
            "operator" => "Cyfrowy Polsat",
            "price" => "2.46",
            "price_wo_vat" => "2.0",
            "sender" => "570380486",
            "service_id" => "9a07067d6b3c1bdcd2275688eba85daa",
            "shortcode" => "7268",
            "sig" => "285633833b4af2f9d1d8dcbb2198f1f4",
            "status" => "pending",
            "test" => "true"
        ];

        $fortumo = new FortumoProvider($params);

        self::assertTrue($fortumo->checkSignature());
    }

    /**
     * @expectedException \App\Api\Sms\ProviderException\FortumoInvalidParametersException
     */
    public function testCheckSignatureWithInalidQueryMethod()
    {
        $params = [
            "billing_type" => "MO",
            "country" => "PL",
            "currency" => "PLN",
            "keyword" => "TXT BITMOBILE",
            "message" => "TEEEEEEEEEEST",
            "message_id" => "4ef1744aacb8a1abb5cde7a54da4cf2b",
            "operator" => "Cyfrowy Polsat",
            "price" => "2.46",
            "price_wo_vat" => "2.0",
            "sender" => "570380486",
            "shortcode" => "7268",
            "sig" => "285633833b4af2f9d1d8dcbb2198f1f4",
            "status" => "pending",
            "test" => "true"
        ];

        new FortumoProvider($params);
    }

    /**
     * @expectedException \App\Api\Sms\ProviderException\FortumoInvalidParametersException
     */
    public function testCheckSignatureWithInalidQueryMethod2()
    {
        $params = [
            "billing_type" => "MO",
            "country" => "PL",
            "currency" => "PLN",
            "keyword" => "TXT BITMOBILE",
            "message" => "TEEEEEEEEEEST",
            "test" => "true"
        ];

        new FortumoProvider($params);
    }

    /**
     * @expectedException \App\Api\Sms\ProviderException\FortumoInvalidParametersException
     */
    public function testCheckSignatureWithInalidQueryMethod3()
    {
        $params = [];

        new FortumoProvider($params);
    }


    public function testCheckSignatureWithInvalidQueryMethod()
    {
        $params = [
            "billing_type" => "MO",
            "country" => "PL",
            "currency" => "PLN",
            "keyword" => "TXT BITMOBILE",
            "message" => "INVALID",
            "message_id" => "4ef1744aacb8a1abb5cde7a54da4cf2b",
            "operator" => "Cyfrowy Polsat",
            "price" => "2.46",
            "price_wo_vat" => "2.0",
            "sender" => "570380486",
            "service_id" => "9a07067d6b3c1bdcd2275688eba85daa",
            "shortcode" => "7268",
            "sig" => "285633833b4af2f9d1d8dcbb2198f1f4",
            "status" => "pending",
            "test" => "true"
        ];

        $fortumo = new FortumoProvider($params);

        self::assertFalse($fortumo->checkSignature());
    }
}