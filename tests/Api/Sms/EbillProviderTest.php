<?php
declare(strict_types = 1);


use App\Api\Sms\EbillProvider;

class EbillProviderTest extends TestCase
{
    // Test code
    private static $CODE = 'dfadgdfge24534t24513536yt5363563w4t554wt543w5ttey73e5';
    private $smsRepository;
    private $sms;
    protected $em;

    public function setUp()
    {
        parent::setUp();
        $this->initializeTestsForUserOperations();
        $this->smsRepository = $this->app->make(\App\Entities\Repositories\RepositoriesInterfaces\SmsRepository::class);
        $this->sms = $this->smsRepository->findOneByServiceId('68741');
    }

    public function testEbillCheckCodeMethod()
    {
        $api = new EbillProvider($this->sms);
        $api->setTestingEnvironment();
        $result = $api->checkCode(self::$CODE);

        self::assertTrue($result);
    }


    public function testSetTestingEnvironmentMethod()
    {
        $api = new EbillProvider($this->sms);
        self::assertTrue($api->isProduction());
        self::assertFalse($api->isTesting());
        $api->setTestingEnvironment();
        self::assertTrue($api->isTesting());
        self::assertFalse($api->isProduction());
        $api->setProductionEnvironment();
        self::assertTrue($api->isProduction());
        self::assertFalse($api->isTesting());
    }

}