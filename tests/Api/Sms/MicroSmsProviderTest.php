<?php
declare(strict_types = 1);


use App\Api\Sms\MicroSmsProvider;

class MicroSmsProviderTest extends TestCase
{

    private static $NUMBER = '7136';
    private static $CODE = 'p2o6o8f0';

    /**
     * Example returned response from api when code is correct:
     *          {
     *              "connect":true,
     *              "data":{
     *                  "status":1,
     *                  "service":"1831",
     *                  "number":"7055",
     *                  "phone":"48123456789",
     *                  "reply":"Twoj kod do zakonczenia transakcji: g0t0d2w2. Dziekujemy i zapraszamy ponownie."
     *              }
     *          }
     *
     *
     * Example response from api when code is incorrect:
     *          {
     *              "connect":true,
     *              "data":{
     *                  "status":0,
     *                  "service":0,
     *                  "number":0,
     *                  "phone":0,
     *                  "reply":0
     *              }
     *          }
     */

    private $smsRepository;
    private $sms;
    protected $em;

    public function setUp()
    {
        parent::setUp();
        $this->initializeTestsForUserOperations();
        $this->smsRepository = $this->app->make(\App\Entities\Repositories\RepositoriesInterfaces\SmsRepository::class);
        $this->sms = $this->smsRepository->findOneBy([
            'command' => 'MSMS.TRANSFER',
            'laNumber' => '7136'
        ]);
    }

    public function testMicroSmsCheckCodeMethodWithValidParameters()
    {
//        self::markTestIncomplete('Remove this line and set valid $CODE and $NUMBER to send test API request');
        $api = new MicroSmsProvider($this->sms);
        $result = $api->checkCode(self::$CODE, self::$NUMBER);

        self::assertTrue($result);

        $result = $api->getResponse();

        self::assertTrue(array_key_exists('connect', $result));
        self::assertTrue($result['connect']);
        self::assertTrue(array_key_exists('data', $result));
        self::assertCount(5, $result['data']);
        self::assertTrue(array_key_exists('status', $result['data']));
        self::assertEquals($result['data']['status'], 1);
        self::assertTrue(array_key_exists('service', $result['data']));
        self::assertEquals($result['data']['service'], $api->getServiceId());
        self::assertTrue(array_key_exists('number', $result['data']));
        self::assertEquals($result['data']['number'], self::$NUMBER);
        self::assertTrue(array_key_exists('phone', $result['data']));
        self::assertTrue(array_key_exists('reply', $result['data']));
    }

    /**
     * @depends testMicroSmsCheckCodeMethodWithValidParameters
     */
    public function testMicroSmsCheckCodeMethodSecondTimeWithTheSameCode()
    {
        $api = new MicroSmsProvider($this->sms);
        $result = $api->checkCode(self::$CODE, self::$NUMBER);

        self::assertFalse($result);

        $result = $api->getResponse();

        self::assertTrue(array_key_exists('connect', $result));
        self::assertTrue($result['connect']);
        self::assertTrue(array_key_exists('data', $result));
        self::assertCount(5, $result['data']);
        self::assertTrue(array_key_exists('status', $result['data']));
        self::assertEquals($result['data']['status'], 0);
        self::assertTrue(array_key_exists('service', $result['data']));
        self::assertEquals($result['data']['service'], 0);
        self::assertTrue(array_key_exists('number', $result['data']));
        self::assertEquals($result['data']['number'], 0);
        self::assertTrue(array_key_exists('phone', $result['data']));
        self::assertEquals($result['data']['phone'], 0);
        self::assertTrue(array_key_exists('reply', $result['data']));
        self::assertEquals($result['data']['reply'], '');
    }

}