<?php
declare(strict_types = 1);


use App\Api\Sms\JustPayProvider;

class JustPayProviderTest extends TestCase
{
    private static $CODE = 'btec22';
    private $smsRepository;
    private $sms;
    protected $em;

    public function setUp()
    {
        parent::setUp();
        $this->initializeTestsForUserOperations();
        $this->smsRepository = $this->app->make(\App\Entities\Repositories\RepositoriesInterfaces\SmsRepository::class);
        $this->sms = $this->smsRepository->findOneByLaNumber('7055');
    }

    public function testJustPayCheckCodeMethodWithValidParameters()
    {
//        self::markTestIncomplete('Insert valid code and remove this command');
        $api = new JustPayProvider($this->sms);
        $result = $api->checkCode(self::$CODE);

        self::assertTrue($result);
    }


}