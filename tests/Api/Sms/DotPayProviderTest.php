<?php
declare(strict_types = 1);


use App\Api\Sms\DotPayProvider;

class DotPayProviderTest extends TestCase
{
    // Test code
    private static $CODE = 'LKD9972390DJHD';
    private static $INVALIDCODE = 'INVALIDCODE';
    private $smsRepository;
    /** @var  \App\Entities\Sms $sms */
    private $sms;
    protected $em;

    public function setUp()
    {
        parent::setUp();
        $this->initializeTestsForUserOperations();
        $this->smsRepository = $this->app->make(\App\Entities\Repositories\RepositoriesInterfaces\SmsRepository::class);
        $this->sms = $this->smsRepository->findOneByLaNumber('70068');
    }

    public function testDotPayCheckValidCodeMethod()
    {
        $api = new DotPayProvider($this->sms);
        $api->setTestingEnvironment();
        $result = $api->checkCode(self::$CODE);
        self::assertTrue($result);

        $response = $api->getResponse();
        $explodeResponse = explode("\n", $response);
        $command = $this->sms->getCommand();
        $commandExplode = explode('.', $command);

        $this->assertEquals($explodeResponse[0], 1);
        $this->assertEquals($explodeResponse[2], $commandExplode[1]);
    }

    /**
     * @expectedException \App\Api\Sms\ProviderException\DotPayBadServerException
     */
    public function testDotPayCheckInvalidCodeMethod()
    {
        $api = new DotPayProvider($this->sms);
        $result = $api->checkCode(self::$INVALIDCODE);
    }

}