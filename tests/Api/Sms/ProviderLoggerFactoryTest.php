<?php
declare(strict_types = 1);


use App\Api\Sms\ProviderLogger\FortumoProviderLogger;
use App\Api\Sms\ProviderLogger\ProviderLogger;
use App\Api\Sms\ProviderLogger\ProviderLoggerFactory;
use App\Entities\Repositories\RepositoriesInterfaces\PaymentLogsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\PaymentReportsRepository;
use App\Enums\ProviderName;

class ProviderLoggerFactoryTest extends TestCase
{
    /** @var PaymentLogsRepository  */
    private $paymentLogsRepository;
    private $paymentReportsRepository;

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();
        /** @var PaymentLogsRepository repository */
        $this->paymentLogsRepository = $this->app->make(PaymentLogsRepository::class);
        $this->paymentReportsRepository = $this->app->make(PaymentReportsRepository::class);
    }

    public function testStaticCreateMethod()
    {
        /** @var ProviderLogger $providerLogger */
        $providerLogger = ProviderLoggerFactory::create(ProviderName::FORTUMO);
        self::assertInstanceOf(ProviderLogger::class, $providerLogger);
        self::assertEquals($providerLogger->getName(), ProviderName::FORTUMO);
    }

    /**
     * @depends testStaticCreateMethod
     * @expectedException \App\Api\Sms\ProviderLogger\Exceptions\ProviderLoggerNotImplementedException
     */
    public function testStaticCreateMethodForInvalidProviderName()
    {
        ProviderLoggerFactory::create('badname');
    }

    /**
     * @depends testStaticCreateMethod
     */
    public function testFortumoLogger()
    {
        /** @var ProviderLogger $providerLogger */
        $providerLogger = ProviderLoggerFactory::create(ProviderName::FORTUMO);
        self::assertInstanceOf(FortumoProviderLogger::class, $providerLogger);
        self::assertEquals($providerLogger->getName(), ProviderName::FORTUMO);
    }

    /**
     * @depends testFortumoLogger
     */
    public function testFortumoLoggerLogPaymentMethod()
    {
        /** @var FortumoProviderLogger $providerLogger */
        $providerLogger = ProviderLoggerFactory::create(ProviderName::FORTUMO);

        $logsBefore = $this->paymentLogsRepository->findAll();

        $params = [
            "billing_type" => "MO",
            "country" => "PL",
            "currency" => "PLN",
            "keyword" => "TXT BITMOBILE",
            "message" => "TEEEEEEEEEEST",
            "message_id" => "4ef1744aacb8a1abb5cde7a54da4cf2b",
            "operator" => "Cyfrowy Polsat",
            "price" => "2.46",
            "price_wo_vat" => "2.0",
            "sender" => "570380486",
            "service_id" => "9a07067d6b3c1bdcd2275688eba85daa",
            "shortcode" => "7268",
            "sig" => "285633833b4af2f9d1d8dcbb2198f1f4",
            "status" => "pending",
            "test" => "true"
        ];

        $logId = $providerLogger->logPayment($params);

        $logsAfter = $this->paymentLogsRepository->findAll();

        self::assertCount(count($logsBefore)+1, $logsAfter);

        /** @var \App\Entities\PaymentLog $addedLog */
        $addedLog = $this->paymentLogsRepository->findOneById($logId);

        self::assertEquals($params['billing_type'], $addedLog->getBillingType());
        self::assertEquals($params['country'], $addedLog->getCountry());
        self::assertEquals($params['currency'], $addedLog->getCurrency());
        self::assertEquals($params['keyword'], $addedLog->getCommand());
        self::assertEquals($params['message'], $addedLog->getMessage());
        self::assertEquals($params['message_id'], $addedLog->getMessageId());
        self::assertEquals($params['operator'], $addedLog->getOperator());
        self::assertEquals($params['price'], (string)$addedLog->getGrossPrice());
        self::assertEquals($params['price_wo_vat'], (string)$addedLog->getNetPrice());
        self::assertEquals($params['sender'], $addedLog->getPhoneNumber());
        self::assertEquals($params['service_id'], $addedLog->getServiceId());
        self::assertEquals($params['shortcode'], $addedLog->getLaNumber());
        self::assertEquals($params['sig'], $addedLog->getSig());
        self::assertEquals($params['status'], $addedLog->getStatus());
        self::assertEquals((boolean)$params['test'], $addedLog->isTesting());

    }

    public function testFortumoLoggerLogReportMethod()
    {
        /** @var FortumoProviderLogger $providerLogger */
        $providerLogger = ProviderLoggerFactory::create(ProviderName::FORTUMO);

        $logsBefore = $this->paymentReportsRepository->findAll();

        $params = [
            "billing_type" => "MO",
            "country" => "PL",
            "currency" => "PLN",
            "keyword" => "TXT BITMOBILE",
            "message" => "TEEEEEEEEEEST",
            "message_id" => "4ef1744aacb8a1abb5cde7a54da4cf2b",
            "operator" => "Cyfrowy Polsat",
            "price" => "2.46",
            "price_wo_vat" => "2.0",
            "sender" => "570380486",
            "service_id" => "9a07067d6b3c1bdcd2275688eba85daa",
            "shortcode" => "7268",
            "sig" => "285633833b4af2f9d1d8dcbb2198f1f4",
            "status" => "pending",
            "test" => "true"
        ];

        $reportId = $providerLogger->logReport($params);

        $logsAfter = $this->paymentReportsRepository->findAll();

        self::assertCount(count($logsBefore)+1, $logsAfter);

        /** @var \App\Entities\PaymentReport $addedReport */
        $addedReport = $this->paymentReportsRepository->findOneById($reportId);

        self::assertEquals($params['billing_type'], $addedReport->getBillingType());
        self::assertEquals($params['country'], $addedReport->getCountry());
        self::assertEquals($params['currency'], $addedReport->getCurrency());
        self::assertEquals($params['keyword'], $addedReport->getCommand());
        self::assertEquals($params['message'], $addedReport->getMessage());
        self::assertEquals($params['message_id'], $addedReport->getMessageId());
        self::assertEquals($params['operator'], $addedReport->getOperator());
        self::assertEquals($params['price'], (string)$addedReport->getGrossPrice());
        self::assertEquals($params['price_wo_vat'], (string)$addedReport->getNetPrice());
        self::assertEquals($params['sender'], $addedReport->getPhoneNumber());
        self::assertEquals($params['service_id'], $addedReport->getServiceId());
        self::assertEquals($params['shortcode'], $addedReport->getLaNumber());
        self::assertEquals($params['sig'], $addedReport->getSig());
        self::assertEquals($params['status'], $addedReport->getStatus());
        self::assertEquals((boolean)$params['test'], $addedReport->isTesting());

    }
}