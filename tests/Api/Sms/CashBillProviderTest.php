<?php
declare(strict_types = 1);


use App\Api\Sms\CashBillProvider;

class CashBillProviderTest extends TestCase
{
    // Test code
    private static $CODE = 'TMMWWJNC';
    private static $INVALIDCODE = 'INVALIDCODE';
    private $smsRepository;
    private $sms;
    protected $em;

    public function setUp()
    {
        parent::setUp();
        $this->initializeTestsForUserOperations();
        $this->smsRepository = $this->app->make(\App\Entities\Repositories\RepositoriesInterfaces\SmsRepository::class);
        $this->sms = $this->smsRepository->findOneByLaNumber('71880');
    }

    public function testCashBillCheckValidCodeMethod()
    {
//        self::markTestSkipped('Insert valid SMS code and remove this line to run test');
        $api = new CashBillProvider($this->sms);
        $result = $api->checkCode(self::$CODE);

        self::assertTrue($result);

        $response = $api->getResponse();
        self::assertTrue(array_key_exists('active', $response));
        self::assertTrue(array_key_exists('number', $response));
        self::assertTrue(array_key_exists('activeFrom', $response));
        self::assertTrue(array_key_exists('codeValidityTime', $response));
        self::assertTrue(array_key_exists('timeRemaining', $response));
        self::assertTrue($response['active']);
    }


    public function testCashBillCheckInvalidCodeMethod()
    {
        $api = new CashBillProvider($this->sms);
        $result = $api->checkCode(self::$INVALIDCODE);

        self::assertFalse($result);

        $response = $api->getResponse();
        self::assertTrue(array_key_exists('error', $response));
        self::assertTrue(array_key_exists('code', $response['error']));
        self::assertTrue(array_key_exists('message', $response['error']));
    }

}