<?php
declare(strict_types = 1);


use App\Api\Bitbay\Enum\CurrencyName;
use App\Api\CryptocurrencyPrice\CryptoCompareApi;

class CryptoCompareApiTest extends TestCase
{
    private $api;

    public function __construct() {
        $this->api = new CryptoCompareApi();
    }

    public function testGetPriceForBTCMethod()
    {
        $result = $this->api->getPrice(CurrencyName::BTC, CurrencyName::PLN);

        $this->assertNotNull($result);
        $this->assertGreaterThan(2000, $result);
    }

    public function testGetPriceForLTCMethod()
    {
        $result = $this->api->getPrice(CurrencyName::LTC, CurrencyName::PLN);

        $this->assertNotNull($result);
        $this->assertGreaterThan(10, $result);
    }

    public function testGetPriceForETHMethod()
    {
        $result = $this->api->getPrice(CurrencyName::ETH, CurrencyName::PLN);

        $this->assertNotNull($result);
        $this->assertGreaterThan(30, $result);
    }

    public function testGetPriceForLSKMethod()
    {
        $result = $this->api->getPrice(CurrencyName::LSK, CurrencyName::PLN);

        $this->assertNotNull($result);
        $this->assertGreaterThan(0.5, $result);
    }

    /**
     * @expectedException \App\Api\CryptocurrencyPrice\Exceptions\CryptoCompareApiNoResultsException
     */
    public function testGetPriceWithBadParamsMethod()
    {
        $result = $this->api->getPrice('BAD', CurrencyName::PLN);

        $this->assertNotNull($result);
        $this->assertGreaterThan(0.5, $result);
    }
}