<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';
    protected $faker;
    private $authUser = null;

    private $authUserToken = null;

    /** @var \Doctrine\ORM\EntityManager $em */
    protected $em;
    protected $usersRepository;
    /** @var  \App\Entities\User */
    protected $user;
    protected $token;
    protected $headers = [];

    public function __construct()
    {
        $this->faker = Faker\Factory::create();
    }

    public function initializeTestsForUserOperations()
    {
        $this->em = App::make('Doctrine\ORM\EntityManagerInterface');
        $this->usersRepository = $this->em->getRepository(App\Entities\User::class);
        /** @var User[] $users */
        $users = $this->usersRepository->findAll();
        $this->user = $users[0];

        $credentials = ['email' => $this->user->getEmail(), 'password' => 'haslo1234'];

        // verify the credentials and create a token for the user
        $this->token = JWTAuth::attempt($credentials);

        $this->headers = [
            'HTTP_Authorization' => 'Bearer ' . $this->token
        ];
        $this->refreshApplication();
    }

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    public function getAuthUser()
    {
        if (!$this->authUser) {
            $this->setAuthUserToken();
        }

        return $this->authUser;
    }

    public function getAuthUserToken()
    {
        if (!$this->authUserToken) {
            $this->setAuthUserToken();
        }

        return $this->authUserToken;
    }

    /*Laravel angular material starter test helpers*/

    public function seeApiSuccess()
    {
        return $this->seeJsonContains(['errors' => false]);
    }

    public function seeValidationError()
    {
        $this->assertResponseStatus(422);

        return $this->see('"errors":{');
    }

    public function seeApiError($error_code)
    {
        $this->assertResponseStatus($error_code);

        return $this->see('"errors":{');
    }

    public function seeJsonKey($entity)
    {
        return $this->see('"' . $entity . '":');
    }

    public function seeJsonValue($value)
    {
        return $this->see('"' . $value . '"');
    }

    public function seeJsonArray($entity)
    {
        return $this->see('"' . $entity . '":[');
    }

    public function seeJsonObject($entity)
    {
        return $this->see('"' . $entity . '":{');
    }

    /**
     * login the authUser using JWT and store the token.
     */
    private function setAuthUserToken()
    {
        $authUser = factory(App\User::class)->create();

        $this->authUser = $authUser;
        $this->authUserToken = JWTAuth::fromUser($authUser);
    }

    public function authUserGet($uri, $headers = [])
    {
        $headers = ['AUthorization' => 'Bearer ' . $this->getAuthUserToken()];

        return $this->get($uri, $headers);
    }

    public function authUserPost($uri, $parameters = [])
    {
        $uri .= '?token=' . $this->getAuthUserToken();

        return $this->post($uri, $parameters);
    }

    public function authUserPut($uri, $parameters = [])
    {
        $uri .= '?token=' . $this->getAuthUserToken();

        return $this->put($uri, $parameters);
    }

    public function authUserDelete($uri, $parameters = [])
    {
        $uri .= '?token=' . $this->getAuthUserToken();

        return $this->delete($uri, $parameters);
    }

    public function authUserCall($method, $uri, $parameters = [], $cookies = [], $files = [], $server = [], $content = null)
    {
        $uri .= '?token=' . $this->getAuthUserToken();

        return $this->call($method, $uri, $parameters, $cookies, $files, $server, $content);
    }

    protected function tearDown()
    {
        if($this->em) {
            $this->em->close();
        }
    }
}
