<?php


class PublicRoutesTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testPublicPageResponseCode()
    {
        $response = $this->call('GET', '/');

        $this->assertEquals(200, $response->status());
    }

    public function testUnsupportedBrowserPage()
    {
        $this->visit('/unsupported-browser')
             ->see('update your browser')
             ->see('Internet Explorer');
    }

    public function testPublicPagePage() {
        $this->visit('/')
            ->see('Coins4SMS.com -');
    }

    public function testAdminPagePage() {
        $this->visit('/admin/')
            ->see('Cons4SMS.com - Panel administracyjny');
    }

    public function testFortumoPaymentEndpoint() {
        $response = $this->call('GET', '/fortumo/payment/');
        $this->assertEquals(404, $response->status());
    }

    public function testFortumoReportEndpoint() {
        $response = $this->call('GET', '/fortumo/report/');
        $this->assertEquals(404, $response->status());
    }

    public function testFortumoPaymentEndpointWithInvalidParamsMethod()
    {
        $params = [
            "billing_type" => "MO",
            "country" => "PL",
            "currency" => "PLN",
            "keyword" => "TXT BITMOBILE",
            "message" => "INVALID",
            "message_id" => "4ef1744aacb8a1abb5cde7a54da4cf2b",
            "operator" => "Cyfrowy Polsat",
            "price" => "2.46",
            "price_wo_vat" => "2.0",
            "sender" => "570380486",
            "service_id" => "9a07067d6b3c1bdcd2275688eba85daa",
            "shortcode" => "7268",
            "sig" => "285633833b4af2f9d1d8dcbb2198f1f4",
            "status" => "pending",
            "test" => "true"
        ];


        $this->call('GET', '/fortumo/payment/', $params);
        $this->see('Error');

    }
}
