<?php
declare(strict_types = 1);


use App\Api\Sms\Randomizer\SmsProviderRandomizer;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class CustomProvidersRepositoryTest extends TestCase
{
    /** @var  \App\Entities\Repositories\CustomProviderRepository */
    protected $repository;

    public function setUp()
    {
        parent::setUp();
        $this->repository = $this->app->make(\App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository::class);
    }

    public function testFindAllConfiguredAndEnabledMethod() {
        $allConfiguredAndEnabledProviders = $this->repository->findAllConfiguredAndEnabled();

        foreach ($allConfiguredAndEnabledProviders as $provider) {
            self::assertTrue($provider->isConfigured());
            self::assertTrue($provider->isEnabled());
        }
    }

    public function testFindFirstActiveMethod() {
        $activeProvider = $this->repository->findFirstActive();

        self::assertTrue($activeProvider->isActive(), true);
        self::assertTrue($activeProvider->isEnabled(), true);
        self::assertTrue($activeProvider->isConfigured(), true);
    }
}