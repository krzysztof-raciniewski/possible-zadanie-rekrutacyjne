<?php


class DoctrineGeneratorCommandsTest extends TestCase
{
    public function testDoctrineGeneratorsExist()
    {
        $exitCode = Artisan::call('list');
        $output = Artisan::output();

        self::assertEquals($exitCode, 0);

        self::assertContains('doctrine', $output);
        self::assertContains('doctrine:info', $output);
        self::assertContains('doctrine:schema:drop', $output);
        self::assertContains('doctrine:schema:update', $output);
        self::assertContains('doctrine:schema:create', $output);
        self::assertContains('doctrine:mapping:import', $output);
        self::assertContains('doctrine:config:convert', $output);
        self::assertContains('doctrine:convert:mapping', $output);
        self::assertContains('doctrine:schema:validate', $output);
        self::assertContains('doctrine:generate:proxies', $output);
        self::assertContains('doctrine:generate:entities', $output);
        self::assertContains('doctrine:ensure:production', $output);
        self::assertContains('doctrine:clear:query:cache', $output);
        self::assertContains('doctrine:clear:result:cache', $output);
        self::assertContains('doctrine:clear:metadata:cache', $output);
    }
}
