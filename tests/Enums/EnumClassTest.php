<?php


use App\Enums\Enum;

class ExampleEnum extends Enum {
    const A = 1;
    const B = 2;
    const C = 3;
    const D = 4;
}

class EnumClassTest extends TestCase
{

    public function testExistsConstInClass() {
        self::assertEquals(ExampleEnum::A, 1);
        self::assertEquals(ExampleEnum::B, 2);
        self::assertEquals(ExampleEnum::C, 3);
        self::assertEquals(ExampleEnum::D, 4);
    }

}