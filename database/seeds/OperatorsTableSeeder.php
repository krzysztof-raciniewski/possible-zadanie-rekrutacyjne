<?php

use App\Entities\Repositories\RepositoriesInterfaces\OperatorsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Database\Seeder;

class OperatorsTableSeeder extends Seeder
{
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    /**
     * @var OperatorsRepository
     */
    private $operatorsRepository;

    /**
     * CountriesTableSeeder constructor.
     * @param EntityManagerInterface $em
     * @param OperatorsRepository $operatorsRepository
     */
    public function __construct(EntityManagerInterface $em, OperatorsRepository $operatorsRepository)
    {
        $this->em = $em;
        $this->operatorsRepository = $operatorsRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $operators = [
            'T-mobile',
            'Plus GSM',
            'Orange',
            'Play'
        ];

        foreach($operators as $operator) {
            if( !$this->operatorsRepository->findByName($operator)) {
                $newOperator = new \App\Entities\Operator();
                $newOperator->setName($operator);
                $this->em->persist($newOperator);
            }
        }

        $this->em->flush();
    }
}
