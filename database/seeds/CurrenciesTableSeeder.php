<?php

use App\Entities\Repositories\RepositoriesInterfaces\CurrenciesRepository;
use Illuminate\Database\Seeder;
use Doctrine\ORM\EntityManagerInterface;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    /**
     * @var CurrenciesRepository
     */
    private $currenciesRepository;

    /**
     * CountriesTableSeeder constructor.
     * @param EntityManagerInterface $em
     * @param CurrenciesRepository $currenciesRepository
     */
    public function __construct(EntityManagerInterface $em, CurrenciesRepository $currenciesRepository)
    {
        $this->em = $em;
        $this->currenciesRepository = $currenciesRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = [
            [
                'code' => 'PLN',
                'name' => 'Polskie złote'
            ],
            [
                'code' => 'EUR',
                'name' => 'Euro'
            ]
        ];

        foreach($currencies as $currency) {
            if( !$this->currenciesRepository->findByCode($currency['code']) ) {
                $newCurrency = new \App\Entities\Currency();
                $newCurrency->setCode($currency['code']);
                $newCurrency->setName($currency['name']);
                $this->em->persist($newCurrency);
            }
        }

        $this->em->flush();
    }
}
