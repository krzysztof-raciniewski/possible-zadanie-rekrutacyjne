<?php

use App\Entities\BitBay;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Database\Seeder;

class BitBayTableSeeder extends Seeder
{
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;
    private $bitBayRepository;
    private $config;

    /**
     * UsersTableSeeder constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->bitBayRepository = $em->getRepository(BitBay::class);
        $this->config = [
            'key' => '--removed--',
            'secret' => '--removed--'
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bitbay = $this->bitBayRepository->findAll();

        if( count($bitbay) == 0 ) {
            $bitbayObject = new BitBay();
        } else {
            $bitbayObject = $bitbay[0];
        }

        if( !$bitbayObject->getBtcMinPayment() ) {
            $bitbayObject->setBtcMinPayment(0.001);
        }

        if( !$bitbayObject->getLtcMinPayment() ) {
            $bitbayObject->setLtcMinPayment(0.1);
        }

        if( !$bitbayObject->getLskMinPayment() ) {
            $bitbayObject->setLskMinPayment(0.25);
        }

        if( !$bitbayObject->getEthMinPayment() ) {
            $bitbayObject->setEthMinPayment(0.03);
        }

        if( !$bitbayObject->getBtcPaymentFee() ) {
            $bitbayObject->setBtcPaymentFee(0.00035);
        }

        if( !$bitbayObject->getLtcPaymentFee() ) {
            $bitbayObject->setLtcPaymentFee(0.005);
        }

        if( !$bitbayObject->getLskPaymentFee() ) {
            $bitbayObject->setLskPaymentFee(0.2);
        }

        if( !$bitbayObject->getEthPaymentFee() ) {
            $bitbayObject->setEthPaymentFee(0.00126);
        }

        $this->em->persist($bitbayObject);
        $this->em->flush();
    }
}
