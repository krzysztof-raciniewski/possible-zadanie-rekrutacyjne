<?php

use App\Entities\Repositories\RepositoriesInterfaces\UsersRepository;
use App\Entities\User;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    private $usersRepository;

    /**
     * @var array
     */
    private $usersToCreate;

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    /**
     * UsersTableSeeder constructor.
     * @param \App\Entities\Repositories\RepositoriesInterfaces\UsersRepository $usersRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(UsersRepository $usersRepository, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->usersRepository = $usersRepository;
        $this->usersToCreate = [
            [
                'email' => 'krzysztof.raciniewski@gmail.com',
                'first_name' => 'Krzysztof',
                'last_name' => 'Raciniewski',
                'password' => 'haslo1234',
                'image' => ''
            ]
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var User[] $users */
        $users = $this->usersRepository->findAll();
        foreach($this->usersToCreate as $newUser) {
            $userExist = false;
            foreach($users as $user) {
                if( $user->getEmail() === $newUser['email'] ) {
                    $userExist = true;
                    break;
                }
            }

            if( $userExist ) {
                continue;
            }

            $userObject = new User();
            $userObject->setEmail($newUser['email']);
            $userObject->setFirstName($newUser['first_name']);
            $userObject->setLastName($newUser['last_name']);
            $userObject->setPassword(bcrypt($newUser['password']));
            $userObject->setImage($newUser['image']);

            $this->em->persist($userObject);
        }

        $this->em->flush();
    }
}
