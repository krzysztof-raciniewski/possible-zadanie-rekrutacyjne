<?php

use App\Entities\Repositories\RepositoriesInterfaces\SystemSettingsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Database\Seeder;

class SystemSettingsTableSeeder extends Seeder
{

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    private $systemSettingsRepository;

    /**
     * UsersTableSeeder constructor.
     * @param SystemSettingsRepository $systemSettingsRepository
     * @param EntityManagerInterface $em
     * @internal param ProvidersRepository $providersRepository
     * @internal param ProvidersRepository $providerRepository
     * @internal param ProviderRepository $usersRepository
     */
    public function __construct(SystemSettingsRepository $systemSettingsRepository, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->systemSettingsRepository = $systemSettingsRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = new \App\Entities\SystemSettings();
        $settingsObjects = $this->systemSettingsRepository->findAll();
        if(count($settingsObjects) !== 0) {
            $settings = $settingsObjects[0];
        }

        if(!$settings->getNotificationsEmail()) {
            $settings->setNotificationsEmail('--removed--');
        }

        if(!$settings->getContactEmail()) {
            $settings->setContactEmail('--removed--');
        }

        if(!$settings->getDomainName()) {
            $settings->setDomainName('--removed--');
        }

        if(!$settings->getOwnerName()) {
            $settings->setOwnerName('--removed--');
        }

        if(!$settings->getApplicationName()) {
            $settings->setApplicationName('--removed--');
        }

        if(!$settings->getPaymentProcessStatus()) {
            $settings->setPaymentProcessActive(false);
        }

        if(!$settings->isWalletAllertsActive()) {
            $settings->setWalletAllertsActive(false);
        }

        if(!$settings->getBtcWalletFullValue()) {
            $settings->setBtcWalletFullValue(1);
        }

        if(!$settings->getBtcWalletAlertValue()) {
            $settings->setBtcWalletAlertValue(0.1);
        }

        if(!$settings->getLtcWalletFullValue()) {
            $settings->setLtcWalletFullValue(2);
        }

        if(!$settings->getLtcWalletAlertValue()) {
            $settings->setLtcWalletAlertValue(0.5);
        }

        if(!$settings->getLskWalletFullValue()) {
            $settings->setLskWalletFullValue(50);
        }

        if(!$settings->getLskWalletAlertValue()) {
            $settings->setLskWalletAlertValue(10);
        }

        if(!$settings->getEthWalletFullValue()) {
            $settings->setEthWalletFullValue(2);
        }

        if(!$settings->getEthWalletAlertValue()) {
            $settings->setEthWalletAlertValue(0.5);
        }

        if(!$settings->getSaleCommission()) {
            $settings->setSaleCommission(23);
        }

        if(!$settings->getPaymentJobInterval()) {
            $settings->setPaymentJobInterval(1);
        }

        $this->em->persist($settings);
        $this->em->flush();
    }
}
