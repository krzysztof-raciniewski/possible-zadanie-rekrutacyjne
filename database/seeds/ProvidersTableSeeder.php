<?php

use App\Entities\Provider;
use App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository;
use App\Enums\ProviderName;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Database\Seeder;

class ProvidersTableSeeder extends Seeder
{
    private $providersRepository;

    /**
     * @var array
     */
    private $providersToCreate;

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    /**
     * UsersTableSeeder constructor.
     * @param ProvidersRepository $providersRepository
     * @param EntityManagerInterface $em
     * @internal param ProvidersRepository $providerRepository
     * @internal param ProviderRepository $usersRepository
     */
    public function __construct(ProvidersRepository $providersRepository, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->providersRepository = $providersRepository;
        $this->providersToCreate = [
            [
                'name' => ProviderName::CASHBILL,
                'www' => 'https://www.cashbill.pl/',
                'description' => 'CashBill S.A. oferuje system obsługujący bezpieczne płatności on-line. Dzięki naszym rozwiązaniom Państwa klienci będą mogli szybko i sprawnie realizować przelewy online i inne formy płatności.',
                'image' => '/img/providers/cashbill.png',
                'public_provider_information' => '',
                'rules_url' => null,
                'complaint_form_url' => null,
                'configured' => true,
                'enabled' => true,
                'active' => true
            ],
            [
                'name' => ProviderName::EBILL,
                'www' => 'https://www.ebill.pl/',
                'description' => 'Zacznij zarabiać, uruchom już dziś usługę opartą na SMS Premium w systemie eBill.pl za darmo. Płatności SMS, Kody SMS na twoich stronach, łatwe wdrożenie, Bramka XML.',
                'image' => '/img/providers/ebill.png',
                'public_provider_information' => '',
                'rules_url' => null,
                'complaint_form_url' => null,
                'configured' => true,
                'enabled' => true,
                'active' => false
            ],
//            [
//                'name' => ProviderName::FORTUMO,
//                'www' => 'https://fortumo.com/',
//                'description' => 'Direct carrier billing in 95 countries for apps, games, digital media companies, app stores and device manufacturers.',
//                'image' => '/img/providers/fortumo.png',
//                'public_provider_information' => '',
//                'rules_url' => null,
//                'complaint_form_url' => null,
//                'configured' => true,
//                'enabled' => false,
//                'active' => false
//            ],
            [
                'name' => ProviderName::JUSTPAY,
                'www' => 'https://justpay.pl/smscodes/',
                'image' => '/img/providers/justpay.jpg',
                'description' => 'Justpay to. Obsługa SMS w 11 krajach bez dodatkowych opłat (ponad 50 taryf) >>>; Subskrypcje MT uruchamiane w ciągu 48 godzin',
                'public_provider_information' => '',
                'rules_url' => null,
                'complaint_form_url' => null,
                'configured' => true,
                'enabled' => true,
                'active' => false
            ],
            [
                'name' => ProviderName::MICROSMS,
                'www' => 'http://microsms.pl/',
                'description' => 'Oferujemy wypłaty do 24 godzin roboczych od zgłoszenia wypłaty. Zlecając wypłatę w poniedziałek rano pieniądze będą na twoim koncie już we wtorek!',
                'image' => '/img/providers/microsms.png',
                'rules_url' => 'http://microsms.pl/files/regulations/',
                'complaint_form_url' => 'http://microsms.pl/customer/complaint/',
                'public_provider_information' => '',
                'configured' => true,
                'enabled' => true,
                'active' => false
            ],
//            [
//                'name' => ProviderName::PAYSERA,
//                'www' => 'https://www.paysera.pl/',
//                'description' => 'Najszybszy sposób przesyłania i otrzymywania pieniędzy na całym świecie, wymiany waluty oraz pobierania opłat przez Internet i wiadomości SMS.',
//                'image' => '/img/providers/paysera.png',
//                'public_provider_information' => '',
//                'rules_url' => null,
//                'complaint_form_url' => null,
//                'configured' => true,
//                'enabled' => false,
//                'active' => false
//            ],
//            [
//                'name' => ProviderName::TPAY,
//                'www' => 'https://www.tpay.com/',
//                'description' => 'Szanowni Państwo,Mamy przyjemność poinformować, że system płatności tpay.com zdobył nagrodę „Kreator Biznesu 2015” w kategorii bankowość i finanse.',
//                'image' => '/img/providers/tpay.png',
//                'public_provider_information' => '',
//                'rules_url' => null,
//                'complaint_form_url' => null,
//                'configured' => true,
//                'enabled' => false,
//                'active' => false
//            ],
//            [
//                'name' => ProviderName::TXTNATION,
//                'www' => 'http://www.txtnation.com/',
//                'image' => '/img/providers/txtnation.png',
//                'description' => 'Offers mobile messaging and mobile commerce solutions.',
//                'public_provider_information' => '',
//                'rules_url' => null,
//                'complaint_form_url' => null,
//                'configured' => true,
//                'enabled' => false,
//                'active' => false
//            ],
            [
                'name' => ProviderName::DOTPAY,
                'www' => 'http://www.dotpay.pl/',
                'image' => '/img/providers/dotpay.jpg',
                'rules_url' => 'http://www.dotpay.pl/regulamin-serwisow-sms-premium/',
                'complaint_form_url' => 'http://www.dotpay.pl/kontakt',
                'description' => 'Dotpay świadczy usługi w zakresie obsługi płatności interetowych, transakcji kartami Visa i MasterCard, , oraz usług SMS Premium Rate.',
                'public_provider_information' => 'Usługi SMS dostarcza i obsługuje Eurokoncept, grupa Dotpay',
                'configured' => true,
                'enabled' => true,
                'active' => false
            ]
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var Provider[] $providers */
        $providers = $this->providersRepository->findAll();
        foreach($this->providersToCreate as $newProvider) {
            $providerExists = false;
            foreach($providers as $provider) {
                if( $provider->getName() === $newProvider['name'] ) {
                    $providerExists = true;
                    break;
                }
            }

            if( $providerExists ) {
                continue;
            }

            $providerObject = new Provider();
            $providerObject->setName($newProvider['name']);
            $providerObject->setWww($newProvider['www']);
            $providerObject->setImage($newProvider['image']);
            $providerObject->setDescription($newProvider['description']);
            $providerObject->setConfigured($newProvider['configured']);
            $providerObject->setPublicProviderInformation($newProvider['public_provider_information']);
            $providerObject->setEnabled($newProvider['enabled']);
            $providerObject->setActive($newProvider['active']);
            $providerObject->setRulesUrl($newProvider['rules_url']);
            $providerObject->setComplaintFormUrl($newProvider['complaint_form_url']);

            // Persist object
            $this->em->persist($providerObject);
        }

        $this->em->flush();
    }
}
