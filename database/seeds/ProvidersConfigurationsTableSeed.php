<?php

use App\Api\Bitbay\Enum\CurrencyName;
use App\Entities\Provider;
use App\Entities\ProviderConfiguration;
use App\Entities\Repositories\RepositoriesInterfaces\CountriesRepository;
use App\Entities\Repositories\RepositoriesInterfaces\CurrenciesRepository;
use App\Entities\Repositories\RepositoriesInterfaces\OperatorsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\ProviderConfigurationsRepository;
use App\Entities\Repositories\RepositoriesInterfaces\ProvidersRepository;
use App\Entities\Sms;
use App\Enums\CountryShortcut;
use App\Enums\ProviderName;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Database\Seeder;

class ProvidersConfigurationsTableSeed extends Seeder
{
    private $providersRepository;
    private $configRepository;
    private $countriesRepository;
    private $operatorsRepository;

    /**
     * @var array
     */
    private $configurationsToCreate;
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    /**
     * ProvidersConfigurationsTableSeed constructor.
     * @param ProvidersRepository $providersRepository
     * @param ProviderConfigurationsRepository $configRepository
     * @param CountriesRepository $countriesRepository
     * @param CurrenciesRepository $currenciesRepository
     * @param OperatorsRepository $operatorsRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(
        ProvidersRepository $providersRepository,
        ProviderConfigurationsRepository $configRepository,
        CountriesRepository $countriesRepository,
        CurrenciesRepository $currenciesRepository,
        OperatorsRepository $operatorsRepository,
        EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->providersRepository = $providersRepository;
        $this->configRepository = $configRepository;
        $this->countriesRepository = $countriesRepository;
        $this->operatorsRepository = $operatorsRepository;

        $countryPL = $countriesRepository->findOneByCode(CountryShortcut::POLAND);
        $countryDE = $countriesRepository->findOneByCode(CountryShortcut::GERMANY);
        $countryES = $countriesRepository->findOneByCode(CountryShortcut::SPAIN);
        $currencyPLN = $currenciesRepository->findOneByCode(CurrencyName::PLN);
        $currencyEUR = $currenciesRepository->findOneByCode(CurrencyName::EUR);

        // --removed--
}
