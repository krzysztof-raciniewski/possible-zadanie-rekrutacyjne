<?php

use App\Entities\Repositories\RepositoriesInterfaces\CountriesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    /**
     * @var CountriesRepository
     */
    private $countriesRepository;

    /**
     * CountriesTableSeeder constructor.
     * @param EntityManagerInterface $em
     * @param CountriesRepository $countriesRepository
     */
    public function __construct(EntityManagerInterface $em, CountriesRepository $countriesRepository)
    {
        $this->em = $em;
        $this->countriesRepository = $countriesRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
            [
                'code' => 'PL',
                'name' => 'Polska'
            ],
            [
                'code' => 'DE',
                'name' => 'Niemcy'
            ],
            [
                'code' => 'ES',
                'name' => 'Hiszpania'
            ]
        ];

        foreach($countries as $country) {
            if( !$this->countriesRepository->findByCode($country['code']) ) {
                $newCountry = new \App\Entities\Country();
                $newCountry->setCode($country['code']);
                $newCountry->setName($country['name']);
                $this->em->persist($newCountry);
            }
        }

        $this->em->flush();
    }
}
